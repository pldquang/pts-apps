package com.sms.common;

/**
 * <p>Title: SMPP Gateway</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Home</p>
 *
 * @author not attributable
 * @version 1.0
 */
import java.util.*;
import java.util.Collection;

public class StringTool {

    static final String seperators[] = {
        " ", ".", ",", "-", "_", "="
    };

    public StringTool() {
    }

    public static String replaceCharAt(String s, int pos, char c) {
        StringBuffer buf = new StringBuffer(s);
        buf.setCharAt(pos, c);
        return buf.toString();
    }

    public static String removeChar(String s, char c) {
        StringBuffer newString = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            char cur = s.charAt(i);
            if (cur != c) {
                newString.append(cur);
            }
        }

        return newString.toString();
    }

    public static String removeCharAt(String s, int pos) {
        StringBuffer buf = new StringBuffer(s.length() - 1);
        buf.append(s.substring(0, pos)).append(s.substring(pos + 1));
        return buf.toString();
    }

    public static Collection parseString(String text, String seperator) {
        Vector vResult = new Vector();
        if (text == null || "".equals(text)) {
            return vResult;
        }
        String tempStr = text.trim();
        String currentLabel = null;
        for (int index = tempStr.indexOf(seperator); index != -1;
                index = tempStr.indexOf(seperator)) {
            currentLabel = tempStr.substring(0, index).trim();
            if (!"".equals(currentLabel)) {
                vResult.addElement(currentLabel);
            }
            tempStr = tempStr.substring(index + 1);
        }

        currentLabel = tempStr.trim();
        if (!"".equals(currentLabel)) {
            vResult.addElement(currentLabel);
        }
        return vResult;
    }

    public static Collection parseString(String text) {
        Vector vResult = new Vector();
        if (text == null || "".equals(text)) {
            return vResult;
        }
        String tempStr = text.trim();
        String currentLabel = null;
        for (int index = getNextIndex(tempStr); index != -1;
                index = getNextIndex(tempStr)) {
            currentLabel = tempStr.substring(0, index).trim();
            if (!"".equals(currentLabel)) {
                vResult.addElement(currentLabel);
            }
            tempStr = tempStr.substring(index + 1);
        }

        currentLabel = tempStr.trim();
        if (!"".equals(currentLabel)) {
            vResult.addElement(currentLabel);
        }
        return vResult;
    }

    private static int getNextIndex(String text) {
        int index = 0;
        int newIdx = 0;
        boolean hasOne = false;
        for (int i = 0; i < seperators.length; i++) {
            newIdx = text.indexOf(seperators[i]);
            if (!hasOne) {
                if (newIdx != -1) {
                    index = newIdx;
                    hasOne = true;
                }
            } else if (newIdx != -1 && newIdx < index) {
                index = newIdx;
            }
        }

        if (!hasOne) {
            index = -1;
        }
        return index;
    }

    public static Collection parseStringEx(String text) {
        Vector vResult = new Vector();
        if (text == null || "".equals(text)) {
            return vResult;
        }
        String tempStr = text.trim();
        char NINE = '9';
        char ZERO = '0';
        char CH_a = 'a';
        char CH_z = 'z';
        char CH_A = 'A';
        char CH_Z = 'Z';
        String currLabel = "";
        char currChar = '\0';
        for (int i = 0; i < tempStr.length(); i++) {
            currChar = tempStr.charAt(i);
            if (currChar >= ZERO && currChar <= NINE ||
                    currChar >= CH_a && currChar <= CH_z ||
                    currChar >= CH_A && currChar <= CH_Z) {
                currLabel = currLabel + currChar;
            } else if (currLabel.length() > 0) {
                vResult.add(currLabel);
                currLabel = new String("");
            }
        }

        if (currLabel.length() > 0) {
            vResult.add(currLabel);
        }
        return vResult;
    }

    public static boolean isNumberic(String sNumber) {
        if (sNumber == null || "".equals(sNumber)) {
            return false;
        }
        char ch_max = '9';
        char ch_min = '0';
        for (int i = 0; i < sNumber.length(); i++) {
            char ch = sNumber.charAt(i);
            if (ch < ch_min || ch > ch_max) {
                return false;
            }
        }

        return true;
    }

    public static String replaceWhiteLetter(String sInput) {
        String strTmp = sInput;
        String sReturn = "";
        boolean flag = true;
        int i = 0;
        while (i < sInput.length() && flag) {
            char ch = sInput.charAt(i);
            if ((ch >= '0' && ch <= '9') ||
                    (ch >= 'A' && ch <= 'Z') ||
                    (ch >= 'a' && ch <= 'z')) {
                flag = false;
            } else {
                strTmp = sInput.substring(i + 1);
            }
            i++;
        }

        i = strTmp.length() - 1;
        flag = true;
        sReturn = strTmp;
        while (i >= 0 && flag) {
            char ch = strTmp.charAt(i);
            if ((ch >= '0' && ch <= '9') ||
                    (ch >= 'A' && ch <= 'Z') ||
                    (ch >= 'a' && ch <= 'z')) {
                flag = false;
            } else {
                sReturn = strTmp.substring(0, i);
            }
            i--;
        }
        return sReturn;
    }

    public static String replaceSpecialChar2WhiteLetter(String sInput) {
        String strTmp = sInput;
        String sReturn = "";
        boolean flag = true;
        int i = 0;
        while (i < sInput.length() && flag) {
            char ch = sInput.charAt(i);
            if ((ch >= '0' && ch <= '9') ||
                    (ch >= 'A' && ch <= 'Z') ||
                    (ch >= 'a' && ch <= 'z')) {
                flag = false;
            } else {
                strTmp = sInput.substring(i + 1);
            }
            i++;
        }

        i = strTmp.length() - 1;
        flag = true;
        sReturn = strTmp;
        while (i >= 0 && flag) {
            char ch = strTmp.charAt(i);
            if ((ch >= '0' && ch <= '9') ||
                    (ch >= 'A' && ch <= 'Z') ||
                    (ch >= 'a' && ch <= 'z')) {
                flag = false;
            } else {
                sReturn = strTmp.substring(0, i);
            }
            i--;
        }
        return sReturn;
    }

    public static int getIndexOfSeparate(String strInput, int type) {
        if (type == 1) {
            for (int i = strInput.length() - 1; i >= 0; i--) {
                char ch = strInput.charAt(i);
                if ((ch < '0') ||
                        (ch > '9' && ch < 'A') ||
                        (ch > 'Z' && ch < 'a') ||
                        (ch > 'z')) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < strInput.length(); i++) {
                char ch = strInput.charAt(i);
                if ((ch < '0') ||
                        (ch > '9' && ch < 'A') ||
                        (ch > 'Z' && ch < 'a') ||
                        (ch > 'z')) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static String getObjectString(String msg, int index) {
        String sReturn = "";
        String sTmp = replaceWhiteLetter(msg);
        int i = getIndexOfSeparate(sTmp, 0);
        if (i < 0) {
            return "";
        }
        sTmp = sTmp.substring(i + 1);
        sTmp = replaceWhiteLetter(sTmp);
        i = getIndexOfSeparate(sTmp, 0);
        switch (index) {
            case 1:
                if (i < 0) {
                    sReturn = sTmp;
                } else {
                    sReturn = sTmp.substring(0, i);
                }
                break;
            case 2:
                if (i < 0) {
                    return "";
                }
                sTmp = sTmp.substring(i + 1);
                sTmp = replaceWhiteLetter(sTmp);
                i = getIndexOfSeparate(sTmp, 0);
                if (i < 0) {
                    sReturn = sTmp;
                } else {
                    sReturn = sTmp.substring(0, i);
                }
                break;
            case 3:
                if (i < 0) {
                    return "";
                }
                sTmp = sTmp.substring(i + 1);
                sTmp = replaceWhiteLetter(sTmp);
                i = getIndexOfSeparate(sTmp, 0);
                if (i < 0) {
                    return "";
                }
                sTmp = sTmp.substring(i + 1);
                sTmp = replaceWhiteLetter(sTmp);
                i = getIndexOfSeparate(sTmp, 0);
                if (i < 0) {
                    sReturn = sTmp;
                } else {
                    sReturn = sTmp.substring(0, i);
                }
                break;
            case 4:
                if (i < 0) {
                    return "";
                }
                sTmp = sTmp.substring(i + 1);
                sTmp = replaceWhiteLetter(sTmp);
                i = getIndexOfSeparate(sTmp, 0);
                if (i < 0) {
                    return "";
                }
                sTmp = sTmp.substring(i + 1);
                sTmp = replaceWhiteLetter(sTmp);
                i = getIndexOfSeparate(sTmp, 0);
                if (i < 0) {
                    return "";
                }
                sTmp = sTmp.substring(i + 1);
                sTmp = replaceWhiteLetter(sTmp);
                i = getIndexOfSeparate(sTmp, 0);
                if (i < 0) {
                    sReturn = sTmp.toUpperCase();
                } else {
                    sReturn = sTmp.substring(0, i).toUpperCase();
                }
                break;
        }
        if (sReturn == null) {
            sReturn = "";
        }
        return sReturn;
    }

    public static String getContentNoWhiteLetter(String sInput) {
        String sReturn = "";
        if (sInput != null) {
            char[] charr = new char[sInput.length()];
            int i = 0;
            while (i < sInput.length()) {
                char ch = sInput.charAt(i);
                //System.out.println("ch: " + ch);
                if ((ch >= '0' && ch <= '9') ||
                        (ch >= 'A' && ch <= 'Z') ||
                        (ch >= 'a' && ch <= 'z')) {
                    sReturn = sReturn + String.valueOf(ch);
                }
                i++;
            }
        }
        return sReturn;
    }

    public static String getContentOnlyNumber(String sInput) {
        String sReturn = "";
        char[] charr = new char[sInput.length()];
        int i = 0;
        while (i < sInput.length()) {
            char ch = sInput.charAt(i);
            if ((ch >= '0' && ch <= '9')) {
                sReturn = sReturn + String.valueOf(ch);
            }
            i++;
        }
        return sReturn;
    }

    public static String getContentOnlyChar(String sInput) {
        String sReturn = "";
        //char[] charr = new char[sInput.length()];
        int i = 0;
        while (i < sInput.length()) {
            char ch = sInput.charAt(i);
            if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
                sReturn = sReturn + String.valueOf(ch);
            }
            i++;
        }
        return sReturn;
    }

    public static String getContentOnlyCharAndNumber(String sInput) {
        String sReturn = "";
        //char[] charr = new char[sInput.length()];
        int i = 0;
        while (i < sInput.length()) {
            char ch = sInput.charAt(i);
            if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9')) {
                sReturn = sReturn + String.valueOf(ch);
            }
            i++;
        }
        return sReturn;
    }

    public static String getContentOnlyCharEx(String sInput) {
        String sReturn = "";
        //char[] charr = new char[sInput.length()];
        int i = 0;
        while (i < sInput.length()) {
            char ch = sInput.charAt(i);
            if ((ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') ||
                    (ch == '.') || (ch == ',') || (ch == '!') || (ch == ' ') || (ch == 10)) {
                sReturn = sReturn + String.valueOf(ch);
            }
            i++;
        }
        return sReturn;
    }

    public static String convert2Nosign(String org) {
        char arrChar[] = org.toCharArray();
        char result[] = new char[arrChar.length];
        for (int i = 0; i < arrChar.length; i++) {
            switch (arrChar[i]) {
                case '\u00E1':
                case '\u00E0':
                case '\u1EA3':
                case '\u00E3':
                case '\u1EA1':
                case '\u0103':
                case '\u1EAF':
                case '\u1EB1':
                case '\u1EB3':
                case '\u1EB5':
                case '\u1EB7':
                case '\u00E2':
                case '\u1EA5':
                case '\u1EA7':
                case '\u1EA9':
                case '\u1EAB':
                case '\u1EAD':
                case '\u0203':
                case '\u01CE': {
                    result[i] = 'a';
                    break;
                }
                case '\u00E9':
                case '\u00E8':
                case '\u1EBB':
                case '\u1EBD':
                case '\u1EB9':
                case '\u00EA':
                case '\u1EBF':
                case '\u1EC1':
                case '\u1EC3':
                case '\u1EC5':
                case '\u1EC7':
                case '\u0207': {
                    result[i] = 'e';
                    break;
                }
                case '\u00ED':
                case '\u00EC':
                case '\u1EC9':
                case '\u0129':
                case '\u1ECB': {
                    result[i] = 'i';
                    break;
                }
                case '\u00F3':
                case '\u00F2':
                case '\u1ECF':
                case '\u00F5':
                case '\u1ECD':
                case '\u00F4':
                case '\u1ED1':
                case '\u1ED3':
                case '\u1ED5':
                case '\u1ED7':
                case '\u1ED9':
                case '\u01A1':
                case '\u1EDB':
                case '\u1EDD':
                case '\u1EDF':
                case '\u1EE1':
                case '\u1EE3':
                case '\u020F': {
                    result[i] = 'o';
                    break;
                }
                case '\u00FA':
                case '\u00F9':
                case '\u1EE7':
                case '\u0169':
                case '\u1EE5':
                case '\u01B0':
                case '\u1EE9':
                case '\u1EEB':
                case '\u1EED':
                case '\u1EEF':
                case '\u1EF1': {
                    result[i] = 'u';
                    break;
                }
                case '\u00FD':
                case '\u1EF3':
                case '\u1EF7':
                case '\u1EF9':
                case '\u1EF5': {
                    result[i] = 'y';
                    break;
                }
                case '\u0111': {
                    result[i] = 'd';
                    break;
                }
                case '\u00C1':
                case '\u00C0':
                case '\u1EA2':
                case '\u00C3':
                case '\u1EA0':
                case '\u0102':
                case '\u1EAE':
                case '\u1EB0':
                case '\u1EB2':
                case '\u1EB4':
                case '\u1EB6':
                case '\u00C2':
                case '\u1EA4':
                case '\u1EA6':
                case '\u1EA8':
                case '\u1EAA':
                case '\u1EAC':
                case '\u0202':
                case '\u01CD': {
                    result[i] = 'A';
                    break;
                }
                case '\u00C9':
                case '\u00C8':
                case '\u1EBA':
                case '\u1EBC':
                case '\u1EB8':
                case '\u00CA':
                case '\u1EBE':
                case '\u1EC0':
                case '\u1EC2':
                case '\u1EC4':
                case '\u1EC6':
                case '\u0206': {
                    result[i] = 'E';
                    break;
                }
                case '\u00CD':
                case '\u00CC':
                case '\u1EC8':
                case '\u0128':
                case '\u1ECA': {
                    result[i] = 'I';
                    break;
                }
                case '\u00D3':
                case '\u00D2':
                case '\u1ECE':
                case '\u00D5':
                case '\u1ECC':
                case '\u00D4':
                case '\u1ED0':
                case '\u1ED2':
                case '\u1ED4':
                case '\u1ED6':
                case '\u1ED8':
                case '\u01A0':
                case '\u1EDA':
                case '\u1EDC':
                case '\u1EDE':
                case '\u1EE0':
                case '\u1EE2':
                case '\u020E': {
                    result[i] = 'O';
                    break;
                }
                case '\u00DA':
                case '\u00D9':
                case '\u1EE6':
                case '\u0168':
                case '\u1EE4':
                case '\u01AF':
                case '\u1EE8':
                case '\u1EEA':
                case '\u1EEC':
                case '\u1EEE':
                case '\u1EF0': {
                    result[i] = 'U';
                    break;
                }

                case '\u00DD':
                case '\u1EF2':
                case '\u1EF6':
                case '\u1EF8':
                case '\u1EF4': {
                    result[i] = 'Y';
                    break;
                }
                case '\u0110':
                case '\u00D0':
                case '\u0089': {
                    result[i] = 'D';
                    break;
                }
                default:
                    result[i] = arrChar[i];
            }
        }
        return new String(result);
    }

    public static void bubbleSort(Vector v) {
        //Sort by descending length elements in a Vector
        try {
            boolean isSorted;
            String tmp;
            int numberOfTimesLooped = 0;
            do {
                isSorted = true;
                for (int i = 1; i < v.size() - numberOfTimesLooped; i++) {
                    int len1 = ((String) v.elementAt(i)).length();
                    int len2 = ((String) v.elementAt(i - 1)).length();
                    if (len1 > len2) {
                        tmp = (String) v.elementAt(i);
                        v.remove(i);
                        v.add(i, v.elementAt(i - 1));
                        v.remove(i - 1);
                        v.add(i - 1, tmp);
                        isSorted = false;
                    }
                }
                numberOfTimesLooped++;
            } while (!isSorted);

        } catch (Exception ex) {
        }
    }

    public static String splitMsg(String info) {
        String infoTmp = info;
        String content = "";
        int position = 0;

        while (infoTmp.length() >= 160) {
            position = infoTmp.substring(0, 160).lastIndexOf(" ");
            if (content.equals("")) {
                content = infoTmp.substring(0, position);
            } else {
                if (position > 0) {
                    content = content + "!xxx!" + infoTmp.substring(0, position);
                } else {
                    content = info;
                }
            }
            infoTmp = info.substring(position + 1, info.length());
            info = info.substring(position + 1, info.length());
        //System.out.println(info);
        }
        content = content + "!xxx!" + info;
        return content;
    }

    public static void main(String[] args) {
        String test = "QDT Nokia";
        String subCode1 = "", subCode2 = "";
        subCode1 = StringTool.getObjectString(test, 1);
        subCode2 = StringTool.getObjectString(test, 2);
        subCode1 = subCode1 + subCode2;
        subCode1 = subCode1.replaceAll(" ", "");
        System.out.println(subCode1);
        System.out.println(subCode2);
    }
}
