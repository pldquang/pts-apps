package com.sms.common;

/**
 * <p>Title: SMPP Gateway</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Home</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class HexaTool {

  static char hexChar[] = {
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
      'A', 'B', 'C', 'D', 'E', 'F'
  };

  public HexaTool() {
  }

  private static int charToNibble(char c) {
    if ('0' <= c && c <= '9')
      return c - 48;
    if ('a' <= c && c <= 'f')
      return (c - 97) + 10;
    if ('A' <= c && c <= 'F')
      return (c - 65) + 10;
    else
      throw new IllegalArgumentException("Invalid hex character: " + c);
  }

  public static byte[] fromHexString(String s) throws IllegalArgumentException {
    int stringLength = s.length();
    if ( (stringLength & 1) != 0)
      throw new IllegalArgumentException(
          "fromHexString requires an even number of hex characters");
    byte b[] = new byte[stringLength / 2];
    int i = 0;
    for (int j = 0; i < stringLength; j++) {
      int high = charToNibble(s.charAt(i));
      int low = charToNibble(s.charAt(i + 1));
      b[j] = (byte) (high << 4 | low);
      i += 2;
    }

    return b;
  }

  public static String toHexString(byte b[]) {
    StringBuffer sb = new StringBuffer(b.length * 2);
    for (int i = 0; i < b.length; i++) {
      sb.append(hexChar[ (b[i] & 0xf0) >>> 4]);
      sb.append(hexChar[b[i] & 0xf]);
    }

    return sb.toString();
  }

  public static String Byte2HexString(byte[] b) {
      if (b == null)
          return null;
      StringBuffer sb = new StringBuffer(b.length * 2);
      for (int i = 0; i < b.length; i++) {
          // look up high nibble char
          sb.append(hexChar[(b[i] & 0xf0) >>> 4]);
          // look up low nibble char
          sb.append(hexChar[b[i] & 0x0f]);
      }
      return sb.toString();
  }

  public static void main(String[] args) {
    HexaTool hexaTool = new HexaTool();

    hexaTool.fromHexString(hexaTool.toHexString("keke :http://testing.com.vn".getBytes()));
    System.out.println(":" + hexaTool.toHexString("[keke] :http://testing.com.vn".getBytes()) );
  }

}
