package com.sms.common;

/*
 * Copyright (c) 1996-2001
 * Logica Mobile Networks Limited
 * All rights reserved.
 *
 * This software is distributed under Logica Open Source License Version 1.0
 * ("Licence Agreement"). You shall use it and distribute only in accordance
 * with the terms of the License Agreement.
 *
 */
import java.util.Vector;
import java.util.LinkedList;

/**
 * Implements fifo style queue with removal of specified element from inside of
 * the queue.
 *
 * @author Logica Mobile Networks SMPP Open Source Team
 * @version $Revision: 1.1 $
 */
public class Queue {
//  protected Vector queue;
  private LinkedList queueData = new LinkedList();
  private Object mutex;

  public Queue() {
//    queue = new Vector();
    mutex = this;
  }

  /**
   * If there is no element in the queue.
   */
  public boolean isEmpty() {
    synchronized (mutex) {
      return queueData.isEmpty();
    }
  }

  /**
   * Current count of the elements in the queue.
   */
  public int size() {
    synchronized (mutex) {
      return queueData.size();
    }
  }

  public void enqueue(Object b) {
    synchronized (mutex) {
      queueData.add(b);
    }
  }

  public Object dequeue() {
    synchronized (mutex) {
      Object first = null;
      if (size() > 0) {
        first = queueData.removeFirst();
      }
      return first;
    }
  }

}

