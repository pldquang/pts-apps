package com.sms.common;

import org.apache.log4j.*;

public class MyLogger {
    private static Logger logger = Logger.getLogger(MyLogger.class.getName());

    public MyLogger() {
    }

    public static void log(String msg) {
        logger.info(msg);
    }

}
