package com.sms.common;

/**
 * <p>Title: SMPP Gateway</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: Home</p>
 * @author not attributable
 * @version 1.0
 */
import java.io.*;
import java.util.*;
import java.sql.*;
import java.math.BigDecimal;
import java.net.*;
import java.text.*;
import java.math.*;

import oracle.jdbc.driver.OracleDriver;
import java.text.SimpleDateFormat;

import com.sms.process.DBTool;

public class Utilities {

    static FileOutputStream fout = null;
    static final boolean VERBOSE = true;
    static char hexChar[] = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        'A', 'B', 'C', 'D', 'E', 'F'
    };

    public Utilities() {
    }

    public Connection getDBConnection(String url, String user,
            String password) throws SQLException {
        Connection conn = null;
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            conn = DriverManager.getConnection(url, user, password);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return conn;
    }

    public void cleanup(Connection con, PreparedStatement ps) {
        try {
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        } catch (Exception e) {
        }
    }

    public void cleanup(PreparedStatement ps, Statement stmt) {
        try {
            if (ps != null) {
                ps.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        } catch (Exception e) {
        }
    }

    public void cleanup(ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (Exception e) {
        }
    }

    public void closeConnection(Connection connection, Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
        }
    }

    public static java.sql.Timestamp createTimestampFullDDMMYY(String strTime) {
        SimpleDateFormat formatterdv = new SimpleDateFormat("dd/MM/yy");
        java.util.Date date = null;
        try {
            date = formatterdv.parse(strTime);
            return new java.sql.Timestamp(date.getTime());
        } catch (Exception ex) {
        }
        return null;
    }

    public static byte[] getBytes(Object obj) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(obj);
        oos.flush();
        oos.close();
        bos.close();
        byte data[] = bos.toByteArray();
        return data;
    }

    public static String toHexString(byte b[]) {
        StringBuffer sb = new StringBuffer(b.length * 2);
        for (int i = 0; i < b.length; i++) {
            sb.append(hexChar[(b[i] & 0xf0) >>> 4]);
            sb.append(hexChar[b[i] & 0xf]);
        }

        return sb.toString();
    }

    public static byte[] fromHexString(String s) {
        int stringLength = s.length();
        if ((stringLength & 1) != 0) {
            throw new IllegalArgumentException(
                    "fromHexString requires an even number of hex characters");
        }
        byte b[] = new byte[stringLength / 2];
        int i = 0;
        for (int j = 0; i < stringLength; j++) {
            int high = charToNibble(s.charAt(i));
            int low = charToNibble(s.charAt(i + 1));
            b[j] = (byte) (high << 4 | low);
            i += 2;
        }

        return b;
    }

    public static String getHexString(byte[] b) {
        StringBuffer sb = new StringBuffer(b.length * 2);
        for (int i = 0; i < b.length; i++) {
            // look up high nibble char
            sb.append(hexChar[(b[i] & 0xf0) >>> 4]);
            // look up low nibble char
            sb.append(hexChar[b[i] & 0x0f]);
        }
        return sb.toString();
    }

    public static byte[] readByteBlob(Blob myBlob) throws IOException,
            SQLException {
        if (myBlob == null) {
            return null;
        }
        InputStream is = myBlob.getBinaryStream();
        byte[] b1 = null;
        java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream();

        byte[] line = new byte[1024];

        int i = 0;
        do {
            i = is.read(line, 0, line.length);
            if (i != -1) {
                bos.write(line, 0, i);
            }
        } while (i != -1);
        byte[] return_value = null;
        return_value = bos.toByteArray();
        return return_value;
    }

    private static int charToNibble(char c) {
        if ('0' <= c && c <= '9') {
            return c - 48;
        }
        if ('a' <= c && c <= 'f') {
            return (c - 97) + 10;
        }
        if ('A' <= c && c <= 'F') {
            return (c - 65) + 10;
        } else {
            throw new IllegalArgumentException("Invalid hex character: " + c);
        }
    }

    public static double round(double value, int decimalPlace) {
        double power_of_ten;
        for (power_of_ten = 1.0D; decimalPlace-- > 0; power_of_ten *= 10D);
        return (double) Math.round(value * power_of_ten) / power_of_ten;
    }

    public static double roundEx(double value, int decimalPlace) {
        BigDecimal bigValue = new BigDecimal(value);
        bigValue = bigValue.setScale(decimalPlace, 4);
        return bigValue.doubleValue();
    }

    public static String buildMobileOperator(String mobileNumber) {
        String operator = "";
        if (mobileNumber.startsWith("0")) {
            mobileNumber = mobileNumber.substring(1, mobileNumber.length());
        } else if (mobileNumber.startsWith("84")) {
            mobileNumber = mobileNumber.substring(2, mobileNumber.length());
        }
        //841223245558
        if ((mobileNumber.startsWith("9") && (mobileNumber.length() == 9)) ||
                (mobileNumber.startsWith("16") && (mobileNumber.length() == 10)) ||
                (mobileNumber.startsWith("121") && (mobileNumber.length() == 10)) ||
                (mobileNumber.startsWith("122") && (mobileNumber.length() == 10)) ||
                (mobileNumber.startsWith("123") && (mobileNumber.length() == 10)) ||
                (mobileNumber.startsWith("125") && (mobileNumber.length() == 10)) ||
                (mobileNumber.startsWith("126") && (mobileNumber.length() == 10)) ||
                (mobileNumber.startsWith("128") && (mobileNumber.length() == 10)) ||
                (mobileNumber.startsWith("127") && (mobileNumber.length() == 10))) {
            if (mobileNumber.startsWith("90")) {
                operator = "VMS";
            } else if (mobileNumber.startsWith("93")) {
                operator = "VMS";
            } else if (mobileNumber.startsWith("121")) {
                operator = "VMS";
            } else if (mobileNumber.startsWith("122")) {
                operator = "VMS";
            } else if (mobileNumber.startsWith("126")) {
                operator = "VMS";
            } else if (mobileNumber.startsWith("128")) {
                operator = "VMS";
            } else if (mobileNumber.startsWith("91")) {
                operator = "GPC";
            } else if (mobileNumber.startsWith("94")) {
                operator = "GPC";
            } else if (mobileNumber.startsWith("123")) {
                operator = "GPC";
            } else if (mobileNumber.startsWith("125")) {
                operator = "GPC";
            } else if (mobileNumber.startsWith("98")) {
                operator = "VIETEL";
            } else if (mobileNumber.startsWith("97")) {
                operator = "VIETEL";
            } else if (mobileNumber.startsWith("16")) {
                operator = "VIETEL";
            }else if (mobileNumber.startsWith("127")) {
                operator = "GPC";
            }
        }
        return operator;
    }

    public static String rebuildUserId(String userId) {
        String userIdRebuilt = "";
        try {
            if (userId.startsWith("0")) {
                userId = userId.substring(1);
            }
            if (!userId.startsWith("84")) {
                userId = "84" + userId;
            }
            userIdRebuilt = userId;
        } catch (Exception ex) {
        }
        return userIdRebuilt;
    }

    public static boolean isValidUserId(String mobileNumber) {
        boolean b = false;
        if (mobileNumber == null) {
            return false;
        }
        if (mobileNumber.startsWith("0")) {
            mobileNumber = mobileNumber.substring(1, mobileNumber.length());
        } else if (mobileNumber.startsWith("84")) {
            mobileNumber = mobileNumber.substring(2, mobileNumber.length());
        }

        if ((mobileNumber.startsWith("9") && (mobileNumber.length() == 9)) ||
                (mobileNumber.startsWith("16") && (mobileNumber.length() == 10)) ||
                (mobileNumber.startsWith("122") && (mobileNumber.length() == 10)) ||
                (mobileNumber.startsWith("123") && (mobileNumber.length() == 10)) ||
                (mobileNumber.startsWith("125") && (mobileNumber.length() == 10)) ||
                (mobileNumber.startsWith("126") && (mobileNumber.length() == 10)) ||
                (mobileNumber.startsWith("128") && (mobileNumber.length() == 10)) ||
                (mobileNumber.startsWith("127") && (mobileNumber.length() == 10))) {
            if (mobileNumber.startsWith("90")) {
                b = true;
            } else if (mobileNumber.startsWith("93")) {
                b = true;
            } else if (mobileNumber.startsWith("122")) {
                b = true;
            } else if (mobileNumber.startsWith("91")) {
                b = true;
            } else if (mobileNumber.startsWith("94")) {
                b = true;
            } else if (mobileNumber.startsWith("123")) {
                b = true;
            } else if (mobileNumber.startsWith("125")) {
                b = true;
            } else if (mobileNumber.startsWith("126")) {
                b = true;
            } else if (mobileNumber.startsWith("98")) {
                b = true;
            } else if (mobileNumber.startsWith("97")) {
                b = true;
            } else if (mobileNumber.startsWith("16")) {
                b = true;
            } else if (mobileNumber.startsWith("128")) {
                b = true;
            } else if (mobileNumber.startsWith("127")) {
                b = true;
            }

        }
        return b;
    }

    public static String removePlusSign(String userId) {
        String temp = userId;
        if (temp.startsWith("+")) {
            temp = temp.substring(1);
        }
        return temp;
    }

    public static String rebuildServiceId(String serviceId) {
        String temp = serviceId;
        if (temp.startsWith("+")) {
            temp = temp.substring(1);
        }
        if ((temp.startsWith("84") || temp.startsWith("04")) &&
                (serviceId.length() == 6)) {
            temp = temp.substring(2);
        }
        return temp;
    }

    public static byte[] PictureMsgEncode(String sText, byte[] bOtb) {
        ByteArrayOutputStream encoded = null;
        try {
            encoded = new ByteArrayOutputStream();
            DataOutputStream dout = new DataOutputStream(encoded);
            dout.writeByte(0x30); //version 0

            dout.writeByte(0x00); //"00"

            if (sText != null) {
                dout.writeShort(sText.length());
                dout.writeBytes(sText);
            } else {
                dout.writeShort(0x0000);
            }
            dout.writeByte(0x02);
            dout.writeShort(0x0100);
            encoded.write(bOtb);
            return encoded.toByteArray();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String getObjectString(String msg, int index) {
        String sReturn = "";
        String sTmp = replaceWhiteLetter(msg);
        int i = getIndexOfSeparate(sTmp, 0);
        if (i < 0) {
            return null;
        }
        sTmp = sTmp.substring(i + 1);
        sTmp = replaceWhiteLetter(sTmp);
        i = getIndexOfSeparate(sTmp, 0);
        switch (index) {
            case 1:
                if (i < 0) {
                    sReturn = sTmp;
                } else {
                    sReturn = sTmp.substring(0, i);
                }
                break;
            case 2:
                if (i < 0) {
                    return null;
                }
                sTmp = sTmp.substring(i + 1);
                sTmp = replaceWhiteLetter(sTmp);
                i = getIndexOfSeparate(sTmp, 0);
                if (i < 0) {
                    sReturn = sTmp;
                } else {
                    sReturn = sTmp.substring(0, i);
                }
                break;
            case 3:
                if (i < 0) {
                    return null;
                }
                sTmp = sTmp.substring(i + 1);
                sTmp = replaceWhiteLetter(sTmp);
                i = getIndexOfSeparate(sTmp, 0);
                if (i < 0) {
                    return null;
                }
                sTmp = sTmp.substring(i + 1);
                sTmp = replaceWhiteLetter(sTmp);
                i = getIndexOfSeparate(sTmp, 0);
                if (i < 0) {
                    sReturn = sTmp;
                } else {
                    sReturn = sTmp.substring(0, i);
                }
                break;
            case 4:
                if (i < 0) {
                    return null;
                }
                sTmp = sTmp.substring(i + 1);
                sTmp = replaceWhiteLetter(sTmp);
                i = getIndexOfSeparate(sTmp, 0);
                if (i < 0) {
                    return null;
                }
                sTmp = sTmp.substring(i + 1);
                sTmp = replaceWhiteLetter(sTmp);
                i = getIndexOfSeparate(sTmp, 0);
                if (i < 0) {
                    return null;
                }
                sTmp = sTmp.substring(i + 1);
                sTmp = replaceWhiteLetter(sTmp);
                i = getIndexOfSeparate(sTmp, 0);
                if (i < 0) {
                    sReturn = sTmp.toUpperCase();
                } else {
                    sReturn = sTmp.substring(0, i).toUpperCase();
                }
                break;
        }
        return sReturn;
    }

    public static String replaceWhiteLetter(String sInput) {
        String strTmp = sInput;
        String sReturn = "";
        boolean flag = true;
        int i = 0;
        while (i < sInput.length() && flag) {
            char ch = sInput.charAt(i);
            if ((ch >= '0' && ch <= '9') ||
                    (ch >= 'A' && ch <= 'Z') ||
                    (ch >= 'a' && ch <= 'z')) {
                flag = false;
            } else {
                strTmp = sInput.substring(i + 1);
            }
            i++;
        }

        i = strTmp.length() - 1;
        flag = true;
        sReturn = strTmp;
        while (i >= 0 && flag) {
            char ch = strTmp.charAt(i);
            if ((ch >= '0' && ch <= '9') ||
                    (ch >= 'A' && ch <= 'Z') ||
                    (ch >= 'a' && ch <= 'z')) {
                flag = false;
            } else {
                sReturn = strTmp.substring(0, i);
            }
            i--;
        }
        return sReturn;
    }

    public static int getIndexOfSeparate(String strInput, int type) {
        if (type == 1) {
            for (int i = strInput.length() - 1; i >= 0; i--) {
                char ch = strInput.charAt(i);
                if ((ch < '0') ||
                        (ch > '9' && ch < 'A') ||
                        (ch > 'Z' && ch < 'a') ||
                        (ch > 'z')) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < strInput.length(); i++) {
                char ch = strInput.charAt(i);
                if ((ch < '0') ||
                        (ch > '9' && ch < 'A') ||
                        (ch > 'Z' && ch < 'a') ||
                        (ch > 'z')) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static String formatUserId(String userId, int formatType) {
        if (userId == null || "".equals(userId)) {
            return null;
        }
        String temp = userId;
        switch (formatType) {
            case 0: // '\0'

                if (temp.startsWith("9")) {
                    temp = "84" + temp;
                    break;
                }
                if (temp.startsWith("0")) {
                    temp = "84" + temp.substring(1);
                    break;
                }
                if (temp.startsWith("1")) {
                    temp = "84" + temp;
                    break;
                }
                break;
            case 2: // '\002'

                if (temp.startsWith("84")) {
                    temp = temp.substring(2);
                    break;
                }
                if (temp.startsWith("09")) {
                    temp = temp.substring(1);
                }
                break;

            case 1: // '\001'

                if (temp.startsWith("84")) {
                    temp = "0" + temp.substring(2);
                    break;
                }
                if (temp.startsWith("1")) {
                    temp = "0" + temp;
                    break;
                }
                if (temp.startsWith("9")) {
                    temp = "0" + temp;
                }
                break;

            default:
                System.out.println("Invalid userId format type " + formatType);
                return null;
        }
        return temp;
    }

    public static String getDataOTAFromUrl(String urlStr) {
        String dataHex = "";
        if (!urlStr.equals("")) {
            try {
                URL url = new URL(urlStr);
                URLConnection urlConn = url.openConnection();
                BufferedInputStream bis = new BufferedInputStream(urlConn.getInputStream());
                int len = bis.available();
                byte data[] = new byte[len];
                bis.read(data);
                dataHex = Utilities.getHexString(data);
            } catch (Exception ex) {
                System.out.println(urlStr + " can't read !!");
            }
        } else {
            System.out.println("urlString is empty !!");
        }
        return dataHex;
    }

    public static String getLunarYear(String year) {
        String result = "";
        try {
            int remainder = 0;
            remainder = (Integer.parseInt(year) - 1900) % 12;
            switch (remainder) {
                case 0:
                    result = "TI";
                    break;
                case 1:
                    result = "SUU";
                    break;
                case 2:
                    result = "DAN";
                    break;
                case 3:
                    result = "MAO";
                    break;
                case 4:
                    result = "THIN";
                    break;
                case 5:
                    result = "TY";
                    break;
                case 6:
                    result = "NGO";
                    break;
                case 7:
                    result = "MUI";
                    break;
                case 8:
                    result = "THAN";
                    break;
                case 9:
                    result = "DAU";
                    break;
                case 10:
                    result = "TUAT";
                    break;
                case 11:
                    result = "HOI";
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public static String getLunarThapCan(String year) {
        String result = "";
        try {
            String endDigitStr = year.substring(year.length() - 1, year.length());
            int endDigit = Integer.parseInt(endDigitStr);
            switch (endDigit) {
                case 0:
                    result = "CANH";
                    break;
                case 1:
                    result = "TAN";
                    break;
                case 2:
                    result = "NHAM";
                    break;
                case 3:
                    result = "QUY";
                    break;
                case 4:
                    result = "GIAP";
                    break;
                case 5:
                    result = "AT";
                    break;
                case 6:
                    result = "BINH";
                    break;
                case 7:
                    result = "DINH";
                    break;
                case 8:
                    result = "MAU";
                    break;
                case 9:
                    result = "KY";
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public static int sumData(String inStr) {
        int tmp = 0;
        int total = 0;
        inStr = StringTool.getContentNoWhiteLetter(inStr.toUpperCase());
        inStr = inStr.replaceAll("A", "1");
        inStr = inStr.replaceAll("J", "1");
        inStr = inStr.replaceAll("S", "1");
        inStr = inStr.replaceAll("B", "2");
        inStr = inStr.replaceAll("K", "2");
        inStr = inStr.replaceAll("T", "2");
        inStr = inStr.replaceAll("C", "3");
        inStr = inStr.replaceAll("U", "3");
        inStr = inStr.replaceAll("L", "3");
        inStr = inStr.replaceAll("D", "4");
        inStr = inStr.replaceAll("M", "4");
        inStr = inStr.replaceAll("V", "4");
        inStr = inStr.replaceAll("E", "5");
        inStr = inStr.replaceAll("N", "5");
        inStr = inStr.replaceAll("W", "5");
        inStr = inStr.replaceAll("F", "6");
        inStr = inStr.replaceAll("O", "6");
        inStr = inStr.replaceAll("X", "6");
        inStr = inStr.replaceAll("G", "7");
        inStr = inStr.replaceAll("P", "7");
        inStr = inStr.replaceAll("Y", "7");
        inStr = inStr.replaceAll("H", "8");
        inStr = inStr.replaceAll("Q", "8");
        inStr = inStr.replaceAll("Z", "8");
        inStr = inStr.replaceAll("R", "9");
        inStr = inStr.replaceAll("I", "9");

        tmp = inStr.length();
        while (tmp > 1) {
            for (int i = 0; i < tmp; i++) {
                total = total + Integer.parseInt(inStr.substring(i, i + 1));
            }
            tmp = String.valueOf(total).length();
            if (total >= 10) {
                inStr = String.valueOf(total);
                total = 0;
            } else {
                break;
            }
        }
        return total;
    }

    public static String getCungChiemTinh(String timeStr) {
        //dd-mm-yyyy
        String currentYearStr = DateProc.getCurrentYYYY();
        timeStr = timeStr + "-" + currentYearStr;
        String result = "";
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

            int currentYearInt = Integer.parseInt(DateProc.getCurrentYYYY());
            java.util.Date inDate = df.parse(timeStr);

            if ((inDate.after(df.parse("22-12-" + String.valueOf(currentYearInt - 1))) || (inDate.equals(df.parse("22-12-" + String.valueOf(currentYearInt - 1))))) &&
                    ((inDate.before(df.parse("19-01-" + currentYearStr)))) || (inDate.equals(df.parse("19-01-" + currentYearStr)))) {
                result = "MAKET";
            } else if ((inDate.after(df.parse("22-12-" + String.valueOf(currentYearInt))) || inDate.equals(df.parse("22-12-" + String.valueOf(currentYearInt)))) &&
                    (inDate.before(df.parse("19-01-" + String.valueOf(currentYearInt + 1))) ||
                    inDate.equals(df.parse("19-01-" + String.valueOf(currentYearInt + 1))))) {
                result = "MAKET";
            } else if ((inDate.after(df.parse("20-01-" + currentYearStr)) || inDate.equals(df.parse("20-01-" + currentYearStr))) &&
                    (inDate.before(df.parse("18-02-" + currentYearStr)) || inDate.equals(df.parse("18-02-" + currentYearStr)))) {
                result = "BAOBINH";
            } else if ((inDate.after(df.parse("19-02-" + currentYearStr)) || inDate.equals(df.parse("19-02-" + currentYearStr))) &&
                    (inDate.before(df.parse("20-03-" + currentYearStr)) || inDate.equals(df.parse("20-03-" + currentYearStr)))) {
                result = "SONGNGU";
            } else if ((inDate.after(df.parse("21-03-" + currentYearStr)) || inDate.equals(df.parse("21-03-" + currentYearStr))) &&
                    (inDate.before(df.parse("20-04-" + currentYearStr)) || inDate.equals(df.parse("20-04-" + currentYearStr)))) {
//                result = "DUONGCUU";
                result = "BACHDUONG";
            } else if ((inDate.after(df.parse("21-04-" + currentYearStr)) || inDate.equals(df.parse("21-04-" + currentYearStr))) &&
                    (inDate.before(df.parse("20-05-" + currentYearStr)) || inDate.equals(df.parse("20-05-" + currentYearStr)))) {
                result = "KIMNGUU";
            } else if ((inDate.after(df.parse("21-05-" + currentYearStr)) || inDate.equals(df.parse("21-05-" + currentYearStr))) &&
                    (inDate.before(df.parse("21-06-" + currentYearStr)) || inDate.equals(df.parse("21-06-" + currentYearStr)))) {
//                result = "SONGSINH";
                result = "SONGTU";
            } else if ((inDate.after(df.parse("22-06-" + currentYearStr)) || inDate.equals(df.parse("22-06-" + currentYearStr))) &&
                    (inDate.before(df.parse("22-07-" + currentYearStr)) || inDate.equals(df.parse("22-07-" + currentYearStr)))) {
                result = "CUGIAI";
            } else if ((inDate.after(df.parse("23-07-" + currentYearStr)) || inDate.equals(df.parse("23-07-" + currentYearStr))) &&
                    (inDate.before(df.parse("22-08-" + currentYearStr)) || inDate.equals(df.parse("22-08-" + currentYearStr)))) {
                result = "SUTU";
            } else if ((inDate.after(df.parse("23-08-" + currentYearStr)) || inDate.equals(df.parse("23-08-" + currentYearStr))) &&
                    (inDate.before(df.parse("23-09-" + currentYearStr)) || inDate.equals(df.parse("23-09-" + currentYearStr)))) {
                result = "XUNU";
            } else if ((inDate.after(df.parse("24-09-" + currentYearStr)) || inDate.equals(df.parse("24-09-" + currentYearStr))) &&
                    (inDate.before(df.parse("23-10-" + currentYearStr)) || inDate.equals(df.parse("23-10-" + currentYearStr)))) {
                result = "THIENBINH";
            } else if ((inDate.after(df.parse("24-10-" + currentYearStr)) || inDate.equals(df.parse("24-10-" + currentYearStr))) &&
                    (inDate.before(df.parse("22-11-" + currentYearStr)) || inDate.equals(df.parse("22-11-" + currentYearStr)))) {
//                result = "BOCAP";
                result = "HOCAP";
            } else if ((inDate.after(df.parse("23-11-" + currentYearStr)) || inDate.equals(df.parse("23-11-" + currentYearStr))) &&
                    (inDate.before(df.parse("21-12-" + currentYearStr)) || inDate.equals(df.parse("21-12-" + currentYearStr)))) {
                result = "NHANMA";
            }
        } catch (Exception ex) {
        }
        return result;
    }

    public static String[] getAmDuong(String checkNumber) {

        String[] content = new String[2];
        try {
            String result = "";
            double nLunar = 0;
            double nSolar = 0;
            int length = 0;
            double point = 0;
            if (StringTool.isNumberic(checkNumber)) {
                length = checkNumber.length();
                for (int i = 1; i < length; i++) {
                    if ((Integer.parseInt(checkNumber.substring(i, i + 1)) % 2) == 0) {
                        nLunar = nLunar + 1;
                    } else {
                        nSolar = nSolar + 1;
                    }
                }
                if (nSolar != nLunar) {
                    if (((nLunar < nSolar) && ((nLunar / length) * 100 > 30)) || ((nLunar > nSolar) && ((nSolar / length) * 100 > 30))) {
                        result = "Van Am:" + (int) ((nLunar / length) * 100) + "%," + "Van Duong:" + (int) ((nSolar / length) * 100) +
                                "%.Am duong kha can bang,day so kha hoa hop";
                        point = 1.5;
                    } else {
                        result = "Van Am:" + (int) ((nLunar / length) * 100) + "%,Van Duong:" + (int) ((nSolar / length) * 100) +
                                "%.Am duong khong can bang,D/S khong thuc su hoa hop";
                        point = 1;
                    }
                } else {
                    point = 2;
                    result = "Van Am va Duong can bang,day so hoa hop";
                }
            }
            content[0] = result;
            content[1] = String.valueOf(point);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            return content;
        }
    }

    public static int countNumber(String number, String compareNumber) {
        int total = 0;
        int length = 0;

        length = number.length();
        for (int i = 0; i < length; i++) {
            if (number.substring(i, i + 1).equals(compareNumber)) {
                total = total + 1;
            }
        }
        return total;
    }

    public static String[] getNguHanh(String checkNumber, String birthYear) {
        String[] result = new String[2];
        try {
            DBTool dbTool = new DBTool();
            String content = "";
            String menhSo = "";
            int nMax1 = 0;
            int nMax2 = 0;
            int nNeedSwap1 = 0;
            int nNeedSwap2 = 0;
            int numOfHanh = 0;
            double point = 0;

            String can = getLunarThapCan(birthYear);
            String chi = getLunarYear(birthYear);
            String[] dataNguHanh = new String[3];
            dataNguHanh = dbTool.getDataNguHanh(can, chi);
            String birthYearLunar = dataNguHanh[0];
            String menhChu = dataNguHanh[1];
            String tenGoi = dataNguHanh[2];
            //Kiem tra menh cua so
            String checkNumTmp = checkNumber.replaceAll("0", "1"); //Thuy

            checkNumTmp = checkNumTmp.replaceAll("5", "2"); //Tho

            checkNumTmp = checkNumTmp.replaceAll("8", "2"); //Tho

            checkNumTmp = checkNumTmp.replaceAll("4", "3"); //Moc

            checkNumTmp = checkNumTmp.replaceAll("7", "4"); //Kim

            checkNumTmp = checkNumTmp.replaceAll("6", "3"); //Kim

            checkNumTmp = checkNumTmp.replaceAll("9", "5"); //Hoa

            for (int i = 1; i <= 5; i++) {
                if (countNumber(checkNumTmp, String.valueOf(i)) > nMax1) {
                    nMax1 = countNumber(checkNumTmp, String.valueOf(i));
                    nNeedSwap1 = i;
                }
                try {
                    if (checkNumTmp.substring(i, i + 1).equals("0")) {
                        numOfHanh = numOfHanh + 1;
                    }
                } catch (Exception ex) {
                }
            }

            if (checkNumber == null) {
                nMax2 = 0;
            } else {
                for (int i = 1; i <= 5; i++) {
                    if (countNumber(checkNumTmp, String.valueOf(i)) > nMax2) {
                        nMax2 = countNumber(checkNumTmp, String.valueOf(i));
                        nNeedSwap2 = i;
                    }
                }
            }

            if ((nMax1 - nMax2 == 1) && (numOfHanh == 2)) {
                nMax2 = nMax1;
            }
            if ((double) (nMax1) / (checkNumber.length()) > (double) 1 / 3) {
                if (nMax2 == nMax1) {
                    if ((nNeedSwap1 == 2) && (nNeedSwap2 == 2)) {
                        menhSo = "THO";
                    } else if ((nNeedSwap1 == 5) && (nNeedSwap2 == 5)) {
                        menhSo = "HOA";
                    } else if ((nNeedSwap1 == 1) && (nNeedSwap2 == 1)) {
                        menhSo = "THUY";
                    } else if ((nNeedSwap1 == 3) && (nNeedSwap2 == 3)) {
                        menhSo = "MOC";
                    } else {
                        menhSo = "KIM";
                    }
                } else if (nMax1 - nMax2 > 0) {
                    if (nNeedSwap1 == 1) {
                        menhSo = "THUY";
                    } else if (nNeedSwap1 == 2) {
                        menhSo = "THO";
                    } else if (nNeedSwap1 == 3) {
                        menhSo = "MOC";
                    } else if (nNeedSwap1 == 4) {
                        menhSo = "KIM";
                    } else if (nNeedSwap1 == 5) {
                        menhSo = "HOA";
                    }
                }
            } else {
//            nNeedSwap1 = Integer.parseInt(checkNumber.substring(checkNumber.length() - 1));
                nNeedSwap1 = Integer.parseInt(checkNumTmp.substring(checkNumber.length() - 1));
                if (nNeedSwap1 == 1) {
                    menhSo = "THUY";
                } else if (nNeedSwap1 == 2) {
                    menhSo = "THO";
                } else if (nNeedSwap1 == 3) {
                    menhSo = "MOC";
                } else if (nNeedSwap1 == 4) {
                    menhSo = "KIM";
                } else if (nNeedSwap1 == 5) {
                    menhSo = "HOA";
                }
            }

            if (menhChu.equals("THUY")) {
                if (menhSo.equals("KIM")) {
                    point = (float) 2.5;
                } else if (menhSo.equals("THO")) {
                    point = (float) 0.25;
                } else {
                    point = (float) 1.5;
                }
            } else if (menhChu.equals("MOC")) {
                if (menhSo.equals("THUY")) {
                    point = (float) 2.5;
                } else if (menhSo.equals("KIM")) {
                    point = (float) 0.25;
                } else {
                    point = (float) 1.5;
                }
            } else if (menhChu.equals("HOA")) {
                if (menhSo.equals("MOC")) {
                    point = (float) 2.5;
                } else if (menhSo.equals("THUY")) {
                    point = (float) 0.25;
                } else {
                    point = (float) 1.5;
                }
            } else if (menhChu.equals("THO")) {
                if (menhSo.equals("HOA")) {
                    point = (float) 2.5;
                } else if (menhSo.equals("MOC")) {
                    point = (float) 0.25;
                } else {
                    point = (float) 1.5;
                }
            } else if (menhChu.equals("KIM")) {
                if (menhSo.equals("THO")) {
                    point = (float) 2.5;
                } else if (menhSo.equals("HOA")) {
                    point = (float) 0.25;
                } else {
                    point = (float) 1.5;
                }
            }

            if (point == 2.5) {
                content = "Day la so hop voi than chu,rat tot";
            } else if (point == 0.25) {
                content = "Day la so tuong khac voi than chu,kha xau";
            } else if (point == 1.5) {
                content = "Day la so khong tuong khac,chap nhan duoc";
            }

            content = "Than chu " + birthYearLunar + ",menh " + menhChu + "\nDay so menh " + menhSo + "\n" + content;
            result[0] = content;
            result[1] = String.valueOf(point);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            return result;
        }
    }

    public static int sum(String checkNumber) {
        int result = 0;
        int length = 0;

        length = checkNumber.length();
        for (int i = 0; i < length; i++) {
            result = result + Integer.parseInt(checkNumber.substring(i, i + 1));
        }
        return result;
    }

    public static String[] getQuaiKhi(String checkNumber) {
        String result[] = new String[2];
        try {
            String content = "";
            int length = 0;
            int nNoiQuai = 0;
            int nNgoaiQuai = 0;
            checkNumber = checkNumber.replaceAll("o", "0");
            checkNumber = checkNumber.replaceAll("O", "0");
            length = checkNumber.length();
            if ((length % 2) == 0) {
                nNoiQuai = Integer.parseInt(checkNumber.substring(0, checkNumber.length() / 2));
                nNgoaiQuai = Integer.parseInt(checkNumber.substring(checkNumber.length() / 2));
            } else {
                nNoiQuai = Integer.parseInt(checkNumber.substring(0, (int) Math.round((float) checkNumber.length() / 2)));
                if ((-Math.round((float) length / 2) + 1) >= 0) {
                    nNgoaiQuai = Integer.parseInt(checkNumber.substring(-Math.round((float) length / 2) + 1, length));
                } else {
                    nNgoaiQuai = Integer.parseInt(checkNumber.substring(length + (-Math.round((float) checkNumber.length() / 2) + 1), length));
                }
            }

            nNoiQuai = sum(String.valueOf(nNoiQuai));
            nNgoaiQuai = sum(String.valueOf(nNgoaiQuai));

            String[] dataQuaiKhi = new String[3];
            DBTool dbTool = new DBTool();
            dataQuaiKhi = dbTool.getDataQuaiKhi(nNoiQuai, nNgoaiQuai);
            content = "Ten Que dich:" + dataQuaiKhi[0] + "\n" + dataQuaiKhi[1];
            result[0] = content;
            result[1] = dataQuaiKhi[2]; //diem

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            return result;
        }
    }

    public static String[] getQuanNiem(String checkNumber) {
        String[] result = new String[2];
        try {
            int length = 0;
            int soNut = 0;
            double diemCong = 0;
            String congStr = "";

            double diemSoTien = 0;
            String soTien = "";

            double diemPhatLoc = 0;
            String phatLoc = "";

            double diemHoangTai = 0;
            String hoangTai = "";

            double diemTuQuy = 0;
            String tuQuy = "";

            double diemTu = 0;
            String tu = "";

            double diemThatBat = 0;
            String thatBat = "";

            int soDem = 0;
            String tmp = "";

            double tongDiem = 0;
            String content = "";

            length = checkNumber.length();
            for (int i = 0; i < length; i++) {
                soNut = soNut + Integer.parseInt(checkNumber.substring(i, i + 1));
            }

            if ((soNut % 10) < 4) {
                diemCong = -0.25;
                congStr = "So nut " + String.valueOf(soNut % 10) + ",day la so nuoc tit,rat xau";
            } else if ((soNut % 10) < 5) {
                diemCong = 0;
                congStr = "So nut " + String.valueOf(soNut % 10) + ",day la so binh thuong";
            } else if ((soNut % 10) < 8) {
                diemCong = 0.25;
                congStr = "So nut " + String.valueOf(soNut % 10) + ",day la so kha dep";
            } else if ((soNut % 10) <= 9) {
                diemCong = 0.5;
                congStr = "So nut " + String.valueOf(soNut % 10) + ",day la so dep";
            }

            //Kiem tra so trung nhau
            if (length >= 3) {
                tmp = checkNumber.substring(checkNumber.length() - 1);
                soDem = 0;

                for (int i = 1; i <= length; i++) {
                    if (!(checkNumber.substring(checkNumber.length() - i, checkNumber.length() - i + 1)).equals(tmp)) {
                        break;
                    } else {
                        soDem = soDem + 1;
                    }
                }
            }

            if (soDem >= 4) {
                diemTuQuy = 1;
                tuQuy = ".So tu quy,rat dep";
            } else if (soDem == 3) {
                diemTuQuy = 0.5;
                tuQuy = ".So tam hoa,de nho";
            }


            if (checkNumber.endsWith("39") || checkNumber.endsWith("79")) {
                diemHoangTai = 0.75;
                hoangTai = ".So Hoang Tai,so dep";
            }


            if (checkNumber.endsWith("68") || checkNumber.endsWith("86")) {
                diemPhatLoc = 1;
                phatLoc = ".So Loc phat,rat dep";
            }

            if (checkNumber.endsWith("78")) {
                diemThatBat = -0.5;
                thatBat = ".So That bat,rat xau";
            }

            if (checkNumber.endsWith("4")) {
                diemTu = -0.25;
                tu = ".Chuoi so co duoi 4,kha xau";
            }

            //Kiem tra so tien
            tmp = checkNumber.substring(checkNumber.length() - 1);
            soDem = 0;
            for (int i = 1; i < length; i++) {
                if (Integer.parseInt(checkNumber.substring(checkNumber.length() - i - 1, checkNumber.length() - i)) > Integer.parseInt(tmp)) {
                    break;
                } else {
                    soDem = soDem + 1;
                    tmp = checkNumber.substring(checkNumber.length() - i - 1, checkNumber.length() - i);
                }
            }

            if (soDem >= 3) {
                diemSoTien = 0.75;
                soTien = ".Day la so tien ra dep,de nho";
            } else if (soDem >= 2) {
                diemSoTien = 0.5;
                soTien = ".Day la so tien dep,de nho";
            }

            tongDiem = diemCong + diemTuQuy + diemPhatLoc + diemHoangTai + diemSoTien + diemTu + diemThatBat;
            content = congStr + "!x!" + tu + "!x!" + thatBat + "!x!" + tuQuy + "!x!" + phatLoc + "!x!" + hoangTai + "!x!" + soTien;
            while (content.indexOf("!x!") != -1) {
                content = content.replaceAll("!x!", "");
            }

            if (tongDiem > 2) {
                tongDiem = 2.0;
            }
            result[0] = content;
            result[1] = String.valueOf(tongDiem);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            return result;
        }
    }

    public static String[] getThienThoi(String checkNumber) {
        String[] result = new String[2];
        try {
            double diem = 0;
            String content = "";

            if (checkNumber.indexOf("8") != -1) {
                content = "Day so co Vuong khi";
                diem = 0.5;
            } else {
                content = "Day so khong duoc ve thien thoi";
                diem = 0;
            }

            result[0] = content;
            result[1] = String.valueOf(diem);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            return result;
        }
    }

    public static String getLastName(String str) {
        str = str.toUpperCase();
        String result = "";
        if (str.equals("A")) {
            result = "Ky Mon";
        } else if (str.equals("B")) {
            result = "Huyen Thien";
        } else if (str.equals("C")) {
            result = "Nhat Nguyet";
        } else if (str.equals("D")) {
            result = "Doc Long";
        } else if (str.equals("E")) {
            result = "Da Xoa";
        } else if (str.equals("G")) {
            result = "Than Duong";
        } else if (str.equals("H")) {
            result = "Ngoc Nu";
        } else if (str.equals("I")) {
            result = "Tieu Dieu";
        } else if (str.equals("K")) {
            result = "Thien Canh";
        } else if (str.equals("L")) {
            result = "Thien Vu";
        } else if (str.equals("M")) {
            result = "Bat Quai";
        } else if (str.equals("N")) {
            result = "Thai At";
        } else if (str.equals("O")) {
            result = "Lac Anh";
        } else if (str.equals("U")) {
            result = "Than Mon";
        } else if (str.equals("P")) {
            result = "Nhan Xa";
        } else if (str.equals("Q")) {
            result = "Thai Cuc";
        } else if (str.equals("V")) {
            result = "Luc Hop";
        } else if (str.equals("R")) {
            result = "Hoi Phong";
        } else if (str.equals("S")) {
            result = "Hon Don";
        } else if (str.equals("T")) {
            result = "Can Khon";
        } else if (str.equals("X")) {
            result = "Thien Mon";
        } else if (str.equals("Y")) {
            result = "Cuu Thien";
        }
        return result;
    }

    public static String getMidName(String str) {
        str = str.toUpperCase();
        String result = "";
        if (str.equals("A")) {
            result = "Phat Huyet";
        } else if (str.equals("B")) {
            result = "Giang Ma";
        } else if (str.equals("C")) {
            result = "Tich Lich";
        } else if (str.equals("D")) {
            result = "Am Duong";
        } else if (str.equals("E")) {
            result = "Tang Mon";
        } else if (str.equals("G")) {
            result = "Tu La";
        } else if (str.equals("H")) {
            result = "Toai Thach";
        } else if (str.equals("I")) {
            result = "Cuu Cuu";
        } else if (str.equals("K")) {
            result = "Cam";
        } else if (str.equals("L")) {
            result = "Vo Ngan";
        } else if (str.equals("M")) {
            result = "Luong Nghi";
        } else if (str.equals("N")) {
            result = "Ngu Than";
        } else if (str.equals("O")) {
            result = "Xuyen Van";
        } else if (str.equals("U")) {
            result = "Vo Anh";
        } else if (str.equals("P")) {
            result = "Pha Ngoc";
        } else if (str.equals("Q")) {
            result = "Ky";
        } else if (str.equals("V")) {
            result = "Vo Song";
        } else if (str.equals("R")) {
            result = "Tan Loi";
        } else if (str.equals("S")) {
            result = "Phuc Ma";
        } else if (str.equals("T")) {
            result = "Du Than";
        } else if (str.equals("X")) {
            result = "Lien Hoan";
        } else if (str.equals("Y")) {
            result = "Than";
        }
        return result;
    }

    public static String getFirstName(String str) {
        str = str.toUpperCase();
        String result = "";
        if (str.equals("A")) {
            result = "Cham";
        } else if (str.equals("B")) {
            result = "Bong";
        } else if (str.equals("C")) {
            result = "Chuong";
        } else if (str.equals("D")) {
            result = "Dao";
        } else if (str.equals("E")) {
            result = "Chao";
        } else if (str.equals("G")) {
            result = "Chi";
        } else if (str.equals("H")) {
            result = "Phu";
        } else if (str.equals("I")) {
            result = "Cau";
        } else if (str.equals("K")) {
            result = "Con";
        } else if (str.equals("L")) {
            result = "Truong";
        } else if (str.equals("M")) {
            result = "Tien";
        } else if (str.equals("N")) {
            result = "Kiem";
        } else if (str.equals("O")) {
            result = "Tieu";
        } else if (str.equals("U")) {
            result = "Chao";
        } else if (str.equals("P")) {
            result = "Dao";
        } else if (str.equals("Q")) {
            result = "Quyen";
        } else if (str.equals("V")) {
            result = "Mau";
        } else if (str.equals("R")) {
            result = "Thu";
        } else if (str.equals("S")) {
            result = "Cong";
        } else if (str.equals("T")) {
            result = "Chuy";
        } else if (str.equals("X")) {
            result = "Thuong";
        } else if (str.equals("Y")) {
            result = "Kiem";
        }
        return result;
    }

    public static int getValueFromChar(char c) {
        //Su dung trong Boi Kieu
        int value = 0;
        if ((c == 'A') || (c == 'J') || (c == 'S') || (c == '1')) {
            value = 1;
        } else if ((c == 'B') || (c == 'K') || (c == 'T') || (c == '2')) {
            value = 2;
        } else if ((c == 'C') || (c == 'L') || (c == 'U') || (c == '3')) {
            value = 3;
        } else if ((c == 'D') || (c == 'M') || (c == 'V') || (c == '4')) {
            value = 4;
        } else if ((c == 'E') || (c == 'N') || (c == 'W') || (c == '5')) {
            value = 5;
        } else if ((c == 'F') || (c == 'O') || (c == 'X') || (c == '6')) {
            value = 6;
        } else if ((c == 'G') || (c == 'P') || (c == 'Y') || (c == '7')) {
            value = 7;
        } else if ((c == 'H') || (c == 'Q') || (c == 'Z') || (c == '8')) {
            value = 8;
        } else if ((c == 'I') || (c == 'R') || (c == '9')) {
            value = 9;
        } else if (c == '0') {
            value = 0;
        }
        return value;
    }

    public static int getValueFromString(String inputStr) {
        //Su dung trong Boi Kieu
        int result = 0;
        int resultTmp = 0;
        try {
            inputStr = inputStr.toUpperCase();
            int len = inputStr.length();
            for (int i = 0; i < len; i++) {
                char c = (char) inputStr.charAt(i);
                result = result + getValueFromChar(c);
            }

            resultTmp = result;
            while (resultTmp >= 10) {
                String resultStr = String.valueOf(resultTmp);
                for (int i = 0; i < resultStr.length(); i++) {
                    char cc = resultStr.charAt(i);
                    if (i == 0) {
                        resultTmp = getValueFromChar(cc);
                    } else {
                        resultTmp = resultTmp + getValueFromChar(cc);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultTmp;
    }

    public static boolean isDT(String dt) {
        boolean ret = false;
        int t = 0;
        try {
            if (dt.length() <= 3) {
                return true;
            } else {
                dt = dt.substring(3);
                for (int i = 0; i < dt.length(); i++) {
                    t = (int) dt.charAt(i);
                    if ((t >= 48) && (t <= 57)) {
                        ret = true;
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ret;
    }

    public static void main(String[] args) {
        Utilities util = new Utilities();
        try {
//            String a = "QQ 1,2,3,4,5";
//            System.out.println(Utilities.getObjectString(a, 0));
            System.out.println(buildMobileOperator("841275892444"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
