/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sms.common;

import java.util.Calendar;

/**
 *
 * @author hungdt
 */
public class Common {

    public static java.util.Date addDaysToDate(java.util.Date date, int daysToAdd) throws Exception {
        Calendar now = Calendar.getInstance();
        now.setTime(date);
        now.add(Calendar.DAY_OF_MONTH, daysToAdd);
        return now.getTime();
    }

    public static java.sql.Date getSqlDate(java.util.Date date) {
        if (date == null) {
            return null;
        } else {
            java.sql.Date sqlDate = new java.sql.Date(date.getTime());
            return new java.sql.Date(date.getTime());
        }
    }
}
