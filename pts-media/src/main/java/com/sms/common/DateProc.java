package com.sms.common;

/**
 * <p>Title: SMPP Gateway</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: Home</p>
 * @author not attributable
 * @version 1.0
 */

import java.io.PrintStream;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.*;

public class DateProc {
  public DateProc() {
  }

  public static int getDay(Timestamp ts) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date(ts.getTime()));
    return calendar.get(5);
  }

  public static int getCurrentDay() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date(System.currentTimeMillis()));
    return calendar.get(5);
  }

  public static int getMonth(Timestamp ts) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date(ts.getTime()));
    return calendar.get(2) + 1;
  }

  public static int getCurrentMonth() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date(System.currentTimeMillis()));
    return calendar.get(2) + 1;
  }

  public static String getCurrentMM() {
      String result = "";
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date(System.currentTimeMillis()));
    int i = calendar.get(2) + 1;
    if(i<10) {
        result = "0" + String.valueOf(i);
    } else {
        result = String.valueOf(i);
    }
    return result;
  }

  public static String getCurrentDD() {
      String result = "";
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date(System.currentTimeMillis()));
    result = String.valueOf(calendar.get(calendar.DAY_OF_MONTH));
    if(result.length() < 2) {
        result = "0" + result;
    }
    return result;
  }

  public static String getCurrentDDMM() {
      String DD = "";
      String MM = "";
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(new Date(System.currentTimeMillis()));
      DD = String.valueOf(calendar.get(calendar.DAY_OF_MONTH));
      MM = String.valueOf(calendar.get(calendar.MONTH) + 1);
      if (DD.length() < 2) {
          DD = "0" + DD;
      }
      if (MM.length() < 2) {
          MM = "0" + MM;
      }

      return DD + "/" + MM;
  }

  public static String getCurrentYYYYMMDDHH24MISS() {
      String result = "";
      String yyyy = "";
      String mm = "";
      String dd = "";
      String hh24 = "";
      String mi = "";
      String ss = "";
      Calendar cal = Calendar.getInstance();
      cal.setTime(new Date(System.currentTimeMillis()));
      yyyy = String.valueOf(cal.get(cal.YEAR));
      mm = String.valueOf(cal.get(cal.MONTH));
      if(mm.length() < 2) {
          mm = "0" + mm;
      }
      dd = String.valueOf(cal.get(cal.DAY_OF_MONTH));
      if (dd.length() < 2) {
          dd = "0" + dd;
      }
      hh24 = String.valueOf(cal.get(cal.HOUR));
      if (hh24.length() < 2) {
          hh24 = "0" + hh24;
      }
      mi = String.valueOf(cal.get(cal.MINUTE));
      if (mi.length() < 2) {
          mi = "0" + mi;
      }
      ss = String.valueOf(cal.get(cal.MILLISECOND));
      if (ss.length() < 2) {
          ss = "0" + ss;
      } else {
          ss = ss.substring(0,2);
      }
      result = yyyy + "/" + mm + "/" + dd + "/" + hh24 + "/" + mi + "/" + ss;
    return result;
  }

  public static Timestamp createTimestamp() {
    Calendar calendar = Calendar.getInstance();
    return new Timestamp(calendar.getTime().getTime());
  }

  public static Timestamp createDateTimestamp(Date date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    return new Timestamp(calendar.getTime().getTime());
  }

  public static Timestamp String2TimestampEx(String strInputDate) {
      System.out.println("strInputDate:"+strInputDate);
    String strDate = strInputDate;
    String strSub = null;
    int i = strDate.indexOf("/");
    System.out.println("i:"+i);
    if (i < 0)
      return createTimestamp();
    strSub = strDate.substring(0, i);
    int nDay = (new Integer(strSub.trim())).intValue();
    strDate = strDate.substring(i + 1);
    System.out.println("strDate:"+strDate);
    i = strDate.indexOf("/");
    if (i < 0)
      return createTimestamp();
    strSub = strDate.substring(0, i);
    int nMonth = (new Integer(strSub.trim())).intValue() - 1;
    strDate = strDate.substring(i + 1);
    if (strDate.length() < 4)
      if (strDate.substring(0, 1).equals("9"))
        strDate = "19" + strDate.trim();
      else
        strDate = "20" + strDate.trim();
    int nYear = (new Integer(strDate)).intValue();


    System.out.println("nMonth:"+nMonth);
    System.out.println("nYear:"+nYear);

    Calendar calendar = Calendar.getInstance();
    calendar.set(nYear, nMonth, nDay);
    return new Timestamp(calendar.getTime().getTime());
  }

  public static Timestamp String2Timestamp(String strInputDate) {
      strInputDate = "TEST " + strInputDate;
      try {
          String dd = StringTool.getObjectString(strInputDate, 1);
          String mm = StringTool.getObjectString(strInputDate, 2);
          String yy = StringTool.getObjectString(strInputDate, 3);

          if(dd.equals("") || mm.equals("")) {
              return createTimestamp();
          } else {
              if(yy.equals("")) {
                  yy = getCurrentYYYY();
              } else {
                  if (Integer.parseInt(yy) < 10) {
                      yy = "200" + yy;
                  } else if ((Integer.parseInt(yy) > 10) && (Integer.parseInt(yy) < 100)) {
                      yy = "19" + yy;
                  } else {
                      return createTimestamp();
                  }
              }
              Calendar calendar = Calendar.getInstance();
              calendar.set(Integer.parseInt(yy), Integer.parseInt(mm) -1, Integer.parseInt(dd));
              return new Timestamp(calendar.getTime().getTime());
          }
      } catch (Exception ex) {
          return createTimestamp();
      }
  }

  public static String getDateTimeString(Timestamp ts) {
    if (ts == null)
      return "";
    else
      return Timestamp2DDMMYYYY(ts) + " " + Timestamp2HHMMSS(ts, 1);
  }

  public static String getDateString(Timestamp ts) {
    if (ts == null)
      return "";
    else
      return Timestamp2DDMMYYYY(ts);
  }

  public static String getTimeString(Timestamp ts) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date(ts.getTime()));
    return calendar.get(11) + ":" + calendar.get(12) + ":" + calendar.get(13);
  }

  public static String Timestamp2YYYYMMDD(Timestamp ts) {
    if (ts == null)
      return "";
    String sYear = "";
    String sMonth = "";
    String sDay = "";
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date(ts.getTime()));
    sDay = "" + calendar.get(5);
    if (calendar.get(5) < 10)
      sDay = "0" + sDay;
    if (calendar.get(2) + 1 < 10) {
      sMonth = "0" + (calendar.get(2) + 1);
    }
    else {
      sMonth = "" + (calendar.get(2) + 1);
    }
    sYear = "" + calendar.get(1);
    return sYear + sMonth + sDay;
  }

  public static String Timestamp2YYYYMMDD(Timestamp ts, String seperator) {
    if (ts == null)
      return "";
    String sYear = "";
    String sMonth = "";
    String sDay = "";
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date(ts.getTime()));
    sDay = "" + calendar.get(5);
    if (calendar.get(5) < 10)
      sDay = "0" + sDay;
    if (calendar.get(2) + 1 < 10) {
      sMonth = "0" + (calendar.get(2) + 1);
    }
    else {
      sMonth = "" + (calendar.get(2) + 1);
    }

    sYear = "" + calendar.get(1);
    return sYear + seperator + sMonth + seperator + sDay;
  }

  public static String Timestamp2DDMMYYYY(Timestamp ts) {
    if (ts == null)
      return "";
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date(ts.getTime()));
    String strTemp = Integer.toString(calendar.get(5));
    if (calendar.get(5) < 10)
      strTemp = "0" + strTemp;
    if (calendar.get(2) + 1 < 10) {
      return strTemp + "/0" + (calendar.get(2) + 1) + "/" + calendar.get(1);
    }
    else {
      return strTemp + "/" + (calendar.get(2) + 1) + "/" + calendar.get(1);
    }
  }

  public static String Timestamp2DDMMYY(Timestamp ts) {
    if (ts == null)
      return "";
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date(ts.getTime()));
    String strTemp = Integer.toString(calendar.get(5));
    int endYear = calendar.get(1) % 100;
    if (calendar.get(5) < 10)
      strTemp = "0" + strTemp;
    if (calendar.get(2) + 1 < 10)
      if (endYear < 10) {
        return strTemp + "/0" + (calendar.get(2) + 1) + "/0" + endYear;
      }
      else {
        return strTemp + "/0" + (calendar.get(2) + 1) + "/" + endYear;
      }
    if (endYear < 10) {
      return strTemp + "/" + (calendar.get(2) + 1) + "/0" + endYear;
    }
    else {
      return strTemp + "/" + (calendar.get(2) + 1) + "/" + endYear;
    }
  }

  public static String Timestamp2HHMM(Timestamp ts) {
    if (ts == null)
      return "";
    String sHour = "";
    String sMinunute = "";
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date(ts.getTime()));
    if (calendar.get(11) < 10) {
      sHour = "0" + calendar.get(11);
    }
    else {
      sHour = "" + calendar.get(11);
    }
    if (calendar.get(12) < 10) {
      sMinunute = "0" + calendar.get(12);
    }
    else {
      sMinunute = "" + calendar.get(12);
    }
    return sHour + sMinunute;
  }

  public static String Timestamp2HHMMSS(Timestamp ts) {
    if (ts == null)
      return "";
    String sHour = "";
    String sMinunute = "";
    String sSecond = "";
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date(ts.getTime()));
    if (calendar.get(11) < 10)
      sHour = "0" + calendar.get(11);
    else
      sHour = "" + calendar.get(11);
    if (calendar.get(12) < 10) {
      sMinunute = "0" + calendar.get(12);
    }
    else {
      sMinunute = "" + calendar.get(12);
    }

    if (calendar.get(13) < 10) {
      sSecond = "0" + calendar.get(13);
    }
    else {
      sSecond = "" + calendar.get(13);
    }
    return sHour + sMinunute + sSecond;
  }

  public static String Timestamp2HHMMSS(Timestamp ts, int iStyle) {
    if (ts == null)
      return "";
    String sHour = "";
    String sMinunute = "";
    String sSecond = "";
    String strTemp = "";
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date(ts.getTime()));
    if (iStyle == 0)
      sHour = "" + calendar.get(11);
    else
      sHour = "" + calendar.get(10);
    if (sHour.length() < 2)
      sHour = "0" + sHour;
    if (calendar.get(12) < 10) {
      sMinunute = "0" + calendar.get(12);
    }
    else {
      sMinunute = "" + calendar.get(12);
    }
    if (calendar.get(13) < 10) {
      sSecond = "0" + calendar.get(13);
    }
    else {
      sSecond = "" + calendar.get(13);
    }
    strTemp = sHour + ":" + sMinunute + ":" + sSecond;
    if (iStyle != 0) {
      if (calendar.get(9) == 0)
        strTemp = strTemp + " AM";
      else
        strTemp = strTemp + " PM";
    }
    return strTemp;
  }

  public static String getYYYYMMDDHHMMString(Timestamp ts) {
    if (ts == null)
      return "";
    else
      return Timestamp2YYYYMMDD(ts) + Timestamp2HHMM(ts);
  }

  public static String getYYYYMMDDHHMMSSString(Timestamp ts) {
    if (ts == null)
      return "";
    else
      return Timestamp2YYYYMMDD(ts) + Timestamp2HHMMSS(ts);
  }

  public static String getDateTime24hString(Timestamp ts) {
    if (ts == null)
      return "";
    else
      return Timestamp2DDMMYYYY(ts) + " " + Timestamp2HHMMSS(ts, 0);
  }

  public static String getDateTime12hString(Timestamp ts) {
    if (ts == null)
      return "";
    else
      return Timestamp2DDMMYYYY(ts) + " " + Timestamp2HHMMSS(ts, 1);
  }

  public static String TimestampPlusDay2DDMMYYYY(Timestamp ts, int iDayPlus) {
    if (ts == null) {
      return "";
    }
    else {
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(new Date(ts.getTime()));
      int iDay = calendar.get(5);
      calendar.set(5, iDay + iDayPlus);
      Timestamp tsNew = new Timestamp(calendar.getTime().getTime());
      return Timestamp2DDMMYYYY(tsNew);
    }
  }

  public static Timestamp getPreviousDate(Timestamp ts) {
    if (ts == null) {
      return null;
    }
    else {
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(new Date(ts.getTime()));
      int iDay = calendar.get(5);
      calendar.set(5, iDay - 1);
      Timestamp tsNew = new Timestamp(calendar.getTime().getTime());
      return tsNew;
    }
  }

  public static String getLastestDateOfMonth(String strMonthYear) {
    String strDate = strMonthYear;
    String strSub = null;
    int i = strDate.indexOf("/");
    if (i < 0)
      return "";
    strSub = strDate.substring(0, i);
    int nMonth = (new Integer(strSub)).intValue();
    strDate = strDate.substring(i + 1);
    int nYear = (new Integer(strDate)).intValue();
    boolean leapyear = false;
    if (nYear % 100 == 0) {
      if (nYear % 400 == 0)
        leapyear = true;
    }
    else
    if (nYear % 4 == 0)
      leapyear = true;
    if (nMonth == 2)
      if (leapyear)
        return "29/" + strDate;
      else
        return "28/" + strDate;
    if (nMonth == 1 || nMonth == 3 || nMonth == 5 || nMonth == 7 || nMonth == 8 ||
        nMonth == 10 || nMonth == 12)
      return "31/" + strDate;
    if (nMonth == 4 || nMonth == 6 || nMonth == 9 || nMonth == 11)
      return "30/" + strDate;
    else
      return "";
  }

  public static int getDay(long miliSecs) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date(miliSecs));
    return calendar.get(5);
  }

  public static int getCurrentHH24() {
      String time24h = getDateTime24hString(new Timestamp(System.currentTimeMillis()));
      return Integer.parseInt(time24h.substring(11,13));
  }

  public static int getCurrentMI() {
      String time24h = getDateTime24hString(new Timestamp(System.currentTimeMillis()));
      return Integer.parseInt(time24h.substring(14,16));
  }

  public static String getCurrentDDMMYY() {// DD/MM/YY
      return DateProc.Timestamp2DDMMYY(new Timestamp(System.currentTimeMillis()));
  }

  public static String getCurrentYYYY() {
      String time24h = getDateTime24hString(new Timestamp(System.currentTimeMillis()));
      return time24h.substring(6,10);
  }

  public static void main(String[] args) {
    DateProc dateProc = new DateProc();
    System.out.println(dateProc.String2Timestamp("12/13").toString());
  }

}
