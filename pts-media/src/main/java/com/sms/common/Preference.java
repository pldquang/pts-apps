package com.sms.common;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import java.util.*;
import java.io.*;

import com.sms.process.DBTool;

public class Preference {
    private static Properties properties = new Properties();

    public static String db_driver_remote = "oracle.jdbc.driver.OracleDriver";
    public static String db_url_remote = "jdbc:oracle:thin:@localhost:1521:orcl";
    public static String db_user_remote = "dbremote";
    public static String db_password_remote = "dbremote13%&";
    public static int pooldb_max_remote = 3;

    public static String db_driver_content = "oracle.jdbc.driver.OracleDriver";
    public static String db_url_content = "jdbc:oracle:thin:@localhost:1521:orcl";
    public static String db_user_content = "dbcontent";
    public static String db_password_content = "dbcontent13%&%&";
    public static int pooldb_max_content = 3;

    public static Vector adminPhoneNumber = null;

    public static Vector dailyCode = null;
    public static Vector dailyCode1 = null;
    public static Vector mediaCode = null;
    public static Vector simpleCode = null;
    public static Vector wmusicCode = null;

    public Preference() {
        adminPhoneNumber = new Vector();
        dailyCode = new Vector();
        dailyCode1 = new Vector();
        mediaCode = new Vector();
        simpleCode = new Vector();
        wmusicCode = new Vector();
    }

    public static void loadProperties(String fileName) throws IOException {
        Preference pre = new Preference();
        System.out.println("Reading configuration file " + fileName + "...");
        FileInputStream propsFile = new FileInputStream(fileName);
        properties.load(propsFile);
        propsFile.close();
        System.out.println("Setting default parameters...");

        db_driver_remote = properties.getProperty("db_driver_remote", db_driver_remote);
        db_url_remote = properties.getProperty("db_url_remote", db_url_remote);
        db_user_remote = properties.getProperty("db_user_remote", db_user_remote);
        db_password_remote = properties.getProperty("db_password_remote", db_password_remote);
        pooldb_max_remote = getIntProperty("pooldb_max_remote", pooldb_max_remote);

        db_driver_content = properties.getProperty("db_driver_content", db_driver_content);
        db_url_content = properties.getProperty("db_url_content", db_url_content);
        db_user_content = properties.getProperty("db_user_content", db_user_content);
        db_password_content = properties.getProperty("db_password_content", db_password_content);
        pooldb_max_content = getIntProperty("pooldb_max_content", pooldb_max_content);

        getAdminPhoneNumber();
        getMediaCode();
        getDailyCode();
        getSimpleCode();
        getWmusicCode();
    }

    static byte getByteProperty(String propName, byte defaultValue) {
        return Byte.parseByte(properties.getProperty(propName,
                Byte.toString(defaultValue)).trim());
    }

    static int getIntProperty(String propName, int defaultValue) {
        try {
            return Integer.parseInt(properties.getProperty(propName,
                    Integer.toString(defaultValue)).trim());
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

//    public static void getCommandCode() {
//        String keyStr = properties.getProperty("command_code");
//        StringTokenizer tokenizer = new StringTokenizer(keyStr, ",");
//        while (tokenizer.hasMoreTokens()) {
//            String key = tokenizer.nextToken();
//            commandCode.add(key);
//        }
//        StringTool.bubbleSort(commandCode);
//    }

    public static void getAdminPhoneNumber() {
        String keyStr = properties.getProperty("admin_phone_number");
        StringTokenizer tokenizer = new StringTokenizer(keyStr, ",");
        while (tokenizer.hasMoreTokens()) {
            String key = tokenizer.nextToken();
            adminPhoneNumber.add(key);
        }
    }

    public static void getMediaCode() {
        String keyStr = properties.getProperty("media_code");
        StringTokenizer tokenizer = new StringTokenizer(keyStr, ",");
        while (tokenizer.hasMoreTokens()) {
            String key = tokenizer.nextToken();
            mediaCode.add(key);
        }
    }

    public static void getDailyCode() {
        String keyStr = properties.getProperty("daily_code");
        StringTokenizer tokenizer = new StringTokenizer(keyStr, ",");
        while (tokenizer.hasMoreTokens()) {
            String key = tokenizer.nextToken();
            dailyCode.add(key);
        }

        String keyStr1 = properties.getProperty("daily_code_1");
        StringTokenizer tokenizer1 = new StringTokenizer(keyStr1, ",");
        while (tokenizer1.hasMoreTokens()) {
            String key1 = tokenizer1.nextToken();
            dailyCode1.add(key1);
        }
    }

    public static void getSimpleCode() {
        String keyStr = properties.getProperty("simple_code");
        StringTokenizer tokenizer = new StringTokenizer(keyStr, ",");
        while (tokenizer.hasMoreTokens()) {
            String key = tokenizer.nextToken();
            simpleCode.add(key);
        }


    }

    public static void getWmusicCode() {
        String keyStr = properties.getProperty("wmusic_code");
        StringTokenizer tokenizer = new StringTokenizer(keyStr, ",");
        while (tokenizer.hasMoreTokens()) {
            String key = tokenizer.nextToken();
            wmusicCode.add(key);
        }
    }

    public static void main(String[] args) {
    }

}
