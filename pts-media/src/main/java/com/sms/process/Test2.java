package com.sms.process;

import com.sms.common.MyLogger;
import com.sms.common.StringTool;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2008</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import com.sms.common.Utilities;
public class Test2 {
    public Test2() {
    }

    public static void main(String[] args) {
        String commandId = "XEM";
        String info = "XEM DT 8.3";
        String commandCode = "XEM";
        String infoTmp = commandCode + " " + info.substring(commandCode.length(), info.length()).trim();
        String subCode1 = "DT";
        String ddMM = "";
        ddMM = infoTmp.substring(commandCode.length()).trim();
        ddMM = ddMM.substring(subCode1.length()).trim();

        ddMM = ddMM.replaceAll("o", "0");
        ddMM = ddMM.replaceAll("O", "0");
        ddMM = StringTool.getContentOnlyNumber(ddMM);

        if (ddMM.length() > 4)
            ddMM = ddMM.substring(0, 4);
            boolean trueFormat = false;
            if (ddMM.length() == 2) {
                String firstDigit = ddMM.substring(0, 1);
                String secondDigit = ddMM.substring(1, 2);
                if ((Integer.parseInt(firstDigit) > 0) && (Integer.parseInt(secondDigit) > 0)) {
                    ddMM = "0" + firstDigit + "-" + "0" + secondDigit;
                    trueFormat = true;
                }
            } else if (ddMM.length() == 3) {
                String firstDigit = ddMM.substring(0, 1);
                String secondDigit = ddMM.substring(1, 2);
                String thirdDigit = ddMM.substring(2, 3);

                if ((Integer.parseInt(firstDigit) > 0) && (Integer.parseInt(secondDigit + thirdDigit) <= 12) &&
                    (Integer.parseInt(secondDigit + thirdDigit) > 0)) {
                    // XYZ -> 0X-YZ
                    ddMM = "0" + firstDigit + "-" + secondDigit + thirdDigit;
                    trueFormat = true;
                } else if ((!trueFormat) && (Integer.parseInt(firstDigit) == 0) && (Integer.parseInt(secondDigit) > 0) &&
                           (Integer.parseInt(thirdDigit) > 0)) {
                    //XYZ -> XY-0Z
                    ddMM = firstDigit + secondDigit + "-" + "0" + thirdDigit;
                    trueFormat = true;
                } else if ((!trueFormat) && (Integer.parseInt(firstDigit + secondDigit) > 0) &&
                           (Integer.parseInt(firstDigit + secondDigit) <= 31) &&
                           (Integer.parseInt(thirdDigit) > 0)) {
                    //XYZ -> XY-0Z
                    ddMM = firstDigit + secondDigit + "-" + "0" + thirdDigit;
                    trueFormat = true;
                }
            } else if (ddMM.length() == 4) {
                if ((Integer.parseInt(ddMM.substring(0, 2)) > 0) && (Integer.parseInt(ddMM.substring(0, 2)) <= 31) &&
                    (Integer.parseInt(ddMM.substring(2, 4)) > 0) && (Integer.parseInt(ddMM.substring(2, 4)) <= 12)) {
                    ddMM = ddMM.substring(0, 2) + "-" + ddMM.substring(2, 4);
                    trueFormat = true;
                }
            }
            String chiemTinh = "";
            if (trueFormat) {
                chiemTinh = Utilities.getCungChiemTinh(ddMM);
            }

            System.out.println("ddMM: " + ddMM);
            System.out.println("chiemTinh: " + chiemTinh);
            System.out.println("trueFormat: " + trueFormat);
        }
    }

