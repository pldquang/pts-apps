package com.sms.process;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import java.util.*;
import java.sql.*;
import java.math.BigDecimal;

import com.sms.common.*;
import com.sms.item.*;
import java.io.InputStream;

public class DBTool {

    private Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    String sqlStr = "";

    public DBTool() {
    }

    public void freeResouceContent(PreparedStatement pstmt, ResultSet rs, Connection conn) {
        try {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if ((conn != null) && (!conn.getAutoCommit())) {
                conn.setAutoCommit(true);
            }
            DBPool.putConnection2DBContent(conn);
        } catch (Exception ex) {
        }
    }

    public void freeResouceRemote(PreparedStatement pstmt, ResultSet rs, Connection conn) {
        try {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if ((conn != null) && (!conn.getAutoCommit())) {
                conn.setAutoCommit(true);
            }
            DBPool.putConnection2DBRemote(conn);
        } catch (Exception ex) {
        }
    }

    public boolean removeSMSReceiveQueue(String mobileOperator, BigDecimal id) {
        boolean b = false;
        PreparedStatement pstmt = null;
        String sqlStr = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionRemote()) != null) {
                conn.setAutoCommit(false);
                sqlStr = "DELETE FROM " + mobileOperator +
                        ".SMS_RECEIVE_QUEUE WHERE ID = ?";
//                System.out.println(sqlStr);
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, id);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceRemote(pstmt, null, conn);
            return b;
        }
    }

    protected BigDecimal nextSequenceGW(String name) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection conn = null;
        String sqlStr = null;
        BigDecimal nextId = null;
        try {
            conn = (Connection) DBPool.getConnectionRemote();
            if (conn != null) {
                sqlStr = "SELECT " + name + ".NEXTVAL FROM DUAL";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    nextId = rs.getBigDecimal(1);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            freeResouceRemote(pstmt, rs, conn);
            return nextId;
        }
    }

    /*
    public boolean sendMT(String userId, String serviceId,
    String mobileOperator,
    String commandCode,
    int contentType, String text,
    int messageType, BigDecimal requestId) {
    BigDecimal id = null;
    Connection conn = null;
    PreparedStatement pstmt = null;
    //        if (text.length() > 160)
    //            text = text.substring(0, 160);
    String sqlStr = "";
    boolean b = false;
    try {
    if (contentType == 0) {
    text = text.replaceAll("\r\n", "\n");
    //                if (text.charAt(0)==(char)10)
    //                    text = text.substring(1);
    //                if (text.charAt(text.length()-1)==(char)10)
    //                    text = text.substring(0, text.length() - 1);
    text = StringTool.convert2Nosign(text);
    }
    if (removeSMSReceiveQueue(mobileOperator, requestId)) {
    id = nextSequenceGW(mobileOperator + ".SEQ_SMS_SEND_QUEUE_ID");
    if ((conn = (Connection)  DBPool.getConnectionRemote()) != null) {
    conn.setAutoCommit(false);
    sqlStr = "INSERT INTO " + mobileOperator +
    ".SMS_SEND_QUEUE (ID, USER_ID, SERVICE_ID, MOBILE_OPERATOR, COMMAND_CODE, CONTENT_TYPE, INFO, MESSAGE_TYPE, REQUEST_ID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    pstmt = conn.prepareStatement(sqlStr);
    pstmt.setBigDecimal(1, id);
    pstmt.setString(2, userId);
    pstmt.setString(3, serviceId);
    pstmt.setString(4, mobileOperator);
    pstmt.setString(5, commandCode);
    pstmt.setInt(6, contentType);
    pstmt.setString(7, text);
    pstmt.setInt(8, messageType);
    pstmt.setBigDecimal(9, requestId);
    if (pstmt.executeUpdate() != -1) {
    conn.commit();
    b = true;
    }
    }
    }
    } catch (Exception ex) {
    ex.printStackTrace();
    conn.rollback();
    } finally {
    freeResouceRemote(pstmt, null, conn);
    return b;
    }
    }
     */
    public Vector getRequestFromQueue(String mobileOperator, Vector commandCode) {
        Vector v = new Vector();
        try {
            String condition = "";
            int size = commandCode.size();
            for (int i = 0; i < size; i++) {
                if (condition.equals("")) {
                    condition = "COMMAND_CODE = '" + commandCode.elementAt(i) +
                            "'";
                } else {
                    condition = condition + " OR COMMAND_CODE = '" +
                            commandCode.elementAt(i) + "'";
                }
            }
            condition = "(" + condition + ")" + " AND SERVICE_ID <> '8084'";
            condition = condition +
                    " AND ((STATUS = 0 ) OR (STATUS = 2) OR (TRUNC((86400*(SYSDATE-RECEIVE_TIME))/60)-60*(TRUNC(((86400*(SYSDATE-RECEIVE_TIME))/60)/60)) >= 3))";
            if ((conn = (Connection) DBPool.getConnectionRemote()) != null) {
                sqlStr =
                        "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,INFO,COMMAND_CODE,STATUS " +
                        "FROM " + mobileOperator + ".SMS_RECEIVE_QUEUE WHERE " +
                        condition;
//                System.out.println(sqlStr);
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    SmsReceiveQueue receiveQueue = new SmsReceiveQueue();
                    receiveQueue.setId(rs.getBigDecimal(1));
                    receiveQueue.setUserId(rs.getString(2));
                    receiveQueue.setServiceId(rs.getString(3));
                    receiveQueue.setMobileOperator(rs.getString(4));
                    receiveQueue.setInfo(rs.getString(5));
                    receiveQueue.setCommandCode(rs.getString(6));
                    receiveQueue.setStatus(rs.getInt(7));
                    v.add(receiveQueue);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceRemote(pstmt, rs, conn);
            return v;
        }
    }

    public boolean sendMTEx(String userId, String serviceId,
            String mobileOperator,
            String commandCode,
            int contentType, String text,
            int messageType, BigDecimal requestId,
            String subCode1, String subCode2, String subCode3) {
        BigDecimal id = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
//        if (text.length() > 160)
//            text = text.substring(0, 160);
        String sqlStr = "";
        boolean b = false;
        try {
            boolean check = false;
            if (messageType == 1) {
                if (removeSMSReceiveQueue(mobileOperator, requestId)) {
                    check = true;
                }
            } else {
                check = true;
            }

            if (check) {
                id = nextSequenceGW(mobileOperator + ".SEQ_SMS_SEND_QUEUE_ID");
                if ((conn = DBPool.getConnectionRemote()) != null) {
                    conn.setAutoCommit(false);
                    if (subCode1.length() > 15) {
                        subCode1 = subCode1.substring(0, 15);
                    }
                    if (subCode2.length() > 15) {
                        subCode2 = subCode2.substring(0, 15);
                    }
                    if (subCode3.length() > 15) {
                        subCode3 = subCode3.substring(0, 15);
                    }

//                    if(mobileOperator.equalsIgnoreCase("VIETEL")) {
//                        if(text.indexOf("1900561577") != -1)
//                            text = text.replaceAll("1900561577","19008088");
//                    }

                    sqlStr = "INSERT INTO " + mobileOperator +
                            ".SMS_SEND_QUEUE (ID, USER_ID, SERVICE_ID, MOBILE_OPERATOR, COMMAND_CODE, CONTENT_TYPE, INFO, MESSAGE_TYPE, REQUEST_ID,SUBCODE1,SUBCODE2,SUBCODE3) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    pstmt = conn.prepareStatement(sqlStr);
                    pstmt.setBigDecimal(1, id);
                    pstmt.setString(2, userId);
                    pstmt.setString(3, serviceId);
                    pstmt.setString(4, mobileOperator);
                    pstmt.setString(5, commandCode);
                    pstmt.setInt(6, contentType);
                    pstmt.setString(7, text);
                    pstmt.setInt(8, messageType);
                    pstmt.setBigDecimal(9, requestId);
                    pstmt.setString(10, subCode1);
                    pstmt.setString(11, subCode2);
                    pstmt.setString(12, subCode3);
                    if (pstmt.executeUpdate() != -1) {
                        conn.commit();
                        b = true;
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            conn.rollback();
        } finally {
            freeResouceRemote(pstmt, null, conn);
            return b;
        }
    }

    public String getRingToneMono(String commandId) {
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr =
                        "SELECT CONTENT FROM MBL_MEDIA WHERE COMMAND_ID = ? AND ACTIVE = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, commandId);
                pstmt.setString(2, "Y");
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString(1);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result = Utilities.getHexString(result.getBytes());
        }
    }

    public String searchWMusic(String nameNC, String mobileOperator, boolean isSearchSinger) {
        nameNC = (nameNC.trim()).toUpperCase();
        nameNC = StringTool.getContentNoWhiteLetter(nameNC);
        String sqlStr = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String result = "";
        char enter = (char) 10;
        String lastest = "Cac bai hat MoiNhat" + enter;
        String guideVT = "Soan BH MaBaiHat gui 1221 de hoan tat cai dat" + enter + "DT ho tro: 1900561577";
        String guideVMS = "B1:Soan DK gui9224" + enter + "B2:Soan CHON MaBaiHat gui 9224" + enter + "DT ho tro: 1900561577";
        String guideGPC = "B1:Soan DK gui 9194" + enter + " B2:Soan TUNE MaBaiHat gui 9194" + enter + "B3:Soan MD MaBaiHat gui 9194" + enter +
                "DT ho tro: 1900561577";
        ;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                if (!nameNC.equalsIgnoreCase("MOINHAT")) {
                    sqlStr = "SELECT FULL_NAME,CODE FROM W_MUSIC WHERE NOSIGN_NAME LIKE '%" + nameNC + "%' AND MOBILE_OPERATOR =? AND ROWNUM <= 3";
                    sqlStr = "SELECT FULL_NAME,CODE FROM (SELECT FULL_NAME,CODE FROM W_MUSIC WHERE (UPPER(REPLACE(SINGER,' ','')) LIKE '%" + nameNC +
                            "%' OR NOSIGN_NAME LIKE '%" + nameNC + "%') AND MOBILE_OPERATOR =? ORDER BY DBMS_RANDOM.VALUE) WHERE ROWNUM <= 6 ";
                    sqlStr = "SELECT FULL_NAME,CODE FROM (SELECT FULL_NAME,CODE FROM W_MUSIC WHERE (UPPER(REPLACE(SINGER,' ','')) = '" + nameNC +
                            "' OR NOSIGN_NAME = '" + nameNC + "') AND MOBILE_OPERATOR =? ORDER BY DBMS_RANDOM.VALUE) WHERE ROWNUM <= 6 ";
                } else {
                    sqlStr = "SELECT FULL_NAME,CODE FROM (SELECT FULL_NAME,CODE FROM W_MUSIC WHERE MOBILE_OPERATOR =? ORDER BY ID DESC) WHERE ROWNUM <= 6 ";
                }

                if (isSearchSinger) {
                    sqlStr = "SELECT FULL_NAME,CODE FROM (SELECT FULL_NAME,CODE FROM W_MUSIC WHERE UPPER(REPLACE(SINGER,' ','')) LIKE '" + nameNC +
                            "%' AND MOBILE_OPERATOR =? ORDER BY DBMS_RANDOM.VALUE) WHERE ROWNUM <= 6 ";
                    sqlStr = "SELECT FULL_NAME,CODE FROM (SELECT FULL_NAME,CODE FROM W_MUSIC WHERE UPPER(REPLACE(SINGER,' ','')) = '" + nameNC +
                            "' AND MOBILE_OPERATOR =? ORDER BY DBMS_RANDOM.VALUE) WHERE ROWNUM <= 6 ";
                } else if (nameNC.equalsIgnoreCase("MOINHAT")) {
                    sqlStr = "SELECT FULL_NAME,CODE FROM (SELECT FULL_NAME,CODE FROM W_MUSIC WHERE MOBILE_OPERATOR =? ORDER BY ID DESC) WHERE ROWNUM <= 6 ";
                } else {
                    sqlStr = "SELECT FULL_NAME,CODE FROM (SELECT FULL_NAME,CODE FROM W_MUSIC WHERE (UPPER(REPLACE(SINGER,' ','')) LIKE '" + nameNC +
                            "%' OR NOSIGN_NAME LIKE '%" + nameNC + "%') AND MOBILE_OPERATOR =? ORDER BY DBMS_RANDOM.VALUE) WHERE ROWNUM <= 3 ";
                    sqlStr = "SELECT FULL_NAME,CODE FROM (SELECT FULL_NAME,CODE FROM W_MUSIC WHERE (UPPER(REPLACE(SINGER,' ','')) = '" + nameNC +
                            "' OR NOSIGN_NAME = '" + nameNC + "') AND MOBILE_OPERATOR =? ORDER BY DBMS_RANDOM.VALUE) WHERE ROWNUM <= 3 ";
                }

                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, mobileOperator);
                rs = pstmt.executeQuery();
                String previousResult = "";
                while (rs.next()) {
                    result = result + rs.getString(2).trim() + ": " + rs.getString(1).trim() + enter;

                    if (mobileOperator.equals("VMS")) {
                        if (nameNC.equals("MOINHAT") || isSearchSinger) {
                            if (nameNC.equals("MOINHAT")) {
                                if (result.length() < (160 - lastest.length())) {
                                    previousResult = result;
                                } else {
                                    break;
                                }
                            } else if (isSearchSinger) {
                                if (result.length() < 160) {
                                    previousResult = result;
                                } else {
                                    break;
                                }
                            }
                        } else {
                            if (result.length() < (160 - guideVMS.length())) {
                                previousResult = result;
                            } else {
                                break;
                            }
                        }
                    } else if (mobileOperator.equals("VIETEL")) {
                        if (nameNC.equals("MOINHAT")) {
                            if (result.length() < (160 - lastest.length())) {
                                previousResult = result;
                            } else {
                                break;
                            }

                        } else if (isSearchSinger) {
                            if (result.length() < 160) {
                                previousResult = result;
                            } else {
                                break;
                            }

                        } else {
                            if (result.length() < (160 - guideVT.length())) {
                                previousResult = result;
                            } else {
                                break;
                            }
                        }
                    } else if (mobileOperator.equals("GPC")) {
                        if (nameNC.equals("MOINHAT")) {
                            if (result.length() < (160 - lastest.length())) {
                                previousResult = result;
                            } else {
                                break;
                            }

                        } else if (isSearchSinger) {
                            if (result.length() < 160) {
                                previousResult = result;
                            } else {
                                break;
                            }
                        } else {
                            if (result.length() < 160 - guideGPC.length()) {
                                previousResult = result;
                            } else {
                                break;
                            }
                        }
                    }
                }

                result = previousResult;

                if (mobileOperator.equals("VIETEL")) {
//                    if (nameNC.equals("MOINHAT") || isSearchSinger) {
//                        if (result.length() > (320 - 21 - guideVT.length())) {
//                            result = result.substring(0, 320 - 21 - guideVT.length());
//                        }
//                    } else {
//                        if (result.length() > (160 - 21 - guideVT.length())) {
//                            result = result.substring(0, 160 - 21 - (1 + guideVT.length()));
//                        }
//                    }

                    if (!result.equals("")) {
                        if (nameNC.equalsIgnoreCase("MOINHAT")) {
                            result = lastest + result;
                            result = result + "!xxx!" + guideVT;
                        } else if (isSearchSinger) {
                            result = result + "!xxx!" + guideVT;
                        } else {
                            result = result + enter + guideVT;
                        }
                    }
                } else if (mobileOperator.equals("VMS")) {
//                    if (nameNC.equals("MOINHAT") || isSearchSinger) {
//                        if (result.length() > (320 - 21 - guideVMS.length())) {
//                            result = result.substring(0, 320 - 21 - guideVMS.length());
//                        }
//                    } else {
//                        if (result.length() > (160 - 21 - guideVMS.length())) {
//                            result = result.substring(0, 160 - 21 - (1 + guideVMS.length()));
//                        }
//                    }
                    if (!result.equals("")) {
                        if (nameNC.equalsIgnoreCase("MOINHAT")) {
                            result = lastest + result;
                            result = result + "!xxx!" + guideVMS;
                        } else if (isSearchSinger) {
                            result = result + "!xxx!" + guideVMS;
                        } else {
                            result = result + enter + guideVMS;
                        }
                    }
                } else if (mobileOperator.equals("GPC")) {
//                    if (nameNC.equals("MOINHAT") || isSearchSinger) {
//                        if (result.length() > (320 - 21 - guideGPC.length())) {
//                            result = result.substring(0, 320 - 21 - guideGPC.length());
//                        }
//                    } else {
//                        if (result.length() > 160 - 21 - guideGPC.length()) {
//                            result = result.substring(0, 160 - 21 - (1 + guideGPC.length()));
//                        }
//                    }
                    if (!result.equals("")) {
                        if (nameNC.equalsIgnoreCase("MOINHAT")) {
                            result = lastest + result;
                            result = result + "!xxx!" + guideGPC;
                        } else if (isSearchSinger) {
                            result = result + "!xxx!" + guideGPC;
                        } else {
                            result = result + enter + guideGPC;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public String getDataMedia(String dataId) {
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT CONTENT FROM MBL_MEDIA WHERE COMMAND_ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, dataId);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString(1);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public String searchDataMedia(String searchStr, int dataType) {
        //1: Mono; 2: Ploly; 3: Word
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                if (dataType == 1) {
                    //mono
                    sqlStr = "SELECT CONTENT FROM MBL_MEDIA WHERE UPPER(COMMENTS) LIKE '%" + searchStr + "%' AND COMMAND_ID LIKE '6%'";
                } else if (dataType == 2) {
                    //poly
                    sqlStr = "SELECT CONTENT FROM MBL_MEDIA WHERE UPPER(COMMENTS) LIKE '%" + searchStr + "%' AND COMMAND_ID LIKE '5%'";
                } else if (dataType == 3) {
                    //Word
                    sqlStr = "SELECT CONTENT FROM MBL_MEDIA WHERE UPPER(COMMENTS) LIKE '%" + searchStr + "%' AND COMMAND_ID LIKE '4%'";
                }
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString(1);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public void formatNC() {
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT ID,NOSIGN_NAME FROM W_MUSIC";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    BigDecimal id = rs.getBigDecimal(1);
                    String nosignName = rs.getString(2);
                    nosignName = StringTool.getContentNoWhiteLetter(nosignName).toUpperCase();
                    sqlStr = "UPDATE W_MUSIC SET NOSIGN_NAME = ? WHERE ID = ?";
                    PreparedStatement pstmt1 = conn.prepareStatement(sqlStr);
                    pstmt1.setString(1, nosignName);
                    pstmt1.setBigDecimal(2, id);
                    pstmt1.executeUpdate();
                    freeResouceContent(pstmt1, null, null);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
        }
    }

    public void deleteTrungLap() {
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT code,mobile_operator,count(*) " +
                        "  FROM w_music " +
                        "  group by code,mobile_operator " +
                        "  having count(*) > 1 ";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    String code = rs.getString(1);
                    String mobileOperator = rs.getString(2);
                    sqlStr = "SELECT ID FROM W_MUSIC WHERE CODE = ? AND MOBILE_OPERATOR = ?";
                    PreparedStatement pstmt1 = conn.prepareStatement(sqlStr);
                    pstmt1.setString(1, code);
                    pstmt1.setString(2, mobileOperator);
                    ResultSet rs1 = pstmt1.executeQuery();
                    if (rs1.next()) {
                        sqlStr = "DELETE FROM W_MUSIC WHERE ID = ?";
                        PreparedStatement pstmt2 = conn.prepareStatement(sqlStr);
                        pstmt2 = conn.prepareStatement(sqlStr);
                        pstmt2.setBigDecimal(1, rs1.getBigDecimal(1));
                        pstmt2.executeUpdate();
                        freeResouceContent(pstmt2, null, null);
                    }
                    freeResouceContent(pstmt1, rs1, null);
                }
                conn.commit();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
        }
    }

    public Vector getObjectList(String commandId) {
        Vector v = new Vector();
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT DISTINCT(PARAM1) FROM TXT_DATA WHERE COMMAND_ID = ? ORDER BY LENGTH(PARAM1) DESC";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, commandId);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    if ((rs.getString(1) != null) && !"".equals(rs.getString(1))) {
                        v.add(rs.getString(1));
                    }
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return v;
        }
    }

    public String[] getDataNguHanh(String can, String chi) {
        String[] nguHanh = new String[3];
        /*
        nguHanh[0] = can_chi
        nguHanh[1] = hanh
        nguHanh[2] = ten_goi
         */
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT UPPER(CAN_CHI),UPPER(HANH),UPPER(TEN_GOI) FROM TBL_SOMAYMAN_NGUHANH " +
                        "WHERE UPPER(?)||UPPER(?)=UPPER(REPLACE(CAN_CHI,' ',''))";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, can);
                pstmt.setString(2, chi);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    nguHanh[0] = rs.getString(1);
                    nguHanh[1] = rs.getString(2);
                    nguHanh[2] = rs.getString(3);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return nguHanh;
        }
    }

    public String[] getDataQuaiKhi(int noiQuai, int ngoaiQuai) {
        String[] quaiKhi = new String[3];
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT TENQUE,YNGHIA,DIEM FROM TBL_SOMAYMAN_QUAIKHI " +
                        "WHERE QUE = MOD(?,8)||MOD(?,8)";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setInt(1, noiQuai);
                pstmt.setInt(2, ngoaiQuai);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    quaiKhi[0] = rs.getString(1);
                    quaiKhi[1] = rs.getString(2);
                    quaiKhi[2] = rs.getString(3);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return quaiKhi;
        }
    }

    public boolean checkContentMedia(String dataType, BigDecimal contentId) {
        boolean b = false;
        String tableName = "";
        String dataField = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                if (dataType.equals("12")) {
                    tableName = "RINGTONE";
                    dataField = "POLY_MID";
                } else if (dataType.equals("14")) {
                    tableName = "RINGTONE";
                    dataField = "WORD_AMR";
                } else if (dataType.equals("24")) {
                    tableName = "PHOTO";
                    dataField = "CONTENT_SIZE1";
                } else if (dataType.equals("31")) {
                    tableName = "JAVAGAME";
                    dataField = "CONTENT_JAR";
                } else if (dataType.equals("32")) {
                    tableName = "THEME";
                    dataField = "CONTENT_JAR";
                }

                sqlStr = "SELECT " + dataField + " FROM " + tableName + " WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, contentId);
                rs = pstmt.executeQuery();
                byte[] data = null;
                if (rs.next()) {
                    Blob dataBlob = rs.getBlob(1);
                    InputStream is = dataBlob.getBinaryStream();
                    data = new byte[(int) dataBlob.length()];
                    is.read(data);
                    is.close();
                }
                if ((data != null) && (data.length > 0)) {
                    b = true;
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return b;
        }

    }

    public String getDataName(String dataType, BigDecimal contentId) {
        String tableName = "";
        String dataField = "";
        String result = "";
        String ext = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                if (dataType.equals("12") || dataType.equals("13") || dataType.equals("14")) {
                    tableName = "RINGTONE";
                    dataField = "NOSIGN_NAME";
                } else if (dataType.equals("24")) {
                    tableName = "PHOTO";
                    dataField = "NOSIGN_NAME";
                } else if (dataType.equals("31")) {
                    tableName = "JAVAGAME";
                    dataField = "FILENAME";
                } else if (dataType.equals("32")) {
                    tableName = "THEME";
                    dataField = "FILENAME";
                }

                sqlStr = "SELECT " + dataField + " FROM " + tableName + " WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, contentId);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString(1);
                }

                if ((result == null) || (result.equals(""))) {
                    result = String.valueOf(contentId);
                } else if (result.length() > 20) {
                    result = result.substring(0, 20);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public String add2PushInfo(String receiver, String sender, BigDecimal contentId, String dataType) {
        PreparedStatement pstmt = null;
        String sqlStr = "";
        long random = (long) (Math.random() * 10000000);
        String auth = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                if (checkContentMedia(dataType, contentId)) {
                    auth = contentId.toString() + random;
                    sqlStr = "INSERT INTO INFO_PUSH(ID,CONTENT_ID,SENDER,RECEIVER,AUTH,DATA_TYPE) VALUES(INFO_PUSH_ID_SEQ.NEXTVAL,?,?,?,?,?)";
                    pstmt = conn.prepareStatement(sqlStr);
                    pstmt.setBigDecimal(1, contentId);
                    pstmt.setString(2, sender);
                    pstmt.setString(3, receiver);
                    pstmt.setString(4, auth);
                    pstmt.setString(5, dataType);
                    pstmt.executeUpdate();
                    conn.commit();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return auth;
        }
    }

    public String getRingToneMono(BigDecimal id) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        String data = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT MONO_OTA FROM RINGTONE WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, id);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    Blob dataOta = rs.getBlob(1);
                    //byte[] dataByte = dataOta.getBytes(1,(int)dataOta.length());
                    data = new String(Utilities.readByteBlob(dataOta));
                //data = Utilities.getHexString(dataByte);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return data;
        }
    }

    public void cleanPush() {
        PreparedStatement pstmt = null;
        String sqlStr = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "DELETE FROM INFO_PUSH WHERE TRUNC((((86400*(SYSDATE-PUSH_TIME))/60)/60)/24) >= 5";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
        }
    }

    public boolean updateStatus(BigDecimal id, int status, String mobileOperator) {
        PreparedStatement pstmt = null;
        String sqlStr = "";
        boolean b = false;
        try {
            if ((conn = (Connection) DBPool.getConnectionRemote()) != null) {
                sqlStr = "UPDATE " + mobileOperator + ".SMS_RECEIVE_QUEUE SET STATUS = ? WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setInt(1, status);
                pstmt.setBigDecimal(2, id);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceRemote(pstmt, null, conn);
            return b;
        }
    }

    public boolean checkExistInMakeFriend(String foneNumber) {
        boolean b = false;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT ID FROM MAKE_FRIEND WHERE FONE_NUMBER = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, foneNumber);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return b;
        }
    }

    public boolean updateMakeFriend(MakeFriendItem makeFriendItem, boolean isUpdateRecommended) {
        boolean b = false;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                conn.setAutoCommit(false);
                if (!isUpdateRecommended) {
                    sqlStr = "UPDATE MAKE_FRIEND SET SEX =?, FONE_NUMBER =?, NAME = ?, YEAR_OF_BIRTH = ? WHERE ID = ?";
                    pstmt = conn.prepareStatement(sqlStr);
                    pstmt.setInt(1, makeFriendItem.getSex());
                    pstmt.setString(2, makeFriendItem.getFoneNumber());
                    pstmt.setString(3, makeFriendItem.getName());
                    pstmt.setInt(4, makeFriendItem.getYearOfBirth());
                    pstmt.setBigDecimal(5, makeFriendItem.getId());
                    if (pstmt.executeUpdate() != -1) {
                        b = true;
                        conn.commit();
                        conn.setAutoCommit(true);
                    }
                } else {
                    sqlStr = "UPDATE MAKE_FRIEND SET RECOMMENDED = RECOMMENDED || '" + makeFriendItem.getRecommended() + "' WHERE FONE_NUMBER = ?";
                    pstmt = conn.prepareStatement(sqlStr);
                    pstmt.setString(1, makeFriendItem.getFoneNumber());
                    if (pstmt.executeUpdate() != -1) {
                        b = true;
                        conn.commit();
                        conn.setAutoCommit(true);
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("Make friend with FoneNumber:" + makeFriendItem.getFoneNumber());
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public boolean addMakeFriend(MakeFriendItem makeFriendItem) {
        boolean b = false;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                conn.setAutoCommit(false);
                sqlStr = "INSERT INTO MAKE_FRIEND(ID,FONE_NUMBER,NAME,YEAR_OF_BIRTH,SEX) " +
                        "VALUES(MAKE_FRIEND_ID_SEQ.NEXTVAL,?,?,?,?)";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, makeFriendItem.getFoneNumber());
                pstmt.setString(2, makeFriendItem.getName());
                pstmt.setInt(3, makeFriendItem.getYearOfBirth());
                pstmt.setInt(4, makeFriendItem.getSex());
                if (pstmt.executeUpdate() != -1) {
                    b = true;
                    conn.commit();
                    conn.setAutoCommit(true);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public boolean removeFriend(String userId) {
        boolean b = false;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                conn.setAutoCommit(false);
                sqlStr = "DELETE FROM MAKE_FRIEND WHERE FONE_NUMBER = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, userId);
                if (pstmt.executeUpdate() != -1) {
                    b = true;
                    conn.commit();
                    conn.setAutoCommit(true);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public String getRecommentMakeFriend(String foneNumber, int birthYear, int sex) {
        String str = "";
        char enter = (char) 10;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                if (sex == 1) {
                    //Female
                    sqlStr = "SELECT NAME,FONE_NUMBER,YEAR_OF_BIRTH FROM(SELECT NAME,FONE_NUMBER,YEAR_OF_BIRTH FROM MAKE_FRIEND WHERE INSTR(UPPER(REPLACE(FONE_NUMBER,' ', '')),?) = 0 AND YEAR_OF_BIRTH <= ? AND SEX = 0 ORDER BY DBMS_RANDOM.VALUE) WHERE ROWNUM <= 5";
                    sqlStr = "SELECT NAME,FONE_NUMBER,YEAR_OF_BIRTH FROM(SELECT NAME,FONE_NUMBER,YEAR_OF_BIRTH FROM MAKE_FRIEND WHERE INSTR(UPPER(REPLACE(FONE_NUMBER,' ', '')),?) = 0 AND (? - YEAR_OF_BIRTH >= 0) AND (? - YEAR_OF_BIRTH <= 12) AND SEX = 0 ORDER BY DBMS_RANDOM.VALUE) WHERE ROWNUM <= 5";
                } else {
                    //Male
                    sqlStr = "SELECT NAME,FONE_NUMBER,YEAR_OF_BIRTH FROM(SELECT NAME,FONE_NUMBER,YEAR_OF_BIRTH FROM MAKE_FRIEND WHERE INSTR(UPPER(REPLACE(FONE_NUMBER,' ', '')),?) = 0 AND YEAR_OF_BIRTH >= ? AND SEX = 1 ORDER BY DBMS_RANDOM.VALUE) WHERE ROWNUM <= 5";
                    sqlStr = "SELECT NAME,FONE_NUMBER,YEAR_OF_BIRTH FROM(SELECT NAME,FONE_NUMBER,YEAR_OF_BIRTH FROM MAKE_FRIEND WHERE INSTR(UPPER(REPLACE(FONE_NUMBER,' ', '')),?) = 0 AND (YEAR_OF_BIRTH - ? >= 0) AND (YEAR_OF_BIRTH - ? <= 12) AND SEX = 1 ORDER BY DBMS_RANDOM.VALUE) WHERE ROWNUM <= 5";
                }

                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, foneNumber);
                pstmt.setInt(2, birthYear);
                pstmt.setInt(3, birthYear);
                rs = pstmt.executeQuery();
                int i = 0;
                int age = 0;
                String recommend = "";
                while (rs.next()) {
                    age = rs.getInt(3);
                    recommend = recommend + "," + rs.getString(2);
                    if (rs.getString(1) != null) {
                        if (i == 0) {
                            str = rs.getString(1) + ",sn " + age + ",sdt:" + Utilities.formatUserId(rs.getString(2), 1) + enter;
                        } else {
                            str = str + rs.getString(1) + ",sn" + age + ",sdt:" + Utilities.formatUserId(rs.getString(2), 1) + enter;
                        }
                    } else {
                        if (i == 0) {
                            str = "sn" + age + ",sdt:" + Utilities.formatUserId(rs.getString(2), 1) + enter;
                        } else {
                            str = str + "sn" + age + ",sdt:" + Utilities.formatUserId(rs.getString(2), 1) + enter;
                        }
                    }
                    i++;
                }

                try {
                    MakeFriendItem makeFriendItem = new MakeFriendItem();
                    makeFriendItem.setRecommended(recommend);
                    makeFriendItem.setFoneNumber(foneNumber);
                    updateMakeFriend(makeFriendItem, true);
                } catch (Exception ex) {
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return str;
        }
    }

    public String getRecommentMakeFriend(String foneNumber) {
        MakeFriendItem getMakeFriendItem = getMakeFriendItem(foneNumber);
        int sex = getMakeFriendItem.getSex();
        int birthYear = getMakeFriendItem.getYearOfBirth();
        String str = "";
        char enter = (char) 10;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                if (sex == 1) {
                    //Female
                    sqlStr = "SELECT NAME,FONE_NUMBER,YEAR_OF_BIRTH FROM(SELECT NAME,FONE_NUMBER,YEAR_OF_BIRTH FROM MAKE_FRIEND WHERE INSTR(UPPER(REPLACE(FONE_NUMBER,' ', '')),?) = 0 AND YEAR_OF_BIRTH <= ? AND SEX = 0 ORDER BY DBMS_RANDOM.VALUE) WHERE ROWNUM <= 5";
                } else {
                    //Male
                    sqlStr = "SELECT NAME,FONE_NUMBER,YEAR_OF_BIRTH FROM(SELECT NAME,FONE_NUMBER,YEAR_OF_BIRTH FROM MAKE_FRIEND WHERE INSTR(UPPER(REPLACE(FONE_NUMBER,' ', '')),?) = 0 AND YEAR_OF_BIRTH >= ? AND SEX = 1 ORDER BY DBMS_RANDOM.VALUE) WHERE ROWNUM <= 5";
                }

                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, foneNumber);
                pstmt.setInt(2, birthYear);
                rs = pstmt.executeQuery();
                int i = 0;
                int age = 0;
                String recommend = "";
                while (rs.next()) {
                    age = rs.getInt(3);
                    recommend = recommend + "," + rs.getString(2);
                    if (rs.getString(1) != null) {
                        if (i == 0) {
                            str = rs.getString(1) + ",sn " + age + ",sdt:" + rs.getString(2) + enter;
                        } else {
                            str = str + rs.getString(1) + ",sn" + age + ",sdt:" + rs.getString(2) + enter;
                        }
                    } else {
                        if (i == 0) {
                            str = "sn" + age + ",sdt:" + rs.getString(2) + enter;
                        } else {
                            str = str + "sn" + age + ",sdt:" + rs.getString(2) + enter;
                        }
                    }
                    i++;
                }

                try {
                    MakeFriendItem makeFriendItem = new MakeFriendItem();
                    makeFriendItem.setRecommended(recommend);
                    makeFriendItem.setFoneNumber(foneNumber);
                    updateMakeFriend(makeFriendItem, true);
                } catch (Exception ex) {
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return str;
        }
    }

    public Vector getCodeSchool(String code) {
        Vector v = new Vector();
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT MA_TRUONG FROM ";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return v;
        }
    }

    public MakeFriendItem getMakeFriendItem(String foneNumber) {
        MakeFriendItem makeFriendItem = new MakeFriendItem();
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT YEAR_OF_BIRTH,SEX FROM MAKE_FRIEND WHERE FONE_NUMBER = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, foneNumber);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    makeFriendItem.setFoneNumber(foneNumber);
                    makeFriendItem.setYearOfBirth(rs.getInt(1));
                    makeFriendItem.setSex(rs.getInt(2));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return makeFriendItem;
        }
    }

    public Vector getSchoolCode() {
        Vector v = new Vector();
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT CODE FROM SCHOOL_CODE";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    String code = rs.getString(1);
                    if ((code != null) && (!code.equals(""))) {
                        v.add(code);
                    }
                }
                StringTool.bubbleSort(v);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return v;
        }
    }

    public Vector getSchoolCodeHavePoint() {
        Vector v = new Vector();
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT DISTINCT(MA_TRUONG) FROM DIEMTHI";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    String code = rs.getString(1);
                    if ((code != null) && (!code.equals(""))) {
                        v.add(code);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return v;
        }
    }

    public String getSchoolName(String schoolCode) {
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT SCHOOL_NAME FROM SCHOOL_CODE WHERE CODE = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, schoolCode);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString(1);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public String getDiemThiEx(String sbd) {
        String result = "";
        String d1 = "";
        String d2 = "";
        String d3 = "";
        BigDecimal id = null;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT HO_TEN,NGAY_SINH,MA_TRUONG,KHOI,SOBD,D1,D2,D3,ID FROM DIEMTHI " +
                        "WHERE MA_TRUONG||KHOI||SOBD = ? OR MA_TRUONG||KHOI||'0'||SOBD = ? " +
                        "OR MA_TRUONG||KHOI||'00'||SOBD = ? OR MA_TRUONG||KHOI||'000'||SOBD = ? " +
                        "OR MA_TRUONG||SOBD = ? OR MA_TRUONG||'0'||SOBD = ? OR MA_TRUONG||'00'||SOBD = ? OR MA_TRUONG||'000'||SOBD = ?";

                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, sbd);
                pstmt.setString(2, sbd);
                pstmt.setString(3, sbd);
                pstmt.setString(4, sbd);
                pstmt.setString(5, sbd);
                pstmt.setString(6, sbd);
                pstmt.setString(7, sbd);
                pstmt.setString(8, sbd);

                rs = pstmt.executeQuery();
                if (rs.next()) {
                    String hoTen = rs.getString(1);
                    String ngaySinh = rs.getString(2);
                    String maTruong = rs.getString(3);
                    String khoi = rs.getString(4);
                    String soBD = rs.getString(5);
                    id = rs.getBigDecimal(6);
                    d1 = rs.getString(6);
                    if ((d1 != null) && (!d1.equals("")) && (d1.length() == 4)) {
                        if (d1.indexOf(".") == -1) {
                            d1 = d1.substring(0, 2) + "," + d1.substring(2);
                            if (d1.startsWith("0")) {
                                d1 = d1.substring(1);
                            }
                        }
                    } else if ((d1 != null) && (!d1.equals(""))) {
                        if (d1.indexOf(".") != -1) {
                            d1 = d1.replace('.', ',');
                        }
                    } else {
                        d1 = "0";
                    }

                    d2 = rs.getString(7);
                    if ((d2 != null) && (!d2.equals("")) && (d2.length() == 4)) {
                        if (d2.indexOf(".") == -1) {
                            d2 = d2.substring(0, 2) + "," + d2.substring(2);
                            if (d2.startsWith("0")) {
                                d2 = d2.substring(1);
                            }
                        }
                    } else if ((d2 != null) && (!d2.equals(""))) {
                        if (d2.indexOf(".") != -1) {
                            d2 = d2.replace('.', ',');
                        }
                    } else {
                        d2 = "0";
                    }
                    d3 = rs.getString(8);
                    if ((d3 != null) && (!d3.equals("")) && (d3.length() == 4)) {
                        if (d3.indexOf(".") == -1) {
                            d3 = d3.substring(0, 2) + "," + d3.substring(2);
                            if (d3.startsWith("0")) {
                                d3 = d3.substring(1);
                            }
                        }
                    } else if ((d3 != null) && (!d3.equals(""))) {
                        if (d3.indexOf(".") != -1) {
                            d3 = d3.replace('.', ',');
                        }
                    } else {
                        d3 = "0";
                    }
                    result = hoTen + "\n";
                    result = result + "Sinh ngay: " + ngaySinh + "\n";
                    result = result + "SoBD: " + maTruong + khoi + soBD + "\n";
                    result = result + "Diem 1: " + d1 + "\n";
                    result = result + "Diem 2: " + d2 + "\n";
                    result = result + "Diem 3: " + d3;
                }
            }
        } catch (Exception ex) {
            System.out.println("In getDiemThi::" + id);
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public String getDiemThi(String sbd) {
        String result = "";
        String d1 = "";
        String d2 = "";
        String d3 = "";
        BigDecimal id = null;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT HO_TEN,NGAY_SINH,MA_TRUONG,KHOI,SOBD,D1,D2,D3,ID FROM DIEMTHI " +
                        "WHERE MA_TRUONG||KHOI||SOBD = ? OR MA_TRUONG||KHOI||'0'||SOBD = ? " +
                        "OR MA_TRUONG||KHOI||'00'||SOBD = ? OR MA_TRUONG||KHOI||'000'||SOBD = ? " +
                        "OR MA_TRUONG||SOBD = ? OR MA_TRUONG||'0'||SOBD = ? OR MA_TRUONG||'00'||SOBD = ? OR MA_TRUONG||'000'||SOBD = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, sbd);
                pstmt.setString(2, sbd);
                pstmt.setString(3, sbd);
                pstmt.setString(4, sbd);
                pstmt.setString(5, sbd);
                pstmt.setString(6, sbd);
                pstmt.setString(7, sbd);
                pstmt.setString(8, sbd);

                rs = pstmt.executeQuery();
                if (rs.next()) {
                    String hoTen = rs.getString(1);
                    String ngaySinh = rs.getString(2);
                    String maTruong = rs.getString(3);
                    String khoi = rs.getString(4);
                    String soBD = rs.getString(5);
                    id = rs.getBigDecimal(6);
                    d1 = rs.getString(6);
                    if ((d1 != null) && (!d1.equals("")) && (d1.length() == 4)) {
                        if (d1.indexOf(".") == -1) {
                            d1 = d1.substring(0, 2) + "," + d1.substring(2);
                            if (d1.startsWith("0")) {
                                d1 = d1.substring(1);
                            }
                        }
                    } else if ((d1 != null) && (!d1.equals(""))) {
                        if (d1.indexOf(".") != -1) {
                            d1 = d1.replace('.', ',');
                        }
                    } else {
                        d1 = "0";
                    }

                    d2 = rs.getString(7);
                    if ((d2 != null) && (!d2.equals("")) && (d2.length() == 4)) {
                        if (d2.indexOf(".") == -1) {
                            d2 = d2.substring(0, 2) + "," + d2.substring(2);
                            if (d2.startsWith("0")) {
                                d2 = d2.substring(1);
                            }
                        }
                    } else if ((d2 != null) && (!d2.equals(""))) {
                        if (d2.indexOf(".") != -1) {
                            d2 = d2.replace('.', ',');
                        }
                    } else {
                        d2 = "0";
                    }
                    d3 = rs.getString(8);
                    if ((d3 != null) && (!d3.equals("")) && (d3.length() == 4)) {
                        if (d3.indexOf(".") == -1) {
                            d3 = d3.substring(0, 2) + "," + d3.substring(2);
                            if (d3.startsWith("0")) {
                                d3 = d3.substring(1);
                            }
                        }
                    } else if ((d3 != null) && (!d3.equals(""))) {
                        if (d3.indexOf(".") != -1) {
                            d3 = d3.replace('.', ',');
                        }
                    } else {
                        d3 = "0";
                    }
                    result = hoTen + "\n";
                    result = result + "Sinh ngay: " + ngaySinh + "\n";
                    result = result + "SoBD: " + maTruong + khoi + soBD + "\n";
                    result = result + "Diem 1: " + d1 + "\n";
                    result = result + "Diem 2: " + d2 + "\n";
                    result = result + "Diem 3: " + d3;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public boolean add2DiemThiWait(BigDecimal requestId, String userId, String serviceId, String mobileOperator, String commandCode, String info) {
        boolean b = false;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "INSERT INTO DIEMTHI_WAIT(ID,REQUEST_ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMMAND_CODE,INFO) " +
                        "VALUES(DIEMTHI_WAIT_ID_SEQ.NEXTVAL,?,?,?,?,?,?)";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, requestId);
                pstmt.setString(2, userId);
                pstmt.setString(3, serviceId);
                pstmt.setString(4, mobileOperator);
                pstmt.setString(5, commandCode);
                pstmt.setString(6, info);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public boolean add2DiemThiWait(BigDecimal requestId, String userId, String serviceId, String mobileOperator, String commandCode, String info,
            String schoolCode, int status, String data) {
        boolean b = false;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "INSERT INTO DIEMTHI_WAIT(ID,REQUEST_ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMMAND_CODE,INFO,SCHOOL_CODE,STATUS,DATA) " +
                        "VALUES(DIEMTHI_WAIT_ID_SEQ.NEXTVAL,?,?,?,?,?,?,?,?,?)";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, requestId);
                pstmt.setString(2, userId);
                pstmt.setString(3, serviceId);
                pstmt.setString(4, mobileOperator);
                pstmt.setString(5, commandCode);
                pstmt.setString(6, info);
                pstmt.setString(7, schoolCode);
                pstmt.setInt(8, status);
                pstmt.setString(9, data);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public Vector reProcessMark(String mobileOperator, Vector commandCode) {
        Vector v = new Vector();
        SmsReceiveQueue smsQueue = null;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                String condition = "";
                int size = commandCode.size();
                for (int i = 0; i < size; i++) {
                    if (condition.equals("")) {
                        condition = "COMMAND_CODE = '" + commandCode.elementAt(i) +
                                "'";
                    } else {
                        condition = condition + " OR COMMAND_CODE = '" +
                                commandCode.elementAt(i) + "'";
                    }
                }
                condition = "(" + condition + ")" + " AND SERVICE_ID <> '8084'  AND SERVICE_ID <> '8184'  AND SERVICE_ID <> '8284'";
                sqlStr =
                        "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,INFO,COMMAND_CODE,STATUS,TO_CHAR(RECEIVE_TIME,'DD-MM-YY') " +
                        "FROM " + mobileOperator +
                        ".SMS_RECEIVE_LOG PARTITION(SMS_RECEIVE_LOG_P200807) WHERE RECEIVE_TIME < TO_DATE('22/07/08','DD/MM/YY') AND " +
                        condition;
                //System.out.println(sqlStr);
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    smsQueue = new SmsReceiveQueue();
                    smsQueue.setId(rs.getBigDecimal(1));
                    smsQueue.setUserId(rs.getString(2));
                    smsQueue.setServiceId(rs.getString(3));
                    smsQueue.setMobileOperator(rs.getString(4));
                    smsQueue.setInfo(rs.getString(5));
                    smsQueue.setCommandCode(rs.getString(6));
                    v.add(smsQueue);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return v;
        }
    }

    public Vector getRequestFromDTWait() {
        Vector v = new Vector();
        DiemthiWaitItem diemthiItem = null;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr =
                        "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,INFO,COMMAND_CODE,REQUEST_ID " +
                        "FROM DIEMTHI_WAIT WHERE STATUS < 2";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    diemthiItem = new DiemthiWaitItem();
                    diemthiItem.setId(rs.getBigDecimal(1));
                    diemthiItem.setUserId(rs.getString(2));
                    diemthiItem.setServiceId(rs.getString(3));
                    diemthiItem.setMobileOperator(rs.getString(4));
                    diemthiItem.setInfo(rs.getString(5));
                    diemthiItem.setCommandCode(rs.getString(6));
                    diemthiItem.setRequestId(rs.getBigDecimal(7));
                    v.add(diemthiItem);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return v;
        }
    }

    public Vector getRequestFromDTWaitHasPoint() {
        Vector v = new Vector();
        DiemthiWaitItem diemthiItem = null;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr =
                        "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,INFO,COMMAND_CODE,REQUEST_ID " +
                        "FROM DIEMTHI_WAIT WHERE school_code in (select distinct(ma_truong) from diemthi ) and STATUS < 2";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    diemthiItem = new DiemthiWaitItem();
                    diemthiItem.setId(rs.getBigDecimal(1));
                    diemthiItem.setUserId(rs.getString(2));
                    diemthiItem.setServiceId(rs.getString(3));
                    diemthiItem.setMobileOperator(rs.getString(4));
                    diemthiItem.setInfo(rs.getString(5));
                    diemthiItem.setCommandCode(rs.getString(6));
                    diemthiItem.setRequestId(rs.getBigDecimal(7));
                    v.add(diemthiItem);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return v;
        }
    }

    public boolean schoolisHasPoint(String schoolCode) {
        boolean ret = false;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "select distinct(ma_truong) from diemthi where ma_truong=?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, schoolCode);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    ret = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
        }
        return ret;
    }

    public Vector getRequestFromDTWaitHasStandardMark() {
        Vector v = new Vector();
        DiemthiWaitItem diemthiItem = null;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr =
                        "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,INFO,COMMAND_CODE,REQUEST_ID " +
                        "FROM DIEMTHI_WAIT WHERE school_code in (SELECT CODE FROM SCHOOL_CODE WHERE STANDARD_MARK IS NOT NULL AND CODE IS NOT NULL) and STATUS < 4";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    diemthiItem = new DiemthiWaitItem();
                    diemthiItem.setId(rs.getBigDecimal(1));
                    diemthiItem.setUserId(rs.getString(2));
                    diemthiItem.setServiceId(rs.getString(3));
                    diemthiItem.setMobileOperator(rs.getString(4));
                    diemthiItem.setInfo(rs.getString(5));
                    diemthiItem.setCommandCode(rs.getString(6));
                    diemthiItem.setRequestId(rs.getBigDecimal(7));
                    v.add(diemthiItem);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return v;
        }
    }

    public boolean removeDiemThiWait(BigDecimal id) {
        boolean b = false;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "DELETE FROM DIEMTHI_WAIT WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, id);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public boolean updateDiemThiWait(BigDecimal id, String schoolCode, int status, String data) {
        boolean b = false;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "UPDATE DIEMTHI_WAIT SET SCHOOL_CODE =?,STATUS=?, DATA= ? WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, schoolCode);
                pstmt.setInt(2, status);
                pstmt.setString(3, data);
                pstmt.setBigDecimal(4, id);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public Hashtable getStandardMark() {
        Hashtable ht = new Hashtable();
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT CODE,STANDARD_MARK FROM SCHOOL_CODE WHERE STANDARD_MARK IS NOT NULL AND CODE IS NOT NULL";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    if (!rs.getString(2).trim().equals("")) {
                        ht.put(rs.getString(1), rs.getString(2));
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return ht;
        }
    }

    //----------------------
    public String getTXTData(String commandId) {
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT CONTENT FROM " +
                        "(SELECT CONTENT FROM TXT_DATA A " +
                        "WHERE COMMAND_ID=? AND NVL(ACTIVE,' ') ='Y' ORDER BY DBMS_RANDOM.VALUE) " +
                        "WHERE ROWNUM = 1";

                sqlStr = "SELECT CONTENT FROM " +
                        "(SELECT CONTENT FROM TXT_DATA A " +
                        "WHERE COMMAND_ID=? AND NVL(ACTIVE,' ') ='Y' AND PARAM1 IS NULL ORDER BY DBMS_RANDOM.VALUE) " +
                        "WHERE ROWNUM = 1";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, commandId);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString("CONTENT");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public String getTXTData_1(String commandId, String param1) {
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT CONTENT FROM " +
                        "(SELECT CONTENT FROM TXT_DATA A " +
                        "WHERE COMMAND_ID=? AND PARAM1=? AND NVL(ACTIVE,' ') ='Y' ORDER BY DBMS_RANDOM.VALUE) " +
                        "WHERE ROWNUM = 1";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, commandId);
                pstmt.setString(2, param1);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString("CONTENT");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public String getTXTData_2(String commandId, String param1, String comments) {
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT CONTENT FROM " +
                        "(SELECT CONTENT FROM TXT_DATA A " +
                        "WHERE COMMAND_ID=? AND PARAM1=? AND COMMENTS=? AND NVL(ACTIVE,' ') ='Y' ORDER BY DBMS_RANDOM.VALUE) " +
                        "WHERE ROWNUM = 1";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, commandId);
                pstmt.setString(2, param1);
                pstmt.setString(3, comments);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString("CONTENT");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public String getTXTData_3(String commandId, String comments) {
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT CONTENT FROM " +
                        "(SELECT CONTENT FROM TXT_DATA A " +
                        "WHERE COMMAND_ID=? AND COMMENTS=? AND NVL(ACTIVE,' ') ='Y' ORDER BY DBMS_RANDOM.VALUE) " +
                        "WHERE ROWNUM = 1";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, commandId);
                pstmt.setString(2, comments);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString("CONTENT");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public String getTXTData_4(String commandId, String param1, String param2, String param3) {
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT CONTENT FROM " +
                        "(SELECT CONTENT FROM TXT_DATA A " +
                        "WHERE COMMAND_ID=? AND PARAM1=? AND PARAM2=? AND PARAM3=? AND NVL(ACTIVE,' ') ='Y' ORDER BY DBMS_RANDOM.VALUE) " +
                        "WHERE ROWNUM = 1";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, commandId);
                pstmt.setString(2, param1);
                pstmt.setString(3, param2);
                pstmt.setString(4, param3);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString("CONTENT");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public String getTXTData_5(String commandId, String param1, String param2) {
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT CONTENT FROM " +
                        "(SELECT CONTENT FROM TXT_DATA A " +
                        "WHERE COMMAND_ID=? AND PARAM1=? AND PARAM2=? AND NVL(ACTIVE,' ') ='Y' ORDER BY DBMS_RANDOM.VALUE) " +
                        "WHERE ROWNUM = 1";
//                System.out.println(sqlStr);
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, commandId);
                pstmt.setString(2, param1);
                pstmt.setString(3, param2);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString("CONTENT");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public String getTXTData_6(String commandId, String comments) {
        //THOITIET
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT CONTENT FROM " +
                        "(SELECT CONTENT FROM TXT_DATA A " +
                        "WHERE COMMAND_ID=? AND INSTR(UPPER(REPLACE(COMMENTS,' ', '')),?) <> 0 AND NVL(ACTIVE,' ') ='Y' ORDER BY DBMS_RANDOM.VALUE) " +
                        "WHERE ROWNUM = 1";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, commandId);
                pstmt.setString(2, comments);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString("CONTENT");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public String getTXTData_7(String commandId, String param1) {
        //Dau gia
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT CONTENT FROM TXT_DATA " +
                        "WHERE COMMAND_ID=? AND NVL(ACTIVE,' ') ='Y' AND PARAM1 = ? AND (TO_DATE('" + DateProc.getCurrentDDMMYY() +
                        "','DD/MM/YY') BETWEEN TO_DATE(PARAM2,'DD/MM/YY') AND TO_DATE(PARAM3,'DD/MM/YY'))";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, commandId);
                pstmt.setString(2, param1);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString("CONTENT");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public String getLogoOTA(BigDecimal id) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        String data = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT LOGO_CONTENT_OTA FROM LOGO WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, id);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    Blob dataOta = rs.getBlob(1);
                    data = HexaTool.toHexString(Utilities.readByteBlob(dataOta));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return data;
        }
    }

    public boolean checkSpam(String mobileOperator, String userId, String serviceId) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        boolean isSpam = false;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT COUNT(*) FROM " + mobileOperator + ".SMS_RECEIVE_LOG PARTITION SMS_RECEIVE_LOG_P" + DateProc.getCurrentYYYY() +
                        DateProc.getCurrentMM() +
                        "WHERE USER_ID = ? AND SERVICE_ID = ? AND TO_CHAR(RECEIVE_TIME,'DD/MM/YY') = ? AND " +
                        "(TRUNC((86400*(SYSDATE-RECEIVE_TIME))/60)-60*(TRUNC(((86400*(SYSDATE-RECEIVE_TIME))/60)/60)) >= 5)";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, userId);
                pstmt.setString(2, serviceId);
                pstmt.setString(3, DateProc.getCurrentDDMMYY());
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    if (rs.getInt(1) > 3) {
                        isSpam = true;
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return isSpam;
        }
    }

    public BigDecimal getNextSeq(String seqName) {
        BigDecimal id = null;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECST " + seqName + ".NEXTVAL FROM DUAL";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    id = rs.getBigDecimal(1);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return id;
        }
    }

    public String getCurrentYYYYMMDDHH24MISS() {
        //"2008/11/07/14/10/00"
        String time = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT TO_CHAR(SYSDATE,'YYYY/MM/DD/HH24/MI/SS') FROM DUAL";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    time = rs.getString(1);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return time;
        }
    }

    public BigDecimal add2GPRSLog(String userId, String serviceId, String commandCode, String mobileOperator, BigDecimal requestId) {
        BigDecimal id = null;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                id = getNextSeq("GPRS_LOG_ID_SEQ");
                sqlStr = "INSERT INTO GPRS_LOG(ID,USER_ID,SERVICE_ID,COMMAND_CODE,MOBILE_OPERATOR,REQUEST_ID) " +
                        "VALUES(?,?,?,?,?,?)";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, id);
                pstmt.setString(2, userId);
                pstmt.setString(3, serviceId);
                pstmt.setString(4, commandCode);
                pstmt.setString(5, mobileOperator);
                pstmt.setBigDecimal(6, requestId);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                } else {
                    id = null;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return id;
        }
    }

    public boolean updateStatus2GPRSLog(BigDecimal id, int status, String responseIdStr) {
        boolean b = false;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "UPDATE GPRS_LOG SET STATUS = ?, RESPONSE_ID = ? WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setInt(1, status);
                pstmt.setString(2, responseIdStr);
                pstmt.setBigDecimal(3, id);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public boolean checkDateTime(String ddMM) {
        boolean result = false;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT TO_DATE('" + ddMM + "','DD/MM') FROM DUAL";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = true;
                }
            } else {
                result = true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public boolean addQueueWaiting(BigDecimal requestId, String userId, String receiver, String serviceId, String commandCode, Timestamp timeSend) {
        boolean b = false;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "INSERT INTO QUEUE_WAITING(ID,REQUEST_ID,USER_ID,SERVICE_ID,COMMAND_CODE,TIME_SEND,RECEIVER) " +
                        "VALUES(QUEUE_WAITING_ID_SEQ.NEXTVAL,?,?,?,?,?,?)";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, requestId);
                pstmt.setString(2, userId);
                pstmt.setString(3, serviceId);
                pstmt.setString(4, commandCode);
                pstmt.setTimestamp(5, timeSend);
                pstmt.setString(6, receiver);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public boolean removeQueueWaiting(BigDecimal id) {
        boolean b = false;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "DELETE FROM QUEUE_WAITING WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, id);
                pstmt.executeUpdate();
                b = true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public Vector getQueueWaiting(String timeSend) {
        Vector v = new Vector();
        String result = "";
        //result="NQ ID,USER_ID,RECEIVER,SERVICE_ID,REQUEST_ID"
        try {
//            String condition = "";
//            int size = commandCode.size();
//            for (int i = 0; i < size; i++) {
//                if (condition.equals("")) {
//                    condition = "COMMAND_CODE = '" + commandCode.elementAt(i) +
//                                "'";
//                } else {
//                    condition = condition + " OR COMMAND_CODE = '" +
//                                commandCode.elementAt(i) + "'";
//                }
//            }
//            condition = "(" + condition + ")" + " AND SERVICE_ID <> '8084'";
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT ID,USER_ID,RECEIVER,SERVICE_ID,REQUEST_ID,COMMAND_CODE FROM QUEUE_WAITING WHERE TO_CHAR(TIME_SEND,'DD/MM')=?";
                pstmt = conn.prepareStatement(sqlStr);
//                pstmt.setString(1,commandCode);
                pstmt.setString(1, timeSend);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    result = rs.getString(1) + "," + rs.getString(2) + "," + rs.getString(3) + "," + rs.getString(4) + "," + rs.getString(5) + "," +
                            rs.getString(6);
                    v.add(result);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return v;
        }
    }

    public String getCKName(String ckCode) {
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT * FROM CK_COMPANY WHERE CODE = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, ckCode);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString("CODE");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
        }
        return result;
    }

    public int getCKId(String ckCode) {
        int result = 0;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT * FROM CK_COMPANY WHERE CODE = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, ckCode);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getInt("ID");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
        }
        MyLogger.log(ckCode + " --> getCKId --> " + result);
        return result;
    }

    public int getCKId1(String ckCode) {
        int result = 0;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT * FROM CK_COMPANY WHERE CODE = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, ckCode);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getInt("ID");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        MyLogger.log(ckCode + " --> getCKId --> " + result);
        return result;
    }

    public String getCKByCode(String ckCode) {
        String result = "";
        String d = "", ret = "";
        int cpn = 0;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                cpn = getCKId1(ckCode);
                System.out.println("CK CPN_ID = " + cpn);
                sqlStr = "SELECT * FROM CK_RESULT WHERE COMPANY_ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setInt(1, cpn);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    d = rs.getString("RESULT_DATE");
                    ret = rs.getString("RESULT");
                    result = ckCode.toUpperCase() + " " + d + "\r\n" + ret;
                    System.out.println("ret = " + ret + " date = " + d);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
        }
        return result;
    }

    public String getCKByLastDay(String ckCode) {
        String result = "";
        String d = "", ret = "";
        int cpn = 0;
        Calendar cal = Calendar.getInstance();
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                cpn = getCKId1(ckCode);
                System.out.println("CKC CPN_ID = " + cpn);
                if (cpn > 0) {
                    sqlStr = "SELECT * FROM CK_RESULT WHERE LAST_UPDATE_TIME > ? and COMPANY_ID = ?";
                    pstmt = conn.prepareStatement(sqlStr);
                    pstmt.setDate(1, Common.getSqlDate(Common.addDaysToDate(cal.getTime(), -5)));
                    pstmt.setInt(2, cpn);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        d = rs.getString("RESULT_DATE");
                        ret = rs.getString("RESULT");
                        result += ckCode.toUpperCase() + " " + d + "\r\n" + ret + "!xxx!";
                        System.out.println("ret = " + ret + " date = " + d);
                    }
                    if (result.endsWith("!xxx!")) {
                        result = result.substring(0, result.length() - 5);
                    }
                } else {
                    result = "Hien tai he thong khong tim thay du lieu cho ma Chung Khoan " + ckCode + " ban yeu cau. DT ho tro: 1900561577.";
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
        }
        return result;
    }

    public String getCKByTG() {
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT * FROM CK_TOP WHERE CODE = 'TG'";
                pstmt = conn.prepareStatement(sqlStr);
//                pstmt.setInt(1, getCKId(ckCode));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString("RESULT_DATE") + "\r\n" + rs.getString("RESULT");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
        }
        return result;
    }

    public String getCKByGG() {
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT * FROM CK_TOP WHERE CODE = 'GG'";
                pstmt = conn.prepareStatement(sqlStr);
//                pstmt.setInt(1, getCKId(ckCode));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString("RESULT_DATE") + "\r\n" + rs.getString("RESULT");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
        }
        return result;
    }

    public String getCKByLocal(String local) {
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT * FROM CK_TOP WHERE CODE = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, local);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = local.toUpperCase() + " " + rs.getString("RESULT_DATE") + "\r\n" + rs.getString("RESULT");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
        }
        return result;
    }

    public boolean add2CKDaily(CkItem lotteryDaily) {
        boolean b = false;
        try {
//            removeLotteryDaily(lotteryDaily.getUserId(), lotteryDaily.getServiceId(), lotteryDaily.getCompanyId());
            conn = DBPool.getConnectionContent();
            if ((conn != null) && (!lotteryDaily.getUserId().equals("")) && (!lotteryDaily.getServiceId().equals("")) &&
                    (lotteryDaily.getCompanyId() != 0)) {
                conn.setAutoCommit(false);
                BigDecimal id = nextSequence("CK_DAILY_ID_SEQ");
                sqlStr =
                        "INSERT INTO CK_DAILY(ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMPANY_ID,REMAIN,REQUEST_ID,COMMAND_CODE,STATUS,INFO,RESPONSE_DATE) " +
                        "VALUES(?,?,?,?,?,?,?,?,?,?,?)";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, id);
                pstmt.setString(2, lotteryDaily.getUserId());
                pstmt.setString(3, lotteryDaily.getServiceId());
                pstmt.setString(4, lotteryDaily.getMobileOperator());
                pstmt.setInt(5, lotteryDaily.getCompanyId());
                pstmt.setInt(6, lotteryDaily.getRemain());
                pstmt.setBigDecimal(7, lotteryDaily.getRequestId());
                pstmt.setString(8, lotteryDaily.getCommandCode());
                pstmt.setInt(9, lotteryDaily.getStatus());
                pstmt.setString(10, lotteryDaily.getInfo());
                pstmt.setTimestamp(11, Utilities.createTimestampFullDDMMYY(lotteryDaily.getResponseDate()));

                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public Vector getRequestFromCKWait() {
        Vector v = new Vector();
        CkItem ckItem = null;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr =
                        "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,INFO,COMMAND_CODE,REQUEST_ID,REMAIN " +
                        "FROM CK_DAILY WHERE REMAIN > 0";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    ckItem = new CkItem();
                    ckItem.setId(rs.getBigDecimal(1));
                    ckItem.setUserId(rs.getString(2));
                    ckItem.setServiceId(rs.getString(3));
                    ckItem.setMobileOperator(rs.getString(4));
                    ckItem.setInfo(rs.getString(5));
                    ckItem.setCommandCode(rs.getString(6));
                    ckItem.setRequestId(rs.getBigDecimal(7));
                    ckItem.setRemain(rs.getInt(8));
                    v.add(ckItem);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return v;
        }
    }

    public boolean updateCKWait(BigDecimal id, int remain) {
        boolean b = false;
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "UPDATE CK_DAILY SET REMAIN =? WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setInt(1, remain);
                pstmt.setBigDecimal(2, id);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public boolean checkCKbyHour(String h) {
        boolean result = false;
        String d = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT RESULT_DATE FROM (SELECT * FROM CK_RESULT  ORDER BY ID DESC) WHERE ROWNUM <=1";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    d = rs.getString("RESULT_DATE");
                }
                if (d.indexOf(" ") > 0) {
                    d = d.substring(d.indexOf(" ")).trim();
                    if (d.indexOf(":") > 0) {
                        d = d.substring(0, d.indexOf(":")).trim();
                    }
                }

                if (d.equalsIgnoreCase(h)) {
                    result = true;
                }
            }
            System.out.println("Check ck by hour h = " + h + " --->d = " + d + " --->r= " + result);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
        }
        return result;
    }

    protected BigDecimal nextSequence(String name) {
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        Connection conn = null;
//        String sqlStr = null;
        BigDecimal nextId = null;
        try {
            conn = DBPool.getConnectionContent();
            if (conn != null) {
                sqlStr = "SELECT " + name + ".NEXTVAL FROM DUAL";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    nextId = rs.getBigDecimal(1);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return nextId;
        }
    }

    public String getSpam() {
        String result = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT CONTENT FROM (SELECT CONTENT FROM SPAMDATA WHERE ACTIVE='1' ORDER BY dbms_random.VALUE) WHERE ROWNUM < 2";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString("CONTENT");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
        }
//        System.out.println("spam:" + result);
        return result;
    }

    public static void main(String[] args) {
        String d1 = "2.25";
        System.out.println(d1.indexOf("."));
        if (d1.indexOf(".") == -1) {
            d1 = d1.substring(0, 2) + "," + d1.substring(2);
            if (d1.startsWith("0")) {
                d1 = d1.substring(1);
            }

        }

        System.out.println(d1);
    }
}
