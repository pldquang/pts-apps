package com.sms.process;

import java.util.Vector;
import com.sms.item.SmsReceiveQueue;
import java.math.BigDecimal;

import com.sms.common.*;
import java.util.StringTokenizer;

import com.sms.webservice.Submit2Visky;

public class ProcessSimpleService extends Thread {

    static String http = ":http://222.252.16.196/";
    DBTool dbTool = null;
    static String newLine = "\n";
    static char enter = (char) 10;
    static String gpcGPRS = "Thue bao tra sau,soan:SET GPRS gui den 333.\n" +
            "Tra truoc:Vao vinaphone.com.vn hoac goi so 18001091 de nho kich hoat,sau khi kich hoat soan SET GPRS gui 333";
    static String vmsGPRS1 = "B1:soan DK GPRS gui994" + enter +
            "B2:soan GPRS<TENHANG><KIEUMAY> gui994, VD: GPRS N7210 gui994(TEN HANG; Nokia N, Motorola M, Samsung SA, LG L, Simens S, Sony Ericsson E)";
    static String vmsGPRS2 = "B3: Tat may va khoi dong lai." + enter + "Soan HUY GPRS gui 994 neu muon huy dich vu GPRS";
    static String vietelGPRS1 = "Buoc1 soan GPRS1 gui 191(5000/thang).\nBuoc2:soan GPRS TenMay gui 191.Vidu:GPRS 7610 gui 191.";
    static String vietelGPRS2 =
            "Buoc3 nhap ma 1111 khi nhan tin tra ve yeu cau.(Ban phai luu lai cac tin nhan he thong tra ve)\nBuoc4:Tat may va khoi dong lai";
    static String gnVMS = "Hay soan DK gui 9232 de dang ky, Soan: MCA gui 9232 de tra cuu cac cuoc goi nho gan nhat\nDT ho tro:1900561577";
    static String gnGPC1 =
            "Buoc1:Bam *62*1800333#  roi bam OK.Buoc2:Soan ABS#ThongDiep gui 333, trong do ThongDiep la noi dung ban se gui toi nguoi goi khi ban bi tat may";
    static String gnGPC2 = "Thue bao TRA TRUOC: Vao trang www.vinaphone.com.vn, vao muc cai dat dich vu de kich hoat dich vu CF, vao trang ca nhan soan thao noi dung se gui toi nguoi goi.";
    static String gnVIETEL = "Hay soan MCA ON gui 193 de dang ky(5000d/thang).Soan MCA OFF gui 193 de huy goi nho.\nDT ho tro:1900561577";
    static String guideActGPRS_GPC =
            "Nhap ma pin 1111 neu duoc yeu cau va khoi dong lai may.Thue bao tra truoc truy cap website www.vinaphone.com.vn de kich hoat" + enter +
            "Ban duoc tang 5nhacchuong&5hinhanh";
    static String guideActGPRS_VMS = "Nhap ma pin 1111 neu duoc yeu cau va khoi dong lai may.Tiep theo soan tin DK GPRS gui 994 de kich hoat" + enter +
            "Ban duoc tang 5nhacchuong&5hinhanh";
    static String guideActGPRS_VIETEL =
            "Nhap ma pin 1111 neu duoc yeu cau va khoi dong lai may.Tiep theo soan: GPRS1 hoac GPRS2 gui 191 de dang ky goi cuoc" + enter +
            "Ban duoc tang 5nhacchuong&5hinhanh";
    String result = "";
    String subCode1 = "";
    String subCode2 = "";
    String subCode3 = "";
    String alert = "";
    SmsReceiveQueue smsReceiveQueue = null;
    BigDecimal requestId = null;
    String userId = "";
    String info = "";
    String serviceId = "";
    String commandCode = "";
    int status = 0;
    String infoTmp = "";
    Submit2Visky submit2Visky = null;

    public ProcessSimpleService() {
        dbTool = new DBTool();
        submit2Visky = new Submit2Visky();
    }

    public void processRequestQueue(String mobileOperator) {
        Vector listRequest = dbTool.getRequestFromQueue(mobileOperator, Preference.simpleCode);
        try {
            for (int i = 0; i < listRequest.size(); i++) {
                smsReceiveQueue = (SmsReceiveQueue) listRequest.elementAt(i);
                requestId = smsReceiveQueue.getId();
                userId = smsReceiveQueue.getUserId();
                info = smsReceiveQueue.getInfo().toUpperCase();
                serviceId = smsReceiveQueue.getServiceId();
                commandCode = smsReceiveQueue.getCommandCode();
                status = smsReceiveQueue.getStatus();
                if (commandCode.equals("HELP")) {
                    infoTmp = "HELP" + info.substring(commandCode.length());
                    infoTmp = StringTool.getContentNoWhiteLetter(infoTmp).trim().toUpperCase();
                    String dataHelp = "";

                    if (!infoTmp.equals("")) {
                        dataHelp = dbTool.getTXTData_1("HELP", infoTmp);
                    } else {
                        dataHelp = dbTool.getTXTData("HELP");
                    }

                    boolean updateStatus = false;
                    if (dataHelp.equals("") || (status != 0)) {
                        dataHelp = dbTool.getTXTData("HELP");
                    } else {
                        dbTool.updateStatus(requestId, 1, mobileOperator);
                        updateStatus = true;
                    }

                    if (!dataHelp.equals("")) {
                        boolean deductMoney = false;
                        dataHelp = dataHelp.replaceAll("!xxx!", "|");
                        StringTokenizer stToken = new StringTokenizer(dataHelp, "|", false);
                        int stTokenLen = stToken.countTokens();
                        for (int ii = 0; ii < stTokenLen; ii++) {
                            String txtSms = stToken.nextToken();
                            if (!deductMoney) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                        txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                deductMoney = true;
                                MyLogger.log(userId + "<==HELP<==" + serviceId);
                            } else {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                        txtSms, 0, requestId, subCode1, subCode2, subCode3);
                            }
                            if (ii > 5) {
                                break;
                            }
                        }
                    } else if (!updateStatus) {
                        alert = "Xin loi ban,hien nay he thong chua co thong tin help.Xin goi 1900561577 de dc tro giup.";
                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                alert, 1, requestId, subCode1, subCode2, subCode3);
                    }
                } else if (commandCode.equals("GPRS") || commandCode.equals("GPRSR") || commandCode.equals("GPRST") || commandCode.equals("GPRSV") ||
                        commandCode.equals("GPRSA") || commandCode.equals("GN") || commandCode.equals("NHO") || commandCode.equals("GNA") || commandCode.equals("QPRS") || commandCode.equals("HPRS")) {
                    if (mobileOperator.equals("GPC")) {
                        if (commandCode.equals("GPRS") || commandCode.equals("GPRSR") || commandCode.equals("GPRST") || commandCode.equals("GPRSV") ||
                                commandCode.equals("GPRSA") || commandCode.equals("QPRS") || commandCode.equals("HPRS")) {
                            if (commandCode.equals("GPRSA") || commandCode.equals("QPRS") || commandCode.equals("HPRS")) {
                                if (serviceId.endsWith("8684") || serviceId.endsWith("8784") || (Preference.adminPhoneNumber.contains(userId))) {
                                    if (dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, guideActGPRS_GPC, 1, requestId, subCode1, subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
//                                    //#1
//                                    String txt = "0B05040B8423F0000304040101062F1F2DB69181923937464637323037344142344339303031383744313131443041444235373034363135364446443000030B6A0045C65601870706035669736B7956696E610001871506035669736B7956696E615F50726F7879000101C65501870706035669736B7956696E610001871106035669736B7956696E615F4E";
//                                    dbTool.sendMTEx(userId,serviceId,mobileOperator,commandCode,5,txt,0,requestId);
//                                    //#2
//                                    txt = "0B05040B8423F00003040402415049440001871006AB01870806036D332D776F726C6400018709068901C65A01870C069A01870D06036D6D730001870E06036D6D7300010101C65101870706035669736B7956696E610001871506035669736B7956696E615F50726F78790001871C0603687474703A2F2F7761702E76696D6F62692E766E0001C659018719";
//                                    dbTool.sendMTEx(userId,serviceId,mobileOperator,commandCode,5,txt,0,requestId);
//                                    //#3
//                                    txt = "0B05040B8423F00003040403069C01871A06036D6D730001871B06036D6D73000101C65201872F06035669736B7956696E615F506850726F787900018720060331302E312E31302E343600018721068501872206035669736B7956696E615F4E415049440001C6530187230603383030300001872406CB01010101C600015501873600000603773200018707";
//                                    dbTool.sendMTEx(userId,serviceId,mobileOperator,commandCode,5,txt,0,requestId);
//                                    //#4
//                                    txt = "0B05040B8423F0000304040406035669736B7956696E61000187000139000006035669736B7956696E615F50726F78790001C6000159018707000006035669736B7956696E6100018700013A00000603687474703A2F2F7761702E76696D6F62692E766E0001871C01010101";
//                                    dbTool.sendMTEx(userId,serviceId,mobileOperator,commandCode,5,txt,0,requestId);

                                        BigDecimal gprsLogId = dbTool.add2GPRSLog(userId, serviceId, commandCode, mobileOperator, requestId);
                                        if (gprsLogId != null) {
                                            String gprsLogIdStr = String.valueOf(gprsLogId);
                                            String responseIdStr = submit2Visky.send2Visky(userId, serviceId, commandCode, DateProc.getCurrentYYYYMMDDHH24MISS(),
                                                    gprsLogIdStr);
                                            if (!responseIdStr.equals("") && !responseIdStr.startsWith("0") && !responseIdStr.startsWith("No")) {
                                                dbTool.updateStatus2GPRSLog(gprsLogId, 1, responseIdStr); //Update status is had sent

                                            }

                                            //Promotion
                                            String result = dbTool.getTXTData(commandCode);
                                            if ((result != null) && (!result.equals(""))) {
                                                StringTokenizer tk = new StringTokenizer(result, ",", false);
                                                String prefixCode = "";
                                                String dataType = "";
                                                String auth = "";
                                                String dataStr = "";
                                                String dataName = "";
                                                while (tk.hasMoreTokens()) {
                                                    String idFull = tk.nextToken();
                                                    prefixCode = idFull.substring(0, 3);
                                                    dataType = idFull.substring(1, 3);
                                                    String code = idFull.substring(3);

                                                    if (prefixCode.startsWith("812") || prefixCode.startsWith("813") || prefixCode.startsWith("814")) {
                                                        //Poly-TrueTone-Word
                                                        auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                                        dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                                                        dataStr = "[PTS] " + idFull + http + "push/" + dataName + "?auth=" + auth;
                                                        if (!auth.equals("")) {
                                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 0, requestId, subCode1, subCode2, subCode3);
                                                        }
                                                    } else if (prefixCode.startsWith("824")) {
                                                        //Photo
                                                        auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                                        dataStr = "[PTS] " + idFull + http + "push/" + idFull + ".gif?auth=" + auth;
                                                        if (!auth.equals("")) {
                                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 0, requestId, subCode1, subCode2, subCode3);
                                                        }
                                                    }

                                                }
                                            } //End promotion

                                        }
                                    }
                                } else {
                                    alert = "Ban gui sai so dich vu,hay soan GPRSA gui8784 de duoc cai dat GPRS tu dong" + enter + "DT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==Wrong ServiceId:" + serviceId);
                                }
                            } else if (!serviceId.endsWith("8184") || (Preference.adminPhoneNumber.contains(userId))) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, gpcGPRS, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                            } else {
                                //alert = "Ban gui sai so dich vu,hay soan " + info + " gui 8384 de duoc huong dan cai dat GPRS\nDT ho tro 1900561577";
                                alert = "Ban gui sai so dich vu,hay soan GPRS gui 8384 de duoc huong dan cai dat GPRS" + enter + "Soan: GPRSA gui 8784 de duoc cai dat GPRS tu dong.DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==Wrong ServiceId:" + serviceId);
                            }
                        } else if (commandCode.equals("GN") || commandCode.equals("NHO") || commandCode.equals("GNA")) {
                            if (!serviceId.endsWith("8184") || (Preference.adminPhoneNumber.contains(userId))) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, gnGPC1, 1, requestId, subCode1, subCode2, subCode3);
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, gnGPC2, 0, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                            } else {
                                alert = "Ban gui sai so dich vu,hay soan " + info + " gui 8384 de duoc huong dan cai dat goi nho\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==Wrong ServiceId:" + serviceId);
                            }
                        }
                    } else if (mobileOperator.equals("VMS")) {
                        if (commandCode.equals("GPRS") || commandCode.equals("GPRSR") || commandCode.equals("GPRST") || commandCode.equals("GPRSV") ||
                                commandCode.equals("GPRSA") || commandCode.equals("QPRS") || commandCode.equals("HPRS")) {
                            if (commandCode.equals("GPRSA") || commandCode.equals("QPRS") || commandCode.equals("HPRS")) {
                                if (serviceId.endsWith("8684") || serviceId.endsWith("8784") || (Preference.adminPhoneNumber.contains(userId))) {
                                    if (dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, guideActGPRS_VMS, 1, requestId, subCode1, subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
//                                    //#1
//                                    String txt = "0B05040B8423F0000304040101062F1F2DB69181923941324233414646464445344130324539363243304232354339393038354436433837324342424500030B6A0045C65601870706035669736B794D6F62690001871506035669736B794D6F62695F50726F7879000101C65501870706035669736B794D6F62690001871106035669736B794D6F62695F4E";
//                                    dbTool.sendMTEx(userId,serviceId,mobileOperator,commandCode,5,txt,0,requestId);
//                                    //#2
//                                    txt = "0B05040B8423F00003040402415049440001871006AB01870806036D2D77617000018709068901C65A01870C069A01870D06036D6D730001870E06036D6D7300010101C65101870706035669736B794D6F62690001871506035669736B794D6F62695F50726F78790001871C0603687474703A2F2F7761702E76696D6F62692E766E0001C659018719069C01";
//                                    dbTool.sendMTEx(userId,serviceId,mobileOperator,commandCode,5,txt,0,requestId);
//                                    //#3
//                                    txt = "0B05040B8423F00003040403871A06036D6D730001871B06036D6D73000101C65201872F06035669736B794D6F62695F506850726F78790001872006033230332E3136322E3032312E31303700018721068501872206035669736B794D6F62695F4E415049440001C6530187230603393230310001872406CB01010101C60001550187360000060377320001";
//                                    dbTool.sendMTEx(userId,serviceId,mobileOperator,commandCode,5,txt,0,requestId);
//                                    //#4
//                                    txt = "0B05040B8423F00003040404870706035669736B794D6F6269000187000139000006035669736B794D6F62695F50726F78790001C6000159018707000006035669736B794D6F626900018700013A00000603687474703A2F2F7761702E76696D6F62692E766E0001871C01010101";
//                                    dbTool.sendMTEx(userId,serviceId,mobileOperator,commandCode,5,txt,0,requestId);

                                        BigDecimal gprsLogId = dbTool.add2GPRSLog(userId, serviceId, commandCode, mobileOperator, requestId);
                                        if (gprsLogId != null) {
                                            String gprsLogIdStr = String.valueOf(gprsLogId);
                                            String responseIdStr = submit2Visky.send2Visky(userId, serviceId, commandCode, DateProc.getCurrentYYYYMMDDHH24MISS(),
                                                    gprsLogIdStr);
                                            if (!responseIdStr.equals("") && !responseIdStr.startsWith("0") && !responseIdStr.startsWith("No")) {
                                                dbTool.updateStatus2GPRSLog(gprsLogId, 1, responseIdStr); //Update status is had sent

                                            }

                                            //Promotion
                                            String result = dbTool.getTXTData(commandCode);
                                            if ((result != null) && (!result.equals(""))) {
                                                StringTokenizer tk = new StringTokenizer(result, ",", false);
                                                String prefixCode = "";
                                                String dataType = "";
                                                String auth = "";
                                                String dataStr = "";
                                                String dataName = "";
                                                while (tk.hasMoreTokens()) {
                                                    String idFull = tk.nextToken();
                                                    prefixCode = idFull.substring(0, 3);
                                                    dataType = idFull.substring(1, 3);
                                                    String code = idFull.substring(3);

                                                    if (prefixCode.startsWith("812") || prefixCode.startsWith("813") || prefixCode.startsWith("814")) {
                                                        //Poly-TrueTone-Word
                                                        auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                                        dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                                                        dataStr = "[PTS] " + idFull + http + "push/" + dataName + "?auth=" + auth;
                                                        if (!auth.equals("")) {
                                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 0, requestId, subCode1, subCode2, subCode3);
                                                        }
                                                    } else if (prefixCode.startsWith("824")) {
                                                        //Photo
                                                        auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                                        dataStr = "[PTS] " + idFull + http + "push/" + idFull + ".gif?auth=" + auth;
                                                        if (!auth.equals("")) {
                                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 0, requestId, subCode1, subCode2, subCode3);
                                                        }
                                                    }

                                                }
                                            } //End promotion

                                        }
                                    }
                                } else {
                                    alert = "Ban gui sai so dich vu,hay soan GPRSA gui8784 de duoc cai dat GPRS tu dong" + enter +
                                            "DT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==Wrong ServiceId:" + serviceId);
                                }
                            } else if (!serviceId.endsWith("8184") || (Preference.adminPhoneNumber.contains(userId))) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, vmsGPRS1, 1, requestId, subCode1, subCode2, subCode3);
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, vmsGPRS2, 0, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                            } else {
                                alert = "Ban gui sai so dich vu,hay soan GPRS gui 8384 de duoc huong dan cai dat GPRS" + enter + "Soan: GPRSA gui 8784 de duoc cai dat GPRS tu dong.DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==Wrong ServiceId:" + serviceId);
                            }
                        } else if (commandCode.equals("GN") || commandCode.equals("NHO") || commandCode.equals("GNA")) {
                            if (!serviceId.endsWith("8184") || (Preference.adminPhoneNumber.contains(userId))) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, gnVMS, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                            } else {
                                alert = "Ban gui sai so dich vu,hay soan " + info + " gui 8384 de duoc huong dan cai dat goi nho\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==Wrong ServiceId:" + serviceId);
                            }
                        }
                    } else if (mobileOperator.equals("VIETEL")) {
                        if (commandCode.equals("GPRS") || commandCode.equals("GPRSR") || commandCode.equals("GPRST") || commandCode.equals("GPRSV") ||
                                commandCode.equals("GPRSA") || commandCode.equals("QPRS") || commandCode.equals("HPRS")) {
                            if (commandCode.equals("GPRSA") || commandCode.equals("QPRS") || commandCode.equals("HPRS")) {
                                if (serviceId.endsWith("8684") || serviceId.endsWith("8784") || (Preference.adminPhoneNumber.contains(userId))) {
                                    if (dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, guideActGPRS_VIETEL, 1, requestId, subCode1, subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
//                                    //#1
//                                    String txt = "0B05040B8423F0000304040101062F1F2DB69181923343303132413144334538343231434143464330323642303134393330323646434536443837333600030B6A0045C65601870706035669736B7956540001871506035669736B7956545F50726F7879000101C65501870706035669736B7956540001871106035669736B7956545F4E4150494400018710";
//                                    dbTool.sendMTEx(userId,serviceId,mobileOperator,commandCode,5,txt,0,requestId);
//                                    //#2
//                                    txt = "0B05040B8423F0000304040206AB0187080603762D7761700001870906890101C65101870706035669736B7956540001871506035669736B7956545F50726F78790001871C0603687474703A2F2F7761702E76696D6F62692E766E0001C65201872F06035669736B7956545F506850726F78790001872006033139322E3136382E3233332E31300001872106";
//                                    dbTool.sendMTEx(userId,serviceId,mobileOperator,commandCode,5,txt,0,requestId);
//                                    //#3
//                                    txt = "0B05040B8423F000030404038501872206035669736B7956545F4E415049440001C6530187230603383038300001872406CB01010101C60001550187360000060377320001870706035669736B795654000187000139000006035669736B7956545F50726F78790001C6000159018707000006035669736B79565400018700013A00000603687474703A2F2F";
//                                    dbTool.sendMTEx(userId,serviceId,mobileOperator,commandCode,5,txt,0,requestId);
//                                    //#4
//                                    txt = "0B05040B8423F000030404047761702E76696D6F62692E766E0001871C01010101";
//                                    dbTool.sendMTEx(userId,serviceId,mobileOperator,commandCode,5,txt,0,requestId);

                                        BigDecimal gprsLogId = dbTool.add2GPRSLog(userId, serviceId, commandCode, mobileOperator, requestId);
                                        if (gprsLogId != null) {
                                            String gprsLogIdStr = String.valueOf(gprsLogId);
                                            String responseIdStr = submit2Visky.send2Visky(userId, serviceId, commandCode, DateProc.getCurrentYYYYMMDDHH24MISS(),
                                                    gprsLogIdStr);
                                            if (!responseIdStr.equals("") && !responseIdStr.startsWith("0") && !responseIdStr.startsWith("No")) {
                                                dbTool.updateStatus2GPRSLog(gprsLogId, 1, responseIdStr); //Update status is had sent

                                            }

                                            //Promotion
                                            String result = dbTool.getTXTData(commandCode);
                                            if ((result != null) && (!result.equals(""))) {
                                                StringTokenizer tk = new StringTokenizer(result, ",", false);
                                                String prefixCode = "";
                                                String dataType = "";
                                                String auth = "";
                                                String dataStr = "";
                                                String dataName = "";
                                                while (tk.hasMoreTokens()) {
                                                    String idFull = tk.nextToken();
                                                    prefixCode = idFull.substring(0, 3);
                                                    dataType = idFull.substring(1, 3);
                                                    String code = idFull.substring(3);

                                                    if (prefixCode.startsWith("812") || prefixCode.startsWith("813") || prefixCode.startsWith("814")) {
                                                        //Poly-TrueTone-Word
                                                        auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                                        dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                                                        dataStr = "[PTS] " + idFull + http + "push/" + dataName + "?auth=" + auth;
                                                        if (!auth.equals("")) {
                                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 0, requestId, subCode1, subCode2, subCode3);
                                                        }
                                                    } else if (prefixCode.startsWith("824")) {
                                                        //Photo
                                                        auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                                        dataStr = "[PTS] " + idFull + http + "push/" + idFull + ".gif?auth=" + auth;
                                                        if (!auth.equals("")) {
                                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 0, requestId, subCode1, subCode2, subCode3);
                                                        }
                                                    }
                                                }
                                            } //End promotion

                                        }
                                    }
                                } else {
                                    alert = "Ban gui sai so dich vu,hay soan GPRSA gui8784 de duoc cai dat GPRS tu dong" + enter + "DT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==Wrong ServiceId:" + serviceId);
                                }
                            } else if (!serviceId.endsWith("8184") || (Preference.adminPhoneNumber.contains(userId))) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, vietelGPRS1, 1, requestId, subCode1, subCode2, subCode3);
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, vietelGPRS2, 0, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                            } else {
                                alert = "Ban gui sai so dich vu,hay soan GPRS gui 8384 de duoc huong dan cai dat GPRS" + enter + "Soan: GPRSA gui 8784 de duoc cai dat GPRS tu dong.DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==Wrong ServiceId:" + serviceId);
                            }
                        } else if (commandCode.equals("GN") || commandCode.equals("NHO") || commandCode.equals("GNA")) {
                            if (!serviceId.endsWith("8184") || (Preference.adminPhoneNumber.contains(userId))) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, gnVIETEL, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                            } else {
                                alert = "Ban gui sai so dich vu,hay soan " + info + " gui 8384 de duoc huong dan cai dat goi nho\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==Wrong ServiceId:" + serviceId);
                            }
                        }
                    }
                } else if (commandCode.equals("UH")) {
                    if (!dbTool.checkSpam(mobileOperator, userId, serviceId)) {
                        String money = "";
                        if (serviceId.equals("8284")) {
                            money = "2000";
                        } else if (serviceId.equals("8384")) {
                            money = "3000";
                        } else if (serviceId.equals("8484")) {
                            money = "4000";
                        } else if (serviceId.equals("8584")) {
                            money = "5000";
                        } else if (serviceId.equals("8684")) {
                            money = "10000";
                        } else if (serviceId.equals("8784")) {
                            money = "15000";
                        }
                        alert = "Cam on ban da ung ho Thuong benh binh,nan nhan chat doc mau da cam..." + money + " VND." + enter + "DT ho tro 1900561577";
                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                    } else {
                        alert = "De ung ho Thuong benh binh,nan nhan chat doc mau da cam...moi soan khong qua 3tin trong 5phut" + enter +
                                "DT ho tro 1900561577";
                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                        MyLogger.log(userId + "<==" + commandCode + "<==SPAM:" + serviceId);
                    }
                } else if (commandCode.equalsIgnoreCase("Q2")) {
                    infoTmp = "Q2" + info.substring(commandCode.length());
                    infoTmp = StringTool.getContentNoWhiteLetter(infoTmp).trim().toUpperCase();
                    String dataHelp = "";

                    if (!infoTmp.equals("")) {
                        dataHelp = dbTool.getTXTData_1("Q2", infoTmp);
                    } else {
                        dataHelp = dbTool.getTXTData("Q2");
                    }

                    boolean updateStatus = false;
                    if (dataHelp.equals("") || (status != 0)) {
                        dataHelp = dbTool.getTXTData("Q2");
                    } else {
                        dbTool.updateStatus(requestId, 1, mobileOperator);
                        updateStatus = true;
                    }

                    if (!dataHelp.equals("")) {
                        boolean deductMoney = false;
                        dataHelp = dataHelp.replaceAll("!xxx!", "|");
                        StringTokenizer stToken = new StringTokenizer(dataHelp, "|", false);
                        int stTokenLen = stToken.countTokens();
                        for (int ii = 0; ii < stTokenLen; ii++) {
                            String txtSms = stToken.nextToken();
                            if (!deductMoney) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                        txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                deductMoney = true;
                                MyLogger.log(userId + "<==Q2<==" + serviceId);
                            } else {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                        txtSms, 0, requestId, subCode1, subCode2, subCode3);
                            }
                            if (ii > 5) {
                                break;
                            }
                        }
                    } else if (!updateStatus) {
                        alert = "Xin loi ban,hien nay he thong chua co thong tin Q2.Xin goi 1900561577 de dc tro giup.";
                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                alert, 1, requestId, subCode1, subCode2, subCode3);
                    }
                }
            } //End for

        } catch (Exception ex) {
        }
    }

    public void run() {
        MyLogger.log("Simple Services process Thread Start.");
        while (Main.running) {
            try {
                processRequestQueue("VIETEL");
                processRequestQueue("GPC");
                processRequestQueue("VMS");
                Thread.sleep(5000);
            } catch (Exception ex) {
            }
        }
    }

    public static void main(String[] args) {
//        MyLogger.log("Buoc1:Bam *62*1800333#  roi bam OK.Buoc2:Soan ABS#ThongDiep gui 333, trong do ThongDiep la noi dung ban se gui toi nguoi goi khi ban bi tat may!xxx!Thue bao TRA TRUOC: Vao trang www.vinaphone.com.vn, vao muc cai dat dich vu de kich hoat dich vu CF, vao trang ca nhan soan thao noi dung se gui toi nguoi goi".
//                           length());
//        MyLogger.log(":" + vietelGPRS2.length());
    }
}
