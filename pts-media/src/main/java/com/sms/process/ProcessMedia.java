package com.sms.process;

import java.util.*;
import java.math.BigDecimal;

import com.sms.item.SmsReceiveQueue;
import com.sms.common.*;
import java.security.SecureRandom;

public class ProcessMedia extends Thread {

    DBTool dbTool = null;
    String alert = "";
    String dataStr = "";
    String auth = "";
    String dataType = "";
    String dataName = "";
    String dataStrSend = "";
    String mobileOperatorFriend = "";
    String prefixCode = "";
    static String http = ":http://222.252.16.196/";
    static String gnVMS = "Hay soan DK gui 9232 de dang ky, Soan: MCA gui 9232 de tra cuu cac cuoc goi nho gan nhat\nDT ho tro:1900561577";
    static String gnGPC1 =
            "Buoc1:Bam *62*1800333#  roi bam OK.Buoc2:Soan ABS#ThongDiep gui 333, trong do ThongDiep la noi dung ban se gui toi nguoi goi khi ban bi tat may";
    static String gnGPC2 = "Thue bao TRA TRUOC: Vao trang www.vinaphone.com.vn, vao muc cai dat dich vu de kich hoat dich vu CF, vao trang ca nhan soan thao noi dung se gui toi nguoi goi\nDT ho tro:1900561577";
    static String gnVIETEL = "Hay soan MCA ON gui 193 de dang ky(5000d/thang).Soan MCA OFF gui 193 de huy goi nho.\nDT ho tro:1900561577";
    String commandId = "";
    String param1 = "";
    String param2 = "";
    String comment = "";
    String spam = "";
    static char enter = (char) 10;

    public ProcessMedia() {
        dbTool = new DBTool();
    }

    public void processRequestQueue(String mobileOperator) {
        /*
        1 - Hinhdong
        2 - Hinhnenmau
        3 - Logo Den trang
        4 - Am thanh thuc
        5 - Daam
        6 - Don am
        7 - Theme
        8 - Clip
        9 - javaGame
         */

        Vector listRequest = dbTool.getRequestFromQueue(mobileOperator, Preference.mediaCode);
        try {
            for (int i = 0; i < listRequest.size(); i++) {
                SmsReceiveQueue smsReceiveQueue = (SmsReceiveQueue) listRequest.elementAt(i);
                BigDecimal requestId = smsReceiveQueue.getId();
                String userId = smsReceiveQueue.getUserId();
                String serviceId = smsReceiveQueue.getServiceId();
                int status = smsReceiveQueue.getStatus();
                String commandCode = smsReceiveQueue.getCommandCode();
                String info = (smsReceiveQueue.getInfo()).toUpperCase().trim();

                String infoTmp = "";
                try {
                    infoTmp = commandCode + " " + info.substring(commandCode.length(), info.length()).trim();
                } catch (Exception ex) {
                }

                String subCode1 = "";
                String subCode2 = "";
                String subCode3 = "";

                subCode1 = StringTool.getObjectString(infoTmp, 1);
                subCode2 = StringTool.getObjectString(infoTmp, 2);
                subCode3 = StringTool.getObjectString(infoTmp, 3);
//                System.out.println("infoTmp = " + infoTmp);
                MyLogger.log("infoTmp = " + infoTmp + "@" + commandCode);
                try {
                    if (commandCode.equals("DA") && (!info.startsWith("DA"))) {
                        MyLogger.log("commandCode is DA");
                        if (StringTool.isNumberic(info.substring(0, 7))) { //Data default

                            String dataIdStr = info.substring(0, 7);
                            dataStr = dbTool.getDataMedia(dataIdStr);
                            //Check send content for friend
                            String phoneNumberFriend = info.substring(dataIdStr.length(), info.length());
                            String mOperator = "";
                            if (!phoneNumberFriend.equals("")) {
                                phoneNumberFriend = phoneNumberFriend.replaceAll("O", "0");
                                phoneNumberFriend = Utilities.rebuildUserId(StringTool.getContentOnlyNumber(
                                        phoneNumberFriend));
                                mOperator = Utilities.buildMobileOperator(phoneNumberFriend);
                            }
                            MyLogger.log("dataStr = " + dataIdStr);
                            if (!dataStr.equals("")) {
                                subCode1 = dataStr;
                                if (mOperator.equals("") || (userId.equals(phoneNumberFriend))) { //Get yourself content

                                    if (dataIdStr.startsWith("1") || dataIdStr.startsWith("2") || dataIdStr.startsWith("3") ||
                                            dataIdStr.startsWith("4") || dataIdStr.startsWith("5") || dataIdStr.startsWith("6") ||
                                            dataIdStr.startsWith("7")) { //Not javaGame

                                        if ((serviceId.endsWith("8184") || serviceId.endsWith("8284")) && (!Preference.adminPhoneNumber.contains(userId))) {
                                            alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                    " gui den 8384.DT ho tro 1900561577.";
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2,
                                                    subCode3);
                                            System.out.println("Call Spam funtions...");
                                            spam = dbTool.getSpam();
                                            if (!spam.equalsIgnoreCase("")) {
                                                if (status == 0) {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                            subCode3);
                                                    MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                                }
                                            }
                                            MyLogger.log(userId + "<==DA<==" + serviceId + ":Wrong serviceId");
                                        } else {
                                            if (dataIdStr.startsWith("3") || dataIdStr.startsWith("6")) { //OTA data

                                                if (dataIdStr.startsWith("3")) { //Back & White logo

                                                    dataStrSend = Utilities.getDataOTAFromUrl(dataStr);
                                                    if (!dataStrSend.equals("")) {
                                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 2, dataStrSend, 1,
                                                                requestId, subCode1, subCode2, subCode3);
                                                        spam = dbTool.getSpam();
                                                        if (!spam.equalsIgnoreCase("")) {
                                                            if (status == 0) {
                                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                                        subCode3);
                                                                MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                                            }
                                                        }
                                                        MyLogger.log(userId + "<==DA<==" + serviceId);
                                                    } else {
                                                        if (status != 0) {
                                                            alert =
                                                                    "Xin loi ban,du lieu ban yeu cau dang co loi.De duoc ho tro xin goi 1900561577.Xin cam on.";
                                                            subCode1 = "";
                                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1,
                                                                    subCode2, subCode3);
                                                            spam = dbTool.getSpam();
                                                            if (!spam.equalsIgnoreCase("")) {
                                                                if (status == 0) {
                                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                                            subCode3);
                                                                    MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                                                }
                                                            }
                                                            MyLogger.log(userId + "<==DA<==" + serviceId + ":Error Content data:" + info);
                                                        } else {
                                                            dbTool.updateStatus(requestId, 1, mobileOperator);
                                                        }
                                                    }
                                                } else { // ringtone mono

                                                    if (!(dataStrSend = Utilities.getHexString(dataStr.getBytes())).equals("")) {
                                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 1, dataStrSend, 1,
                                                                requestId, subCode1, subCode2, subCode3);
                                                        spam = dbTool.getSpam();
                                                        if (!spam.equalsIgnoreCase("")) {
                                                            if (status == 0) {
                                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                                        subCode3);
                                                                MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                                            }
                                                        }
                                                        MyLogger.log(userId + "<==DA<==" + serviceId);
                                                    } else {
                                                        if (status != 0) {
                                                            alert =
                                                                    "Xin loi ban,du lieu ban yeu cau dang co loi.De duoc ho tro xin goi 1900561577.Xin cam on.";
                                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1,
                                                                    subCode2, subCode3);
                                                            spam = dbTool.getSpam();
                                                            if (!spam.equalsIgnoreCase("")) {
                                                                if (status == 0) {
                                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                                            subCode3);
                                                                    MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                                                }
                                                            }
                                                            MyLogger.log(userId + "<==DA<==" + serviceId + ":Error Content data:" + info);
                                                        } else {
                                                            dbTool.updateStatus(requestId, 1, mobileOperator);
                                                        }
                                                    }
                                                }
                                            } else if (dataIdStr.startsWith("7")) {
                                                if ((serviceId.endsWith("8184") || serviceId.endsWith("8284") || serviceId.endsWith("8384")) &&
                                                        (!Preference.adminPhoneNumber.contains(userId))) {
                                                    alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                            " gui den 8484.DT ho tro 1900561577.";
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2,
                                                            subCode3);
                                                    spam = dbTool.getSpam();
                                                    if (!spam.equalsIgnoreCase("")) {
                                                        if (status == 0) {
                                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                                    subCode3);
                                                            MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                                        }
                                                    }
                                                    MyLogger.log(userId + "<==DA<==" + serviceId + ":Wrong serviceId");
                                                } else {
                                                    dataStrSend = "[PTS]" + ":" + dataStr;
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStrSend, 1, requestId, subCode1,
                                                            subCode2, subCode3);
                                                    spam = dbTool.getSpam();
                                                    if (!spam.equalsIgnoreCase("")) {
                                                        if (status == 0) {
                                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                                    subCode3);
                                                            MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                                        }
                                                    }
                                                    MyLogger.log(userId + "<==DA<==" + serviceId);
                                                }
                                            } else { //Wappush data

                                                dataStrSend = "[PTS]" + ":" + dataStr;
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStrSend, 1, requestId, subCode1,
                                                        subCode2, subCode3);
                                                spam = dbTool.getSpam();
                                                if (!spam.equalsIgnoreCase("")) {
                                                    if (status == 0) {
                                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                                subCode3);
                                                        MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                                    }
                                                }
                                                MyLogger.log(userId + "<==DA<==" + serviceId);
                                            }
                                        }
                                    } else if (dataIdStr.startsWith("8")) { //Is Clip (84 upper)

                                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384")) ||
                                                (Preference.adminPhoneNumber.contains(userId))) {
                                            dataStrSend = "[PTS]" + ":" + dataStr;
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStrSend, 1, requestId, subCode1, subCode2,
                                                    subCode3);
                                            spam = dbTool.getSpam();
                                            if (!spam.equalsIgnoreCase("")) {
                                                if (status == 0) {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                            subCode3);
                                                    MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                                }
                                            }
                                            MyLogger.log(userId + "<==DA<==" + serviceId);
                                        } else {
                                            alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                    " gui den 8484.DT ho tro 1900561577.";
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2,
                                                    subCode3);
                                            spam = dbTool.getSpam();
                                            if (!spam.equalsIgnoreCase("")) {
                                                if (status == 0) {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                            subCode3);
                                                    MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                                }
                                            }
                                            MyLogger.log(userId + "<==DA<==" + serviceId + ":Wrong serviceId");
                                        }
                                    } else {
                                        if (serviceId.endsWith("8784") || (Preference.adminPhoneNumber.contains(userId))) {
                                            dataStrSend = "[PTS]" + ":" + dataStr;
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStrSend, 1, requestId, subCode1, subCode2,
                                                    subCode3);
                                            spam = dbTool.getSpam();
                                            if (!spam.equalsIgnoreCase("")) {
                                                if (status == 0) {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                            subCode3);
                                                    MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                                }
                                            }
                                            MyLogger.log(userId + "<==DA<==" + serviceId);
                                        } else {
                                            alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                    " gui den 8784.DT ho tro 1900561577.";
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2,
                                                    subCode3);
                                            spam = dbTool.getSpam();
                                            if (!spam.equalsIgnoreCase("")) {
                                                if (status == 0) {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                            subCode3);
                                                    MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                                }
                                            }
                                            MyLogger.log(userId + "<==DA<==" + serviceId + ":Wrong serviceId");
                                        }
                                    }
                                } else { //Get your friend content

                                    if (dataIdStr.startsWith("1") || dataIdStr.startsWith("2") || dataIdStr.startsWith("3") ||
                                            dataIdStr.startsWith("4") || dataIdStr.startsWith("5") || dataIdStr.startsWith("6") ||
                                            dataIdStr.startsWith("7")) { //Not javaGame

                                        if (serviceId.endsWith("8184") || serviceId.endsWith("8284")) {
                                            alert = "Ban gui tang sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                    " gui den 8384.DT ho tro 1900561577.";
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2,
                                                    subCode3);
                                            MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" + userId +
                                                    ":Wrong serviceId");
                                        } else {
                                            if (dataIdStr.startsWith("3") || dataIdStr.startsWith("6")) { //OTA data

                                                if (dataIdStr.startsWith("3")) {
                                                    //Back & White logo
                                                    dataStrSend = Utilities.getDataOTAFromUrl(dataStr);
                                                    if (!dataStrSend.equals("")) {
                                                        alert = "Ban vua gui tang so dien thoai " + phoneNumberFriend + " logo co ma so " +
                                                                dataIdStr + ".\nDT ho tro 1900561577.";
                                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1,
                                                                requestId, subCode1, subCode2, subCode3);
                                                        alert = "So dien thoai " + userId + " gui tang ban logo co ma so " + dataIdStr +
                                                                " tu he thong http://pts.vn/mobile\nDT ho tro 1900561577.";
                                                        dbTool.sendMTEx(phoneNumberFriend, serviceId, mOperator, commandCode, 0, alert, 0,
                                                                requestId, subCode1, subCode2, subCode3);
                                                        dbTool.sendMTEx(phoneNumberFriend, serviceId, mOperator, commandCode, 2, dataStrSend,
                                                                0,
                                                                requestId, subCode1, subCode2, subCode3);
                                                        spam = dbTool.getSpam();
                                                        if (!spam.equalsIgnoreCase("")) {
                                                            if (status == 0) {
                                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                                        subCode3);
                                                                MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                                            }
                                                        }
                                                        MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" +
                                                                userId);
                                                    } else {
                                                        alert =
                                                                "Xin loi ban,du lieu ban yeu cau dang co loi.De duoc ho tro xin goi 1900561577.Xin cam on.";
                                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1,
                                                                subCode2, subCode3);
                                                        MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" +
                                                                userId);
                                                    }
                                                } else { // ringtone mono

                                                    if (!(dataStrSend = Utilities.getHexString(dataStr.getBytes())).equals("")) {
                                                        alert = "Ban vua gui tang so dien thoai " + phoneNumberFriend +
                                                                " bai hat don am co ma so " + dataIdStr +
                                                                " tu he thong http://pts.vn/mobile\nDT ho tro 1900561577.";
                                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1,
                                                                requestId, subCode1, subCode2, subCode3);
                                                        alert = "So dien thoai " + userId + " gui tang ban bai hat don am co ma so " +
                                                                dataIdStr + " tu he thong http://pts.vn/mobile\nDT ho tro 1900561577.";
                                                        dbTool.sendMTEx(phoneNumberFriend, serviceId, mOperator, commandCode, 1, alert, 0,
                                                                requestId, subCode1, subCode2, subCode3);
                                                        spam = dbTool.getSpam();
                                                        if (!spam.equalsIgnoreCase("")) {
                                                            if (status == 0) {
                                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                                        subCode3);
                                                                MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                                            }
                                                        }
                                                        MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" +
                                                                userId);
                                                    } else {
                                                        if (status != 0) {
                                                            alert =
                                                                    "Xin loi ban,du lieu ban yeu cau dang co loi.De duoc ho tro xin goi 1900561577.Xin cam on.";
                                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1,
                                                                    subCode2, subCode3);
                                                            spam = dbTool.getSpam();
                                                            if (!spam.equalsIgnoreCase("")) {
                                                                if (status == 0) {
                                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                                            subCode3);
                                                                    MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                                                }
                                                            }
                                                            MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" +
                                                                    userId + ":Error:" + info);
                                                        } else {
                                                            dbTool.updateStatus(requestId, 1, mobileOperator);
                                                        }
                                                    }
                                                }
                                            } else { //Wappush data

                                                alert = "Ban vua gui tang so dien thoai " + phoneNumberFriend + " noi dung co ma so " +
                                                        dataIdStr + " tu he thong http://pts.vn/mobile\nDT ho tro 1900561577.";
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1,
                                                        requestId, subCode1, subCode2, subCode3);
                                                dataStrSend = "[PTS]" + userId + " tangban:" + dataStr;
                                                dbTool.sendMTEx(phoneNumberFriend, serviceId, mOperator, commandCode, 8, dataStrSend, 0,
                                                        requestId, subCode1, subCode2, subCode3);
                                                spam = dbTool.getSpam();
                                                if (!spam.equalsIgnoreCase("")) {
                                                    if (status == 0) {
                                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                                subCode3);
                                                        MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                                    }
                                                }
                                                MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" + userId);
                                            }
                                        }
                                    } else if (dataIdStr.startsWith("8")) { //Is Clip (84 upper)

                                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384")) ||
                                                (Preference.adminPhoneNumber.contains(userId))) {
                                            alert = "Ban vua gui tang so dien thoai " + phoneNumberFriend + " clip co ma so " + dataIdStr +
                                                    " tu he thong http://pts.vn/mobile\nDT ho tro 1900561577.";
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1,
                                                    requestId, subCode1, subCode2, subCode3);
                                            dataStrSend = "[PTS]" + userId + " tangban:" + dataStr;
                                            dbTool.sendMTEx(phoneNumberFriend, serviceId, mOperator, commandCode, 8, dataStrSend, 0,
                                                    requestId, subCode1, subCode2, subCode3);
                                            MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" + userId);
                                        } else {
                                            alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                    " gui den 8484.DT ho tro 1900561577.";
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2,
                                                    subCode3);
                                            MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" + userId +
                                                    ":Wrong serviceId");
                                        }
                                    } else {
                                        if (serviceId.endsWith("8784")) {
                                            alert = "Ban vua gui tang so dien thoai " + phoneNumberFriend + " game co ma so " + dataIdStr +
                                                    " tu he thong http://pts.vn/mobile\nDT ho tro 1900561577.";
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1,
                                                    requestId, subCode1, subCode2, subCode3);
                                            dataStrSend = "[PTS]" + userId + " tangban:" + dataStr;
                                            dbTool.sendMTEx(phoneNumberFriend, serviceId, mOperator, commandCode, 8, dataStrSend, 0,
                                                    requestId, subCode1, subCode2, subCode3);
                                            MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" + userId);
                                        } else {
                                            alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                    " gui den 8784.DT ho tro 1900561577.";
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2,
                                                    subCode3);
                                            MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" + userId +
                                                    ":Wrong serviceId");
                                        }
                                    }
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Xin loi ban,ma so noi dung ban yeu cau khong tim thay.De duoc ho tro xin goi 1900561577.Xin cam on.";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    spam = dbTool.getSpam();
                                    if (!spam.equalsIgnoreCase("")) {
                                        if (status == 0) {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                                    subCode3);
                                            MyLogger.log(userId + "<==DA<==" + serviceId + ":send Spam");
                                        }
                                    }
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Data not found:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        } else {
                            alert =
                                    "Xin loi ban,ma so noi dung ban yeu cau khong tim thay.De duoc ho tro xin goi 1900561577.Xin cam on.";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Data not found:" + info);
                        }
                    } else if (commandCode.equals("DONAM") || commandCode.equals("DAAM")) {
                        //String infoTmp = commandCode + " " + info.substring(commandCode.length(), info.length());
                        String dataSearch = StringTool.getContentNoWhiteLetter(infoTmp.substring(commandCode.length()));
                        String phoneNumberFriend = StringTool.getContentOnlyNumber(infoTmp.substring(commandCode.length()).replaceAll("O", "0"));
                        String mOperator = "";
                        if (!phoneNumberFriend.equals("")) {
                            phoneNumberFriend = Utilities.rebuildUserId(StringTool.getContentOnlyNumber(phoneNumberFriend));
                            mOperator = Utilities.buildMobileOperator(phoneNumberFriend);
                        }
                        subCode1 = dataSearch;
                        if ((phoneNumberFriend == null) || mOperator.equals("")) { //get your self

                            if (commandCode.equals("DONAM")) {
                                dataStr = dbTool.searchDataMedia(dataSearch.toUpperCase(), 1);
                                if (!(dataStrSend = Utilities.getHexString(dataStr.getBytes())).equals("")) {
                                    if ((!serviceId.endsWith("8084") && !serviceId.endsWith("8184")) || (Preference.adminPhoneNumber.contains(userId))) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 1, dataStrSend, 1,
                                                requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==DONAM<==" + serviceId);
                                    } else {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                " gui den 8384.DT ho tro 1900561577.";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" + userId +
                                                ":Wrong serviceId");
                                    }
                                } else {
                                    if (status != 0) {
                                        alert =
                                                "Bai hat Don am ban yeu cau khong co,hay soan tin nhan voi tu khoa tim kiem khac va gui toi 8384.De duoc ho tro xin goi 1900561577.Xin cam on.";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==DA<==" + serviceId + ":Error Content data:" + info);
                                    } else {
                                        dbTool.updateStatus(requestId, 1, mobileOperator);
                                    }
                                }
                            } else {
                                dataStr = dbTool.searchDataMedia(dataSearch, 2);
                                if (!dataStr.equals("")) {
                                    if ((!serviceId.endsWith("8084") && !serviceId.endsWith("8184")) || (Preference.adminPhoneNumber.contains(userId))) {
                                        dataStrSend = "[PTS]" + ":" + dataStr;
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStrSend, 1, requestId, subCode1, subCode2,
                                                subCode3);
                                        MyLogger.log(userId + "<==DA<==" + serviceId);
                                    } else {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                " gui den 8384.DT ho tro 1900561577.";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" + userId +
                                                ":Wrong serviceId");
                                    }
                                } else {
                                    if (status != 0) {
                                        alert =
                                                "Bai hat Da am ban tim khong co,hay soan lai tin nhan voi tu khoa tim kiem khac va gui toi 8384.De duoc ho tro xin goi 1900561577.Xin cam on.";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==DA<==" + serviceId + ":NotFound Content data:" + info);
                                    } else {
                                        dbTool.updateStatus(requestId, 1, mobileOperator);
                                    }
                                }
                            }
                        } else {
                            if (commandCode.equals("DONAM")) {
                                dataStr = dbTool.searchDataMedia(dataSearch, 1);
                                if (!(dataStrSend = Utilities.getHexString(dataStr.getBytes())).equals("")) {
                                    if (!serviceId.endsWith("8084") && !serviceId.endsWith("8184")) {
                                        alert = "Ban vua gui tang so dien thoai " + phoneNumberFriend +
                                                " bai hat don am " + dataSearch +
                                                " tu he thong http://pts.vn/mobile\nDT ho tro 1900561577.";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1,
                                                requestId, subCode1, subCode2, subCode3);
                                        alert = "So dien thoai " + userId + " gui tang ban bai hat don am " +
                                                dataSearch + " tu he thong http://pts.vn/mobile\nDT ho tro 1900561577.";
                                        dbTool.sendMTEx(phoneNumberFriend, serviceId, mOperator, commandCode, 1, alert, 0,
                                                requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" +
                                                userId);
                                    } else {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                " gui den 8384.DT ho tro 1900561577.";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" + userId +
                                                ":Wrong serviceId");
                                    }
                                } else {
                                    if (status != 0) {
                                        alert =
                                                "Bai hat Don am ban yeu cau khong co,hay soan tin nhan voi tu khoa tim kiem khac va gui toi 8384.De duoc ho tro xin goi 1900561577.Xin cam on.";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==DA<==" + serviceId + ":Error Content data:" + info);
                                    } else {
                                        dbTool.updateStatus(requestId, 1, mobileOperator);
                                    }
                                }
                            } else {
                                dataStr = dbTool.searchDataMedia(dataSearch, 2);
                                if (!dataStr.equals("")) {
                                    if (!serviceId.endsWith("8084") && !serviceId.endsWith("8184")) {
                                        alert = "Ban vua gui tang so dien thoai " + phoneNumberFriend + " bai hat da am " +
                                                dataSearch + " tu he thong http://pts.vn/mobile\nDT ho tro 1900561577.";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1,
                                                requestId, subCode1, subCode2, subCode3);
                                        dataStrSend = "[PTS]" + userId + " tangban:" + dataStr;
                                        dbTool.sendMTEx(phoneNumberFriend, serviceId, mOperator, commandCode, 8, dataStrSend, 0,
                                                requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" + userId);
                                    } else {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                " gui den 8384.DT ho tro 1900561577.";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" + userId +
                                                ":Wrong serviceId");
                                    }
                                } else {
                                    if (status != 0) {
                                        alert =
                                                "Bai hat Da am ban tim khong co,hay soan lai tin nhan voi tu khoa tim kiem khac va gui toi 8384.De duoc ho tro xin goi 1900561577.Xin cam on.";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==DA<==" + serviceId + ":NotFound Content data:" + info);
                                    } else {
                                        dbTool.updateStatus(requestId, 1, mobileOperator);
                                    }
                                }
                            }
                        }
                    } else if (commandCode.equals("LOI")) {
                        //String infoTmp = commandCode + " " + info.substring(commandCode.length(), info.length());
                        String dataSearch = StringTool.getContentNoWhiteLetter(infoTmp.substring(commandCode.length()));
                        String phoneNumberFriend = StringTool.getContentOnlyNumber(infoTmp.substring(commandCode.length()).replaceAll("O", "0"));
                        subCode1 = dataSearch;
                        String mOperator = "";
                        if ((phoneNumberFriend != null) && !phoneNumberFriend.equals("")) {
                            phoneNumberFriend = Utilities.rebuildUserId(StringTool.getContentOnlyNumber(phoneNumberFriend));
                            mOperator = Utilities.buildMobileOperator(phoneNumberFriend);
                        }
                        dataStr = dbTool.searchDataMedia(dataSearch.toUpperCase(), 3);

                        if ((phoneNumberFriend == null) || mOperator.equals("")) { //get your self

                            if (!dataStr.equals("")) {
                                if ((!serviceId.endsWith("8084") && !serviceId.endsWith("8184")) || (Preference.adminPhoneNumber.contains(userId))) {
                                    dataStrSend = "[PTS]" + ":" + dataStr;
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStrSend, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==DA<==" + serviceId);
                                } else {
                                    alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                            " gui den 8384.DT ho tro 1900561577.";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" + userId +
                                            ":Wrong serviceId");
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Bai hat Co loi ban tim khong co,hay soan lai tin nhan voi tu khoa tim kiem khac va gui toi 8384.De duoc ho tro xin goi 1900561577.Xin cam on.";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==DA<==" + serviceId + ":NotFound Content data:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        } else { //for friend

                            if (!dataStr.equals("")) {
                                if ((!serviceId.endsWith("8084") && !serviceId.endsWith("8184")) || (Preference.adminPhoneNumber.contains(userId))) {
                                    alert = "Ban vua gui tang so dien thoai " + phoneNumberFriend + " bai hat co loi " +
                                            dataSearch + " tu he thong http://pts.vn/mobile\nDT ho tro 1900561577.";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1,
                                            requestId, subCode1, subCode2, subCode3);
                                    dataStrSend = "[PTS]" + userId + " tangban:" + dataStr;
                                    dbTool.sendMTEx(phoneNumberFriend, serviceId, mOperator, commandCode, 8, dataStrSend, 0,
                                            requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" + userId);
                                } else {
                                    alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                            " gui den 8384.DT ho tro 1900561577.";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" + userId +
                                            ":Wrong serviceId");
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Bai hat Co loi ban tim khong co,hay soan lai tin nhan voi tu khoa tim kiem khac va gui toi 8384.De duoc ho tro xin goi 1900561577.Xin cam on.";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound Content data:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        }
                    } else if ((commandCode.equals("DA") && info.startsWith("DA")) || (commandCode.equals("TA") && info.startsWith("TA")) ||
                            (commandCode.equals("TC") && info.startsWith("TC")) ||
                            (commandCode.equals("AG") && info.startsWith("AG")) ||
                            commandCode.equals("TND") || commandCode.equals("THD") || commandCode.equals("DPD") || commandCode.equals("HA") || commandCode.equals("QA")) {

                        info = commandCode + " " + info.substring(commandCode.length(), info.length()).trim();

                        String code = "";
                        String receiver = "";
                        String idFull = "";

                        try {
                            code = Utilities.getObjectString(info, 1);
                            receiver = ((Utilities.getObjectString(info, 2)).replaceAll("o", "0")).replaceAll("O", "0");
                            if (!Utilities.isValidUserId(receiver)) {
                                receiver = ((Utilities.getObjectString(info, 3)).replaceAll("o", "0")).replaceAll("O", "0");
                            } else if (!Utilities.isValidUserId(receiver)) {
                                receiver = ((Utilities.getObjectString(info, 4)).replaceAll("o", "0")).replaceAll("O", "0");
                            }
                        } catch (Exception ex) {
                        } finally {
                            if (code == null) {
                                code = "";
                            }
                            if (receiver == null) {
                                receiver = "";
                            }
                        }
                        if (receiver.equals("")) {
                            receiver = userId;
                        }
                        code = code.replaceAll("o", "0").replaceAll("O", "0");
                        idFull = code;
                        if (!code.startsWith("GN")) {
                            if ((!StringTool.isNumberic(code)) || (code.length() < 3)) {
                                code = "";
                            } else {
                                prefixCode = code.substring(0, 3);
                                dataType = code.substring(1, 3);
                                code = code.substring(3);
                            }
                        }

                        if (code.equals("")) {
                            if (status != 0) {
                                alert =
                                        "Ma so ban gui khong chinh xac." + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound Content data:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else if (code.startsWith("GN")) {
                            if (mobileOperator.equals("VIETEL")) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, gnVIETEL, 1, requestId, subCode1, subCode2, subCode3);
                            } else if (mobileOperator.equals("VMS")) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, gnVMS, 1, requestId, subCode1, subCode2, subCode3);
                            } else if (mobileOperator.equals("GPC")) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, gnGPC1, 1, requestId, subCode1, subCode2, subCode3);
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, gnGPC2, 0, requestId, subCode1, subCode2, subCode3);
                            }
                        } else if (code.startsWith("HOT") || code.startsWith("TOP")) {
                            if (commandCode.equals("TOP")) {
                                commandId = "TOP";
                            } else if (commandCode.equals("HOT")) {
                                commandId = "HOT";
                            }
                            String result = dbTool.getTXTData(commandId);
                            if ((result != null) && !result.equals("")) {
                                if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                                    if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                        result = StringTool.splitMsg(result);
                                    }

                                    boolean deductMoney = false;
                                    result = result.replaceAll("!xxx!", "|");
                                    StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                    int stTokenLen = stToken.countTokens();
                                    for (int ii = 0; ii < stTokenLen; ii++) {
                                        String txtSms = stToken.nextToken();
                                        if (!deductMoney) {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                            deductMoney = true;
                                            MyLogger.log(userId + "<==HOT/TOP<==" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                        }
                                        if (ii > 5) {
                                            break;
                                        }
                                    }

                                } else {
                                    if (commandCode.equals("HA") || commandCode.equals("QA")) {
                                        alert =
                                                "Ban gui sai so dich vu,de nhan thong tin ve TOP va HOT soan " + info +
                                                " gui8484.\nDT ho tro 1900561577";
                                    } else {
                                        alert =
                                                "Ban gui sai so dich vu,de nhan thong tin ve TOP va HOT soan " + info +
                                                " gui8384.\nDT ho tro 1900561577";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==TOP/HOT<==" + serviceId + ":WrongServiceId:" + info);
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Xin loi ban,hien tai he thong chua co thong tin ma ban yeu cau.Soan TOP hoac HOT gui8384 de nhan MaSo hinhanh/nhac chuong HOT.\nDT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==TOP/HOT<==" + serviceId + ":NotFound:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        } else if (prefixCode.startsWith("815")) {
                            //Mono
                            subCode1 = idFull;
                            dataStr = dbTool.getRingToneMono(new BigDecimal(code));
                            if (!dataStr.equals("")) {
                                if ((!commandCode.equals("TND") && !commandCode.equals("THD") && !commandCode.equals("DPD") && (("8184,8284").indexOf(serviceId) == -1)) ||
                                        ((commandCode.equals("TND") || commandCode.equals("THD") || commandCode.equals("DPD")) && (("8184,8284,8384,8484").indexOf(serviceId) == -1)) ||
                                        (!commandCode.equals("HA") && !commandCode.equals("QA") && (("8184,8284").indexOf(serviceId) == -1)) ||
                                        ((commandCode.equals("HA") || commandCode.equals("QA")) && (("8184,8284,8384").indexOf(serviceId) == -1)) ||
                                        (Preference.adminPhoneNumber.contains(userId))) {

                                    receiver = Utilities.formatUserId(receiver, 0);
                                    if (Utilities.isValidUserId(receiver) && !receiver.equals(userId)) {
                                        if (commandCode.equals("TND") || commandCode.equals("THD") || commandCode.equals("DPD") || commandCode.equals("HA") || commandCode.equals("QA")) {
                                            alert = "Ban vua gui ringtone co ma so " + idFull + " toi so may " + receiver + enter +
                                                    "DT ho tro 1900561577";
                                        } else {
                                            alert = "Ban vua gui ringtone co ma so " + idFull + " toi so may " + receiver +
                                                    ".\nDe biet nhung bai nhac TOP soan TOP gui8384,nhung hinh anh HOT soan HOT gui8384.";
                                        }
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        mobileOperatorFriend = Utilities.buildMobileOperator(receiver);
                                        if (dbTool.sendMTEx(receiver, serviceId, mobileOperatorFriend, commandCode, 1, dataStr, 0, requestId, subCode1,
                                                subCode2, subCode3)) {
                                            MyLogger.log(receiver + "<==" + commandCode + ":" + serviceId + "<==" + userId);
                                        }
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 1, dataStr, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + ":" + serviceId);
                                    }
                                } else {
                                    if (commandCode.equals("TND") || commandCode.equals("THD") || commandCode.equals("DPD")) {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                " gui den 8584.\nDT ho tro 1900561577.";
                                    } else if (commandCode.equals("HA") || commandCode.equals("QA")) {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                " gui den 8484.\nDT ho tro 1900561577.";
                                    } else {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                " gui den 8384.\nDT ho tro 1900561577.";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong serviceId");
                                }
                            } else {
                                if (status != 0) {
                                    subCode1 = "";
                                    if (commandCode.equals("TND") || commandCode.equals("THD") || commandCode.equals("DPD")) {
                                        alert =
                                                "Ma so ban gui khong chinh xac,hay soan " + commandCode +
                                                " <MaSo> gui 8584 de nhan duoc noi dung media cho di dong." + enter +
                                                "DT ho tro 1900561577";
                                    } else if (commandCode.equals("HA") || commandCode.equals("QA")) {
                                        alert =
                                                "Ma so ban gui khong chinh xac,hay soan " + commandCode +
                                                " <MaSo> gui 8484 de nhan duoc noi dung media cho di dong." + enter +
                                                "DT ho tro 1900561577";
                                    } else {
                                        alert =
                                                "Ma so ban gui khong chinh xac,hay soan " + commandCode +
                                                " <MaSo> gui 8384 de nhan duoc noi dung media cho di dong.De biet MaSo Hot nhat soan HOT gui 8584.\n" +
                                                "DT ho tro 1900561577";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound Content data:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        } else if (prefixCode.startsWith("812") || prefixCode.startsWith("813") || prefixCode.startsWith("814")) {
                            //Poly-TrueTone-Word
                            subCode1 = idFull;
                            auth = dbTool.add2PushInfo(receiver, userId, new BigDecimal(code), dataType);
                            dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                            dataStr = "[PTS] " + idFull + http + "push/" + dataName + "?auth=" + auth;
                            if (!auth.equals("")) {
                                if ((!commandCode.equals("TND") && !commandCode.equals("THD") && !commandCode.equals("DPD") && (("8184,8284").indexOf(serviceId) == -1)) ||
                                        ((commandCode.equals("TND") || commandCode.equals("THD") || commandCode.equals("DPD")) && (("8184,8284,8384,8484").indexOf(serviceId) == -1)) ||
                                        (!commandCode.equals("HA") && !commandCode.equals("QA") && (("8184,8284").indexOf(serviceId) == -1)) ||
                                        ((commandCode.equals("HA") || commandCode.equals("QA")) && (("8184,8284,8384").indexOf(serviceId) == -1)) ||
                                        (Preference.adminPhoneNumber.contains(userId))) {
                                    receiver = Utilities.formatUserId(receiver, 0);
                                    if (Utilities.isValidUserId(receiver) && !receiver.equals(userId)) {
                                        if (commandCode.equals("TND") || commandCode.equals("THD") || commandCode.equals("DPD") || commandCode.equals("HA") || commandCode.equals("QA")) {
                                            alert = "Ban vua gui tang nhac chuong co ma so " + idFull + " toi so may " + receiver + enter +
                                                    "DT ho tro 1900561577";
                                        } else {
                                            alert = "Ban vua gui tang nhac chuong co ma so " + idFull + " toi so may " + receiver + "\n" +
                                                    "De biet nhung bai nhac TOP soan TOP gui8384,nhung hinh anh HOT soan HOT gui8384.";
                                        }
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        mobileOperatorFriend = Utilities.buildMobileOperator(receiver);
                                        dataStr = "[PTS] " + userId + " Tangban " + http + "push/" + dataName + "?auth=" + auth;
                                        if (dbTool.sendMTEx(receiver, serviceId, mobileOperatorFriend, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                subCode2, subCode3)) {
                                            MyLogger.log(receiver + "<==" + commandCode + ":" + serviceId + "<==" + userId);
                                        }
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + ":" + serviceId);
                                    }
                                } else {
                                    if (commandCode.equals("TND") || commandCode.equals("THD") || commandCode.equals("DPD")) {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                " gui den 8584.\nDT ho tro 1900561577.";
                                    } else if (commandCode.equals("HA") || commandCode.equals("QA")) {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                " gui den 8484.\nDT ho tro 1900561577.";
                                    } else {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                " gui den 8384.\nDT ho tro 1900561577.";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong serviceId");
                                }
                            } else {
                                if (status != 0) {
                                    if (commandCode.equals("TND") || commandCode.equals("THD") || commandCode.equals("DPD")) {
                                        alert =
                                                "Ma so ban gui khong chinh xac,hay soan " + commandCode +
                                                " <MaSo> gui 8584 de nhan duoc noi dung media cho di dong." + enter +
                                                "DT ho tro 1900561577";
                                    } else if (commandCode.equals("HA") || commandCode.equals("QA")) {
                                        alert =
                                                "Ma so ban gui khong chinh xac,hay soan " + commandCode +
                                                " <MaSo> gui 8484 de nhan duoc noi dung media cho di dong." + enter +
                                                "DT ho tro 1900561577";
                                    } else {
                                        alert =
                                                "Ma so ban gui khong chinh xac,hay soan " + commandCode +
                                                " <MaSo> gui 8384 de nhan duoc noi dung media cho di dong.De biet MaSo Hot nhat soan HOT gui 8584.\n" +
                                                "DT ho tro 1900561577";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound Content data:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        } else if (prefixCode.startsWith("824")) {
                            //Photo
                            subCode1 = idFull;
                            auth = dbTool.add2PushInfo(receiver, userId, new BigDecimal(code), dataType);
                            dataStr = "[PTS] " + idFull + http + "push/" + idFull + ".gif?auth=" + auth;
                            if (!auth.equals("")) {
                                if ((!commandCode.equals("TND") && !commandCode.equals("THD") && !commandCode.equals("DPD") && (("8184,8284").indexOf(serviceId) == -1)) ||
                                        ((commandCode.equals("TND") || commandCode.equals("THD") || commandCode.equals("DPD")) && (("8184,8284,8384,8484").indexOf(serviceId) == -1)) ||
                                        (!commandCode.equals("HA") && !commandCode.equals("QA") && (("8184,8284").indexOf(serviceId) == -1)) ||
                                        ((commandCode.equals("HA") || commandCode.equals("QA")) && (("8184,8284,8384").indexOf(serviceId) == -1)) ||
                                        (Preference.adminPhoneNumber.contains(userId))) {
                                    receiver = Utilities.formatUserId(receiver, 0);
                                    if (Utilities.isValidUserId(receiver) && !receiver.equals(userId)) {
                                        alert = "Ban vua gui hinh nen co ma so " + idFull + " toi so may " + receiver + enter +
                                                "DT ho tro 1900561577";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        mobileOperatorFriend = Utilities.buildMobileOperator(receiver);
                                        dataStr = "[PTS] " + userId + " Tangban " + http + "push/" + idFull + ".gif?auth=" + auth;
                                        if (dbTool.sendMTEx(receiver, serviceId, mobileOperatorFriend, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                subCode2, subCode3)) {
                                            MyLogger.log(receiver + "<==" + commandCode + ":" + serviceId + "<==" + userId);
                                        }
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + ":" + serviceId);
                                    }
                                } else {
                                    if (commandCode.equals("TND") || commandCode.equals("THD") || commandCode.equals("DPD")) {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                " gui den 8584.\nDT ho tro 1900561577.";
                                    } else if (commandCode.equals("HA") || commandCode.equals("QA")) {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                " gui den 8484.\nDT ho tro 1900561577.";
                                    } else {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                " gui den 8384.\nDT ho tro 1900561577.";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong serviceId");
                                }
                            } else {
                                if (status != 0) {
                                    if (commandCode.equals("TND") || commandCode.equals("THD") || commandCode.equals("DPD")) {
                                        alert =
                                                "Ma so ban gui khong chinh xac,hay soan " + commandCode +
                                                " <MaSo> gui 8584 de nhan duoc noi dung media cho di dong.De biet MaSo Hot nhat soan HOT gui 8584.\n" +
                                                "DT ho tro 1900561577";
                                    } else if (commandCode.equals("HA") || commandCode.equals("QA")) {
                                        alert =
                                                "Ma so ban gui khong chinh xac,hay soan " + commandCode +
                                                " <MaSo> gui 8484 de nhan duoc noi dung media cho di dong.De biet MaSo Hot nhat soan HOT gui 8584.\n" +
                                                "DT ho tro 1900561577";
                                    } else {
                                        alert =
                                                "Ma so ban gui khong chinh xac,hay soan " + commandCode +
                                                " <MaSo> gui 8384 de nhan duoc noi dung media cho di dong.De biet MaSo Hot nhat soan HOT gui 8584.\n" +
                                                "DT ho tro 1900561577";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound Content data:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        } else if (prefixCode.startsWith("831")) {
                            //Java game
                            subCode1 = idFull;
                            auth = dbTool.add2PushInfo(receiver, userId, new BigDecimal(code), dataType);
                            dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                            dataStr = "[PTS] " + idFull + http + "push/" + dataName + "?auth=" + auth;
                            if (!auth.equals("")) {
                                if (serviceId.endsWith("8584") || serviceId.endsWith("8684") || serviceId.endsWith("8784") || (Preference.adminPhoneNumber.contains(userId))) {
                                    receiver = Utilities.formatUserId(receiver, 0);
                                    if (Utilities.isValidUserId(receiver) && !receiver.equals(userId)) {
                                        alert = "Ban vua gui tro choi co ma so " + idFull + " toi so may " + receiver + enter +
                                                "DT ho tro 1900561577";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        mobileOperatorFriend = Utilities.buildMobileOperator(receiver);
                                        dataStr = "[PTS] " + userId + " Tangban " + http + "push/" + dataName + "?auth=" + auth;
                                        if (dbTool.sendMTEx(receiver, serviceId, mobileOperatorFriend, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                subCode2, subCode3)) {
                                            MyLogger.log(receiver + "<==" + commandCode + ":" + serviceId + "<==" + userId);
                                        }
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + ":" + serviceId);
                                    }
                                } else {
                                    alert = "Ban gui sai dau so dich vu.De nhan duoc tro choi hay soan " + info +
                                            " gui den 8584.\nDT ho tro 1900561577.";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong serviceId");
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Ma so ban gui khong chinh xac,hay soan " + commandCode +
                                            " <MaSoGame> gui 8784 de nhan duoc noi dung tro choi." + enter +
                                            "DT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound Content data:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        } else if (prefixCode.startsWith("821")) {
                            //Logo
                            subCode1 = idFull;
                            dataStr = dbTool.getLogoOTA(new BigDecimal(code));
                            if (!dataStr.equals("")) {
                                if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId)) ||
                                        ((commandCode.equals("THD") || commandCode.equals("TND")) && (serviceId.equals("8584") || serviceId.equals("8684") || serviceId.equals("8784"))) ||
                                        ((commandCode.equals("HA") || commandCode.equals("QA")) && (serviceId.equals("8484") || serviceId.equals("8584") || serviceId.equals("8684") || serviceId.equals("8784")))) {
                                    receiver = Utilities.formatUserId(receiver, 0);
                                    if (Utilities.isValidUserId(receiver) && !receiver.equals(userId)) {
                                        alert = "Ban vua gui Logo co ma so " + idFull + " toi so may " + receiver + enter +
                                                "DT ho tro 1900561577";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        mobileOperatorFriend = Utilities.buildMobileOperator(receiver);
                                        if (dbTool.sendMTEx(receiver, serviceId, mobileOperatorFriend, commandCode, 2, dataStr, 0, requestId, subCode1,
                                                subCode2, subCode3)) {
                                            MyLogger.log(receiver + "<==" + commandCode + ":" + serviceId + "<==" + userId);
                                        }
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 2, dataStr, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + ":" + serviceId);
                                    }
                                } else {
                                    if (commandCode.equals("THD") || commandCode.equals("TND")) {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                " gui den8584.\nDT ho tro 1900561577.";
                                    } else if (commandCode.equals("HA") || commandCode.equals("QA")) {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                " gui den8484.\nDT ho tro 1900561577.";
                                    } else {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                                " gui den8384.\nDT ho tro 1900561577.";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong serviceId");
                                }
                            } else {
                                if (status != 0) {
                                    if (commandCode.equals("THD") || commandCode.equals("TND")) {
                                        alert =
                                                "Ma so ban gui khong chinh xac,hay soan " + commandCode + " <MaSo> gui8584" + enter +
                                                "DT ho tro 1900561577";
                                    } else if (commandCode.equals("HA") || commandCode.equals("QA")) {
                                        alert =
                                                "Ma so ban gui khong chinh xac,hay soan " + commandCode + " <MaSo> gui8484" + enter +
                                                "DT ho tro 1900561577";
                                    } else {
                                        alert =
                                                "Ma so ban gui khong chinh xac,hay soan " + commandCode +
                                                " <MaSo> gui8384.De biet ma bai hat TOP nhat soan TOP gui8384,hinh anh HOT soan HOT gui8384.\n" +
                                                "DT ho tro 1900561577";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound Content data:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        } else if (prefixCode.startsWith("832")) {
                            //Theme
                            subCode1 = idFull;
                            auth = dbTool.add2PushInfo(receiver, userId, new BigDecimal(code), dataType);
                            dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                            dataStr = "[PTS] " + idFull + http + "push/" + dataName + "?auth=" + auth;
                            if (!auth.equals("")) {
                                if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) || (Preference.adminPhoneNumber.contains(userId)) ||
                                        ((commandCode.equals("HA") || commandCode.equals("QA")) && (serviceId.equals("8484") || serviceId.equals("8584") || serviceId.equals("8684") || serviceId.equals("8784")))) {
                                    receiver = Utilities.formatUserId(receiver, 0);
                                    if (Utilities.isValidUserId(receiver) && !receiver.equals(userId)) {
                                        alert = "Ban vua gui Theme co ma so " + idFull + " toi so may " + receiver + enter +
                                                "DT ho tro 1900561577";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        mobileOperatorFriend = Utilities.buildMobileOperator(receiver);
                                        dataStr = "[PTS] " + userId + " Tangban " + http + "push/" + dataName + "?auth=" + auth;
                                        if (dbTool.sendMTEx(receiver, serviceId, mobileOperatorFriend, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                subCode2, subCode3)) {
                                            MyLogger.log(receiver + "<==" + commandCode + ":" + serviceId + "<==" + userId);
                                        }
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + ":" + serviceId);
                                    }
                                } else {
                                    if (commandCode.equals("HA") || commandCode.equals("QA")) {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc Theme hay soan " + info +
                                                " gui den 8484.\nDT ho tro 1900561577.";
                                    } else {
                                        alert = "Ban gui sai dau so dich vu.De nhan duoc Theme hay soan " + info +
                                                " gui den 8584.\nDT ho tro 1900561577.";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong serviceId");
                                }
                            } else {
                                if (status != 0) {
                                    if (commandCode.equals("HA") || commandCode.equals("QA")) {
                                        alert =
                                                "Ma so Theme ban gui khong chinh xac,hay soan " + commandCode + " <MaSo> gui 8484." + enter +
                                                "DT ho tro 1900561577";
                                    } else {
                                        alert =
                                                "Ma so Theme ban gui khong chinh xac,hay soan " + commandCode + " <MaSo> gui 8584." + enter +
                                                "DT ho tro 1900561577";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound Content data:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Ma so ban gui khong chinh xac,hay soan " + commandCode +
                                        " MaSo gui8384.De biet ma bai hat TOP nhat soan TOP gui8384,hinh anh HOT soan HOT gui8384.\n" +
                                        "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong format:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                        //add adv
                        spam = dbTool.getSpam();
                        if (!spam.equalsIgnoreCase("")) {
                            if (status == 0) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                        subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":send Spam");
                            }
                        }
                    } else if (commandCode.equals("XOA")) {
                        infoTmp = info.substring(commandCode.length()).trim();
                        if ((infoTmp != null) && !infoTmp.equals("")) {
                            if (status != 0) {
                                dataStr = dbTool.getLogoOTA(new BigDecimal(1)); //ID Code content Logo clear

                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 2, dataStr, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + ":" + serviceId);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            dataStr = dbTool.getLogoOTA(new BigDecimal(1)); //ID Code content Logo clear

                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 2, dataStr, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + ":" + serviceId);
                        }
                    } else if (commandCode.equals("AM") || commandCode.equals("MA") || commandCode.equals("AP") || commandCode.equals("AB") ||
                            commandCode.equals("AZ") || commandCode.equals("STA") || commandCode.equals("TNP") || commandCode.equals("THA") ||
                            commandCode.equals("BNA") || commandCode.equals("DPA")) {
                        info = commandCode + " " + info.substring(commandCode.length(), info.length()).trim();
                        param1 = Utilities.getObjectString(info, 1);
                        if (commandCode.equals("STA") || commandCode.equals("BNA")) {
                            commandId = "STA";
                        } else {
                            commandId = "AM";
                        }
                        String receiver = Utilities.getObjectString(info, 2);
                        receiver = Utilities.formatUserId(receiver, 0);
                        String result = dbTool.getTXTData_1(commandId, param1);
                        if ((result != null) && (!result.equals(""))) {
                            if (serviceId.endsWith("8684") || serviceId.endsWith("8784") || Preference.adminPhoneNumber.contains(userId)) {
                                result = result.trim();
                                result = result.replaceAll("o", "0");
                                result = result.replaceAll("O", "0");
                                if (Utilities.isValidUserId(receiver) && !receiver.equals(userId)) {
                                    //For friend
                                    if (commandCode.equals("STA") || commandCode.equals("THA") || commandCode.equals("TNP") || commandCode.equals("BNA") || commandCode.equals("DPA")) {
                                        alert = "Ban vua gui bo suutap co ma so " + param1 + " toi so may " + receiver +
                                                enter + "Cam on ban da su dung dich vu.DT ho tro 1900561577";
                                    } else {
                                        alert = "Ban vua gui bo suutap co ma so " + param1 + " toi so may " + receiver +
                                                enter + "De biet nhung bai nhac TOP soan TOP gui8384,nhung hinh anh HOT soan HOT gui8384.";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    mobileOperatorFriend = Utilities.buildMobileOperator(receiver);

                                    StringTokenizer tk = new StringTokenizer(result, ",", false);
                                    //int iCount = 0;
                                    while (tk.hasMoreTokens()) {
                                        String idFull = tk.nextToken();
                                        prefixCode = idFull.substring(0, 3);
                                        dataType = idFull.substring(1, 3);
                                        String code = idFull.substring(3);

                                        if (prefixCode.startsWith("812") || prefixCode.startsWith("813") || prefixCode.startsWith("814")) {
                                            //Poly-TrueTone-Word
                                            auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                            dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                                            dataStr = "[PTS] " + userId + " Tangban " + http + "push/" + dataName + "?auth=" + auth;
                                            if (!auth.equals("")) {
                                                dbTool.sendMTEx(receiver, serviceId, mobileOperatorFriend, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                        subCode2, subCode3);
                                            }
                                        } else if (prefixCode.startsWith("824")) {
                                            //Photo
                                            auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                            dataStr = "[PTS] " + userId + " Tangban " + http + "push/" + idFull + ".gif?auth=" + auth;
                                            if (!auth.equals("")) {
                                                dbTool.sendMTEx(receiver, serviceId, mobileOperatorFriend, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                        subCode2, subCode3);
                                            }
                                        } else if (prefixCode.startsWith("831")) {
                                            //Java game
                                            auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                            dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                                            dataStr = "[PTS] " + userId + " Tangban " + http + "push/" + dataName + "?auth=" + auth;
                                            if (!auth.equals("")) {
                                                dbTool.sendMTEx(receiver, serviceId, mobileOperatorFriend, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                        subCode2, subCode3);
                                            }
                                        } else if (prefixCode.startsWith("815")) {
                                            //Mono
                                            dataStr = dbTool.getRingToneMono(new BigDecimal(code));
                                            mobileOperatorFriend = Utilities.buildMobileOperator(receiver);
                                            dbTool.sendMTEx(receiver, serviceId, mobileOperatorFriend, commandCode, 1, dataStr, 0, requestId, subCode1,
                                                    subCode2, subCode3);
                                        }
                                    } //end while

                                    MyLogger.log(receiver + "<==" + commandCode + "<==" + serviceId + "<==" + userId);
                                } else {
                                    //For self
                                    StringTokenizer tk = new StringTokenizer(result, ",", false);
                                    boolean deductMoney = false;
                                    //int iCount = 0;
                                    while (tk.hasMoreTokens()) {
                                        String idFull = tk.nextToken();
                                        prefixCode = idFull.substring(0, 3);
                                        dataType = idFull.substring(1, 3);
                                        String code = idFull.substring(3);

                                        if (prefixCode.startsWith("812") || prefixCode.startsWith("813") || prefixCode.startsWith("814")) {
                                            //Poly-TrueTone-Word
                                            auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                            dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                                            dataStr = "[PTS] " + idFull + http + "push/" + dataName + "?auth=" + auth;
                                            if (!auth.equals("")) {
                                                if (!deductMoney) {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 1, requestId, subCode1,
                                                            subCode2, subCode3);
                                                    deductMoney = true;
                                                } else {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                            subCode2, subCode3);
                                                }
                                            }
                                        } else if (prefixCode.startsWith("824")) {
                                            //Photo
                                            auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                            dataStr = "[PTS] " + idFull + http + "push/" + idFull + ".gif?auth=" + auth;
                                            if (!auth.equals("")) {
                                                if (!deductMoney) {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 1, requestId, subCode1,
                                                            subCode2, subCode3);
                                                    deductMoney = true;
                                                } else {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                            subCode2, subCode3);
                                                }
                                            }
                                        } else if (prefixCode.startsWith("831")) {
                                            //Java game
                                            auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                            dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                                            dataStr = "[PTS] " + idFull + http + "push/" + dataName + "?auth=" + auth;
                                            if (!auth.equals("")) {
                                                if (!deductMoney) {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 1, requestId, subCode1,
                                                            subCode2, subCode3);
                                                    deductMoney = true;
                                                } else {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                            subCode2, subCode3);
                                                }
                                            }
                                        } else if (prefixCode.startsWith("815")) {
                                            dataStr = dbTool.getRingToneMono(new BigDecimal(code));
                                            if (!deductMoney) {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 1, dataStr, 1, requestId, subCode1, subCode2,
                                                        subCode3);
                                                deductMoney = true;
                                            } else {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 1, dataStr, 0, requestId, subCode1, subCode2,
                                                        subCode3);
                                            }
                                        }
                                    } //end while

                                    MyLogger.log(userId + "<==" + commandCode + ":" + serviceId);
                                }
                            } else {
                                alert = "Ban gui sai dau so dich vu.De nhan duoc bo suu tap nay hay soan: " + info +
                                        " gui den 8784.\nDT ho tro 1900561577.";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong serviceId");
                            }
                        } else {
                            if (status != 0) {
                                if (commandCode.equals("STA") || commandCode.equals("THA") || commandCode.equals("TNP") || commandCode.equals("BNA") || commandCode.equals("DPA")) {
                                    alert =
                                            "Ma so ban gui khong chinh xac,hay soan " + commandCode +
                                            " <MaSo> gui 8784 de nhan duoc noi dung ban yeu cau." + enter +
                                            "DT ho tro 1900561577";
                                } else {
                                    alert =
                                            "Ma so ban gui khong chinh xac,hay soan " + commandCode +
                                            " <MaSo> gui 8784 de nhan duoc noi dung ban yeu cau.De biet MaSo Hot nhat soan HOT gui 8584" + enter +
                                            "DT ho tro 1900561577";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong format:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (commandCode.equals("CM")) {
                        String idFull = subCode1;

                        if ((!StringTool.isNumberic(subCode1)) || (subCode1.length() < 3)) {
                            subCode1 = "";
                        } else {
                            prefixCode = subCode1.substring(0, 3);
                            dataType = subCode1.substring(1, 3);
                            subCode1 = subCode1.substring(3);
                        }

                        String phoneNumberFriend = subCode2;
                        phoneNumberFriend = Utilities.rebuildUserId(phoneNumberFriend);
                        String mOperator = Utilities.buildMobileOperator(phoneNumberFriend);

                        if (subCode1.equals("") || mOperator.equals("")) {
                            if (status != 0) {
                                subCode1 = "";
                                alert =
                                        "Ma so ban gui khong chinh xac,hay soan DA <MaSo> gui 8384 de nhan duoc noi dung media cho di dong.De biet MaSo Hot nhat soan HOT gui 8584.\n" +
                                        "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound Content data:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else if (prefixCode.startsWith("814") || prefixCode.startsWith("812")) {
                            //Poly-TrueTone-Word
                            auth = dbTool.add2PushInfo(phoneNumberFriend, userId, new BigDecimal(subCode1), dataType);
                            dataName = dbTool.getDataName(dataType, new BigDecimal(subCode1));
                            dataStr = "[PTS] " + idFull + http + "push/" + dataName + "?auth=" + auth;
                            if (!auth.equals("")) {
                                if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) || (Preference.adminPhoneNumber.contains(userId))) {
                                    if (!phoneNumberFriend.equals(userId)) {
                                        alert = "Ban vua gui tang nhac chuong co ma so " + idFull + " toi so may " + phoneNumberFriend + "\n" +
                                                "De biet nhung bai nhac TOP soan TOP gui8384,nhung hinh anh HOT soan HOT gui8384.";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);

                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 0, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(phoneNumberFriend + "<==" + commandCode + ":" + serviceId + "<== " + userId);

                                        dataStr = "[PTS] " + userId + " Tangban " + http + "push/" + dataName + "?auth=" + auth;
                                        if (dbTool.sendMTEx(phoneNumberFriend, serviceId, mOperator, commandCode, 8, dataStr, 0, requestId, subCode1, subCode2,
                                                subCode3)) {
                                            MyLogger.log(phoneNumberFriend + "<==" + commandCode + ":" + serviceId + "<==" + userId);
                                        }
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(phoneNumberFriend + "<==" + commandCode + ":" + serviceId + "<== " + userId);
                                    }
                                } else {
                                    alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                            " gui den 8584.\nDT ho tro 1900561577.";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong serviceId");
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Ma so ban gui khong chinh xac,hay soan: " + commandCode +
                                            " <MaSo> <SoDTnhan> gui8584 de nhan GiaiDieuTinhYeu.De biet MaSo Hot nhat soan HOT gui 8584.\n" +
                                            "DT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound Content data:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        } else if (prefixCode.startsWith("815")) {
                            //Mono
                            dataStr = dbTool.getRingToneMono(new BigDecimal(subCode1));
                            if (!dataStr.equals("")) {
                                if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) || (Preference.adminPhoneNumber.contains(userId))) {
                                    if (!phoneNumberFriend.equals(userId)) {
                                        alert = "Ban vua gui tang nhac chuong co ma so " + idFull + " toi so may " + phoneNumberFriend + "\n" +
                                                "De biet nhung bai nhac TOP soan TOP gui8384,nhung hinh anh HOT soan HOT gui8384.";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);

                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 1, dataStr, 0, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(phoneNumberFriend + "<==" + commandCode + ":" + serviceId + "<== " + userId);

                                        if (dbTool.sendMTEx(phoneNumberFriend, serviceId, mOperator, commandCode, 1, dataStr, 0, requestId, subCode1, subCode2,
                                                subCode3)) {
                                            MyLogger.log(phoneNumberFriend + "<==" + commandCode + ":" + serviceId + "<==" + userId);
                                        }
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 1, dataStr, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(phoneNumberFriend + "<==" + commandCode + ":" + serviceId + "<== " + userId);
                                    }
                                } else {
                                    alert = "Ban gui sai dau so dich vu.De nhan duoc noi dung hay soan " + info +
                                            " gui den 8584.\nDT ho tro 1900561577.";

                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong serviceId");
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Ma so ban gui khong chinh xac,hay soan: " + commandCode +
                                            " <MaSo> <SoDTnhan> gui8584 de nhan GiaiDieuTinhYeu.De biet MaSo Hot nhat soan HOT gui 8584.\n" +
                                            "DT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound Content data:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Ma so ban gui khong chinh xac,hay soan: " + commandCode +
                                        " <MaSo> <SoDTnhan> gui8584 de nhan GiaiDieuTinhYeu.De biet MaSo Hot nhat soan HOT gui 8584.\n" +
                                        "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound Content data:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (commandCode.equals("STD") || commandCode.equals("BND")) {
                        commandId = "STD";
                        String receiver = subCode2;

                        String result = dbTool.getTXTData_1(commandId, subCode1);

                        if ((result != null) && (!result.equals(""))) {
                            if (serviceId.endsWith("8584") || serviceId.endsWith("8684") || serviceId.endsWith("8784") ||
                                    Preference.adminPhoneNumber.contains(userId)) {
                                result = result.trim();
                                result = result.replaceAll("o", "0");
                                result = result.replaceAll("O", "0");
                                if (Utilities.isValidUserId(receiver) && !receiver.equals(userId)) {
                                    //For friend
                                    alert = "Ban vua gui noi dung co Ma So " + subCode1 + " toi so may " + receiver +
                                            enter + "DT ho tro:1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    mobileOperatorFriend = Utilities.buildMobileOperator(receiver);

                                    StringTokenizer tk = new StringTokenizer(result, ",", false);
                                    //int iCount = 0;
                                    while (tk.hasMoreTokens()) {
                                        String idFull = tk.nextToken();
                                        prefixCode = idFull.substring(0, 3);
                                        dataType = idFull.substring(1, 3);
                                        String code = idFull.substring(3);

                                        if (prefixCode.startsWith("812") || prefixCode.startsWith("813") || prefixCode.startsWith("814")) {
                                            //Poly-TrueTone-Word
                                            auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                            dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                                            dataStr = "[PTS] " + userId + " Tangban " + http + "push/" + dataName + "?auth=" + auth;
                                            if (!auth.equals("")) {
                                                dbTool.sendMTEx(receiver, serviceId, mobileOperatorFriend, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                        subCode2, subCode3);
                                            }
                                        } else if (prefixCode.startsWith("824")) {
                                            //Photo
                                            auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                            dataStr = "[PTS] " + userId + " Tangban " + http + "push/" + idFull + ".gif?auth=" + auth;
                                            if (!auth.equals("")) {
                                                dbTool.sendMTEx(receiver, serviceId, mobileOperatorFriend, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                        subCode2, subCode3);
                                            }
                                        } else if (prefixCode.startsWith("831")) {
                                            //Java game
                                            auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                            dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                                            dataStr = "[PTS] " + userId + " Tangban " + http + "push/" + dataName + "?auth=" + auth;
                                            if (!auth.equals("")) {
                                                dbTool.sendMTEx(receiver, serviceId, mobileOperatorFriend, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                        subCode2, subCode3);
                                            }
                                        }
                                    } //end while

                                    MyLogger.log(receiver + "<==" + commandCode + "<==" + serviceId + "<==" + userId);
                                } else {
                                    //For self
                                    StringTokenizer tk = new StringTokenizer(result, ",", false);
                                    boolean deductMoney = false;
                                    //int iCount = 0;
                                    while (tk.hasMoreTokens()) {
                                        String idFull = tk.nextToken();
                                        prefixCode = idFull.substring(0, 3);
                                        dataType = idFull.substring(1, 3);
                                        String code = idFull.substring(3);

                                        if (prefixCode.startsWith("812") || prefixCode.startsWith("813") || prefixCode.startsWith("814")) {
                                            //Poly-TrueTone-Word
                                            auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                            dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                                            dataStr = "[PTS] " + idFull + http + "push/" + dataName + "?auth=" + auth;
                                            if (!auth.equals("")) {
                                                if (!deductMoney) {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 1, requestId, subCode1,
                                                            subCode2, subCode3);
                                                    deductMoney = true;
                                                } else {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                            subCode2, subCode3);
                                                }
                                            }
                                        } else if (prefixCode.startsWith("824")) {
                                            //Photo
                                            auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                            dataStr = "[PTS] " + idFull + http + "push/" + idFull + ".gif?auth=" + auth;
                                            if (!auth.equals("")) {
                                                if (!deductMoney) {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 1, requestId, subCode1,
                                                            subCode2, subCode3);
                                                    deductMoney = true;
                                                } else {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                            subCode2, subCode3);
                                                }
                                            }
                                        } else if (prefixCode.startsWith("831")) {
                                            //Java game
                                            auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                            dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                                            dataStr = "[PTS] " + idFull + http + "push/" + dataName + "?auth=" + auth;
                                            if (!auth.equals("")) {
                                                if (!deductMoney) {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 1, requestId, subCode1,
                                                            subCode2, subCode3);
                                                    deductMoney = true;
                                                } else {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                            subCode2, subCode3);
                                                }
                                            }
                                        }
                                    } //end while

                                    MyLogger.log(userId + "<==" + commandCode + ":" + serviceId);
                                }
                            } else {
//                                alert = "Ban gui sai dau so dich vu.De nhan noi dung theo yeu cau soan: " + info +
//                                        " gui den 8584" + enter + "DT ho tro 1900561577.";
                                alert = "Ban gui sai dau so dich vu,de tai nhac chuong theo yeu cau,hay soan:" + info + " gui den 8584" + enter +
                                        "DT ho tro 1900561577.";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong serviceId");
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Ma so ban gui khong chinh xac,hay soan " + commandCode +
                                        " <MaSo> gui 8584 de nhan Nhac Chuong Sieu Nhon." + enter +
                                        "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong format:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (commandCode.equals("XIN")) {
                        commandId = "XIN";
                        String result = dbTool.getTXTData_1(commandId, subCode1);
                        if ((result != null) && (!result.equals(""))) {
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) ||
                                    Preference.adminPhoneNumber.contains(userId)) {
                                result = result.trim();
                                result = result.replaceAll("o", "0");
                                result = result.replaceAll("O", "0");
                                //For self
                                StringTokenizer tk = new StringTokenizer(result, ",", false);
                                int ii = tk.countTokens();
                                Vector tkV = new Vector();
                                String idFlower = "";
                                while (tk.hasMoreTokens()) {
                                    tkV.add(tk.nextToken());
                                }

                                try {
                                    SecureRandom srd = SecureRandom.getInstance("SHA1PRNG", "SUN");
                                    idFlower = (String) tkV.elementAt(srd.nextInt(ii));
                                    String dataType = "24";
                                    String auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(idFlower.substring(3)), dataType);
                                    String dataStr = "[XinChuCauMay] " + idFlower + http + "push/" + idFlower + ".gif?auth=" + auth;
                                    if (!auth.equals("")) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + "<==" + userId);
                                    } else {
                                        System.out.println("Auth in XIN CHU Null");
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            } else {
                                alert = "Ban gui sai dau so dich vu.Xin Chu Cau May soan: " + info +
                                        " gui den 8584.\nDT ho tro 1900561577.";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong serviceId");
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Cu phap khong chinh xac,hay soan " + commandCode +
                                        " CHU gui8584 de nhan Chu Cau May.De biet MaSo Hot nhat soan HOT gui 8584.\n" +
                                        "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong format:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (commandCode.equals("TM") || commandCode.equals("DPT")) {
                        //TM CHUDE --> 8584
                        commandId = "TM";
                        String result = dbTool.getTXTData_1(commandId, subCode1);
                        if ((result != null) && (!result.equals(""))) {
                            if (serviceId.endsWith("8584") || serviceId.endsWith("8684") || serviceId.endsWith("8784") || (Preference.adminPhoneNumber.contains(userId))) {
                                StringTokenizer tk = new StringTokenizer(result, ",", false);
                                //int iCount = 0;
                                boolean deductMoney = false;
                                while (tk.hasMoreTokens()) {
                                    String idFull = tk.nextToken();
                                    prefixCode = idFull.substring(0, 3);
                                    dataType = idFull.substring(1, 3);
                                    String code = idFull.substring(3);
                                    auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                    dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                                    dataStr = "[PTS] " + idFull + http + "push/" + dataName + "?auth=" + auth;
                                    if (!auth.equals("")) {
                                        if (!deductMoney) {
                                            deductMoney = true;
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 1, requestId, subCode1, subCode2,
                                                    subCode3);
                                            MyLogger.log(userId + "<==" + commandCode + ":" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 0, requestId, subCode1, subCode2,
                                                    subCode3);
                                        }
                                    }
                                }
                            } else {
                                alert = "Ban gui sai dau so dich vu.De nhan duoc Theme hay soan: " + info +
                                        " gui8584.\nDT ho tro 1900561577.";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong serviceId");
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Chu de Theme ban gui khong chinh xac,hay soan " + commandCode +
                                        " <ChuDe> gui8584.De biet MaSo Hot nhat soan HOT gui 8584.\n" +
                                        "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound Content data:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    }
                } catch (Exception ex) {
                    //ex.printStackTrace();
                    if (status != 0) {
                        alert =
                                "Thong tin ban gui khong ro rang,tin nhan chi duoc dung Tieng Viet Khong Dau.De duoc ho tro xin goi 1900561577.Xin cam on.";
                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Wrong data:" + info);
                    } else {
                        dbTool.updateStatus(requestId, 1, mobileOperator);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void run() {
        MyLogger.log("Process Media Thread Start.");
        while (Main.running) {
            try {
                processRequestQueue("VIETEL");
                processRequestQueue("GPC");
                processRequestQueue("VMS");
                Thread.sleep(3000);
            } catch (Exception ex) {
            }
        }
    }
}
