package com.sms.process;

import java.util.*;
import com.sms.item.*;
import java.math.BigDecimal;
import java.security.SecureRandom;

import com.sms.common.*;

public class ProcessDailyService1 extends Thread {

    DBTool dbTool = null;
    DateProc dateProc = null;
    static String newLine = "\n";
    static char enter = (char) 10;
    String alert = "";
    String result = "";
    String infoFormated = "";
    String commandId = "";
    SmsReceiveQueue smsReceiveQueue = null;
    BigDecimal requestId = null;
    String userId = "";
    String serviceId = "";
    String commandCode = "";
    int status = 0;
    String info = "";
    String infoTmp = "";
    static String http = ":http://222.252.16.196/";

    public ProcessDailyService1() {
        dbTool = new DBTool();
        dateProc = new DateProc();
    }

    public void processRequestQueue(String mobileOperator) {
        Vector listRequest = dbTool.getRequestFromQueue(mobileOperator, Preference.dailyCode1);
        int index = 0;
        for (int i = 0; i < listRequest.size(); i++) {
            index = i;
            smsReceiveQueue = (SmsReceiveQueue) listRequest.elementAt(i);
            requestId = smsReceiveQueue.getId();
            userId = smsReceiveQueue.getUserId();
            serviceId = smsReceiveQueue.getServiceId();
            commandCode = smsReceiveQueue.getCommandCode();
            status = smsReceiveQueue.getStatus();
            info = (smsReceiveQueue.getInfo()).toUpperCase().trim();
            infoTmp = commandCode + " " + info.substring(commandCode.length(), info.length()).trim();

            String subCode1 = "";
            String subCode2 = "";
            String subCode3 = "";

            subCode1 = StringTool.getObjectString(infoTmp, 1);
            subCode2 = StringTool.getObjectString(infoTmp, 2);
            subCode3 = StringTool.getObjectString(infoTmp, 3);

            try {
                if (commandCode.equals("TH") || commandCode.equals("BN")) {
                    if (subCode1.equals("CS") || subCode1.equals("TP") || subCode1.equals("XX") || subCode1.equals("YY") || subCode1.equals("NGHE") || subCode1.equals("NS") || subCode1.equals("TA") || subCode1.equals("TE")) {
                        commandId = "XEM";
                        String ddMM = "";
                        ddMM = infoTmp.substring(commandCode.length()).trim();
                        ddMM = ddMM.substring(subCode1.length()).trim();

                        ddMM = ddMM.replaceAll("o", "0");
                        ddMM = ddMM.replaceAll("O", "0");
                        ddMM = StringTool.getContentOnlyNumber(ddMM);

                        if (ddMM.length() > 4) {
                            ddMM = ddMM.substring(0, 4);
                        }
                        if ((ddMM.length() == 0) || (ddMM.length() == 1)) {
                            if (status != 0) {
                                alert =
                                        "Ban soan tin sai cu phap " + commandCode + " " + subCode1 + " <NgayThangSinh> gui8584.Vi du " +
                                        commandCode + " " + subCode1 + " 2011.\nDT 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongFormat:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            //2-3-4
                            boolean trueFormat = false;
                            if (ddMM.length() == 2) {
                                String firstDigit = ddMM.substring(0, 1);
                                String secondDigit = ddMM.substring(1, 2);
                                if ((Integer.parseInt(firstDigit) > 0) && (Integer.parseInt(secondDigit) > 0)) {
                                    ddMM = "0" + firstDigit + "-" + "0" + secondDigit;
                                    trueFormat = true;
                                }
                            } else if (ddMM.length() == 3) {
                                String firstDigit = ddMM.substring(0, 1);
                                String secondDigit = ddMM.substring(1, 2);
                                String thirdDigit = ddMM.substring(2, 3);

                                if ((Integer.parseInt(firstDigit) > 0) && (Integer.parseInt(secondDigit + thirdDigit) <= 12) &&
                                        (Integer.parseInt(secondDigit + thirdDigit) > 0)) {
                                    // XYZ -> 0X-YZ
                                    ddMM = "0" + firstDigit + "-" + secondDigit + thirdDigit;
                                    trueFormat = true;
                                } else if ((!trueFormat) && (Integer.parseInt(firstDigit) == 0) && (Integer.parseInt(secondDigit) > 0) &&
                                        (Integer.parseInt(thirdDigit) > 0)) {
                                    //XYZ -> XY-0Z
                                    ddMM = firstDigit + secondDigit + "-" + "0" + thirdDigit;
                                    trueFormat = true;
                                } else if ((!trueFormat) && (Integer.parseInt(firstDigit + secondDigit) > 0) &&
                                        (Integer.parseInt(firstDigit + secondDigit) <= 31) &&
                                        (Integer.parseInt(thirdDigit) > 0)) {
                                    //XYZ -> XY-0Z
                                    ddMM = firstDigit + secondDigit + "-" + "0" + thirdDigit;
                                    trueFormat = true;
                                }
                            } else if (ddMM.length() == 4) {
                                if ((Integer.parseInt(ddMM.substring(0, 2)) > 0) && (Integer.parseInt(ddMM.substring(0, 2)) <= 31) &&
                                        (Integer.parseInt(ddMM.substring(2, 4)) > 0) && (Integer.parseInt(ddMM.substring(2, 4)) <= 12)) {
                                    ddMM = ddMM.substring(0, 2) + "-" + ddMM.substring(2, 4);
                                    trueFormat = true;
                                }
                            }
                            if (trueFormat) {
                                String chiemTinh = Utilities.getCungChiemTinh(ddMM);
                                result = dbTool.getTXTData_2(commandId, subCode1, chiemTinh);
                                if ((result != null) && !result.equals("")) {
                                    if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") &&
                                            !serviceId.endsWith("8484")) ||
                                            (Preference.adminPhoneNumber.contains(userId))) {
                                        if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                            result = StringTool.splitMsg(result);
                                        }

                                        boolean deductMoney = false;
                                        result = result.replaceAll("!xxx!", "|");
                                        StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                        int stTokenLen = stToken.countTokens();
                                        for (int ii = 0; ii < stTokenLen; ii++) {
                                            String txtSms = stToken.nextToken();
                                            if (!deductMoney) {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                        txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                                deductMoney = true;
                                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId);
                                            } else {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                        txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                            }
                                            if (ii > 5) {
                                                break;
                                            }
                                        }
                                    } else {
                                        alert =
                                                "Ban gui sai so dich vu,de nhan thong tin theo yeu cau hay soan: " + info +
                                                " gui8584" + enter + "DT ho tro 1900561577";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId);
                                    }
                                } else {
                                    if (status != 0) {
                                        alert =
                                                "Xin loi ban,hien tai he thong chua y kien cua chuyen gia ve thong tin ban dua ra." + enter +
                                                "DT ho tro 1900561577";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId);
                                    } else {
                                        dbTool.updateStatus(requestId, 1, mobileOperator);
                                    }
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Ban soan tin sai cu phap.Hay soan " + commandCode +
                                            " <Cuphap> <NgayThangSinh> gui8584.Trong do <CuPhap> la CS,XX,YY,TP,TE..." +
                                            "Vi du " + commandCode + " CS 2011" + enter + "DT 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        }
                    } else if (subCode1.equals("TV") || subCode1.equals("NR") || subCode1.equals("GT") || subCode1.equals("MV")) {
                        commandId = "XEM";
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                    (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==TV<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                        " gui8584" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + subCode1 + "<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info + enter +
                                        "Moi soan: " + commandCode + " <CuPhap> <MaSo> gui8584,trong do CuPhap la TV,NR.." + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==TV<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.startsWith("DUYEN")) {
                        commandId = "XEM";
                        //DUYEN length is 5

                        String girlYear = "";
                        String boyYear = "";
                        girlYear = subCode2;
                        boyYear = subCode3;

                        if (!StringTool.isNumberic(girlYear) || (girlYear.length() != 4) || !StringTool.isNumberic(boyYear) || (boyYear.length() != 4)) {
                            if (status != 0) {
                                alert =
                                        "Ban soan tin sai cu phap cua Xem Duyen Hop tuoi.Hay soan " + commandCode +
                                        " DUYEN <NamSinhEm> <NamSinhAnh> gui8584,vi du " + commandCode + " DUYEN 1980 1978" + enter +
                                        "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            String lunarGirlYear = Utilities.getLunarYear(girlYear);
                            String lunarBoyYear = Utilities.getLunarYear(boyYear);
                            result = dbTool.getTXTData_4(commandId, subCode1, lunarGirlYear, lunarBoyYear);
                            if ((result != null) && !result.equals("")) {
                                if ((serviceId.endsWith("8184") || serviceId.endsWith("8284") || serviceId.endsWith("8384") || serviceId.endsWith("8484")) &&
                                        (!Preference.adminPhoneNumber.contains(userId))) {
                                    alert =
                                            "Ban gui sai so dich vu,nhan thong tin Xem Duyen Hop Tuoi theo yeu hay soan " + info +
                                            " gui 8584\nDT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "DUYEN<==" + serviceId + ":WrongServiceId:" + info);
                                } else {
                                    String girlThapCan = Utilities.getLunarThapCan(girlYear);
                                    String boyThapCan = Utilities.getLunarThapCan(boyYear);
                                    result = girlThapCan + " " + lunarGirlYear + "-" + boyThapCan + " " + lunarBoyYear + "\n" + result;

                                    boolean deductMoney = false;
                                    if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                        result = StringTool.splitMsg(result);
                                    }
                                    result = result.replaceAll("!xxx!", "|");
                                    StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                    int stTokenLen = stToken.countTokens();
                                    for (int ii = 0; ii < stTokenLen; ii++) {
                                        String txtSms = stToken.nextToken();
                                        if (!deductMoney) {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                            deductMoney = true;
                                            MyLogger.log(userId + "<==" + commandCode + "<==" + subCode1 + ":" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                        }
                                        if (ii > 10) {
                                            break;
                                        }
                                    }
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Xin loi ban,hien tai he thong chua co tu van chuyen gia ve Xem Duyen voi 2 nam sinh ban dua ra.\nDT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "DUYEN<==" + serviceId + ":NotFound:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        }
                    } else if (subCode1.equals("TK")) {
                        //Boi kieu
                        commandId = "XEM";
                        infoTmp = infoTmp.substring(commandCode.length()).trim();
                        infoTmp = infoTmp.substring(subCode1.length()).trim();
                        infoTmp = StringTool.getContentNoWhiteLetter(infoTmp);

                        if ((infoTmp != null) && (!infoTmp.equals("")) && StringTool.isNumberic(infoTmp)) {
                            result = "";
                        } else {
                            subCode2 = String.valueOf(Utilities.getValueFromString(infoTmp));
                            result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        }

                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            //result = StringTool.getContentOnlyCharEx(result);
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                    (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + subCode1 + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                        " gui8584\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + subCode1 + "<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info + enter +
                                        "Nhan thong tin BoiKieu soan: " + commandCode + " TK <TenAnh> <TenEm> gui8584" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("HT")) {
                        //XEM HT TY (Xem Hop tuoi)
                        commandId = "XEM";
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            result = result.replaceAll("\n", "|");
                            result = result.replace('|', enter);

                            result = StringTool.getContentOnlyCharEx(result);
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                    (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + subCode1 + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                        " gui 8584" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + subCode1 + "<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                        +enter + "Xem hoptuoi lam an soan: " + commandCode + " HT TenConGiap gui 8584.Vi du: " + commandCode +
                                        " HT TUAT gui8584" +
                                        enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("TEN")) {
                        commandId = "XEM";

                        infoTmp = infoTmp.substring(commandCode.length()).trim();
                        infoTmp = infoTmp.substring(subCode1.length()).trim();

                        if (infoTmp.equals("")) {
                            if (status != 0) {
                                alert =
                                        "Ban soan tin sai cu phap cua XEM TEN.Hay soan " + commandCode +
                                        " TEN <TenCuaBan> gui8584,vidu: " + commandCode + " TEN Nguyen Minh Nga" + enter +
                                        "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongFormat:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            int sumData = Utilities.sumData(infoTmp);
                            result = dbTool.getTXTData_5(commandId, subCode1, String.valueOf(sumData));
                            if ((result != null) && !result.equals("")) {
                                if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                        (Preference.adminPhoneNumber.contains(userId))) {
                                    if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                        result = StringTool.splitMsg(result);
                                    }

                                    boolean deductMoney = false;
                                    result = result.replaceAll("!xxx!", "|");
                                    StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                    int stTokenLen = stToken.countTokens();
                                    for (int ii = 0; ii < stTokenLen; ii++) {
                                        String txtSms = stToken.nextToken();
                                        if (!deductMoney) {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                            deductMoney = true;
                                            MyLogger.log(userId + "<==" + commandCode + " TEN<==" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                        }
                                        if (ii > 5) {
                                            break;
                                        }
                                    }
                                } else {
                                    alert =
                                            "Ban gui sai so dich vu,de nhan thong tin Xem Ten theo yeu cau hay soan " + info + " gui 8584" + enter +
                                            "DT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " TEN<==" + serviceId + ":WrongServiceId");
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Xin loi ban,hien tai he thong chua y kien cua chuyen gia ve Xem Ten voi thong tin ban dua ra.\nDT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " TEN<==" + serviceId + ":NotFound:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        }
                    } else if (subCode1.equals("SO") || subCode1.equals("XO")) {
                        try {
                            if (!StringTool.isNumberic(subCode2) || !StringTool.isNumberic(subCode3) || (subCode3.length() != 4)) {
                                if (status != 0) {
                                    alert =
                                            "Ban soan tin sai cu phap.Soan " + commandCode + " SO <SoCanXem> <NamSinh> gui8584,vi du " + commandCode +
                                            " SO 0904686868 1975" + enter + "DT 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " SO<==" + serviceId + ":WrongFormat:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            } else {
                                String content = "";
                                double tongDiem = 0;

                                String[] amDuong = Utilities.getAmDuong(subCode2);
                                content = "1.AmDuong" + "\n" + amDuong[0] + "\n" + "Diem:" + String.valueOf(amDuong[1]) + "/2\n";
                                String[] thienThoi = Utilities.getThienThoi(subCode2);
                                content = content + "2.ThienThoi" + "\n" + thienThoi[0] + "\n" + "Diem:" + thienThoi[1] + "/0.5!xxx!";
                                String[] nguHanh = Utilities.getNguHanh(subCode2, subCode3);
                                content = content + "3.NguHanh" + "\n" + nguHanh[0] + "\n" + "Diem:" + nguHanh[1] + "/2.5!xxx!";
                                String[] quaiKhi = Utilities.getQuaiKhi(subCode2);
                                content = content + "4.QuaiKhi" + "\n" + quaiKhi[0] + "\n" + "Diem:" + quaiKhi[1] + "/3!xxx!";
                                String[] quanNiem = Utilities.getQuanNiem(subCode2);
                                content = content + "5.QuanNiem" + "\n" + quanNiem[0] + "\n" + "Diem:" + quanNiem[1] + "/2\n";
                                tongDiem = Double.parseDouble(amDuong[1]) + Double.parseDouble(thienThoi[1]) + Double.parseDouble(nguHanh[1]) +
                                        Double.parseDouble(quaiKhi[1]) + Double.parseDouble(quanNiem[1]);

                                if (tongDiem < 4) {
                                    content = content + "\n" + "Tong:" + String.valueOf(tongDiem) + "/10\n" + "Ket luan:Day so khong phu hop voi ban";
                                } else if (tongDiem < 5) {
                                    content = content + "\n" + "Tong:" + String.valueOf(tongDiem) + "/10\n" + "Ket luan:Day so kha binh thuong";
                                } else if (tongDiem < 6) {
                                    content = content + "\n" + "Tong:" + String.valueOf(tongDiem) + "/10\n" + "Ket luan:Day so tam duoc";
                                } else if (tongDiem < 8) {
                                    content = content + "\n" + "Tong:" + String.valueOf(tongDiem) + "/10\n" + "Ket luan:Ban dang so huu so kha dep";
                                } else if (tongDiem <= 10) {
                                    content = content + "\n" + "Tong:" + String.valueOf(tongDiem) + "/10\n" + "Ket luan:Ban dang so huu so rat dep";
                                }

                                //content = StringTool.splitMsg(content);
                                if (!content.equals("")) {
                                    if ((serviceId.endsWith("8084") || serviceId.endsWith("8184") || serviceId.endsWith("8284") || serviceId.endsWith("8384") ||
                                            serviceId.endsWith("8484")) &&
                                            (!Preference.adminPhoneNumber.contains(userId))) {
                                        alert =
                                                "Ban gui sai toi dau so dich vu.De xem so theo yeu cau,hay soan " + info + " gui 8584" + enter + "DT 1900561577";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + subCode1 + "<==" + serviceId + ":WrongServiceId");
                                    } else {
                                        if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                            result = StringTool.splitMsg(result);
                                        }

                                        boolean deductMoney = false;
                                        content = content.replaceAll("!xxx!", "|");
                                        StringTokenizer stToken = new StringTokenizer(content, "|", false);
                                        int stTokenLen = stToken.countTokens();
                                        for (int ii = 0; ii < stTokenLen; ii++) {
                                            String txtSms = stToken.nextToken();
                                            if (!deductMoney) {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                        txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                                deductMoney = true;
                                                MyLogger.log(userId + "<==" + commandCode + "SO<==" + serviceId);
                                            } else {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                        txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                            }
                                            if (ii > 5) {
                                                break;
                                            }
                                        }
                                    }
                                } else {
                                    alert =
                                            "Hien tai chua co y kien chuyen gia ve day so ma ban xem.Soan " + commandCode +
                                            " SO <SoCanXem> <NamSinh> gui 8584,vi du " + commandCode + " SO 0904686868 1975.\nDT 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " SO<==" + serviceId + ":Content empty:" + info);
                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                } else if (commandCode.equals("SNA") || commandCode.equals("SNU")) {
                    //Dat ten con theo chu cai
                    commandId = commandCode;
                    result = dbTool.getTXTData_1(commandId, subCode1);
                    if ((result != null) && !result.equals("")) {
                        result = result.replaceAll("\r\n", "\n");
                        //result = StringTool.getContentOnlyCharEx(result);
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484") &&
                                !serviceId.endsWith("8584")) ||
                                (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert =
                                    "Ban gui sai so dich vu, de nhan thong tin tu van dat ten con,soan tin:" + info + " gui8684" + enter +
                                    "DT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==GT<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                    "\nDat ten con: " + commandCode + " <ChuCai> gui8684" + enter + "DT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==GT<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("CA")) {
//                    if (commandCode.equals("STG")) {
//                        result = dbTool.getTXTData_1("GIA", "TYGIA");
//                    } else {
//                        result = dbTool.getTXTData_1("CA", "T4");
//                    }

                    result = dbTool.getTXTData_1("CA", "T4");
                    if ((result != null) && !result.equals("")) {
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,de nhan thong tin theo yeu cau soan: " + info +
                                    " gui8584\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        alert =
                                "Xin loi ban,hien tai he thong chua co thong tin ban yeu cau\nDT ho tro 1900561577";
                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                    }
                } else if (commandCode.equals("STG") || commandCode.equals("SGV") || commandCode.equals("BNV") || commandCode.equals("BNT")) {
                    if (commandCode.equals("SGV") || commandCode.equals("BNV")) {
                        result = dbTool.getTXTData_1("GIA", "VANG");
                    } else if (commandCode.equals("STG") || commandCode.equals("BNT")) {
                        result = dbTool.getTXTData_1("GIA", "TYGIA");
                    }

                    if ((result != null) && !result.equals("")) {
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484") &&
                                !serviceId.endsWith("8584")) || ((commandCode.equals("BNV") || commandCode.equals("BNT")) && (serviceId.endsWith("8584") || serviceId.endsWith("8684") || serviceId.endsWith("8784"))) ||
                                (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            if (commandCode.equals("BNV") || commandCode.equals("BNT")) {
                                if (commandCode.equals("BNV")) {
                                    alert =
                                            "Ban gui sai so dich vu,de nhan thong tin GiaVang  theo yeu cau hay soan: " + commandCode + " gui8684" + enter +
                                            "DT ho tro: 1900561577.";
                                }

                                if (commandCode.equals("BNT")) {
                                    alert = "Ban gui sai so dich vu,de nhan thong tin  TYGIA theo yeu cau hay soan: " + commandCode + " BNT gui8684" + enter +
                                            "DT ho tro: 1900561577.";
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin theo yeu cau hay soan " + info +
                                        " gui8684\nDT ho tro 1900561577";
                            }
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        alert =
                                "Xin loi ban,hien tai he thong chua co thong tin GIAVANG hoac TYGIA ban yeu cau\nDT ho tro 1900561577";
                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                    }
                } else if (commandCode.equals("SGT")) {
                    commandId = "SGT";
                    result = dbTool.getTXTData_1(commandId, subCode1);
                    if ((result != null) && !result.equals("")) {
                        result = result.replaceAll("\r\n", "\n");
                        //result = StringTool.getContentOnlyCharEx(result);
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                    " gui8584\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==GT<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                    "\nNhan thong tin gioitinh soan: " + commandCode + " <MaSo> gui8584" + enter + "DT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==GT<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } //                    else if (commandCode.equals("SDT")) {
                //                    //XEM SO
                //                    try {
                //                        if (!StringTool.isNumberic(subCode1) || !StringTool.isNumberic(subCode2) || (subCode2.length() != 4)) {
                //                            if (status != 0) {
                //                                alert =
                //                                        "Ban soan tin sai cu phap.Soan " + commandCode + " <SoCanXem> <NamSinh> gui8784,vi du " + commandCode +
                //                                        "0904686868 1975" + enter + "DT 1900561577";
                //                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                //                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                //                            } else {
                //                                dbTool.updateStatus(requestId, 1, mobileOperator);
                //                            }
                //                        } else {
                //                            String content = "";
                //                            double tongDiem = 0;
                //
                //                            String[] amDuong = Utilities.getAmDuong(subCode1);
                //                            content = "1.AmDuong" + "\n" + amDuong[0] + "\n" + "Diem:" + String.valueOf(amDuong[1]) + "/2\n";
                //                            String[] thienThoi = Utilities.getThienThoi(subCode2);
                //                            content = content + "2.ThienThoi" + "\n" + thienThoi[0] + "\n" + "Diem:" + thienThoi[1] + "/0.5!xxx!";
                //                            String[] nguHanh = Utilities.getNguHanh(subCode1, subCode2);
                //                            content = content + "3.NguHanh" + "\n" + nguHanh[0] + "\n" + "Diem:" + nguHanh[1] + "/2.5!xxx!";
                //                            String[] quaiKhi = Utilities.getQuaiKhi(subCode1);
                //                            content = content + "4.QuaiKhi" + "\n" + quaiKhi[0] + "\n" + "Diem:" + quaiKhi[1] + "/3!xxx!";
                //                            String[] quanNiem = Utilities.getQuanNiem(subCode1);
                //                            content = content + "5.QuanNiem" + "\n" + quanNiem[0] + "\n" + "Diem:" + quanNiem[1] + "/2\n";
                //                            tongDiem = Double.parseDouble(amDuong[1]) + Double.parseDouble(thienThoi[1]) + Double.parseDouble(nguHanh[1]) +
                //                                       Double.parseDouble(quaiKhi[1]) + Double.parseDouble(quanNiem[1]);
                //
                //                            if (tongDiem < 4) {
                //                                content = content + "\n" + "Tong:" + String.valueOf(tongDiem) + "/10\n" + "Ket luan:Day so khong phu hop voi ban";
                //                            } else if (tongDiem < 5) {
                //                                content = content + "\n" + "Tong:" + String.valueOf(tongDiem) + "/10\n" + "Ket luan:Day so kha binh thuong";
                //                            } else if (tongDiem < 6) {
                //                                content = content + "\n" + "Tong:" + String.valueOf(tongDiem) + "/10\n" + "Ket luan:Day so tam duoc";
                //                            } else if (tongDiem < 8) {
                //                                content = content + "\n" + "Tong:" + String.valueOf(tongDiem) + "/10\n" + "Ket luan:Ban dang so huu so kha dep";
                //                            } else if (tongDiem <= 10) {
                //                                content = content + "\n" + "Tong:" + String.valueOf(tongDiem) + "/10\n" + "Ket luan:Ban dang so huu so rat dep";
                //                            }
                //
                //                            //content = StringTool.splitMsg(content);
                //                            if (!content.equals("")) {
                //                                if (!serviceId.endsWith("8784") && !Preference.adminPhoneNumber.contains(userId)) {
                //                                    alert =
                //                                            "Ban gui sai toi dau so dich vu.Hay soan " + info + " gui 8784.\nDT 1900561577";
                //                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                //                                    MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongServiceId");
                //                                } else {
                //                                    if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                //                                        result = StringTool.splitMsg(result);
                //                                    }
                //
                //                                    boolean deductMoney = false;
                //                                    content = content.replaceAll("!xxx!", "|");
                //                                    StringTokenizer stToken = new StringTokenizer(content, "|", false);
                //                                    int stTokenLen = stToken.countTokens();
                //                                    for (int ii = 0; ii < stTokenLen; ii++) {
                //                                        String txtSms = stToken.nextToken();
                //                                        if (!deductMoney) {
                //                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                //                                                    txtSms, 1, requestId, subCode1, subCode2, subCode3);
                //                                            deductMoney = true;
                //                                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                //                                        } else {
                //                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                //                                                    txtSms, 0, requestId, subCode1, subCode2, subCode3);
                //                                        }
                //                                        if (ii > 5)
                //                                            break;
                //                                    }
                //                                }
                //                            } else {
                //                                alert =
                //                                        "Hien tai chua co y kien chuyen gia ve day so ma ban xem.Soan " + commandCode +
                //                                        " <SoCanXem> <NamSinh> gui 8784,vi du " + commandCode + " SO 0904686868 1975.\nDT 1900561577";
                //                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                //                                MyLogger.log(userId + "<==" + commandCode + " SO<==" + serviceId + ":Content empty:" + info);
                //                            }
                //                        }
                //                    } catch (Exception ex) {
                //                        ex.printStackTrace();
                //                    }
                //                }
                else if (commandCode.equals("STX")) {
                    commandId = "XEM";
                    //DUYEN length is 5

                    String girlYear = "";
                    String boyYear = "";
                    girlYear = subCode1;
                    boyYear = subCode2;

                    if (!StringTool.isNumberic(girlYear) || (girlYear.length() != 4) || !StringTool.isNumberic(boyYear) || (boyYear.length() != 4)) {
                        if (status != 0) {
                            alert =
                                    "Ban soan tin sai cu phap cua Xem Duyen Hop tuoi.Hay soan " + commandCode +
                                    " <NamSinhEm> <NamSinhAnh> gui8584,vi du " + commandCode + " 1980 1978" + enter +
                                    "DT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    } else {
                        String lunarGirlYear = Utilities.getLunarYear(girlYear);
                        String lunarBoyYear = Utilities.getLunarYear(boyYear);
                        result = dbTool.getTXTData_4(commandId, "DUYEN", lunarGirlYear, lunarBoyYear);
                        if ((result != null) && !result.equals("")) {
                            if ((serviceId.endsWith("8184") || serviceId.endsWith("8284") || serviceId.endsWith("8384") || serviceId.endsWith("8484")) &&
                                    (!Preference.adminPhoneNumber.contains(userId))) {
                                alert =
                                        "Ban gui sai so dich vu,nhan thong tin Xem Duyen Hop Tuoi theo yeu hay soan " + info +
                                        " gui 8584\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                            } else {
                                String girlThapCan = Utilities.getLunarThapCan(girlYear);
                                String boyThapCan = Utilities.getLunarThapCan(boyYear);
                                result = girlThapCan + " " + lunarGirlYear + "-" + boyThapCan + " " + lunarBoyYear + "\n" + result;

                                boolean deductMoney = false;
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 10) {
                                        break;
                                    }
                                }
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi ban,hien tai he thong chua co tu van chuyen gia ve Xem Duyen voi 2 nam sinh ban dua ra.\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    }
                } else if (commandCode.equals("STM") || commandCode.equals("BNM")) {
                    //XEM MV
                    commandId = "STM";
                    result = dbTool.getTXTData_1(commandId, subCode1);
                    if ((result != null) && !result.equals("")) {
                        result = result.replaceAll("\r\n", "\n");
                        //result = StringTool.getContentOnlyCharEx(result);
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                (commandCode.equals("STM") && (serviceId.endsWith("8684") || serviceId.endsWith("8784"))) ||
                                (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            if (commandCode.equals("STM")) {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin tu van theo yeu cau, soan: " + info + " gui8684" + enter + "DT ho tro 1900561577";
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin tu van theo yeu cau, soan: " + info + " gui8584" + enter + "DT ho tro 1900561577";
                            }
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            if (commandCode.equals("STM")) {
                                alert =
                                        "Ban soan sai dinh dang, moi soan " + commandCode + " <MaSo> gui8684" +
                                        enter + "DT ho tro 1900561577";
                            } else {
                                alert =
                                        "Ban soan sai dinh dang, moi soan " + commandCode + " <MaSo> gui8584" +
                                        enter + "DT ho tro 1900561577";
                            }
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("DPM")) {
                    //XEM MV
                    commandId = "MV";
                    result = dbTool.getTXTData_1(commandId, subCode1);
                    if ((result != null) && !result.equals("")) {
                        result = result.replaceAll("\r\n", "\n");
                        //result = StringTool.getContentOnlyCharEx(result);
                        if ((serviceId.indexOf("8184,8284,8384,8484") == -1) ||
                                (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,de nhan thong tin tu van theo yeu cau, soan: " + info + " gui8584" + enter + "DT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Ban soan sai dinh dang, moi soan " + commandCode + " <MaSo> gui8584" +
                                    enter + "DT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("GN") || commandCode.equals("GNA") || commandCode.equals("NHO")) {
                    //GN,GNA,NHO
                    commandId = "GN";
                    result = dbTool.getTXTData_1(commandId, mobileOperator);
                    if ((result != null) && !result.equals("")) {
                        result = result.replaceAll("\r\n", "\n");
                        //result = StringTool.getContentOnlyCharEx(result);
                        if (!serviceId.endsWith("8184") || (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }
                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert = "Ban gui sai so dich vu,hay soan " + info + " gui 8384 de duoc huong dan cai dat goi nho\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    }
                } else if (commandCode.equals("DG")) {
                    commandId = commandCode;
                    if (StringTool.isNumberic(subCode2)) {
                        result = dbTool.getTXTData_7(commandId, subCode1);
                        if ((result == null) || (result.equals(""))) {
                            if (status != 0) {
                                alert =
                                        "San pham ban tra gia da het han Dau gia hoac tin nhan ban soan sai,moi soan " + commandCode +
                                        " <MaSanPham> <GiaTien> gui8484" +
                                        enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384")) ||
                                    (Preference.adminPhoneNumber.contains(userId))) {
                                result = result.replaceAll("xxx", subCode1);
                                result = result.replaceAll("yyy", subCode2);
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }
                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert = "Ban gui sai so dich vu,hay soan " + info + " gui 8484 de tra gia cho san pham DauGia\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Ban soan sai dinh dang, moi soan " + commandCode + " <MaSanPham> <GiaTien> gui8484" +
                                    enter + "DT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("DK") || commandCode.equals("BNK")) {
                    commandId = "DK";
                    if ((subCode1 == null) || (subCode1.equals(""))) {
                        result = dbTool.getTXTData(commandId);
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384")) ||
                                (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }
                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert = "Ban gui sai so dich vu,hay soan " + info + " gui 8484 de biet The le cuoc thi Hoa khoi Trang suc 2009." + enter + "DT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Ban soan sai dinh dang, moi soan " + commandCode + " gui8484 de biet the le cuoc thi" +
                                    enter + "DT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("SBT") || commandCode.equals("BTL")) {
                    commandId = commandCode;
                    result = dbTool.getTXTData(commandId);
                    if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                        result = StringTool.splitMsg(result);
                    }
                    result = result.replaceAll("!xxx!", "|");
                    StringTokenizer stToken = new StringTokenizer(result, "|", false);
                    int stTokenLen = stToken.countTokens();

                    if (serviceId.endsWith("8784") || serviceId.endsWith("8684") || (Preference.adminPhoneNumber.contains(userId))) {

                        if ((subCode1 == null) || (subCode1.equals("")) || (Utilities.formatUserId(subCode1, 0)).equals(userId)) {
                            boolean deductMoney = false;
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 10) {
                                    break;
                                }
                            }
                        } else {
                            if (Utilities.isValidUserId(subCode1)) {

                                alert = "Ban vua gui loi khuyen Bo Thuoc La toi " + subCode1 + " cam on ban da su dung dv" + enter +
                                        "DT ho tro 1900561577.";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                        alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(Utilities.isValidUserId(subCode1) + "<==" + commandCode + "<==" + userId);

                                String receiver = Utilities.formatUserId(subCode1, 0);
                                String mobileOperatorReceiver = Utilities.buildMobileOperator(Utilities.formatUserId(subCode1, 0));

                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    dbTool.sendMTEx(receiver, serviceId, mobileOperatorReceiver, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);

                                    if (ii > 5) {
                                        break;
                                    }
                                }

                            } else {
                                if (status != 0) {
                                    alert =
                                            "Ban soan sai dinh dang,moi soan " + commandCode + " hoac " + commandCode + " <SDT NguoiNhan> gui8784 nhan thong tin Bo Thuoc La" +
                                            enter + "DT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        }
                    } else {
//                        alert = "Ban gui sai so dich vu,moi soan " + commandCode + " hoac " + commandCode + " <SDT NguoiNhan> gui8784 nhan thong tin Bo Thuoc La" + enter + "DT ho tro 1900561577";
                        alert = "Ban gui sai so dich vu,de nhan thong tin tu van bo thuoc la,soan tin: " + commandCode + " gui8784." + enter + "DT ho tro: 190051577.";
                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                    }
                } else if (commandCode.equals("NQ") || commandCode.equals("DPN") || commandCode.equals("TNQ") || commandCode.equals("HNQ") || commandCode.equals("QNQ")) {
                    //          NQ DD/MM
                    //          NQ SDT DD/MM
                    String ddMM = "";
                    if ((subCode3 == null) || (subCode3.equals(""))) {
                        if (subCode1.length() < 2) {
                            subCode1 = "0" + subCode1;
                        }
                        if (subCode2.length() < 2) {
                            subCode2 = "0" + subCode2;
                        }
                        ddMM = subCode1 + "/" + subCode2;
                        // NQ DD/MM
                        if (!StringTool.isNumberic(subCode1) || (Integer.parseInt(subCode1) > 31) ||
                                !StringTool.isNumberic(subCode2) || (Integer.parseInt(subCode2) > 12) || !dbTool.checkDateTime(ddMM)) {
                            if (status != 0) {
                                alert =
                                        "Ban soan sai dinh dang,moi soan " + commandCode + " <NgayThangSinh> gui8584.Vi du " + commandCode + " 22/06 gui8584" +
                                        enter + "DT ho tro 1900561577.";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            if (serviceId.equals("8584") || serviceId.equals("8684") || serviceId.equals("8784") || (Preference.adminPhoneNumber.contains(userId))) {
                                if (ddMM.equals(DateProc.getCurrentDDMM())) {
                                    alert = "Chuc mung sinh nhat ban!Tong dai 8X84 gui tang ban hoa va bai hat mung sinh nhat ban.Chuc ban luon hanh phuc va dat duoc moi uoc mo trong cuoc song.";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                    if (commandCode.equals("NQ")) {
                                        alert = "De gui Hoa va loi chuc den nguoi than,soan tin: " + commandCode +
                                                " SDTnhan Ngaysinhnguoinhan gui8584.Kham pha bi an ten ban,soan: XEM TEN HotenBan gui 8384.DT ho tro:190056157.";
                                    } else {
                                        alert = "De gui Hoa va loi chuc den nguoi than,soan tin: " + commandCode +
                                                " SDTnhan Ngaysinhnguoinhan gui8584.DT ho tro:190056157.";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 0, requestId, subCode1, subCode2, subCode3);

                                    commandId = "NQ";
                                    String prefixCode = "";
                                    String dataType = "";
                                    String code = "";
                                    String auth = "";
                                    String dataStr = "";
                                    String dataName = "";
                                    String result = dbTool.getTXTData(commandId);
                                    StringTokenizer tk = new StringTokenizer(result, ",", false);
                                    while (tk.hasMoreTokens()) {
                                        String idFull = tk.nextToken();
                                        prefixCode = idFull.substring(0, 3);
                                        dataType = idFull.substring(1, 3);
                                        code = idFull.substring(3);

                                        if (prefixCode.startsWith("812") || prefixCode.startsWith("813") || prefixCode.startsWith("814")) {
                                            //Poly-TrueTone-Word
                                            auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                            dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                                            dataStr = "[PTS] " + idFull + http + "push/" + dataName + "?auth=" + auth;
                                            if (!auth.equals("")) {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                        subCode2, subCode3);

                                            }
                                        } else if (prefixCode.startsWith("824")) {
                                            //Photo
                                            auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                            dataStr = "[PTS] " + idFull + http + "push/" + idFull + ".gif?auth=" + auth;
                                            if (!auth.equals("")) {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                        subCode2, subCode3);
                                            }
                                        }
                                    }
                                } else {
                                    //Add to db and alert to userId
                                    if (dbTool.addQueueWaiting(requestId, userId, userId, serviceId, commandCode, dateProc.String2Timestamp(ddMM))) {
                                        if (commandCode.equals("NQ")) {
                                            alert =
                                                    "Cam on ban da su dung dich vu.Ban se nhan duoc mon qua dac biet tu tong dai 8X84 trong ngay sinh nhat.Gui tang,soan:NQ SDTnhan Ngay/Thangsinhnguoinhan gui8584";
                                        } else {
                                            alert =
                                                    "Cam on ban da tham gia chuong trinh 'Tiet lo ngay sinh-Rinh ve qua tang' ban se nhan duoc mon qua dac biet,thu vi tu tong dai 8X84 trong ngay sinh nhat.";
                                        }
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2,
                                                subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                        if (commandCode.equals("NQ")) {
                                            alert =
                                                    "Kham pha tinh cach ban qua cai ten,soan: XEM TEN HotenBan gui8384/Bi an ngay sinh cua ban,soan: XEM NS Ngay/thang sinh gui 8384" + enter + "DT ho tro:1900561577.";
                                        } else {
                                            alert =
                                                    "De gui Hoa va loi chuc den nguoi than,soan tin: " + commandCode + " SDTnhan Ngaysinhnguoinhan gui8584." +
                                                    enter + "DT ho tro 1900561577.";

                                        }
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 0, requestId, subCode1, subCode2,
                                                subCode3);
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai dau so dich vu,hay soan " + info + " gui 8584" + enter + "DT ho tro 1900561577.";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            }
                        }
                    } else {
                        if (subCode2.length() < 2) {
                            subCode2 = "0" + subCode2;
                        }
                        if (subCode3.length() < 2) {
                            subCode3 = "0" + subCode3;
                            //  NQ SDT DD/MM
                        }
                        ddMM = subCode2 + "/" + subCode3;
                        if (!Utilities.isValidUserId(subCode1) || !StringTool.isNumberic(subCode2) || (Integer.parseInt(subCode2) > 31) ||
                                !StringTool.isNumberic(subCode3) || (Integer.parseInt(subCode3) > 12) || !dbTool.checkDateTime(ddMM)) {
                            if (status != 0) {
                                alert =
                                        "Ban soan sai dinh dang,moi soan " + commandCode + " <SDTNhan> <NgaySinhNguoiNhan> gui8584.Vi du " + commandCode + " 0912121212 22/06 gui8584" +
                                        enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            if (serviceId.equals("8584") || serviceId.equals("8684") || serviceId.equals("8784") || (Preference.adminPhoneNumber.contains(userId))) {
                                if (ddMM.equals(DateProc.getCurrentDDMM())) {
                                    if (commandCode.equals("NQ")) {
                                        alert =
                                                "Ban vua gui loi chuc va hoa mung sinh nhat den so dien thoai " + subCode1 +
                                                ".De xem bi mat ten ban, soan tin: XEM TEN Ho ten ban gui 8384.DT ho tro: 1900561577";
                                    } else {
                                        alert =
                                                "Ban vua gui loi chuc va hoa mung sinh nhat den so dien thoai " + subCode1 + ".Cam on ban da su dung DV" + enter +
                                                ".DT ho tro: 1900561577";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(subCode1 + "<==" + commandCode + "<==" + userId + ":" + serviceId);

                                    //==> for receiver
                                    String receiver = Utilities.formatUserId(subCode1, 0);
                                    String mobileOperatorReceiver = Utilities.buildMobileOperator(receiver);
                                    alert = "So dien thoai " + Utilities.formatUserId(userId, 1) + " gui loi chuc mung sinh nhat va hoa den ban.Chuc ban luon vui ve va hanh phuc trong cuoc song!";
                                    dbTool.sendMTEx(receiver, serviceId, mobileOperatorReceiver, commandCode, 0, alert, 0, requestId, subCode1, subCode2, subCode3);

                                    commandId = "NQ";
                                    String prefixCode = "";
                                    String dataType = "";
                                    String code = "";
                                    String auth = "";
                                    String dataStr = "";
                                    String dataName = "";
                                    String result = dbTool.getTXTData(commandId);
                                    StringTokenizer tk = new StringTokenizer(result, ",", false);
                                    while (tk.hasMoreTokens()) {
                                        String idFull = tk.nextToken();
                                        prefixCode = idFull.substring(0, 3);
                                        dataType = idFull.substring(1, 3);
                                        code = idFull.substring(3);

                                        if (prefixCode.startsWith("812") || prefixCode.startsWith("813") || prefixCode.startsWith("814")) {
                                            //Poly-TrueTone-Word
                                            auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                            dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                                            dataStr = "[PTS] " + idFull + http + "push/" + dataName + "?auth=" + auth;
                                            if (!auth.equals("")) {
                                                dbTool.sendMTEx(receiver, serviceId, mobileOperatorReceiver, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                        subCode2, subCode3);

                                            }
                                        } else if (prefixCode.startsWith("824")) {
                                            //Photo
                                            auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                            dataStr = "[PTS] " + idFull + http + "push/" + idFull + ".gif?auth=" + auth;
                                            if (!auth.equals("")) {
                                                dbTool.sendMTEx(receiver, serviceId, mobileOperatorReceiver, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                        subCode2, subCode3);
                                            }
                                        }
                                    }
                                } else {
                                    if (dbTool.addQueueWaiting(requestId, userId, Utilities.formatUserId(subCode1, 0), serviceId, commandCode, dateProc.String2Timestamp(ddMM))) {
                                        alert =
                                                "Ban vua gui loi chuc va hoa mung sinh nhat den so dien thoai " + subCode1 +
                                                ".De kham pha bi mat ten ban,soan tin: XEM TEN HoTenBan gui 8384.DT ho tro 1900561577";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(subCode1 + "<==" + commandCode + "<==" + userId + ":" + serviceId);
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai dau so dich vu,hay soan " + info + " gui 8584" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            }
                        }
                    }
                } else if (commandCode.equals("SDT")) {
                    subCode1 = subCode1 + subCode2;
                    subCode1 = subCode1.replaceAll(" ", "");
                    if ((subCode1.equals("")) || (subCode1.length() < 3)) {
                        if (status != 0) {
                            alert =
                                    "Ban soan tin sai cu phap,nhan diem thi va diem chuan,soan: SDT <Sobaodanh> gui8784.SoBD gom ca phan chu va phan so nhu tren phieu bao thi.DT ho tro:1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    } else {
                        String schoolName = dbTool.getSchoolName(subCode1.substring(0, 3));
                        if ((schoolName != null) && (!schoolName.equals(""))) {
                            if (serviceId.endsWith("8784") || (Preference.adminPhoneNumber.contains(userId))) {
                                if (dbTool.add2DiemThiWait(requestId, userId, serviceId, mobileOperator, commandCode, info, subCode1.substring(0, 3), 0, subCode1)) {
//                                    if (subCode1.length() == 3) {
//                                        alert = "Ban da dang ky nhan DiemChuan DH&CD,chung toi se gui ban ngay khi co kq tu bo GD&DT.De biet diem thi soan: DT <Sobaodanh> gui8584.DT ho tro: 1900561577";
//                                    } else if ((subCode1.length() > 3)) {
                                    alert = "Ban da dang ky nhan Diem thi va DiemChuan DH&CD thanh cong,chung toi se gui ban ngay khi co kq tu bo GD&DT" + enter + "DT ho tro: 1900561577";
//                                    } else {
//                                        alert = "Ban da dang ky nhan Diem thi thanh cong,chung toi se gui ban ngay khi co kq tu bo GD&DT.De biet diem chuan,soan: DT <Matruong> gui 8584 DT ho tro: 1900561577.";
//                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                }
                            } else {
//                                if (subCode1.length() == 3) {
//                                    //==>DiemChuan
//                                    alert =
//                                            "Sai dau so DV.Moi soan: " + info + " gui 8584 de lay Diem Chuan truong: " + schoolName + ".DT ho tro: 1900561577";
//                                } else {
                                alert =
                                        "Ban soan tin sai so dich vu,de nhan diem thi va diem chuan,soan tin: SDT <Sobaodanh> gui 8784.DT ho tro: 1900561577";
//                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Ban soan tin sai ma truong,de tra cuu ket qua diem thiva diem chuan,soan tin: SDT <Sobaodanh> gui 8784.DT ho tro: 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    }
                } else if (commandCode.equals("TNT")) {
                    subCode1 = subCode1 + subCode2;
                    subCode1 = subCode1.replaceAll(" ", "");
                    if ((subCode1.equals("")) || (subCode1.length() < 3)) {
                        if (status != 0) {
                            alert =
                                    "Ban soan tin sai cu phap,nhan diem thi va diem chuan,soan: TNT <Sobaodanh> gui8784.SoBD gom ca phan chu va phan so nhu tren phieu bao thi.DT ho tro:1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    } else {
                        String schoolName = dbTool.getSchoolName(subCode1.substring(0, 3));
                        if ((schoolName != null) && (!schoolName.equals(""))) {
                            if (serviceId.endsWith("8784") || (Preference.adminPhoneNumber.contains(userId))) {
                                if (dbTool.add2DiemThiWait(requestId, userId, serviceId, mobileOperator, commandCode, info, subCode1.substring(0, 3), 0, subCode1)) {
//                                    if (subCode1.length() == 3) {
//                                        alert = "Ban da dang ky nhan DiemChuan DH&CD,chung toi se gui ban ngay khi co kq tu bo GD&DT.De biet diem thi soan: DT <Sobaodanh> gui8584.DT ho tro: 1900561577";
//                                    } else if ((subCode1.length() > 3)) {
                                    alert = "Ban da dang ky nhan Diem thi va DiemChuan DH&CD thanh cong,chung toi se gui ban ngay khi co kq tu bo GD&DT" + enter + "DT ho tro: 1900561577";
//                                    } else {
//                                        alert = "Ban da dang ky nhan Diem thi thanh cong,chung toi se gui ban ngay khi co kq tu bo GD&DT.De biet diem chuan,soan: DT <Matruong> gui 8584 DT ho tro: 1900561577.";
//                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                }
                            } else {
//                                if (subCode1.length() == 3) {
//                                    //==>DiemChuan
//                                    alert =
//                                            "Sai dau so DV.Moi soan: " + info + " gui 8584 de lay Diem Chuan truong: " + schoolName + ".DT ho tro: 1900561577";
//                                } else {
                                alert =
                                        "Ban soan tin sai so dich vu,de nhan diem thi va diem chuan,soan tin: TNT <Sobaodanh> gui 8784.DT ho tro: 1900561577";
//                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Ban soan tin sai ma truong,de tra cuu ket qua diem thi va diem chuan,soan tin: SDT <Sobaodanh> gui 8784.DT ho tro: 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    }
                } else if (commandCode.equals("DC")) {
                    subCode1 = subCode1 + subCode2;
                    subCode1 = subCode1.replaceAll(" ", "");
                    if ((subCode1.equals("")) || (subCode1.length() < 3)) {
                        if (status != 0) {
                            alert =
                                    "Ban soan tin sai cu phap,de tra cuu ket qua diem chuan,soan: DC MaTruong gui 8584 .DT ho tro: 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    } else {
                        String schoolName = dbTool.getSchoolName(subCode1.substring(0, 3));
                        if ((schoolName != null) && (!schoolName.equals(""))) {
                            if (serviceId.endsWith("8484") || serviceId.endsWith("8584") || serviceId.endsWith("8684") || serviceId.endsWith("8784") || (Preference.adminPhoneNumber.contains(userId))) {
                                if (dbTool.add2DiemThiWait(requestId, userId, serviceId, mobileOperator, commandCode, info, subCode1.substring(0, 3), 0, subCode1)) {
//                                    if (subCode1.length() == 3) {
                                    alert = "Ban da dang ky nhan DiemChuan DH&CD,chung toi se gui ban ngay khi co kq tu bo GD&DT.De biet diem thi soan: DIEM <Sobaodanh> gui8584.DT ho tro: 1900561577";
//                                    } else if ((subCode1.length() > 3) && !serviceId.endsWith("8484")&& !serviceId.endsWith("8584")) {
//                                        alert = "Ban da dang ky nhan Diem thi va DiemChuan DH&CD thanh cong,chung toi se gui ban ngay khi co kq tu bo GD&DT" + enter + "DT ho tro: 1900561577";
//                                    } else {
//                                        alert = "Ban da dang ky nhan Diem thi thanh cong,chung toi se gui ban ngay khi co kq tu bo GD&DT" + enter + "DT ho tro: 1900561577";
//                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                }
                            } else {
//                                if (subCode1.length() == 3) {
                                //==>DiemChuan
                                alert =
                                        "Sai dau so DV.Moi soan: " + info + " gui 8584 de lay Diem Chuan truong: " + schoolName + ".DT ho tro: 1900561577";
//                                } else {
//                                    alert =
//                                            "Sai dau so DV.Moi soan: " + info + " gui 8584 de lay Diem Thi hoac gui 8684 de lay DiemThi va DiemChuan.DT ho tro: 1900561577";
//                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Ban soan tin sai ma truong,de tra cuu ket qua diem chuan,soan: DC MaTruong gui 8584.DT ho tro: 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    }
                } else if (commandCode.equals("CK")) {
                    subCode1 = subCode1.toUpperCase();
                    subCode2 = subCode2.toUpperCase();
                    subCode3 = subCode3.toUpperCase();
                    MyLogger.log("CK: subcode1 = " + subCode1 + " subCode2 = " + subCode2 + " subcode3 = " + subCode3);
                    if (subCode1.equals("")) {
                        if (status != 0) {
                            alert =
                                    "Ban soan tin sai cu phap,de tra cuu ket qua chung khoan,soan: CK MaCK gui 8184 .DT ho tro: 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    } else {
                        String ckCode = "";
                        if (subCode1.indexOf("TOP") >= 0) {
                            subCode1 = "TOP";
                            if (subCode2.equals("")) {
                                if (status != 0) {
                                    alert =
                                            "Ban soan tin sai cu phap,de tra cuu top ket qua chung khoan,soan: CK TOP TG hoac CK TOP GG gui 8384 .DT ho tro: 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        }
                        if ((!subCode1.endsWith("TT")) && (!subCode1.endsWith("HN")) && (!subCode1.equals("TOP")) && (!subCode1.equals("HCM"))) {
                            ckCode = dbTool.getCKName(subCode1);
                        } else {
                            ckCode = subCode1;
                        }
                        MyLogger.log("CK: getCKCode " + subCode1 + " --> " + ckCode);
                        if ((ckCode != null) && (!ckCode.equals(""))) {
                            //dang ky ck
                            if (subCode1.equalsIgnoreCase("HN") || subCode1.equalsIgnoreCase("HCM")) {
                                alert = dbTool.getCKByLocal(subCode1);
                                if (alert.equalsIgnoreCase("")) {
                                    alert = "Hien tai he thong chua co ket qua chung khoan " + subCode1 + ". De tra cuu chung khoan theo ngay soan CK " + ckCode + " gui 8584.DT ho tro: 1900561577";
                                }
                                boolean deductMoney = false;
                                alert = alert.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(alert, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==CK HN/HCM<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
//                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Process:" + info);
                            } else if (subCode1.equalsIgnoreCase("TT")) {
                                if (serviceId.endsWith("8384") || serviceId.endsWith("8484") || serviceId.endsWith("8584") || serviceId.endsWith("8684") || serviceId.endsWith("8784") || (Preference.adminPhoneNumber.contains(userId))) {
                                    Calendar cal = Calendar.getInstance();
                                    int h = cal.get(Calendar.HOUR);
                                    if ((h > 12) || (h < 7)) {
                                        alert = dbTool.getCKByCode(subCode2);
                                        if (alert.equalsIgnoreCase("")) {
                                            alert = "Hien tai he thong chua co ket qua chung khoan " + subCode2 + ". De tra cuu chung khoan theo ngay soan CK " + ckCode + " gui 8184.DT ho tro: 1900561577";
                                        }
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":KQ:" + info);
                                    } else {
                                        CkItem ckItem = new CkItem();
                                        ckItem.setCommandCode(commandCode);
                                        ckItem.setCompanyId(dbTool.getCKId(subCode2));
                                        ckItem.setInfo(subCode1 + " " + subCode2);
                                        ckItem.setMobileOperator(mobileOperator);
                                        ckItem.setRemain(3);
                                        ckItem.setRequestDate(requestId.toString());
                                        ckItem.setRequestId(requestId);
                                        ckItem.setServiceId(serviceId);
                                        ckItem.setUserId(userId);
                                        ckItem.setStatus(1);
                                        if (dbTool.add2CKDaily(ckItem)) {
                                            alert = "Ban se nhan duoc ket qua giao dich chung khoan 3 dot cua ngay hom nay.DT ho tro: 1900561577";
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    alert, 1, requestId, subCode1, subCode2, subCode3);
                                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                        }
                                    }
                                } else {
                                    alert =
                                            "Ban soan tin sai dau so,de tra cuu ket qua tuong thuat chung khoan,soan: CK TT MaCK gui 8384.DT ho tro: 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                                }
                            } else if (serviceId.endsWith("8284") || serviceId.endsWith("8384") || serviceId.endsWith("8484") || serviceId.endsWith("8584") || serviceId.endsWith("8684") || serviceId.endsWith("8784")) {
//                               || (Preference.adminPhoneNumber.contains(userId)) 
                                alert = dbTool.getCKByCode(ckCode);
                                //if (alert.equalsIgnoreCase("")) {
                                //    alert = "Hien tai he thong chua co ket qua chung khoan " + ckCode + ". De tra cuu chung khoan theo ngay soan CK " + ckCode + " gui 8584.DT ho tro: 1900561577";
                                //}
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":KQ:" + info);
                                int remain = 0;
                                if (serviceId.endsWith("8284")) {
                                    remain = 1;
                                } else if (serviceId.endsWith("8384")) {
                                    remain = 3;
                                } else if (serviceId.endsWith("8484")) {
                                    remain = 4;
                                } else if (serviceId.endsWith("8454")) {
                                    remain = 5;
                                } else {
                                    remain = 10;
                                }
                                CkItem ckItem = new CkItem();
                                ckItem.setCommandCode(commandCode);
                                ckItem.setCompanyId(dbTool.getCKId(ckCode));
                                ckItem.setInfo(ckCode);
                                ckItem.setMobileOperator(mobileOperator);
                                ckItem.setRemain(remain);
                                ckItem.setRequestDate(requestId.toString());
                                ckItem.setRequestId(requestId);
                                ckItem.setServiceId(serviceId);
                                ckItem.setUserId(userId);
                                ckItem.setStatus(1);
                                if (dbTool.add2CKDaily(ckItem)) {
                                    if (serviceId.equalsIgnoreCase("8284")) {
                                        alert = "Ban se nhan duoc ket qua giao dich chung khoan ngay tiep theo/Nhan ket qua cac phien giao dich trong ngay soan:CK TT Machungkhoan gui8384.CSKH:1900561577.";
                                    } else if (serviceId.equalsIgnoreCase("8384")) {
                                        alert = "Ban se nhan duoc ket qua giao dich chung khoan 3 ngay tiep theo.Nhan ket qua cac phien giao dich trong ngay soan:CK TT Machungkhoan gui8384.CSKH:1900561577.";
                                    } else if (serviceId.equalsIgnoreCase("8484")) {
                                        alert = "Ban se nhan duoc ket qua giao dich chung khoan 4 ngay tiep theo.Nhan ket qua cac phien giao dich trong ngay soan:CK TT Machungkhoan gui8384.CSKH:1900561577.";
                                    } else if (serviceId.equalsIgnoreCase("8584")) {
                                        alert = "Ban se nhan duoc ket qua giao dich chung khoan 5 ngay tiep theo.Nhan ket qua cac phien giao dich trong ngay soan:CK TT Machungkhoan gui8384.CSKH:1900561577.";
                                    } else {
                                        alert = "Ban se nhan duoc ket qua giao dich chung khoan 10 ngay tiep theo.Nhan ket qua cac phien giao dich trong ngay soan:CK TT Machungkhoan gui8384.CSKH:1900561577.";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 0, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                }
                            } else if (subCode1.equalsIgnoreCase("TOP")) {
                                //co phai top ko
                                if (subCode2.equalsIgnoreCase("TG")) {
                                    alert = dbTool.getCKByTG();
                                    if (alert.equalsIgnoreCase("")) {
                                        alert = "Hien tai he thong chua co ket qua chung khoan tang nhieu nhat. De tra cuu chung khoan theo ngay soan CK " + ckCode + " gui 8584.DT ho tro: 1900561577";
                                    }
                                    boolean deductMoney = false;
                                    alert = alert.replaceAll("!xxx!", "|");
                                    StringTokenizer stToken = new StringTokenizer(alert, "|", false);
                                    int stTokenLen = stToken.countTokens();
                                    for (int ii = 0; ii < stTokenLen; ii++) {
                                        String txtSms = stToken.nextToken();
                                        if (!deductMoney) {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                            deductMoney = true;
                                            MyLogger.log(userId + "<==CK<==" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                        }
                                        if (ii > 5) {
                                            break;
                                        }
                                    }
//                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Process TOP TG:" + info);
                                } else {
                                    alert = dbTool.getCKByGG();
                                    if (alert.equalsIgnoreCase("")) {
                                        alert = "Hien tai he thong chua co ket qua chung khoan giam nhieu nhat. De tra cuu chung khoan theo ngay soan CK " + ckCode + " gui 8584.DT ho tro: 1900561577";
                                    }
                                    boolean deductMoney = false;
                                    alert = alert.replaceAll("!xxx!", "|");
                                    StringTokenizer stToken = new StringTokenizer(alert, "|", false);
                                    int stTokenLen = stToken.countTokens();
                                    for (int ii = 0; ii < stTokenLen; ii++) {
                                        String txtSms = stToken.nextToken();
                                        if (!deductMoney) {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                            deductMoney = true;
                                            MyLogger.log(userId + "<==CK<==" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                        }
                                        if (ii > 5) {
                                            break;
                                        }
                                    }
                                }
                            } else {
                                //tra ket qua
                                alert = dbTool.getCKByCode(ckCode);
                                if (alert.equalsIgnoreCase("")) {
                                    alert = "Hien tai he thong chua co ket qua chung khoan " + ckCode + ". De tra cuu chung khoan theo ngay soan CK " + ckCode + " gui 8584.DT ho tro: 1900561577";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":KQ:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Ban soan tin sai ma chung khoan,de tra cuu ket qua chung khoan,soan: CK MaCK gui 8184.DT ho tro: 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    }
                } else if (commandCode.equals("CK5")) {
                    subCode1 = subCode1.toUpperCase();
                    subCode2 = subCode2.toUpperCase();
                    subCode3 = subCode3.toUpperCase();
                    MyLogger.log("CK5: subcode1 = " + subCode1 + " subCode2 = " + subCode2 + " subcode3 = " + subCode3);
                    if (subCode1.equals("")) {
                        if (status != 0) {
                            alert =
                                    "Ban soan tin sai cu phap,de tra cuu ket qua chung khoan 5 ngay gan nhat,soan: CK5 MaCK gui 8584 .DT ho tro: 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                    String ckCode = "";
                    ckCode = dbTool.getCKName(subCode1);
                    MyLogger.log("CK5: getCKCode " + subCode1 + " --> " + ckCode);
                    if ((ckCode != null) && (!ckCode.equals(""))) {
                        alert = dbTool.getCKByLastDay(ckCode);
                        System.out.println("CK5: alert = " + alert);
                        boolean deductMoney = false;
                        alert = alert.replaceAll("!xxx!", "|");
                        StringTokenizer stToken = new StringTokenizer(alert, "|", false);
                        int stTokenLen = stToken.countTokens();
                        for (int ii = 0; ii < stTokenLen; ii++) {
                            String txtSms = stToken.nextToken();
                            if (!deductMoney) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                        txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                deductMoney = true;
                                MyLogger.log(userId + "<==CK5<==" + serviceId);
                            } else {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                        txtSms, 0, requestId, subCode1, subCode2, subCode3);
                            }
                            if (ii > 5) {
                                break;
                            }
                        }
//                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":Process CK5:" + info);
                    } else {
                        if (status != 0) {
                            alert =
                                    "Ban soan tin sai ma chung khoan,de tra cuu ket qua chung khoan 5 ngay gan nhat ,soan: CK5 MaCK gui 8584.DT ho tro: 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("HR") || commandCode.equals("QNR")) {
                    commandId = "XEM";
                    subCode2 = subCode1;
                    subCode1 = "NR";

                    result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                    if ((result != null) && !result.equals("")) {
                        result = result.replaceAll("\r\n", "\n");
                        //result = StringTool.getContentOnlyCharEx(result);
                        if ((("8184,8284,8384,8484").indexOf(serviceId) == -1) ||
                                (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + " NR/NRN<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 10) {
                                    break;
                                }
                            }
                        } else {

                            alert =
                                    "Ban gui sai so dich vu,de nhan Bi mat not ruoi soan: " + info +
                                    " gui8584" + enter + "DT ho tro 1900561577";

                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongServiceId");
                        }
                    } else {
                        if (status != 0) {
                            if (commandCode.equals("HR") || commandCode.equals("QNR")) {
                                commandCode = "XEM";
                            }
                            alert =
                                    "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                    ".Bi mat not ruoi soan " + commandCode + " NR <MaSo> gui 8484,hoac " + commandCode +
                                    " NRN <MaSo> gui8484." + enter + "DT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==NR<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equalsIgnoreCase("TYGIA") || commandCode.equalsIgnoreCase("TIGIA") || commandCode.equalsIgnoreCase("HTG") || commandCode.equalsIgnoreCase("QTG")) {
                    commandId = "GIA";
                    subCode1 = "TYGIA";
                    //GIA TYGIA
                    result = dbTool.getTXTData_1(commandId, subCode1);
                    if ((result != null) && !result.equals("")) {
//                        if ((!serviceId.endsWith("8184")) || (Preference.adminPhoneNumber.contains(userId))||
                        if (Preference.adminPhoneNumber.contains(userId) ||
                                (!commandCode.equals("HTG") && !commandCode.equals("QTG") && (("8184").indexOf(serviceId) == -1)) ||
                                ((commandCode.equals("HTG") || commandCode.equals("QTG")) && (("8184,8284,8384").indexOf(serviceId) == -1))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + " TYGIA<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            if (commandCode.equals("HTG") || commandCode.equals("QTG")) {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin TyGia theo yeu cau hay soan " + info +
                                        " gui 8484\nDT ho tro 1900561577";
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin TyGia theo yeu cau hay soan " + info +
                                        " gui 8384\nDT ho tro 1900561577";
                            }
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + " TYGIA<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        alert =
                                "Xin loi ban,hien tai he thong chua co thong tin ve TyGia ma ban yeu cau\nDT ho tro 1900561577";
                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                        MyLogger.log(userId + "<==" + commandCode + " TYGIA<==" + serviceId + ":NotFound:" + info);
                    }

                } else if (commandCode.equals("HT") || commandCode.equals("QTT")) {
                    commandId = "THOITIET";
                    String comment = infoTmp.substring(commandCode.length()).trim();
                    subCode1 = comment;
                    result = dbTool.getTXTData_6(commandId, comment);
                    if ((result != null) && !result.equals("")) {
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384")) || (Preference.adminPhoneNumber.contains(userId))) {
                            result = comment + "\n" + result;
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,nhan thong tin Thoi Tiet soan " + info +
                                    " gui8484\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Thong tin khong co hoac tin nhan sai.De nhan tin du bao thoi tiet soan: " + commandCode + " <TenTinh> gui 8484,vi du " +
                                    commandCode + " HANOI\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("HGV") || commandCode.equals("QGV")) {
                    commandId = "GIA";
                    subCode1 = "VANG";
                    result = dbTool.getTXTData_1(commandId, subCode1);
                    if ((result != null) && !result.equals("")) {
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384")) || (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + " VANG<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,de nhan thong tin GiaVang theo yeu cau hay soan " + info +
                                    " gui8484\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + " VANG<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        alert =
                                "Xin loi ban,hien tai he thong chua co thong tin Gia VANG ban yeu cau\nDT ho tro 1900561577";
                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                        MyLogger.log(userId + "<==" + commandCode + " VANG<==" + serviceId + ":NotFound:" + info);
                    }
                } else if (commandCode.equals("DT")) {
                    subCode1 = subCode1 + subCode2;
                    subCode1 = subCode1.replaceAll(" ", "");
                    int st = 0;
                    String d = "";
                    String sbd = "";

                    if ((subCode1.equals("")) || (subCode1.length() < 3)) {
                        if (status != 0) {
                            alert =
                                    "De lay ket qua diem thi DH&CD,soan: DT SoBaodanh gui 8584.(SoBD ghi ca phan chu va phan so nhu tren phieu bao thi).VD: DT BKAA123 gui 8584.DT ho tro:1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    } else {
                        String schoolName = dbTool.getSchoolName(subCode1.substring(0, 3));
                        if ((schoolName != null) && (!schoolName.equals(""))) {
                            if (((subCode1.length() == 2) || (subCode1.length() == 4)) && (status == 0)) {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            } else if (!Utilities.isDT(subCode1) && (status == 0)) {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            } else {
                                if (serviceId.endsWith("8384") || serviceId.endsWith("8484") || serviceId.endsWith("8584") || serviceId.endsWith("8684") || serviceId.endsWith("8784") || (Preference.adminPhoneNumber.contains(userId))) {
                                    if (subCode1.length() > 3) {
                                        sbd = subCode1.toUpperCase();
                                        sbd = StringTool.getContentOnlyCharAndNumber(sbd);
                                        d = dbTool.getDiemThi(sbd);
                                        if (!d.equalsIgnoreCase("") || dbTool.schoolisHasPoint(subCode1.substring(0, 3))) {
                                            st = 2;
                                        }
                                    }

                                    if (dbTool.add2DiemThiWait(requestId, userId, serviceId, mobileOperator, commandCode, info, subCode1.substring(0, 3), st, subCode1)) {
                                        if (subCode1.length() == 3) {
                                            alert = "Ban da dang ky nhan DiemChuan DH&CD,chung toi se gui ban ngay khi co kq tu bo GD&DT.De biet diem thi soan: DT <Sobaodanh> gui8584.DT ho tro: 1900561577";
                                        } else if ((subCode1.length() > 3) && !serviceId.endsWith("8384") && !serviceId.endsWith("8484") && !serviceId.endsWith("8584")) {
                                            if (!d.equals("")) {
                                                alert = d + "Chung toi se gui DiemChuan cho ban ngay khi co kq tu bo GD&DT.";
                                            } else if (dbTool.schoolisHasPoint(subCode1.substring(0, 3))) {
                                                alert = "Khong tim thay thong tin diem thi ban yeu cau,moi soan " + commandCode +
                                                        " <SoBD> gui8584,SoBD gom MaTruong,MaKhoi,SoBaoDanh" + enter + "Luu y MaKhoi bao gom ca phan chu va so.";
                                            } else {
                                                alert = "Ban da dang ky nhan Diem thi va DiemChuan DH&CD thanh cong,chung toi se gui ban ngay khi co kq tu bo GD&DT" + enter + "DT ho tro: 1900561577";
                                            }
                                        } else {
                                            if (!d.equals("")) {
                                                alert = d + "De biet DiemChuan soan: DC <MaTruong> gui8584";
                                            } else if (dbTool.schoolisHasPoint(subCode1.substring(0, 3))) {
                                                alert = "Khong tim thay thong tin diem thi ban yeu cau,moi soan " + commandCode +
                                                        " <SoBD> gui8584,SoBD gom MaTruong,MaKhoi,SoBaoDanh" + enter + "Luu y MaKhoi bao gom ca phan chu va so.";
                                            } else {
                                                alert = "Ban da dang ky nhan Diem thi thanh cong,chung toi se gui ban ngay khi co kq tu bo GD&DT.De biet diem chuan,soan: DT <Matruong> gui 8584 DT ho tro: 1900561577.";
                                            }
                                        }
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                    }
                                } else {
                                    if (subCode1.length() == 3) {
                                        //==>DiemChuan
                                        alert =
                                                "Sai dau so DV.Moi soan: " + info + " gui 8584 de lay Diem Chuan truong: " + schoolName + ".DT ho tro: 1900561577";
                                    } else {
                                        alert =
                                                "Sai dau so DV.Moi soan: " + info + " gui 8584 de lay Diem Thi hoac gui 8684 de lay DiemThi va DiemChuan.DT ho tro: 1900561577";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                                }
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Ban soan tin sai ma truong,de tra cuu ket qua diem thi,soan: DT SoBaodanh gui 8584.(SoBD ghi ca phan chu va phan so nhu tren phieu bao thi).DT ho tro: 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    }
                } else if (commandCode.equals("DIEM")) {
                    subCode1 = subCode1 + subCode2;
                    subCode1 = subCode1.replaceAll(" ", "");
                    int st = 0;
                    String d = "";
                    String sbd = "";
                    if ((subCode1.equals("")) || (subCode1.length() < 3)) {
                        if (status != 0) {
                            alert =
                                    "De lay ket qua diem thi DH&CD,soan: DT SoBaodanh gui 8584.(SoBD ghi ca phan chu va phan so nhu tren phieu bao thi).VD: DT BKAA123 gui 8584.DT ho tro:1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    } else {
                        String schoolName = dbTool.getSchoolName(subCode1.substring(0, 3));
                        if ((schoolName != null) && (!schoolName.equals(""))) {
                            if ((subCode1.length() == 4) && (status == 0)) {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            } else if (!Utilities.isDT(subCode1) && (status == 0)) {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            } else {
                                if (serviceId.endsWith("8484") || serviceId.endsWith("8584") || serviceId.endsWith("8684") || serviceId.endsWith("8784") || (Preference.adminPhoneNumber.contains(userId))) {
                                    if (subCode1.length() > 3) {
                                        sbd = subCode1.toUpperCase();
                                        sbd = StringTool.getContentOnlyCharAndNumber(sbd);
                                        d = dbTool.getDiemThi(sbd);
                                        if (!d.equalsIgnoreCase("") || dbTool.schoolisHasPoint(subCode1.substring(0, 3))) {
                                            st = 2;
                                        }
                                    }
                                    if (dbTool.add2DiemThiWait(requestId, userId, serviceId, mobileOperator, commandCode, info, subCode1.substring(0, 3), st, subCode1)) {
                                        if (subCode1.length() == 3) {
                                            alert = "Ban da dang ky nhan DiemChuan DH&CD,chung toi se gui ban ngay khi co kq tu bo GD&DT.De biet diem thi soan: DT <Sobaodanh> gui8584.DT ho tro: 1900561577";
                                        } else if ((subCode1.length() > 3) && !serviceId.endsWith("8484") && !serviceId.endsWith("8584")) {
                                            if (!d.equals("")) {
                                                alert = d + "Chung toi se gui DiemChuan cho ban ngay khi co kq tu bo GD&DT.";
                                            } else if (dbTool.schoolisHasPoint(subCode1.substring(0, 3))) {
                                                alert = "Khong tim thay thong tin diem thi ban yeu cau,moi soan " + commandCode +
                                                        " <SoBD> gui8584,SoBD gom MaTruong,MaKhoi,SoBaoDanh" + enter + "Luu y MaKhoi bao gom ca phan chu va so.";
                                            } else {
                                                alert = "Ban da dang ky nhan Diem thi va DiemChuan DH&CD thanh cong,chung toi se gui ban ngay khi co kq tu bo GD&DT" + enter + "DT ho tro: 1900561577";
                                            }
                                        } else {
                                            if (!d.equals("")) {
                                                alert = d + enter + " De biet DiemChuan soan: DC <MaTruong> gui8584";
                                            } else {
                                                alert = "Ban da dang ky nhan Diem thi thanh cong,chung toi se gui ban ngay khi co kq tu bo GD&DT.De biet diem chuan,soan: DT <Matruong> gui 8584 DT ho tro: 1900561577.";
                                            }
                                        }
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                    }
                                } else {
                                    if (subCode1.length() == 3) {
                                        //==>DiemChuan
                                        alert =
                                                "Sai dau so DV.Moi soan: " + info + " gui 8584 de lay Diem Chuan truong: " + schoolName + ".DT ho tro: 1900561577";
                                    } else {
                                        alert =
                                                "Sai dau so DV.Moi soan: " + info + " gui 8584 de lay Diem Thi hoac gui 8684 de lay DiemThi va DiemChuan.DT ho tro: 1900561577";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                                }
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Ban soan tin sai ma truong,de tra cuu ket qua diem thi,soan: DT SoBaodanh gui 8584.(SoBD ghi ca phan chu va phan so nhu tren phieu bao thi).DT ho tro: 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongFormat:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    }
                } else if (commandCode.equals("HM")) {
                    //HM NhomMau ngaythangsinh => HM O 11/12/09
                    //HM ngaythangsinh => HM 11/12/09

                    /*
                    String codeHM = StringTool.getObjectString(infoTmp,1);
                    String dd = StringTool.getObjectString(infoTmp,2);
                    String mm = StringTool.getObjectString(infoTmp,3);
                    String yy = StringTool.getObjectString(infoTmp,4);
                    
                    if(codeHM.equals("") || !StringTool.isNumberic(dd) || (Integer.parseInt(dd) > 31)
                    || !StringTool.isNumberic(mm) || (Integer.parseInt(mm) > 12)
                    || !StringTool.isNumberic(yy) || (Integer.parseInt(yy) < 1940) ||(Integer.parseInt(yy) > 2000)) {
                    if (status != 0) {
                    alert =
                    "Ban soan tin sai cu phap, de tham gia du thuong cua chuong trinh NHAN NIEM HANH PHUC, soan tin: HM Maso Ngay/thang/Namsinh gui 8184" + enter +
                    "DT ho tro: 1900561577";
                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                    } else {
                    dbTool.updateStatus(requestId, 1, mobileOperator);
                    }
                    } else {
                    subCode1 = codeHM;
                    
                    if(dd.length() < 2) {
                    dd = "0" + dd;
                    }
                    if(mm.length() < 2) {
                    mm = "0" + mm;
                    }
                    if(yy.length() == 2) {
                    yy = "19" + yy;
                    }
                    subCode2 = dd + "/" + mm + "/" + yy;
                    
                    alert =
                    "Cam on Quy vi da HM va tham gia Chuong trinh NHAN NIEM HANH PHUC.Chuc Quy vi manh khoe,  hanh phuc va luon quan tam toi phong trao HMND,tiep tuc HM sau 3 thang";
                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                    }
                     */

                    String codeHM = StringTool.getObjectString(infoTmp, 1);
                    if (codeHM.equals("O") || codeHM.equals("A") || codeHM.equals("B") || codeHM.equals("AB")) {
                        //HM ngay/thang/nam
                        String dd = StringTool.getObjectString(infoTmp, 2);
                        String mm = StringTool.getObjectString(infoTmp, 3);
                        String yy = StringTool.getObjectString(infoTmp, 4);

                        if (!StringTool.isNumberic(dd) || (Integer.parseInt(dd) > 31) || !StringTool.isNumberic(mm) || (Integer.parseInt(mm) > 12) || !StringTool.isNumberic(yy) || (Integer.parseInt(yy) < 1940) || (Integer.parseInt(yy) > 2000)) {
                            if (status != 0) {
                                alert =
                                        "Ban soan tin sai cu phap,de tham gia du thuong cua chuong trinh NHAN NIEM HANH PHUC, soan tin: HM Nhommau Ngay/thang/Namsinh gui 8184" +
                                        enter +
                                        "DT ho tro: 1900561577";
                                subCode1 = "";
                                subCode2 = "";
                                subCode3 = "";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            subCode1 = codeHM;

                            if (dd.length() < 2) {
                                dd = "0" + dd;
                            }
                            if (mm.length() < 2) {
                                mm = "0" + mm;
                            }
                            if (yy.length() == 2) {
                                yy = "19" + yy;
                            }
                            subCode2 = dd + "/" + mm + "/" + yy;
                            subCode3 = "";
                            alert =
                                    "Cam on Quy vi da HM va tham gia Chuong trinh NHAN NIEM HANH PHUC.Chuc Quy vi manh khoe,hanh phuc va luon quan tam toi phong trao HMND,tiep tuc HM sau 3 thang.";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);

                            //Advantage
                            alert =
                                    "Kham pha bi an ten ban,soan tin: XEM TEN Hotenban gui 8384/Kham pha chuyen tinh duyen cua ban va nguoi ay,soan tin: XEM DUYEN NamSinhBan NamSinhNguoiAy gui8384";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 0, requestId, subCode1, subCode2, subCode3);
                        }
                    } else if (StringTool.isNumberic(codeHM)) {
                        //HM ngay/thang/nam
                        String dd = codeHM;
                        String mm = StringTool.getObjectString(infoTmp, 2);
                        String yy = StringTool.getObjectString(infoTmp, 3);

                        if (!StringTool.isNumberic(dd) || (Integer.parseInt(dd) > 31) || !StringTool.isNumberic(mm) || (Integer.parseInt(mm) > 12) || !StringTool.isNumberic(yy) || (Integer.parseInt(yy) < 1940) || (Integer.parseInt(yy) > 2000)) {
                            if (status != 0) {
                                alert =
                                        "Ban soan tin sai cu phap,de tham gia du thuong cua chuong trinh NHAN NIEM HANH PHUC, soan tin: HM Nhommau Ngay/thang/Namsinh gui 8184" +
                                        enter +
                                        "DT ho tro: 1900561577";
                                subCode1 = "";
                                subCode2 = "";
                                subCode3 = "";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            subCode1 = "";
                            subCode2 = codeHM;

                            if (dd.length() < 2) {
                                dd = "0" + dd;
                            }
                            if (mm.length() < 2) {
                                mm = "0" + mm;
                            }
                            if (yy.length() == 2) {
                                yy = "19" + yy;
                            }
                            subCode3 = dd + "/" + mm + "/" + yy;
                            alert =
                                    "Cam on Quy vi da HM va tham gia Chuong trinh NHAN NIEM HANH PHUC.Chuc Quy vi manh khoe,hanh phuc va luon quan tam toi phong trao HMND,tiep tuc HM sau 3 thang.";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);

                            //Advantage
                            alert =
                                    "Kham pha bi an ten ban,soan tin: XEM TEN Hotenban gui 8384/Kham pha chuyen tinh duyen cua ban va nguoi ay,soan tin: XEM DUYEN NamSinhBan NamSinhNguoiAy gui8384";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 0, requestId, subCode1, subCode2, subCode3);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Ban soan tin sai cu phap,de tham gia du thuong cua chuong trinh NHAN NIEM HANH PHUC, soan tin: HM Nhommau Ngay/thang/Namsinh gui 8184" +
                                    enter +
                                    "DT ho tro: 1900561577";
                            subCode1 = "";
                            subCode2 = "";
                            subCode3 = "";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("QDT")) {
                    commandId = "GIA";
                    subCode2 = subCode1;
                    subCode1 = "DT";
                    result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                    if (!subCode2.equals("") && (result != null) && !result.equals("")) {
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384")) || (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + " DT<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,de nhan thong tin Gia dien thoai theo yeu cau hay soan " + info +
                                    " gui 8484\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + " DT<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Tin nhan sai cu phap,de nhan thong tin Gia DT hay soan GIADT TenHangDT gui8384,trong do TenHangDT(NOKIA,LG,SAMSUNG..)\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + " DT<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                alert =
                        "Xin loi ban,Thong tin ban gui khong ro rang.Soan HOT gui8384 de nhan ma so cac anh Bikini tuyet dep.\nDT ho tro 1900561577";
                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                MyLogger.log(userId + "<==WrongFormat<==" + serviceId + ":" + info);
            }
        } //End for

    }

    public void run() {
        MyLogger.log("DailyServices_1 process Thread Start !");
        while (Main.running) {
            try {
                processRequestQueue("VIETEL");
                processRequestQueue("GPC");
                processRequestQueue("VMS");
                Thread.sleep(5000);
            } catch (Exception ex) {
            }
        }
    }

    public static void main(String[] args) {
        String aa = "ABCDEF";
        MyLogger.log(aa.substring(2));
    }
}
