package com.sms.process;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import java.util.*;
import java.io.*;

import org.apache.log4j.*;
import com.sms.common.*;

public class Main extends Thread {

    static Utilities util = null;
    DBTool dbTool = null;
    public static boolean running = false;
    static BufferedReader keyboard;
    public static Vector schoolCode = null;
    public static Vector schoolCodeHavePoint = null;
    public static Hashtable schoolStandardMark = null;
    DBPool dbPool = null;


    static {
        keyboard = new BufferedReader(new InputStreamReader(System.in));
    }

    public Main() {
        dbTool = new DBTool();
        dbPool = new DBPool();
        util = new Utilities();

        schoolCode = new Vector();
        schoolCodeHavePoint = new Vector();
        schoolStandardMark = new Hashtable();
    }

    public void getMarkInfo() {
        schoolCode = dbTool.getSchoolCode();
        schoolCodeHavePoint = dbTool.getSchoolCodeHavePoint();
        schoolStandardMark = dbTool.getStandardMark();
    }

    private void showMenu() {
        String option = "";
        try {
            option = keyboard.readLine();
            if ("Q".equals(option.toUpperCase())) {
                exit();
            }
        } catch (Exception e) {
            System.out.println(":" + e.getMessage());
        }
    }

    private static void exit() {
        running = false;
        DBPool.releaseAllConnection();
        MyLogger.log("Stop.");
        System.exit(0);
    }

    public void run() {
        while (running) {
            showMenu();
        }
    }

    public static void main(String[] args) {
        Main main = new Main();
        try {
            if (System.getProperty("log4j") != null) {
                PropertyConfigurator.configure(System.getProperty("log4j.properties"));
            }

            Preference.loadProperties("cfg.properties");

            (new DBPool()).initConnect2DBRemote();
            (new DBPool()).initConnect2DBContent();

            main.getMarkInfo();
            System.out.println("Get SchoolCode OK !");

            running = true;

            (new ProcessMedia()).start();
            (new ProcessSimpleService()).start();
            (new ProcessNC()).start();
            (new ProcessDailyService()).start();
            (new ProcessDailyService1()).start();
            (new ProcessCleanPush()).start();
//            (new ProcessRefeshData()).start();
//            (new ProcessDT2()).start();
//            (new ProcessDT()).start();
            (new ProcessCK()).start();
            main.start();

        } catch (Exception ex) {
            MyLogger.log(ex.getMessage());
        }
    }
}
