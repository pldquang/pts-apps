package com.sms.process;

import java.util.Vector;
import com.sms.item.SmsReceiveQueue;
import java.math.BigDecimal;

import com.sms.common.*;
import java.util.StringTokenizer;

public class ProcessNC extends Thread {

    DBTool dbTool = null;
    static char newLine = (char) 10;
    static String vmsNCOff = "De huy Nhac Cho soan OFF gui 9224." + newLine +
            "Hoac goi dien den 9224 va lam theo huong dan.Dien thoai ho tro 1900561577";
    static String gpcNCOff = "De tam dung nhac cho hay soan OFF gui 9194." + newLine +
            "De kich hoat lai soan ON gui 9194." + newLine +
            "De huy Nhac Cho soan HUY gui 9194.Dien thoai ho tro 1900561577";
    static String vietelNCOff = "De huy Nhac Cho soan HUY gui 1221." + newLine +
            "Hoac goi dien den 18008198 va lam theo huong dan.Dien thoai ho tro 1900561577";
    String alert = "";
    String commandId = "";
    String result = "";
    String spam = "";

    public ProcessNC() {
        dbTool = new DBTool();
    }

    public void processRequestQueue(String mobileOperator) {
        Vector listRequest = dbTool.getRequestFromQueue(mobileOperator, Preference.wmusicCode);
        try {
            for (int i = 0; i < listRequest.size(); i++) {
                SmsReceiveQueue smsReceiveQueue = (SmsReceiveQueue) listRequest.elementAt(i);
                BigDecimal requestId = smsReceiveQueue.getId();
                String userId = smsReceiveQueue.getUserId();
                String serviceId = smsReceiveQueue.getServiceId();
                String commandCode = smsReceiveQueue.getCommandCode();
                int status = smsReceiveQueue.getStatus();
                String info = (smsReceiveQueue.getInfo()).toUpperCase();
                info = commandCode + " " + info.substring(commandCode.length(), info.length());
                String code = "";
                boolean offNC = false;
                boolean searchNhacCho = false;
                boolean moinhat = false;
                String resultSearchNhacCho = "";

                String subCode1 = "";
                String subCode2 = "";
                String subCode3 = "";
                try {
                    code = Utilities.getObjectString(info, 1);
                    if (commandCode.equals("STOP") || commandCode.equals("HUY") || code.equalsIgnoreCase("OFF") || code.equalsIgnoreCase("HUY") ||
                            code.equalsIgnoreCase("STOP")) {
                        offNC = true;
                    } else if (StringTool.getContentNoWhiteLetter(info.substring(commandCode.length(), info.length())).equalsIgnoreCase("MOINHAT")) {
                        searchNhacCho = true;
                        resultSearchNhacCho = dbTool.searchWMusic("MOINHAT", mobileOperator, false);
                        subCode1 = "MOINHAT";
                    } else {
                        searchNhacCho = true;
                        String searchStr = info.substring(commandCode.length(), info.length());
                        if (commandCode.equals("CS") || commandCode.equals("STC") || commandCode.equals("TNS")) {
                            resultSearchNhacCho = dbTool.searchWMusic(searchStr, mobileOperator, true);
                        } else {
                            resultSearchNhacCho = dbTool.searchWMusic(searchStr, mobileOperator, false);
                        }
                        subCode1 = StringTool.getContentNoWhiteLetter(searchStr);
                    }
                } catch (Exception ex) {
                }

                if (commandCode.equals("CS") || commandCode.equals("STC") || commandCode.equals("TNS") || commandCode.equals("THC") || commandCode.equals("BNS") || commandCode.equals("DPS")|| commandCode.equals("HC")|| commandCode.equals("QC")) {
                    //Nhac cho + Ten CS
//                    if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId)) ||
//                        ((commandCode.equals("TNS") || commandCode.equals("STN") || commandCode.equals("THC") || commandCode.equals("BNS")) &&
//                         (serviceId.endsWith("8584") || serviceId.endsWith("8684") || serviceId.endsWith("8784")))) {
                    if ((!commandCode.equals("TNS") && !commandCode.equals("STN") && !commandCode.equals("THC") && !commandCode.equals("BNS") && !commandCode.equals("DPS") && (!serviceId.equals("8184") && !serviceId.equals("8284"))) ||
                            ((commandCode.equals("TNS") || commandCode.equals("STN") || commandCode.equals("THC") || commandCode.equals("BNS") || commandCode.equals("DPS")) && (!serviceId.equals("8184") && !serviceId.equals("8284") && !serviceId.equals("8384") && !serviceId.equals("8484"))) ||
                            (!commandCode.equals("HC") && !commandCode.equals("QC") && (!serviceId.equals("8184") && !serviceId.equals("8284"))) ||
                            ((commandCode.equals("HC") || commandCode.equals("QC")) && (!serviceId.equals("8184") && !serviceId.equals("8284") && !serviceId.equals("8384"))) ||
                            (Preference.adminPhoneNumber.contains(userId))) {

                        if (resultSearchNhacCho.equals("")) {
                            if (status != 0) {
                                if (commandCode.equals("TNS")) {
                                    alert =
                                            "Ma so BH cua ca si ban yeu cau chua co,vui long soan ten ca si khac hoac soan: TNC MOINHAT gui8584 de lay ma cac BH moi nhat.DT ho tro:1900561577";
                                } else if (commandCode.equals("STC")) {
                                    alert =
                                            "Ma so BH cua ca si ban yeu cau chua co,vui long soan ten ca si khac hoac soan: STN MOINHAT gui8584 de lay ma cac BH moi nhat.DT ho tro:1900561577";
                                } else if (commandCode.equals("THC")) {
                                    alert =
                                            "Ma so BH cua ca si ban yeu cau chua co,vui long soan ten ca si khac hoac soan: THN MOINHAT gui8584 de lay ma cac BH moi nhat.DT ho tro:1900561577";
                                } else if (commandCode.equals("BNS")) {
                                    alert =
                                            "Ma so BH cua ca si ban yeu cau chua co,vui long soan ten ca si khac hoac soan: BNC MOINHAT gui8584 de lay ma cac BH moi nhat.DT ho tro:1900561577";
                                } else if (commandCode.equals("DPS")) {
                                    alert =
                                            "Ma so BH cua ca si ban yeu cau chua co,vui long soan ten ca si khac hoac soan: DPC MOINHAT gui8584 de lay ma cac BH moi nhat.DT ho tro:1900561577";
                                }else if (commandCode.equals("HC")) {
                                    alert =
                                            "Ma so BH cua ca si ban yeu cau chua co,vui long soan ten ca si khac hoac soan: HC MOINHAT gui8484 de lay ma cac BH moi nhat.DT ho tro:1900561577";
                                }else if (commandCode.equals("QC")) {
                                    alert =
                                            "Ma so BH cua ca si ban yeu cau chua co,vui long soan ten ca si khac hoac soan: QC MOINHAT gui8484 de lay ma cac BH moi nhat.DT ho tro:1900561577";
                                } else {
                                    alert =
                                            "Ma so BH cua ca si ban yeu cau chua co,vui long soan ten ca si khac hoac soan: NC MOINHAT gui8384 de lay ma cac BH moi nhat.DT ho tro:1900561577";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + serviceId + ":" + commandCode + " Search Not found");
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            if (resultSearchNhacCho.length() > 160 && (resultSearchNhacCho.indexOf("!xxx!") == -1)) {
                                resultSearchNhacCho = StringTool.splitMsg(resultSearchNhacCho);
                            }

                            boolean deductMoney = false;
                            resultSearchNhacCho = resultSearchNhacCho.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(resultSearchNhacCho, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + serviceId + ":" + commandCode + " Search");
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        }
                    } else {
                        if (commandCode.equals("TNS") || commandCode.equals("STN") || commandCode.equals("THC") || commandCode.equals("BNS") || commandCode.equals("DPS")) {
                            alert = "Ban gui sai so dich vu, hay soan: " + info + " gui8584 de biet MaSo bai hat ban yeu cau." + newLine +
                                    "DT ho tro:1900561577";
                        }else if (commandCode.equals("HC") || commandCode.equals("QC")) {
                            alert = "Ban gui sai so dich vu, hay soan: " + info + " gui8484 de biet MaSo bai hat ban yeu cau." + newLine +
                                    "DT ho tro:1900561577";
                        } else {
                            alert = "Ban gui sai so dich vu, hay soan: " + info + " gui8384 de biet MaSo bai hat ban yeu cau." + newLine +
                                    "DT ho tro:1900561577";
                        }

                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                        MyLogger.log(userId + "<==" + commandCode + ":WrongServiceId:" + "<==" + serviceId);
                    }
                } else if (!offNC && !searchNhacCho) {
                    //Huong dan cai dat Nhac cho
//                    if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId)) ||
//                        ((commandCode.equals("TNC") || commandCode.equals("STN") || commandCode.equals("THN") || commandCode.equals("BNC")) &&
//                         (serviceId.endsWith("8584") || serviceId.endsWith("8684") || serviceId.endsWith("8784")))) {
                    if ((!commandCode.equals("TNC") && !commandCode.equals("STN") && !commandCode.equals("THN") && !commandCode.equals("BNC") && !commandCode.equals("DPC") && !serviceId.equals("8184") && !serviceId.equals("8284")) ||
                            ((commandCode.equals("TNC") || commandCode.equals("STN") || commandCode.equals("THN") || commandCode.equals("BNC") || commandCode.equals("DPC")) && (!serviceId.equals("8184") && !serviceId.equals("8284") && !serviceId.equals("8384") && !serviceId.equals("8484"))) ||
                            (!commandCode.equals("HC") && !commandCode.equals("QC") && !serviceId.equals("8184") && !serviceId.equals("8284")) ||
                            ((commandCode.equals("HCC") || commandCode.equals("QC")) && (!serviceId.equals("8184") && !serviceId.equals("8284") && !serviceId.equals("8384"))) ||
                            (Preference.adminPhoneNumber.contains(userId))) {
                        commandId = "NC";
                        result = dbTool.getTXTData_1(commandId, mobileOperator);
                        result = result.replaceAll("\r\n", "\n");
                        if (commandCode.equals("TNC") || commandCode.equals("STN") || commandCode.equals("THN") || commandCode.equals("BNC") || commandCode.equals("DPC")) {
                            result = result.replaceAll("8384", "8584");
                        }
                        if (commandCode.equals("HC") || commandCode.equals("QC")) {
                            result = result.replaceAll("8384", "8484");
                        }
                        if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                            result = StringTool.splitMsg(result);
                        }
                        boolean deductMoney = false;
                        result = result.replaceAll("!xxx!", "|");
                        StringTokenizer stToken = new StringTokenizer(result, "|", false);
                        int stTokenLen = stToken.countTokens();
                        for (int ii = 0; ii < stTokenLen; ii++) {
                            String txtSms = stToken.nextToken();
                            txtSms = txtSms.replace("NC", commandCode);
                            if (!deductMoney) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                        txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                deductMoney = true;
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                            } else {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                        txtSms, 0, requestId, subCode1, subCode2, subCode3);
                            }
                            if (ii > 5) {
                                break;
                            }
                        }
                    } else {
                        if (commandCode.equals("TNC") || commandCode.equals("STN") || commandCode.equals("THN") || commandCode.equals("BNC") || commandCode.equals("DPC")) {
                            alert = "Ban gui sai so dich vu, hay soan: " + info + " gui8584 de duoc huong dan cai dat nhac cho." + newLine +
                                    "DT ho tro:1900561577";
                            System.out.println("MK 1");
                        }else if (commandCode.equals("HC") || commandCode.equals("QC")) {
                            alert = "Ban gui sai so dich vu, hay soan: " + info + " gui8484 de duoc huong dan cai dat nhac cho." + newLine +
                                    "DT ho tro:1900561577";
                            System.out.println("MK 1.1");
                        } else {
                            alert = "Ban gui sai so dich vu, hay soan: " + info + " gui8384 de duoc huong dan cai dat nhac cho." + newLine +
                                    "DT ho tro:1900561577";
                            System.out.println("MK 2");
                        }
                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                        MyLogger.log(userId + "<==" + commandCode + ":WrongServiceId:" + "<==" + serviceId);
                    }
                } else if (offNC) {
                    String offNCGuide = "";
                    if (mobileOperator.equals("VMS")) {
                        offNCGuide = vmsNCOff;
                    } else if (mobileOperator.equals("GPC")) {
                        offNCGuide = gpcNCOff;
                    } else if (mobileOperator.equals("VIETEL")) {
                        offNCGuide = vietelNCOff;
                    }

                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, offNCGuide, 1, requestId, subCode1, subCode2, subCode3);
                    MyLogger.log(userId + "<==" + serviceId + ":NC OFF");
                } else if (searchNhacCho) {

                    //NC TenBaiHat
                    if (resultSearchNhacCho.equals("")) {
                        if (status != 0) {
                            if (mobileOperator.equals("VMS")) {
                                alert = "Hien tai Mobifone chua co bai hat ban yeu cau.Tim bai khac bang cach soan: " + commandCode +
                                        " MOINHAT gui 8384 de lay ma so bai nhac cho moi nhat.DT ho tro:1900561577";
                            } else if (mobileOperator.equals("GPC")) {
                                alert = "Hien tai Vinaphone chua co bai hat ban yeu cau.Tim bai khac bang cach soan: " + commandCode +
                                        " MOINHAT gui 8384 de lay ma so bai nhac cho moi nhat.DT ho tro:1900561577";
                            } else if (mobileOperator.equals("VIETEL")) {
                                alert = "Hien tai Viettel chua co bai hat ban yeu cau.Tim bai khac bang cach soan: " + commandCode +
                                        " MOINHAT gui 8384 de lay ma so bai nhac cho moi nhat.DT ho tro:1900561577";
                            }
                            if (commandCode.equals("STN") || commandCode.equals("TNC") || commandCode.equals("DPC") || commandCode.equals("BNC")) {
                                alert = alert.replaceAll("8384", "8584");
                            }
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + serviceId + ":" + commandCode + " Search Not found");
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    } else {

//                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId)) ||
//                            ((commandCode.equals("TNC") || commandCode.equals("STN") || commandCode.equals("THN") || commandCode.equals("BNC")) &&
//                             (serviceId.endsWith("8584") || serviceId.endsWith("8684") || serviceId.endsWith("8784")))) {
                        if ((!commandCode.equals("TNC") && !commandCode.equals("STN") && !commandCode.equals("THN") && !commandCode.equals("BNC") && !commandCode.equals("DPC") && (!serviceId.equals("8184") && !serviceId.equals("8284"))) ||
                                ((commandCode.equals("TNC") || commandCode.equals("STN") || commandCode.equals("THN") || commandCode.equals("BNC") || commandCode.equals("DPC")) && (!serviceId.equals("8184") && !serviceId.equals("8284") && !serviceId.equals("8384") && !serviceId.equals("8484"))) ||
                                (!commandCode.equals("HC") && !commandCode.equals("QC") && (!serviceId.equals("8184") && !serviceId.equals("8284"))) ||
                                ((commandCode.equals("HC") || commandCode.equals("QC")) && (!serviceId.equals("8184") && !serviceId.equals("8284") && !serviceId.equals("8384"))) ||
                                (Preference.adminPhoneNumber.contains(userId))) {

                            if (resultSearchNhacCho.length() > 160 && (resultSearchNhacCho.indexOf("!xxx!") == -1)) {
                                resultSearchNhacCho = StringTool.splitMsg(resultSearchNhacCho);
                            }

                            boolean deductMoney = false;
                            resultSearchNhacCho = resultSearchNhacCho.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(resultSearchNhacCho, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + serviceId + ":" + commandCode + " Search");
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }

                        } else {
                            if (commandCode.equals("TNC") || commandCode.equals("STN") || commandCode.equals("THN") || commandCode.equals("BNC") || commandCode.equals("DPC")) {
                                alert = "Ban gui sai so dich vu, hay soan: " + info + " gui8584 de biet MaSo bai hat ban yeu cau." + newLine +
                                        "DT ho tro:1900561577";
                            }else if (commandCode.equals("HC") || commandCode.equals("QC")) {
                                alert = "Ban gui sai so dich vu, hay soan: " + info + " gui8484 de biet MaSo bai hat ban yeu cau." + newLine +
                                        "DT ho tro:1900561577";
                            } else {
                                alert = "Ban gui sai so dich vu, hay soan: " + info + " gui8384 de biet MaSo bai hat ban yeu cau." + newLine +
                                        "DT ho tro:1900561577";
                            }
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + ":WrongServiceId:" + "<==" + serviceId);
                        }
                    }
                }
                //                Send adv
                spam = dbTool.getSpam();
                if (!spam.equalsIgnoreCase("")) {
                    if (status == 0) {
                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, spam, 0, requestId, subCode1, subCode2,
                                subCode3);
                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":send Spam");
                    }
                }
            } //End for

        } catch (Exception ex) {
            MyLogger.log(ex.getMessage());
        }
    }

    public void run() {
        MyLogger.log("NC process Thread Start.");
        while (Main.running) {
            try {
                processRequestQueue("VIETEL");
                processRequestQueue("GPC");
                processRequestQueue("VMS");
                Thread.sleep(3000);
            } catch (Exception ex) {
            }
        }
    }

    public static void main(String[] args) {
    }
}
