/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sms.process;

import com.sms.common.StringTool;
import com.sms.item.DiemthiWaitItem;
import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 *
 * @author hungdt
 */
public class ProcessDT2 extends Thread {

    DBTool dbTool = null;
    String alert = "";
    char enter = (char) 10;
    BigDecimal id = null;
    BigDecimal requestId = null;
    String userId = "";
    String serviceId = "";
    String commandCode = "";
    String info = "";
    String mobileOperator = "";
    String infoTmp = "";
    String data = "";
    String dataTmp = "";
    String result = "";
    boolean isProcessing = false;

    public ProcessDT2() {
        dbTool = new DBTool();
    }

    @Override
    public void run() {
        System.out.println("DiemThi2 process Thread Start !");
        while (Main.running) {
            try {
                processRequestQueue();
                sleep(10 * 60 * 1000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ProcessDT2 p = new ProcessDT2();
//        String cmd = "DIEM";
//        System.out.println(p.processInfo("DIEM <TDLD16997>").substring(cmd.length()));
    }

    private void processRequestQueue() {
        isProcessing = true;
        Vector listRequest = dbTool.getRequestFromDTWaitHasPoint();
        Vector listRequestStandardMark = dbTool.getRequestFromDTWaitHasStandardMark();
        DiemthiWaitItem diemthiWaitItem = null;
        System.out.println("Total in queue HasPoint is: " + listRequest.size());
        System.out.println("Total in queue StandardMark is: " + listRequestStandardMark.size());
        Vector listRequesthasPoint = dbTool.getSchoolCodeHavePoint();
        System.out.println("Total in school has point is: " + listRequesthasPoint.size());
        Hashtable listRequesthasStandardMark = dbTool.getStandardMark();
        System.out.println("Total in school Standard Mark is: " + listRequesthasStandardMark.size());
        for (int i = 0; i < listRequest.size(); i++) {
            try {
                diemthiWaitItem = (DiemthiWaitItem) listRequest.elementAt(i);
                id = diemthiWaitItem.getId();
                requestId = diemthiWaitItem.getRequestId();
                userId = diemthiWaitItem.getUserId();
                serviceId = diemthiWaitItem.getServiceId();
                mobileOperator = diemthiWaitItem.getMobileOperator();
                commandCode = diemthiWaitItem.getCommandCode();
                info = diemthiWaitItem.getInfo().trim();
                System.out.println("Process:" + userId + ":" + info);
                infoTmp = processInfo(info);
                String sbdfull = infoTmp.substring(commandCode.length());
                String sbd = "";
                String d = "";
                if (commandCode.equals("DT") || commandCode.equals("DC") || commandCode.equals("TNT") || commandCode.equals("SDT") || commandCode.equals("DIEM") || commandCode.equals("DIEMCHUAN")) {
                    sbd = sbdfull.toUpperCase();
                    sbd = StringTool.getContentOnlyCharAndNumber(sbd);
                    d = dbTool.getDiemThi(sbd);
                    if (!d.equalsIgnoreCase("")) {
                        if (!serviceId.equals("8484") && !serviceId.equals("8584") && !serviceId.equals("8684") && !serviceId.equals("8784")) {
                            d = d + enter + "De biet DiemChuan soan: " + commandCode + " <MaTruong> gui8584";
                        }

                    } else if ((commandCode.equalsIgnoreCase("DT") || (commandCode.equalsIgnoreCase("DIEM"))) && dbTool.schoolisHasPoint(sbdfull.substring(0, 3)) && (sbdfull.length() > 3)) {
                        d = "Khong tim thay thong tin diem thi ban yeu cau,moi soan " + commandCode +
                                " <SoBD> gui8584,SoBD gom MaTruong,MaKhoi,SoBaoDanh" + enter + "Luu y MaKhoi bao gom ca phan chu va so.";
                    }
                    if (!d.equalsIgnoreCase("")) {
                        if (dbTool.updateDiemThiWait(id, sbdfull.substring(0, 3), 2, data)) {
                            System.out.println("DT-Process: " + d);
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, d, 0, requestId, "", "", "");
                            System.out.println(userId + "<==DT<==" + serviceId);
                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        for (int i = 0; i < listRequestStandardMark.size(); i++) {
            try {
                diemthiWaitItem = (DiemthiWaitItem) listRequestStandardMark.elementAt(i);
                id = diemthiWaitItem.getId();
                requestId = diemthiWaitItem.getRequestId();
                userId = diemthiWaitItem.getUserId();
                serviceId = diemthiWaitItem.getServiceId();
                mobileOperator = diemthiWaitItem.getMobileOperator();
                commandCode = diemthiWaitItem.getCommandCode();
                info = diemthiWaitItem.getInfo().trim();
                System.out.println("Process DC:" + userId + ":" + info);
                infoTmp = processInfo(info);
                String sbdfull = infoTmp.substring(commandCode.length());
//                String sbd = "";
//                String d = "";
                if (commandCode.equals("DT") || commandCode.equals("DC") || commandCode.equals("TNT") || commandCode.equals("SDT") || commandCode.equals("DIEM") || commandCode.equals("DIEMCHUAN")) {
//                    sbd = sbdfull.toUpperCase();
//                    sbd = StringTool.getContentOnlyCharAndNumber(sbd);
//                    d = dbTool.getDiemThi(sbd);
                    result = (String) listRequesthasStandardMark.get(sbdfull.substring(0, 3));
                    if ((result != null) && (!result.equals(""))) {
                        if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                            result = StringTool.splitMsg(result);
                        }

                        boolean deductMoney = false;
                        result = result.replaceAll("!xxx!", "|");
                        StringTokenizer stToken = new StringTokenizer(result, "|", false);
                        int stTokenLen = stToken.countTokens();
                        if (dbTool.updateDiemThiWait(id, sbdfull.substring(0, 3), 4, data)) {
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                        txtSms, 0, requestId, "", "", "");
                                if (ii > 5) {
                                    break;
                                }
                            }
                        }
                        System.out.println(userId + "<==DTC<==" + serviceId);
                    }

//                    if (!d.equalsIgnoreCase("")) {
////                        if (!serviceId.equals("8484") && !serviceId.equals("8584") && !serviceId.equals("8684") && !serviceId.equals("8784")) {
////                            d = d + enter + "De biet DiemChuan soan: " + commandCode + " <MaTruong> gui8584";
////                        }
//                        if (dbTool.updateDiemThiWait(id, sbdfull.substring(0, 3), 4, data)) {
//                            System.out.println("DT-Process DC: " + d);
//                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, d, 0, requestId, "", "", "");
//                            System.out.println(userId + "<==DT<==" + serviceId);
//                        }
//                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public String processInfo(String info) {
        String ret = "";
        int t;
        for (int i = 0; i < info.length(); i++) {
            t = (int) info.charAt(i);
            if ((t == 47) || ((t >= 48) && (t <= 57)) || ((t >= 65) && (t <= 122))) {
                ret += info.charAt(i);
            }
        }
        return ret;
    }
}
