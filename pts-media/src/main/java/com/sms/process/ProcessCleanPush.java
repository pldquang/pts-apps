package com.sms.process;

import java.util.*;
import com.sms.common.*;
import java.math.BigDecimal;

public class ProcessCleanPush extends Thread {
    DBTool dbTool = null;
    static String http = ":http://222.252.16.196/";

    public ProcessCleanPush() {
        dbTool = new DBTool();
    }

    public void run() {
        System.out.println("Thread Clean Push started !");
        while (Main.running) {
            try {
                if ((DateProc.getCurrentHH24() == 2) && (DateProc.getCurrentMI() >= 30) && (DateProc.getCurrentMI() <= 31)) {
                    dbTool.cleanPush();
                }

                //Process for NQ
                //result="NQ ID,USER_ID,RECEIVER,SERVICE_ID,REQUEST_ID"
                if (DateProc.getCurrentHH24() == 8) {
                    Vector v = dbTool.getQueueWaiting( DateProc.getCurrentDDMM());
                    String data = "";
                    String idStr = "";
                    String userId = "";
                    String receiver = "";
                    String serviceId = "";
                    String alert = "";
                    String requestId = "";
                    String commandCode = "";

                    if (v != null && v.size() > 0) {
                        for (int i = 0; i < v.size(); i++) {
                            data = (String) v.elementAt(i);

                            StringTokenizer tkTmp = new StringTokenizer(data, ",", false);
                            int ii = 0;
                            while (tkTmp.hasMoreTokens()) {
                                if (ii == 0) {
                                    idStr = tkTmp.nextToken();
                                } else if (ii == 1) {
                                    userId = tkTmp.nextToken();
                                } else if (ii == 2) {
                                    receiver = tkTmp.nextToken();
                                } else if (ii == 3) {
                                    serviceId = tkTmp.nextToken();
                                } else if (ii == 4) {
                                    requestId = tkTmp.nextToken();
                                } else if (ii == 5) {
                                    commandCode = tkTmp.nextToken();
                                }

                                ii++;
                            }
//                            System.out.println(idStr + ":" + userId + ":" + receiver + ":" + serviceId + ":" + requestId);
                            boolean b = dbTool.removeQueueWaiting(new BigDecimal(idStr));
                            String commandId = "NQ";
                            if (userId.equals(receiver) && b) {
                                //Send for myself
                                alert =
                                        "Chuc mung sinh nhat ban!Tong dai 8X84 gui tang ban hoa va bai hat mung sinh nhat ban.Chuc ban luon hanh phuc va dat duoc moi uoc mo trong cuoc song.";
                                dbTool.sendMTEx(userId, serviceId, Utilities.buildMobileOperator(userId), commandCode, 0, alert, 0, new BigDecimal(requestId),
                                                " ", " ", " ");
                                if(commandCode.equals("NQ")) {
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                    alert = "De gui Hoa va loi chuc den nguoi than,soan tin:NQ SDTnhan Ngaysinhnguoinhan gui8584.Kham pha bi an ten ban,soan: XEM TEN HotenBan gui 8384.DT ho tro:190056157.";
                                    dbTool.sendMTEx(userId, serviceId, Utilities.buildMobileOperator(userId), commandCode, 0, alert, 0,
                                            new BigDecimal(requestId),
                                            " ", " ", " ");
                                }
                                String prefixCode = "";
                                String dataType = "";
                                String code = "";
                                String auth = "";
                                String dataStr = "";
                                String dataName = "";
                                String result = dbTool.getTXTData(commandId);
                                StringTokenizer tk = new StringTokenizer(result, ",", false);
                                while (tk.hasMoreTokens()) {
                                    String idFull = tk.nextToken();
                                    prefixCode = idFull.substring(0, 3);
                                    dataType = idFull.substring(1, 3);
                                    code = idFull.substring(3);

                                    if (prefixCode.startsWith("812") || prefixCode.startsWith("813") || prefixCode.startsWith("814")) {
                                        //Poly-TrueTone-Word
                                        auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                        dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                                        dataStr = "[PTS] " + idFull + http + "push/" + dataName + "?auth=" + auth;
                                        if (!auth.equals("")) {
                                            dbTool.sendMTEx(userId, serviceId, Utilities.buildMobileOperator(userId), commandCode, 8, dataStr, 0,
                                                    new BigDecimal(requestId), " ", " ", " ");

                                        }
                                    } else if (prefixCode.startsWith("824")) {
                                        //Photo
                                        auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                        dataStr = "[PTS] " + idFull + http + "push/" + idFull + ".gif?auth=" + auth;
                                        if (!auth.equals("")) {
                                            dbTool.sendMTEx(userId, serviceId, Utilities.buildMobileOperator(userId), commandCode, 8, dataStr, 0,
                                                    new BigDecimal(requestId), " ", " ", " ");
                                        }
                                    }
                                }
                            } else if(!userId.equals(receiver) && b){
                                //4 friend
                                alert = "So dien thoai " + Utilities.formatUserId(userId,1) +
                                        " gui loi chuc mung sinh nhat va hoa den ban.Chuc ban luon vui ve va hanh phuc trong cuoc song!";
                                dbTool.sendMTEx(receiver, serviceId, Utilities.buildMobileOperator(receiver), commandCode, 0, alert, 0,
                                                new BigDecimal(requestId), "", "", "");

                                commandId = "NQ";
                                String prefixCode = "";
                                String dataType = "";
                                String code = "";
                                String auth = "";
                                String dataStr = "";
                                String dataName = "";
                                String result = dbTool.getTXTData(commandId);
                                StringTokenizer tk = new StringTokenizer(result, ",", false);
                                while (tk.hasMoreTokens()) {
                                    String idFull = tk.nextToken();
                                    prefixCode = idFull.substring(0, 3);
                                    dataType = idFull.substring(1, 3);
                                    code = idFull.substring(3);

                                    if (prefixCode.startsWith("812") || prefixCode.startsWith("813") || prefixCode.startsWith("814")) {
                                        //Poly-TrueTone-Word
                                        auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                        dataName = dbTool.getDataName(dataType, new BigDecimal(code));
                                        dataStr = "[PTS] " + idFull + http + "push/" + dataName + "?auth=" + auth;
                                        if (!auth.equals("")) {
                                            dbTool.sendMTEx(receiver, serviceId, Utilities.buildMobileOperator(receiver), commandCode, 8, dataStr, 0,
                                                    new BigDecimal(requestId), " ", " ", " ");

                                        }
                                    } else if (prefixCode.startsWith("824")) {
                                        //Photo
                                        auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(code), dataType);
                                        dataStr = "[PTS] " + idFull + http + "push/" + idFull + ".gif?auth=" + auth;
                                        if (!auth.equals("")) {
                                            dbTool.sendMTEx(receiver, serviceId, Utilities.buildMobileOperator(receiver), commandCode, 8, dataStr, 0,
                                                    new BigDecimal(requestId), " ", " ", " ");
                                        }
                                    }
                                }
                            }
                            Thread.sleep(1000);
                        }
                    }
                }
                Thread.sleep(10000);
            } catch (Exception ex) {}
        }
    }

}
