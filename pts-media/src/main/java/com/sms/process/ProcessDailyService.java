package com.sms.process;

import java.util.*;
import com.sms.item.*;
import java.math.BigDecimal;
import java.security.SecureRandom;

import com.sms.common.*;

public class ProcessDailyService extends Thread {

    DBTool dbTool = null;
    static String newLine = "\n";
    static char enter = (char) 10;
    String alert = "";
    String result = "";
    String infoFormated = "";
    String commandId = "";
    SmsReceiveQueue smsReceiveQueue = null;
    BigDecimal requestId = null;
    String userId = "";
    String serviceId = "";
    String commandCode = "";
    int status = 0;
    String info = "";
    String infoTmp = "";
    static String http = ":http://222.252.16.196/";

    public ProcessDailyService() {
        dbTool = new DBTool();
    }

    public void processRequestQueue(String mobileOperator) {
        Vector listRequest = dbTool.getRequestFromQueue(mobileOperator, Preference.dailyCode);
        int index = 0;
        for (int i = 0; i < listRequest.size(); i++) {
            index = i;
            smsReceiveQueue = (SmsReceiveQueue) listRequest.elementAt(i);
            requestId = smsReceiveQueue.getId();
            userId = smsReceiveQueue.getUserId();
            serviceId = smsReceiveQueue.getServiceId();
            commandCode = smsReceiveQueue.getCommandCode();
            status = smsReceiveQueue.getStatus();
            info = (smsReceiveQueue.getInfo()).toUpperCase().trim();
            infoTmp = commandCode + " " + info.substring(commandCode.length(), info.length()).trim();

            String subCode1 = "";
            String subCode2 = "";
            String subCode3 = "";

            subCode1 = StringTool.getObjectString(infoTmp, 1);
            subCode2 = StringTool.getObjectString(infoTmp, 2);
            subCode3 = StringTool.getObjectString(infoTmp, 3);

//            System.out.println("subCode1:"+subCode1);
//            System.out.println("subCode2:"+subCode2);
//            System.out.println("subCode3:"+subCode3);
            try {
                if (commandCode.equals("XEM") || commandCode.equals("XM") || commandCode.equals("HHT") || commandCode.equals("BOI") || commandCode.equals("PH") ||
                        commandCode.equals("TN") || commandCode.equals("DP") || commandCode.equals("HM") || commandCode.equals("QM")) {
                    if (subCode1.startsWith("DUYEN")) {
                        commandId = "XEM";
                        //DUYEN length is 5

                        String girlYear = "";
                        String boyYear = "";
                        girlYear = subCode2;
                        boyYear = subCode3;

                        if (!StringTool.isNumberic(girlYear) || (girlYear.length() != 4) || !StringTool.isNumberic(boyYear) || (boyYear.length() != 4)) {
                            if (status != 0) {
                                if (commandCode.equalsIgnoreCase("HM") || commandCode.equalsIgnoreCase("QM")) {
                                    alert =
                                            "Ban soan tin sai cu phap cua Xem Duyen Hop tuoi.Hay soan " + commandCode +
                                            " DUYEN <NamSinhEm> <NamSinhAnh> gui8484,vi du " + commandCode + " DUYEN 1980 1978" + enter +
                                            "DT ho tro 1900561577";
                                } else {
                                    alert =
                                            "Ban soan tin sai cu phap cua Xem Duyen Hop tuoi.Hay soan " + commandCode +
                                            " DUYEN <NamSinhEm> <NamSinhAnh> gui8384,vi du " + commandCode + " DUYEN 1980 1978" + enter +
                                            "DT ho tro 1900561577";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "DUYEN<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            String lunarGirlYear = Utilities.getLunarYear(girlYear);
                            String lunarBoyYear = Utilities.getLunarYear(boyYear);
                            result = dbTool.getTXTData_4(commandId, subCode1, lunarGirlYear, lunarBoyYear);
                            if ((result != null) && !result.equals("")) {
                                if ((serviceId.endsWith("8184")) && (!Preference.adminPhoneNumber.contains(userId))) {
                                    if (commandCode.equalsIgnoreCase("HM") || commandCode.equalsIgnoreCase("QM")) {
                                        alert =
                                                "Ban gui sai so dich vu,nhan thong tin Xem Duyen Hop Tuoi theo yeu hay soan " + info +
                                                " gui 8484\nDT ho tro 1900561577";
                                    } else {
                                        alert =
                                                "Ban gui sai so dich vu,nhan thong tin Xem Duyen Hop Tuoi theo yeu hay soan " + info +
                                                " gui 8384\nDT ho tro 1900561577";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "DUYEN<==" + serviceId + ":WrongServiceId:" + info);
                                } else {
                                    boolean isSend = true;
                                    if (commandCode.equalsIgnoreCase("HM") || commandCode.equalsIgnoreCase("QM")) {
                                        if (serviceId.endsWith("8284") || serviceId.endsWith("8384")) {
                                            alert =
                                                    "Ban gui sai so dich vu,nhan thong tin Xem Duyen Hop Tuoi theo yeu hay soan " + info +
                                                    " gui 8484\nDT ho tro 1900561577";
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                            MyLogger.log(userId + "<==" + commandCode + "DUYEN<==" + serviceId + ":WrongServiceId:" + info);
                                            isSend = false;
                                        }
                                    }
                                    if (isSend) {
                                        String girlThapCan = Utilities.getLunarThapCan(girlYear);
                                        String boyThapCan = Utilities.getLunarThapCan(boyYear);
                                        result = girlThapCan + " " + lunarGirlYear + "-" + boyThapCan + " " + lunarBoyYear + "\n" + result;

                                        boolean deductMoney = false;
                                        if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                            result = StringTool.splitMsg(result);
                                        }
                                        result = result.replaceAll("!xxx!", "|");
                                        StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                        int stTokenLen = stToken.countTokens();
                                        for (int ii = 0; ii < stTokenLen; ii++) {
                                            String txtSms = stToken.nextToken();
                                            if (!deductMoney) {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                        txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                                deductMoney = true;
                                                MyLogger.log(userId + "<==" + commandCode + "DUYEN<==" + serviceId);
                                            } else {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                        txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                            }
                                            if (ii > 10) {
                                                break;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Xin loi ban,hien tai he thong chua co tu van chuyen gia ve Xem Duyen voi 2 nam sinh ban dua ra.\nDT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "DUYEN<==" + serviceId + ":NotFound:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        }
                    } else if (subCode1.equals("TUOI")) { //End XEM TUOI

                        commandId = "XEM";
                        String year = subCode2;
                        if (!StringTool.isNumberic(subCode2) || (subCode2.length() != 4)) {
                            if (status != 0) {
                                if (commandCode.equals("HM") || commandCode.equals("QM")) {
                                    alert =
                                            "Ban soan tin sai cu phap cua Xem Tuoi.Hay soan " + commandCode +
                                            " TUOI <Namsinh> gui8484,vidu: " + commandCode + " TUOI 1980 " + enter +
                                            "DT ho tro 1900561577";
                                } else {
                                    alert =
                                            "Ban soan tin sai cu phap cua Xem Tuoi.Hay soan " + commandCode +
                                            " TUOI <Namsinh> gui8384,vidu: " + commandCode + " TUOI 1980 " + enter +
                                            "DT ho tro 1900561577";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongFormat:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            String thapCan = Utilities.getLunarThapCan(year);
                            String yearLunar = Utilities.getLunarYear(year);
                            subCode2 = thapCan + yearLunar;

                            result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                            if ((result != null) && !result.equals("")) {
                                if (serviceId.endsWith("8084") || serviceId.endsWith("8184")) {
                                    if (commandCode.equals("HM") || commandCode.equals("QM")) {
                                        alert =
                                                "Ban gui sai so dich vu, Xem Tuoi theo yeu cau hay soan " + info +
                                                " gui 8484" + enter +
                                                "DT ho tro 1900561577";
                                    } else {
                                        alert =
                                                "Ban gui sai so dich vu, Xem Tuoi theo yeu cau hay soan " + info +
                                                " gui 8384" + enter +
                                                "DT ho tro 1900561577";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongServiceId");
                                } else {
                                    boolean isSend = true;
                                    if (commandCode.equals("HM") || commandCode.equals("QM")) {
                                        if (serviceId.endsWith("8284") || serviceId.endsWith("8384")) {
                                            alert =
                                                    "Ban gui sai so dich vu, Xem Tuoi theo yeu cau hay soan " + info +
                                                    " gui 8484" + enter +
                                                    "DT ho tro 1900561577";
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                            MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongServiceId");
                                            isSend = false;
                                        }
                                    }
                                    if (isSend) {
                                        if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                            result = StringTool.splitMsg(result);
                                        }

                                        boolean deductMoney = false;
                                        result = result.replaceAll("!xxx!", "|");
                                        StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                        int stTokenLen = stToken.countTokens();
                                        for (int ii = 0; ii < stTokenLen; ii++) {
                                            String txtSms = stToken.nextToken();
                                            if (!deductMoney) {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                        txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                                deductMoney = true;
                                                MyLogger.log(userId + "<==" + commandCode + "TUOI<==" + serviceId);
                                            } else {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                        txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                            }
                                            if (ii > 10) {
                                                break;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Xin loi ban,hien tai he thong chua co tu van chuyen gia ve Xem Tuoi voi nam sinh ban dua ra." + enter +
                                            "DT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "TUOI<==" + serviceId + ":NotFound:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        }
                    } else if (subCode1.equals("TEN")) {
                        commandId = "XEM";
                        subCode1 = "TEN";

                        infoTmp = infoTmp.substring(commandCode.length()).trim();
                        infoTmp = infoTmp.substring(subCode1.length()).trim();

                        if (infoTmp.equals("")) {
                            if (status != 0) {
                                if (commandCode.equals("HM") || commandCode.equals("QM")) {
                                    alert =
                                            "Ban soan tin sai cu phap cua XEM TEN.Hay soan " + commandCode +
                                            " TEN <TenCuaBan> gui8484,vidu: " + commandCode + " TEN Nguyen Minh Nga" + enter +
                                            "DT ho tro 1900561577";
                                } else {
                                    alert =
                                            "Ban soan tin sai cu phap cua XEM TEN.Hay soan " + commandCode +
                                            " TEN <TenCuaBan> gui8384,vidu: " + commandCode + " TEN Nguyen Minh Nga" + enter +
                                            "DT ho tro 1900561577";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongFormat:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            int sumData = Utilities.sumData(infoTmp);
                            result = dbTool.getTXTData_5(commandId, subCode1, String.valueOf(sumData));
                            if ((result != null) && !result.equals("")) {
//                                if (!serviceId.endsWith("8184") || (Preference.adminPhoneNumber.contains(userId))) {

                                if ((!commandCode.equals("HM") && (("8284,8384,8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                        (!commandCode.equals("QM") && (("8284,8384,8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                        (Preference.adminPhoneNumber.contains(userId)) ||
                                        (commandCode.endsWith("HM") && (("8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                        (commandCode.endsWith("QM") && (("8484,8584,8684,8784").indexOf(serviceId) != -1))) {
                                    if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                        result = StringTool.splitMsg(result);
                                    }

                                    boolean deductMoney = false;
                                    result = result.replaceAll("!xxx!", "|");
                                    StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                    int stTokenLen = stToken.countTokens();
                                    for (int ii = 0; ii < stTokenLen; ii++) {
                                        String txtSms = stToken.nextToken();
                                        if (!deductMoney) {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                            deductMoney = true;
                                            MyLogger.log(userId + "<==" + commandCode + " TEN<==" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                        }
                                        if (ii > 5) {
                                            break;
                                        }
                                    }
                                } else {
                                    if (commandCode.equals("HM") || commandCode.equals("QM")) {
                                        alert =
                                                "Ban gui sai so dich vu,de nhan thong tin Xem Ten theo yeu cau hay soan " + info +
                                                " gui 8484\nDT ho tro 1900561577";
                                    } else {
                                        alert =
                                                "Ban gui sai so dich vu,de nhan thong tin Xem Ten theo yeu cau hay soan " + info +
                                                " gui 8384\nDT ho tro 1900561577";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " TEN<==" + serviceId + ":WrongServiceId");
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Xin loi ban,hien tai he thong chua y kien cua chuyen gia ve Xem Ten voi thong tin ban dua ra.\nDT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " TEN<==" + serviceId + ":NotFound:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        }
                    } else if (subCode1.equals("EM") || subCode1.equals("ANH") || subCode1.equals("UK") || subCode1.equals("NS") ||
                            subCode1.equals("NGHE") || subCode1.equals("PQ") || subCode1.equals("CD")) {
                        String ddMM = "";
                        commandId = "XEM";

                        infoTmp = infoTmp.substring(commandCode.length()).trim();
                        infoTmp = infoTmp.substring(subCode1.length()).trim();
                        ddMM = infoTmp;
                        ddMM = ddMM.replaceAll("o", "0");
                        ddMM = ddMM.replaceAll("O", "0");
                        ddMM = StringTool.getContentOnlyNumber(ddMM);

                        if (ddMM.length() > 4) {
                            ddMM = ddMM.substring(0, 4);
                        }
                        if ((ddMM.length() == 0) || (ddMM.length() == 1)) {
                            if (status != 0) {
                                if (commandCode.equals("HM") || commandCode.equals("QM")) {
                                    alert =
                                            "Ban soan tin sai cu phap " + commandCode + " <TuKhoa> gui 8484,trong do <TuKhoa> la EM,ANH,NS,TEN..,vi du " +
                                            commandCode +
                                            " EM 0105." + enter +
                                            "DT 1900561577";
                                } else {
                                    alert =
                                            "Ban soan tin sai cu phap " + commandCode + " <TuKhoa> gui 8384,trong do <TuKhoa> la EM,ANH,NS,TEN..,vi du " +
                                            commandCode +
                                            " EM 0105." + enter +
                                            "DT 1900561577";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongFormat:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            //2-3-4
                            boolean trueFormat = false;
                            if (ddMM.length() == 2) {
                                String firstDigit = ddMM.substring(0, 1);
                                String secondDigit = ddMM.substring(1, 2);
                                if ((Integer.parseInt(firstDigit) > 0) && (Integer.parseInt(secondDigit) > 0)) {
                                    ddMM = "0" + firstDigit + "-" + "0" + secondDigit;
                                    trueFormat = true;
                                }
                            } else if (ddMM.length() == 3) {
                                String firstDigit = ddMM.substring(0, 1);
                                String secondDigit = ddMM.substring(1, 2);
                                String thirdDigit = ddMM.substring(2, 3);

                                if ((Integer.parseInt(firstDigit) > 0) && (Integer.parseInt(secondDigit + thirdDigit) <= 12) &&
                                        (Integer.parseInt(secondDigit + thirdDigit) > 0)) {
                                    // XYZ -> 0X-YZ
                                    ddMM = "0" + firstDigit + "-" + secondDigit + thirdDigit;
                                    trueFormat = true;
                                } else if ((!trueFormat) && (Integer.parseInt(firstDigit) == 0) && (Integer.parseInt(secondDigit) > 0) &&
                                        (Integer.parseInt(thirdDigit) > 0)) {
                                    //XYZ -> XY-0Z
                                    ddMM = firstDigit + secondDigit + "-" + "0" + thirdDigit;
                                    trueFormat = true;
                                } else if ((!trueFormat) && (Integer.parseInt(firstDigit + secondDigit) > 0) &&
                                        (Integer.parseInt(firstDigit + secondDigit) <= 31) &&
                                        (Integer.parseInt(thirdDigit) > 0)) {
                                    //XYZ -> XY-0Z
                                    ddMM = firstDigit + secondDigit + "-" + "0" + thirdDigit;
                                    trueFormat = true;
                                }
                            } else if (ddMM.length() == 4) {
                                if ((Integer.parseInt(ddMM.substring(0, 2)) > 0) && (Integer.parseInt(ddMM.substring(0, 2)) <= 31) &&
                                        (Integer.parseInt(ddMM.substring(2, 4)) > 0) && (Integer.parseInt(ddMM.substring(2, 4)) <= 12)) {
                                    ddMM = ddMM.substring(0, 2) + "-" + ddMM.substring(2, 4);
                                    trueFormat = true;
                                }
                            }

                            if (trueFormat) {
                                String chiemTinh = Utilities.getCungChiemTinh(ddMM);
                                result = dbTool.getTXTData_2(commandId, subCode1, chiemTinh);
                                if ((result != null) && !result.equals("")) {
                                    if ((!commandCode.equals("DP") && (("8384,8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                            (!commandCode.equals("HM") && (("8384,8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                            (!commandCode.equals("QM") && (("8384,8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                            (Preference.adminPhoneNumber.contains(userId)) ||
                                            (commandCode.endsWith("DP") && (("8584,8684,8784").indexOf(serviceId) != -1)) ||
                                            (commandCode.endsWith("HM") && (("8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                            (commandCode.endsWith("QM") && (("8484,8584,8684,8784").indexOf(serviceId) != -1))) {
                                        if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                            result = StringTool.splitMsg(result);
                                        }

                                        boolean deductMoney = false;
                                        result = result.replaceAll("!xxx!", "|");
                                        StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                        int stTokenLen = stToken.countTokens();
                                        for (int ii = 0; ii < stTokenLen; ii++) {
                                            String txtSms = stToken.nextToken();
                                            if (!deductMoney) {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                        txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                                deductMoney = true;
                                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId);
                                            } else {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                        txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                            }
                                            if (ii > 10) {
                                                break;
                                            }
                                        }
                                    } else {
                                        if (commandCode.equals("DP")) {
                                            alert =
                                                    "Ban gui sai so dich vu,de nhan thong tin " + commandCode + " theo yeu cau hay soan " + info +
                                                    " gui 8584" + enter + "DT ho tro 1900561577";
                                        } else if (commandCode.equals("HM") || commandCode.equals("QM")) {
                                            alert =
                                                    "Ban gui sai so dich vu,de nhan thong tin " + commandCode + " theo yeu cau hay soan " + info +
                                                    " gui 8484" + enter + "DT ho tro 1900561577";
                                        } else {
                                            alert =
                                                    "Ban gui sai so dich vu,de nhan thong tin " + commandCode + " theo yeu cau hay soan " + info +
                                                    " gui 8384" + enter + "DT ho tro 1900561577";
                                        }
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongServiceId:" + info);
                                    }
                                } else {
                                    if (status != 0) {
                                        alert =
                                                "Xin loi ban,hien tai he thong chua y kien cua chuyen gia ve " + commandCode +
                                                " voi thong tin ban dua ra.\nDT ho tro 1900561577";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                                    } else {
                                        dbTool.updateStatus(requestId, 1, mobileOperator);
                                    }
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Ban soan tin sai cu phap " + commandCode + " <TuKhoa> gui 8384,trong do <TuKhoa> la EM,ANH,NS,TEN..,vi du " +
                                            commandCode +
                                            " EM 0105." + enter +
                                            "DT 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongFormat:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        }
                    } else if (subCode1.equals("SO") || subCode1.equals("XO")) {
                        try {
                            if (!StringTool.isNumberic(subCode2) || !StringTool.isNumberic(subCode3) || (subCode3.length() != 4)) {
                                if (status != 0) {
                                    alert =
                                            "Ban soan tin sai cu phap.Soan " + commandCode + " SO <SoCanXem> <NamSinh> gui8584,vi du " + commandCode +
                                            " SO 0904686868 1975" + enter + "DT 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " SO<==" + serviceId + ":WrongFormat:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            } else {
                                String content = "";
                                double tongDiem = 0;

                                String[] amDuong = Utilities.getAmDuong(subCode2);
                                content = "1.AmDuong" + "\n" + amDuong[0] + "\n" + "Diem:" + String.valueOf(amDuong[1]) + "/2\n";
                                String[] thienThoi = Utilities.getThienThoi(subCode2);
                                content = content + "2.ThienThoi" + "\n" + thienThoi[0] + "\n" + "Diem:" + thienThoi[1] + "/0.5!xxx!";
                                String[] nguHanh = Utilities.getNguHanh(subCode2, subCode3);
                                content = content + "3.NguHanh" + "\n" + nguHanh[0] + "\n" + "Diem:" + nguHanh[1] + "/2.5!xxx!";
                                String[] quaiKhi = Utilities.getQuaiKhi(subCode2);
                                content = content + "4.QuaiKhi" + "\n" + quaiKhi[0] + "\n" + "Diem:" + quaiKhi[1] + "/3!xxx!";
                                String[] quanNiem = Utilities.getQuanNiem(subCode2);
                                content = content + "5.QuanNiem" + "\n" + quanNiem[0] + "\n" + "Diem:" + quanNiem[1] + "/2\n";
                                tongDiem = Double.parseDouble(amDuong[1]) + Double.parseDouble(thienThoi[1]) + Double.parseDouble(nguHanh[1]) +
                                        Double.parseDouble(quaiKhi[1]) + Double.parseDouble(quanNiem[1]);

                                if (tongDiem < 4) {
                                    content = content + "\n" + "Tong:" + String.valueOf(tongDiem) + "/10\n" + "Ket luan:Day so khong phu hop voi ban";
                                } else if (tongDiem < 5) {
                                    content = content + "\n" + "Tong:" + String.valueOf(tongDiem) + "/10\n" + "Ket luan:Day so kha binh thuong";
                                } else if (tongDiem < 6) {
                                    content = content + "\n" + "Tong:" + String.valueOf(tongDiem) + "/10\n" + "Ket luan:Day so tam duoc";
                                } else if (tongDiem < 8) {
                                    content = content + "\n" + "Tong:" + String.valueOf(tongDiem) + "/10\n" + "Ket luan:Ban dang so huu so kha dep";
                                } else if (tongDiem <= 10) {
                                    content = content + "\n" + "Tong:" + String.valueOf(tongDiem) + "/10\n" + "Ket luan:Ban dang so huu so rat dep";
                                }

                                //content = StringTool.splitMsg(content);
                                if (!content.equals("")) {
                                    if ((serviceId.endsWith("8084") || serviceId.endsWith("8184") || serviceId.endsWith("8284") || serviceId.endsWith("8384")) &&
                                            (!Preference.adminPhoneNumber.contains(userId))) {
                                        alert =
                                                "Ban gui sai toi dau so dich vu.Hay soan: " + info + " gui8584.\nDT 1900561577";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongServiceId");
                                    } else {
                                        boolean isSend = true;
                                        if (commandCode.equalsIgnoreCase("HM") || commandCode.equalsIgnoreCase("QM")) {
                                            if (serviceId.endsWith("8484")) {
                                                alert =
                                                        "Ban gui sai toi dau so dich vu.Hay soan: " + info + " gui8584.\nDT 1900561577";
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongServiceId");
                                                isSend = false;
                                            }
                                        }
                                        if (isSend) {
                                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                                result = StringTool.splitMsg(result);
                                            }

                                            boolean deductMoney = false;
                                            content = content.replaceAll("!xxx!", "|");
                                            StringTokenizer stToken = new StringTokenizer(content, "|", false);
                                            int stTokenLen = stToken.countTokens();
                                            for (int ii = 0; ii < stTokenLen; ii++) {
                                                String txtSms = stToken.nextToken();
                                                if (!deductMoney) {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                                    deductMoney = true;
                                                    MyLogger.log(userId + "<==" + commandCode + "SO<==" + serviceId);
                                                } else {
                                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                                }
                                                if (ii > 5) {
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    alert =
                                            "Hien tai chua co y kien chuyen gia ve day so ma ban xem.Soan " + commandCode +
                                            " SO <SoCanXem> <NamSinh> gui 8584,vi du " + commandCode + " SO 0904686868 1975.\nDT 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " SO<==" + serviceId + ":Content empty:" + info);
                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    } else if (subCode1.equals("HK") || subCode1.equals("XX") || subCode1.equals("XY") || subCode1.equals("MENH") ||
                            subCode1.equals("TT") || subCode1.equals("TA") || subCode1.equals("TE") || subCode1.equals("QT")) {
                        commandId = "XEM";

                        String ddMM = "";
                        infoTmp = infoTmp.substring(commandCode.length()).trim();
                        infoTmp = infoTmp.substring(subCode1.length()).trim();
                        ddMM = infoTmp;
                        ddMM = ddMM.replaceAll("o", "0");
                        ddMM = ddMM.replaceAll("O", "0");
                        ddMM = StringTool.getContentOnlyNumber(ddMM);

                        if ((ddMM.length() == 0) || (ddMM.length() == 1)) {
                            if (status != 0) {
                                if (commandCode.equals("DP")) {
                                    alert =
                                            "Tin nhan chua ro rang,soan " + commandCode +
                                            " TT NgayThangSinh gui 8584" + enter + "DT 1900561577";
                                } else if ((commandCode.equals("HM") || commandCode.equals("QM")) && (subCode1.equals("XX") || subCode1.equals("XY"))) {
                                    alert =
                                            "Tin nhan chua ro rang,soan " + commandCode +
                                            " TT NgayThangSinh gui 8484" + enter + "DT 1900561577";
                                } else if ((commandCode.equals("HM") || commandCode.equals("QM")) && (subCode1.equals("TA") || subCode1.equals("TE"))) {
                                    alert =
                                            "Tin nhan chua ro rang,soan " + commandCode +
                                            " TT NgayThangSinh gui 8584" + enter + "DT 1900561577";
                                } else {
                                    alert =
                                            "Tin nhan chua ro rang,soan " + commandCode +
                                            " CuPhap NgayThangSinh gui 8384,CuPhap la HK,XX,XY,MENH.Vidu xem sao chieu menh soan " + commandCode +
                                            " MENH 0605 gui 8384\nDT 1900561577";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " HK/XX/XY/MENH/..<==" + serviceId + ":WrongFormat:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            //2-3-4
                            boolean trueFormat = false;
                            if (ddMM.length() == 2) {
                                String firstDigit = ddMM.substring(0, 1);
                                String secondDigit = ddMM.substring(1, 2);
                                if ((Integer.parseInt(firstDigit) > 0) && (Integer.parseInt(secondDigit) > 0)) {
                                    ddMM = "0" + firstDigit + "-" + "0" + secondDigit;
                                    trueFormat = true;
                                }
                            } else if (ddMM.length() == 3) {
                                String firstDigit = ddMM.substring(0, 1);
                                String secondDigit = ddMM.substring(1, 2);
                                String thirdDigit = ddMM.substring(2, 3);

                                if ((Integer.parseInt(firstDigit) > 0) && (Integer.parseInt(secondDigit + thirdDigit) <= 12) &&
                                        (Integer.parseInt(secondDigit + thirdDigit) > 0)) {
                                    // XYZ -> 0X-YZ
                                    ddMM = "0" + firstDigit + "-" + secondDigit + thirdDigit;
                                    trueFormat = true;
                                } else if ((!trueFormat) && (Integer.parseInt(firstDigit) == 0) && (Integer.parseInt(secondDigit) > 0) &&
                                        (Integer.parseInt(thirdDigit) > 0)) {
                                    //XYZ -> XY-0Z
                                    ddMM = firstDigit + secondDigit + "-" + "0" + thirdDigit;
                                    trueFormat = true;
                                } else if ((!trueFormat) && (Integer.parseInt(firstDigit + secondDigit) > 0) &&
                                        (Integer.parseInt(firstDigit + secondDigit) <= 31) &&
                                        (Integer.parseInt(thirdDigit) > 0)) {
                                    //XYZ -> XY-0Z
                                    ddMM = firstDigit + secondDigit + "-" + "0" + thirdDigit;
                                    trueFormat = true;
                                }
                            } else if (ddMM.length() == 4) {
                                if ((Integer.parseInt(ddMM.substring(0, 2)) > 0) && (Integer.parseInt(ddMM.substring(0, 2)) <= 31) &&
                                        (Integer.parseInt(ddMM.substring(2, 4)) > 0) && (Integer.parseInt(ddMM.substring(2, 4)) <= 12)) {
                                    ddMM = ddMM.substring(0, 2) + "-" + ddMM.substring(2, 4);
                                    trueFormat = true;
                                }
                            }

                            if (trueFormat) {
                                String chiemTinh = Utilities.getCungChiemTinh(ddMM);
                                result = dbTool.getTXTData_2(commandId, subCode1, chiemTinh);
                                if ((result != null) && !result.equals("")) {
                                    result = result.replaceAll("\r\n", "\n");
                                    if ((!commandCode.equals("DP") && (("8184,8284").indexOf(serviceId) == -1)) ||
                                            (!commandCode.equals("HM") && (("8184,8284").indexOf(serviceId) == -1)) ||
                                            (!commandCode.equals("QM") && (("8184,8284").indexOf(serviceId) == -1)) ||
                                            (commandCode.equals("DP") && (("8184,8284,8384,8484").indexOf(serviceId) == -1)) ||
                                            (commandCode.equals("HM") && (("8184,8284,8384").indexOf(serviceId) == -1)) ||
                                            (commandCode.equals("QM") && (("8184,8284,8384").indexOf(serviceId) == -1)) ||
                                            (Preference.adminPhoneNumber.contains(userId))) {
                                        if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                            result = StringTool.splitMsg(result);
                                        }

                                        boolean deductMoney = false;
                                        result = result.replaceAll("!xxx!", "|");
                                        StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                        int stTokenLen = stToken.countTokens();
                                        for (int ii = 0; ii < stTokenLen; ii++) {
                                            String txtSms = stToken.nextToken();
                                            if (!deductMoney) {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                        txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                                deductMoney = true;
                                                MyLogger.log(userId + "<==" + commandCode + " HK/XX/XY/MENH<==" + serviceId);
                                            } else {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                        txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                            }
                                            if (ii > 5) {
                                                break;
                                            }
                                        }
                                    } else {
                                        if (commandCode.indexOf("DP") != -1) {
                                            alert =
                                                    "Ban gui sai so dich vu,hay soan: " + info +
                                                    " gui 8584." + enter +
                                                    "DT ho tro 1900561577";
                                        } else {
                                            alert =
                                                    "Ban gui sai so dich vu,hay soan " + info +
                                                    " gui 8384." + enter +
                                                    "XEM SO soan XEM SO <SoCanXem> <NamSinh> gui8584" + enter +
                                                    "DT ho tro 1900561577";
                                        }
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongServiceId");
                                    }
                                } else {
                                    if (status != 0) {
                                        alert =
                                                "Xin loi ban,hien tai he thong chua y kien cua chuyen gia ve thong tin ban dua ra.\nDT ho tro 1900561577";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + " HK/XX/XY/MENH/..<==" + serviceId + ":NotFound:" + info);
                                    } else {
                                        dbTool.updateStatus(requestId, 1, mobileOperator);
                                    }
                                }
                            } else {
                                if (status != 0) {
                                    if ((commandCode.equals("HM") || commandCode.equals("QM"))) {
                                        alert =
                                                "Tin nhan chua ro rang,soan " + commandCode +
                                                " CuPhap NgayThangSinh gui 8584,CuPhap la HK,XX,XY,MENH..,vi du xem sao chieu menh soan " + commandCode +
                                                " MENH 0605 gui 8384\nDT 1900561577";
                                    } else {
                                        alert =
                                                "Tin nhan chua ro rang,soan " + commandCode +
                                                " CuPhap NgayThangSinh gui 8384,CuPhap la HK,XX,XY,MENH..,vi du xem sao chieu menh soan " + commandCode +
                                                " MENH 0605 gui 8384\nDT 1900561577";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " HK/XX/XY/MENH/..<==" + serviceId + ":WrongFormat:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        }
                    } else if (subCode1.equals("MT")) {
                        commandId = "XEM";
                        subCode2 = subCode2.replaceAll("o", "0");
                        subCode2 = subCode2.replaceAll("O", "0").trim();
                        if (StringTool.isNumberic(subCode2)) {
                            if (subCode2.length() > 3) {
                                subCode2 = subCode2.substring(3);
                            } else if (Integer.parseInt(subCode2) < 20) {
                                if (subCode2.length() == 1) {
                                    subCode2 = "00" + subCode2;
                                }
                                if (subCode2.length() == 2) {
                                    subCode2 = "0" + subCode2;
                                }
                            }
                            result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                            if ((result != null) && !result.equals("")) {
                                result = result.replaceAll("\r\n", "\n");
                                result = StringTool.getContentOnlyCharEx(result);
//                                if (!serviceId.endsWith("8184") || !serviceId.endsWith("8284") || (Preference.adminPhoneNumber.contains(userId))) {
                                if ((!commandCode.equals("QM") && (("8384,8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                        (Preference.adminPhoneNumber.contains(userId)) ||
                                        (commandCode.endsWith("QM") && (("8484,8584,8684,8784").indexOf(serviceId) != -1))) {
                                    if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                        result = StringTool.splitMsg(result);
                                    }

                                    boolean deductMoney = false;
                                    result = result.replaceAll("!xxx!", "|");
                                    StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                    int stTokenLen = stToken.countTokens();
                                    for (int ii = 0; ii < stTokenLen; ii++) {
                                        String txtSms = stToken.nextToken();
                                        if (!deductMoney) {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                            deductMoney = true;
                                            MyLogger.log(userId + "<==" + commandCode + " MT<==" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                        }
                                        if (ii > 5) {
                                            break;
                                        }
                                    }
                                } else {
                                    if (commandCode.equals("QM")) {
                                        alert =
                                                "Ban gui sai so dich vu,de nhan Tu van mua thi hay soan " + info +
                                                " gui 8484" + enter + "DT ho tro 1900561577";
                                    } else {
                                        alert =
                                                "Ban gui sai so dich vu,de nhan Tu van mua thi hay soan " + info +
                                                " gui 8384" + enter + "DT ho tro 1900561577";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " MT<==" + serviceId + ":WrongServiceId");
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                            ".De co thong tin tu van mua thi soan " + commandCode +
                                            " MT MaSo gui 8384,trong do MaSo tu 001 den 019.\nDT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==MT<==" + serviceId + ":NotFound:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Ma so Kien thuc mua thi khong dung,hay soan " + commandCode + " MT MaSo gui 8384,vidu " + commandCode +
                                        " MT 001 gui 8384.\nDT 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " MT<==" + serviceId + ":WrongFormat:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("NR") || subCode1.equals("ND") || subCode1.equals("NRN")) {
                        commandId = "XEM";

                        if (subCode1.startsWith("ND")) {
                            subCode1 = "NR";
                        }

                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            //result = StringTool.getContentOnlyCharEx(result);
                            if ((!commandCode.equals("DP") && (("8184,8284,8384").indexOf(serviceId) == -1)) ||
                                    (Preference.adminPhoneNumber.contains(userId)) ||
                                    (commandCode.equals("DP") && (("8184,8284,8384,8484").indexOf(serviceId) == -1))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + " NR/NRN<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 10) {
                                        break;
                                    }
                                }
                            } else {
                                if (commandCode.equals("DP")) {
                                    alert =
                                            "Ban gui sai so dich vu,de nhan Bi mat not ruoi soan: " + info +
                                            " gui8584" + enter + "DT ho tro 1900561577";
                                } else {
                                    alert =
                                            "Ban gui sai so dich vu,de nhan Bi mat not ruoi soan: " + info +
                                            " gui8484" + enter + "DT ho tro 1900561577";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongServiceId");
                            }
                        } else {
                            if (status != 0) {
                                if (commandCode.equals("NR")) {
                                    commandCode = "XEM";
                                }
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                        ".Bi mat not ruoi soan " + commandCode + " NR <MaSo> gui 8484,hoac " + commandCode +
                                        " NRN <MaSo> gui8484." + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==NR<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("DD") || subCode1.equals("MS")) {
                        //XEM dangdi, Mausac
                        commandId = "XEM";

                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            result = StringTool.getContentOnlyCharEx(result);
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + " DD/MS<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 10) {
                                        break;
                                    }
                                }
                            } else {
                                if (subCode1.equals("MS")) {
                                    alert =
                                            "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                            " gui 8384\nDT ho tro 1900561577";
                                } else {
                                    alert =
                                            "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                            " gui 8584\nDT ho tro 1900561577";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongServiceId");
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                        "\nBi mat DangDi soan: " + commandCode + " DD MaSo gui 8584.Bimat mausac soann:" + commandCode +
                                        "MS Maso gui8384" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("HON")) {
                        //XEM HON
                        commandId = "XEM";
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            //result = StringTool.getContentOnlyCharEx(result);
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==HON<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 10) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                        " gui8384" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + ":" + serviceId + ":WrongServiceId");
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                        "\nNhan thong Kieu Hon soan: " + commandCode + " HON MaSo gui8584" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==HON<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("BK")) {
                        //XEM BK -> So thich an banh keo cua nang
                        commandId = "XEM";
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            //result = StringTool.getContentOnlyCharEx(result);
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==BK<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 10) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                        " gui8384\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + ":" + serviceId + ":WrongServiceId");
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                        "\nBiet tinhcach cua nang qua so thich BanhKeo soan: " + commandCode + " BK MaSo gui8384" + enter +
                                        "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + ":" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("TV")) {
                        commandId = "XEM";

                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) ||
                                    (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==TV<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                        " gui8384\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==TV<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                        "\nBiet van may qua tuong so soan: " + commandCode + " TV MaSo gui8584" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==TV<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("DU")) {
                        commandId = "XEM";

                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            //result = StringTool.getContentOnlyCharEx(result);
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==DU<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                        " gui8384\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==DU<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                        "\nBiet TinhCach qua Do Uong soan: " + commandCode + " DU MaSo gui8584" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==DU<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("MO")) {
                        commandId = "XEM";
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            result = result.trim();
                            //result = StringTool.getContentOnlyCharEx(result);
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==MO<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                        " gui8384\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==MO<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                        "\nBi an Giac Mo soan: " + commandCode + " MO <MaSo> gui8384" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==MO<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("YN")) {
                        commandId = "XEM";
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);

                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            result = result.trim();
                            //result = StringTool.getContentOnlyCharEx(result);
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==YN<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                        " gui8384\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==YN<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                        "\nNguoi yeu phu hop voi tinh cach soan: " + commandCode + " YN <MaSo> gui8384" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==YN<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("YEU")) {
                        commandId = "XEM";
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);

                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            //result = StringTool.getContentOnlyCharEx(result);
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384")) ||
                                    (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==YEU<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                        " gui8484\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==YEU<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                        "\nBiet phong cach YEU qua mau sac soan: " + commandCode + " YEU MaSo gui8484" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==YEU<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("GT") || subCode1.equals("EX")) {
                        commandId = "XEM";
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            //result = StringTool.getContentOnlyCharEx(result);
                            if ((("8184,8284").indexOf(serviceId) == -1) ||
                                    (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==GT<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin soan: " + info +
                                        " gui8584\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==GT<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                        "\nNhan thong tin gioitinh soan: " + commandCode + " GT MaSo gui8584" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==GT<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("GH")) {
                        //Xem GiangHO Lo Van Khoi
                        //infoTmp = commandCode + " " + info.substring(commandCode.length(), info.length()).trim();
                        infoTmp = infoTmp.substring(commandCode.length()).trim();
                        infoTmp = infoTmp.substring(subCode1.length()).trim();
//                        String nameStr = infoTmp.substring(2).toUpperCase().trim();
                        String nameStr = infoTmp.toUpperCase();
                        nameStr = StringTool.getContentOnlyCharEx(nameStr);
                        nameStr = nameStr.replace('.', ' ');
                        nameStr = nameStr.replace('!', ' ');
                        nameStr = nameStr.replace(',', ' ');

                        StringTokenizer tk = new StringTokenizer(nameStr, " ");
                        String nameInOther = "";
                        int countTk = tk.countTokens();
                        int sequenceTk = 0;
                        boolean trueFormat = false;
                        if (countTk == 2) {
                            //Ho Ten
                            while (tk.hasMoreTokens()) {
                                if (sequenceTk == 0) {
                                    nameInOther = Utilities.getLastName(tk.nextToken().substring(0, 1));
                                }
                                if (sequenceTk == 1) {
                                    nameInOther = nameInOther + " " + Utilities.getLastName(tk.nextToken().substring(0, 1));
                                }
                                sequenceTk++;
                            } //end while

                            trueFormat = true;
                            alert = nameStr + " cach goi Giang Ho se la: " + enter + nameInOther;
                        } else if (countTk == 3) {
                            while (tk.hasMoreTokens()) {
                                if (sequenceTk == 0) {
                                    nameInOther = Utilities.getLastName(tk.nextToken().substring(0, 1));
                                } else if (sequenceTk == 1) {
                                    nameInOther = nameInOther + " " + Utilities.getMidName(tk.nextToken().substring(0, 1));
                                } else if (sequenceTk == 2) {
                                    nameInOther = nameInOther + " " + Utilities.getFirstName(tk.nextToken().substring(0, 1));
                                }
                                sequenceTk++;
                            } //end while

                            trueFormat = true;
                        } else if (countTk == 4) {
                            while (tk.hasMoreTokens()) {
                                if (sequenceTk == 0) {
                                    nameInOther = Utilities.getLastName(tk.nextToken().substring(0, 1));
                                } else if (sequenceTk == 1) {
                                    nameInOther = nameInOther + " " + Utilities.getMidName(tk.nextToken().substring(0, 1));
                                } else if (sequenceTk == 2) {
                                    nameInOther = nameInOther + " " + Utilities.getMidName(tk.nextToken().substring(0, 1));
                                } else if (sequenceTk == 3) {
                                    nameInOther = nameInOther + " " + Utilities.getFirstName(tk.nextToken().substring(0, 1));
                                }
                                sequenceTk++;
                            } //end while

                            trueFormat = true;
                        } else {
                            alert = "Tin nhan sai dinh dang,de xem ten theo cach goi Giang Ho soan: " + commandCode +
                                    " GH HoVaTen gui 8584,luu y:HoVaTen viet cach nhau" + enter +
                                    "Vidu soan: " + commandCode + " GH Hoang Nhat Cuong gui8584" + enter + "DT ho tro 1900561577";
                        }

                        if (trueFormat) {
                            if (serviceId.endsWith("8384") || serviceId.endsWith("8484") || serviceId.endsWith("8584") || serviceId.endsWith("8684") ||
                                    serviceId.endsWith("8784") || Preference.adminPhoneNumber.contains(userId)) {
                                nameInOther = nameStr + " co ten Giang Ho la:" + enter + nameInOther +
                                        enter + "De nhan dc Maso cac Bai hat HAY NHAT,MOI NHAT,soan: TOP gui8384" +
                                        enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, nameInOther, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " GH<==" + serviceId);
                            } else {
                                alert = "Ban gui sai so dich vu,de nhan thong tin soan:" + info + " gui8584" + enter +
                                        "De nhan ma so cac hinh anh HOT moi soan: HOT gui8384" + enter +
                                        "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " GH<==" + serviceId + ":WrongServiceId");
                            }
                        } else {
                            if (status != 0) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " GH:WrongFormat<==" + serviceId + ":" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("HT")) {
                        //XEM HT TY (Xem Hop tuoi)
                        commandId = "XEM";
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            result = result.replaceAll("\n", "|");
                            result = result.replace('|', enter);

                            result = StringTool.getContentOnlyCharEx(result);
//                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) ||
//                                    (Preference.adminPhoneNumber.contains(userId))) {

                            if ((!commandCode.equals("QM") && (("8384,8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                    (Preference.adminPhoneNumber.contains(userId)) ||
                                    (commandCode.endsWith("QM") && (("8484,8584,8684,8784").indexOf(serviceId) != -1))) {

                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==HT<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                if (commandCode.equalsIgnoreCase("QM")) {
                                    alert =
                                            "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                            " gui 8484" + enter + "DT ho tro 1900561577";
                                } else {
                                    alert =
                                            "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                            " gui 8384" + enter + "DT ho tro 1900561577";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==HT<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                        +enter + "Xem hoptuoi lam an soan: " + commandCode + " HT TenConGiap gui 8584.Vi du: " + commandCode +
                                        " HT TUAT gui8584" +
                                        enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==HT<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("MV")) {
                        commandId = "XEM";
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            //result = StringTool.getContentOnlyCharEx(result);
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) ||
                                    (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                        " gui8384\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==XEM MV<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Ban soan sai dinh dang, moi soan " + commandCode + " " + subCode1 + " <MaSo> gui8384" +
                                        enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==HT<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("PT")) {
                        //XEM PhongThuy
                        commandId = "XEM";
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            //result = StringTool.getContentOnlyCharEx(result);
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384")) ||
                                    (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==PT<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                        " gui8484\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==PT<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                        "\nNhan thong tin PhongThuy soan: " + commandCode + " PT MaSo gui8584" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==PT<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("TK")) {
                        //Boi kieu
                        commandId = "XEM";
                        infoTmp = infoTmp.substring(commandCode.length()).trim();
                        infoTmp = infoTmp.substring(subCode1.length()).trim();
                        infoTmp = StringTool.getContentNoWhiteLetter(infoTmp);

                        if ((infoTmp != null) && (!infoTmp.equals("")) && StringTool.isNumberic(infoTmp)) {
                            result = "";
                        } else {
                            subCode2 = String.valueOf(Utilities.getValueFromString(infoTmp));
                            result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        }

                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            //result = StringTool.getContentOnlyCharEx(result);

                            if ((!commandCode.equals("HM") && (("8384,8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                    (!commandCode.equals("QM") && (("8384,8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                    (Preference.adminPhoneNumber.contains(userId)) ||
                                    (commandCode.endsWith("HM") && (("8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                    (commandCode.endsWith("QM") && (("8484,8584,8684,8784").indexOf(serviceId) != -1))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==TK<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                if (commandCode.equalsIgnoreCase("HM") || commandCode.equalsIgnoreCase("QM")) {
                                    alert =
                                            "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                            " gui8484\nDT ho tro 1900561577";
                                } else {
                                    alert =
                                            "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                            " gui8384\nDT ho tro 1900561577";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==TK<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                        "\nNhan thong tin BoiKieu soan: " + commandCode + " TK <TenAnh> <TenEm> gui8384" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==TK<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } //                    else if (subCode1.equals("DT")) {
                    //                        infoTmp = infoTmp.substring(commandCode.length()).trim();
                    //                        infoTmp = infoTmp.substring(subCode1.length()).trim();
                    //
                    //                        infoTmp = StringTool.getContentOnlyCharAndNumber(infoTmp);
                    //                        //Kiem tra diem co chua
                    //                        boolean isGetMark = false;
                    //                        boolean isGetStandardMark = false;
                    //                        String schoolCode = "";
                    //
                    //                        for (int j = 0; j < Main.schoolCodeHavePoint.size(); j++) {
                    //                            if ((infoTmp != null) && (!infoTmp.equals("")) && infoTmp.startsWith((String) Main.schoolCodeHavePoint.elementAt(j))) {
                    //                                schoolCode = (String) Main.schoolCodeHavePoint.elementAt(j);
                    //                                if (infoTmp.substring(schoolCode.length()).length() > 2) {
                    //                                    isGetMark = true;
                    //                                } else {
                    //                                    isGetStandardMark = true;
                    //                                }
                    //                                break;
                    //                            }
                    //                        }
                    //
                    //                        for (int j = 0; j < Main.schoolCode.size(); j++) {
                    //                            if ((infoTmp != null) && (!infoTmp.equals("")) && infoTmp.startsWith((String) Main.schoolCode.elementAt(j))) {
                    //                                schoolCode = (String) Main.schoolCode.elementAt(j);
                    //                                if (infoTmp.substring(schoolCode.length()).length() > 2) {
                    //                                    isGetMark = true;
                    //                                } else {
                    //                                    isGetStandardMark = true;
                    //                                }
                    //                                break;
                    //                            }
                    //                        }
                    //                        if (isGetMark) {
                    //                            if ((alert = dbTool.getDiemThi(infoTmp)).equals("")) {
                    //                                if (status != 0) {
                    //                                    alert =
                    //                                            "Khong tim thay thong tin diem thi ban yeu cau,moi soan " + commandCode +
                    //                                            " <SoBD> gui8384,SoBD gom MaTruong,MaKhoi,SoBaoDanh" + enter + "Xem diem Chuan soan: " + commandCode +
                    //                                            " DT <MaTruong> gui8384";
                    //                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                    //                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                    //                                } else {
                    //                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                    //                                }
                    //                            } else {
                    //                                if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                    //                                    alert = alert + enter + "De biet DiemChuan soan: " + commandCode + " DT <MaTruong> gui8384";
                    //                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                    //                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                    //                                } else {
                    //                                    alert =
                    //                                            "Ban gui sai so dich vu,de nhan DiemThi DH&CD soan: " + info + " gui8384\nDT ho tro 1900561577";
                    //                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                    //                                    MyLogger.log(userId + "<==" + commandCode + "<==WrongServiceId:" + serviceId);
                    //                                }
                    //                            }
                    //                        } else if (isGetStandardMark) {
                    //                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                    //                                synchronized (Main.schoolStandardMark) {
                    //                                    result = (String) Main.schoolStandardMark.get(schoolCode);
                    //                                    if ((result != null) && (!result.equals(""))) {
                    //                                        if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                    //                                            result = StringTool.splitMsg(result);
                    //                                        }
                    //
                    //                                        boolean deductMoney = false;
                    //                                        result = result.replaceAll("!xxx!", "|");
                    //                                        StringTokenizer stToken = new StringTokenizer(result, "|", false);
                    //                                        int stTokenLen = stToken.countTokens();
                    //                                        for (int ii = 0; ii < stTokenLen; ii++) {
                    //                                            String txtSms = stToken.nextToken();
                    //                                            if (!deductMoney) {
                    //                                                dbTool.sendMT(userId, serviceId, mobileOperator, commandCode, 0,
                    //                                                        txtSms, 1, requestId);
                    //                                                deductMoney = true;
                    //                                                MyLogger.log(userId + "<==DTC<==" + serviceId);
                    //                                            } else {
                    //                                                dbTool.sendMT(userId, serviceId, mobileOperator, commandCode, 0,
                    //                                                        txtSms, 0, requestId);
                    //                                            }
                    //                                            if (ii > 5)
                    //                                                break;
                    //                                        }
                    //                                    } else if (dbTool.add2DiemThiWait(requestId, userId, serviceId, mobileOperator, commandCode, info, schoolCode, 0, infoTmp)) {
                    //                                        alert = "Ban da dang ky nhan DiemChuan DH&CD thanh cong,chung toi se gui ban ngay khi co kq tu bo GD&DT" +
                    //                                                enter +
                    //                                                "De biet diem thi soan: " + commandCode + " DT <MaTruong,MaKhoi,SoBaoDanh> gui8384.";
                    //                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                    //                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                    //                                    }
                    //                                }
                    //                            } else {
                    //                                alert =
                    //                                        "Ban gui sai so dich vu,de nhan DiemThi DH&CD soan: " + info + " gui8384\nDT ho tro 1900561577";
                    //                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                    //                                MyLogger.log(userId + "<==" + commandCode + "<==WrongServiceId:" + serviceId);
                    //                            }
                    //                        } else {
                    //                            if (status != 0) {
                    //                                alert = "Ban gui sai ma truong DH hoac CD, moi soan: " + commandCode +
                    //                                        " DT <MaTruong,MaKhoi,SoBaoDanh> gui8384.Xem diem Chuan soan: " + commandCode + " DT <MaTruong> gui8384";
                    //                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                    //                                MyLogger.log(userId + "<==" + commandCode + "<==WrongFormat:" + serviceId + ":" + info);
                    //                            } else {
                    //                                dbTool.updateStatus(requestId, 1, mobileOperator);
                    //                            }
                    //                        }
                    //                    }
                    else if (subCode1.equals("NGAY") || subCode1.equals("TUAN") || subCode1.equals("TUAN") || subCode1.equals("THANG") || subCode1.equals("BAI") ||
                            subCode1.equals("NAM")) {
                        commandId = "XEM";
                        //Boi bai
                        int type = 0;
                        boolean wrongServiceId = false;

                        if (subCode1.equals("NGAY")) {
                            //8384
                        } else if (subCode1.equals("TUAN")) {
                            //8484
                            type = 1;
                        } else if (subCode1.equals("THANG")) {
                            //8584
                            type = 2;
                        } else if (subCode1.equals("BAI") || subCode1.equals("NAM")) {
                            //8684
                            subCode1 = "BAI";
                            type = 3;
                        }

                        if (!StringTool.isNumberic(subCode2)) {
                            if (status != 0) {
                                if (type == 0) {
                                    alert =
                                            "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                            "\nBi an trong Ngay cua ban soan: " + commandCode + " NGAY <NgaySinh> gui8484" + enter + "DT ho tro 1900561577";
                                } else if (type == 1) {
                                    alert =
                                            "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                            "\nBi an trong Tuan cua ban soan: " + commandCode + " TUAN <NgaySinh> gui8584" + enter + "DT ho tro 1900561577";
                                } else if (type == 2) {
                                    alert =
                                            "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                            "\nBi an trong Thang cua ban soan: " + commandCode + " THANG <NgaySinh> gui8684" + enter + "DT ho tro 1900561577";
                                } else if (type == 3) {
                                    alert =
                                            "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                            "\nBi an trong Nam cua ban soan: " + commandCode + " NAM <NgaySinh> gui8784" + enter + "DT ho tro 1900561577";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            result = dbTool.getTXTData_1(commandId, subCode1);
                            if ((result != null) && !result.equals("")) {
                                result = result.replaceAll("\r\n", "\n");
                                //result = StringTool.getContentOnlyCharEx(result);
                                if ((type == 0) && (serviceId.endsWith("8184") || serviceId.endsWith("8284")) &&
                                        !Preference.adminPhoneNumber.contains(userId)) {
                                    wrongServiceId = true;
                                } else if ((type == 1) &&
                                        (serviceId.endsWith("8184") || serviceId.endsWith("8284") || serviceId.endsWith("8384")) &&
                                        !Preference.adminPhoneNumber.contains(userId)) {
                                    wrongServiceId = true;
                                } else if ((type == 2) &&
                                        (serviceId.endsWith("8184") || serviceId.endsWith("8284") || serviceId.endsWith("8384") || serviceId.endsWith("8484")) &&
                                        !Preference.adminPhoneNumber.contains(userId)) {
                                    wrongServiceId = true;
                                } else if ((type == 3) &&
                                        (serviceId.endsWith("8184") || serviceId.endsWith("8284") || serviceId.endsWith("8384") || serviceId.endsWith("8484") ||
                                        serviceId.endsWith("8584")) && !Preference.adminPhoneNumber.contains(userId)) {
                                    wrongServiceId = true;
                                }

                                if (!wrongServiceId) {
                                    if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                        result = StringTool.splitMsg(result);
                                    }

                                    boolean deductMoney = false;
                                    result = result.replaceAll("!xxx!", "|");
                                    StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                    int stTokenLen = stToken.countTokens();
                                    for (int ii = 0; ii < stTokenLen; ii++) {
                                        String txtSms = stToken.nextToken();
                                        if (!deductMoney) {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                            deductMoney = true;
                                            MyLogger.log(userId + "<==" + commandCode + "<==" + subCode1 + "<==" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                        }
                                        if (ii > 5) {
                                            break;
                                        }
                                    }
                                } else {
                                    if (type == 0) {
                                        alert =
                                                "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                                " gui8384\nDT ho tro 1900561577";
                                    } else if (type == 1) {
                                        alert =
                                                "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                                " gui8484\nDT ho tro 1900561577";
                                    } else if (type == 2) {
                                        alert =
                                                "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                                " gui8584\nDT ho tro 1900561577";
                                    } else if (type == 3) {
                                        alert =
                                                "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                                " gui8684\nDT ho tro 1900561577";
                                    }

                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + subCode1 + "<==" + serviceId + ":WrongServiceId:" + info);
                                }
                            } else {
                                if (status != 0) {
                                    if (type == 0) {
                                        alert =
                                                "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                                "\nBi an trong Ngay cua ban soan: " + commandCode + " NGAY <NgaySinh> gui8484" + enter + "DT ho tro 1900561577";
                                    } else if (type == 1) {
                                        alert =
                                                "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                                "\nBi an trong Tuan cua ban soan: " + commandCode + " TUAN <NgaySinh> gui8584" + enter + "DT ho tro 1900561577";
                                    } else if (type == 2) {
                                        alert =
                                                "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                                "\nBi an trong Thang cua ban soan: " + commandCode + " THANG <NgaySinh> gui8684" + enter +
                                                "DT ho tro 1900561577";
                                    } else if (type == 3) {
                                        alert =
                                                "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                                "\nBi an trong Nam cua ban soan: " + commandCode + " NAM <NgaySinh> gui8784" + enter + "DT ho tro 1900561577";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        }
                    } else if (subCode1.equals("CS") || subCode1.equals("DT") || subCode1.equals("NQ")) {
                        commandId = "XEM";
                        String ddMM = "";
                        ddMM = infoTmp.substring(commandCode.length()).trim();
                        ddMM = ddMM.substring(subCode1.length()).trim();

                        ddMM = ddMM.replaceAll("o", "0");
                        ddMM = ddMM.replaceAll("O", "0");
                        ddMM = StringTool.getContentOnlyNumber(ddMM);

                        if (ddMM.length() > 4) {
                            ddMM = ddMM.substring(0, 4);
                        }
                        if ((ddMM.length() == 0) || (ddMM.length() == 1)) {
                            if (status != 0) {
                                alert =
                                        "Ban soan tin sai cu phap " + commandCode + " " + subCode1 + " <NgayThangSinh> gui8584.Vi du " +
                                        commandCode + " " + subCode1 + " 2011.\nDT 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongFormat:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            //2-3-4
                            boolean trueFormat = false;
                            if (ddMM.length() == 2) {
                                String firstDigit = ddMM.substring(0, 1);
                                String secondDigit = ddMM.substring(1, 2);
                                if ((Integer.parseInt(firstDigit) > 0) && (Integer.parseInt(secondDigit) > 0)) {
                                    ddMM = "0" + firstDigit + "-" + "0" + secondDigit;
                                    trueFormat = true;
                                }
                            } else if (ddMM.length() == 3) {
                                String firstDigit = ddMM.substring(0, 1);
                                String secondDigit = ddMM.substring(1, 2);
                                String thirdDigit = ddMM.substring(2, 3);

                                if ((Integer.parseInt(firstDigit) > 0) && (Integer.parseInt(secondDigit + thirdDigit) <= 12) &&
                                        (Integer.parseInt(secondDigit + thirdDigit) > 0)) {
                                    // XYZ -> 0X-YZ
                                    ddMM = "0" + firstDigit + "-" + secondDigit + thirdDigit;
                                    trueFormat = true;
                                } else if ((!trueFormat) && (Integer.parseInt(firstDigit) == 0) && (Integer.parseInt(secondDigit) > 0) &&
                                        (Integer.parseInt(thirdDigit) > 0)) {
                                    //XYZ -> XY-0Z
                                    ddMM = firstDigit + secondDigit + "-" + "0" + thirdDigit;
                                    trueFormat = true;
                                } else if ((!trueFormat) && (Integer.parseInt(firstDigit + secondDigit) > 0) &&
                                        (Integer.parseInt(firstDigit + secondDigit) <= 31) &&
                                        (Integer.parseInt(thirdDigit) > 0)) {
                                    //XYZ -> XY-0Z
                                    ddMM = firstDigit + secondDigit + "-" + "0" + thirdDigit;
                                    trueFormat = true;
                                }
                            } else if (ddMM.length() == 4) {
                                if ((Integer.parseInt(ddMM.substring(0, 2)) > 0) && (Integer.parseInt(ddMM.substring(0, 2)) <= 31) &&
                                        (Integer.parseInt(ddMM.substring(2, 4)) > 0) && (Integer.parseInt(ddMM.substring(2, 4)) <= 12)) {
                                    ddMM = ddMM.substring(0, 2) + "-" + ddMM.substring(2, 4);
                                    trueFormat = true;
                                }
                            }
                            if (trueFormat) {
                                String chiemTinh = Utilities.getCungChiemTinh(ddMM);
                                result = dbTool.getTXTData_2(commandId, subCode1, chiemTinh);
                                if ((result != null) && !result.equals("")) {
                                    if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") &&
                                            !serviceId.endsWith("8484")) ||
                                            (Preference.adminPhoneNumber.contains(userId))) {
                                        if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                            result = StringTool.splitMsg(result);
                                        }

                                        boolean deductMoney = false;
                                        result = result.replaceAll("!xxx!", "|");
                                        StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                        int stTokenLen = stToken.countTokens();
                                        for (int ii = 0; ii < stTokenLen; ii++) {
                                            String txtSms = stToken.nextToken();
                                            if (!deductMoney) {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                        txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                                deductMoney = true;
                                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId);
                                            } else {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                        txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                            }
                                            if (ii > 5) {
                                                break;
                                            }
                                        }
                                    } else {
                                        alert =
                                                "Ban gui sai so dich vu,de nhan thong tin theo yeu cau hay soan: " + info +
                                                " gui8584\nDT ho tro 1900561577";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId);
                                    }
                                } else {
                                    if (status != 0) {
                                        alert =
                                                "Xin loi ban,hien tai he thong chua y kien cua chuyen gia ve thong tin ban dua ra.\nDT ho tro 1900561577";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId);
                                    } else {
                                        dbTool.updateStatus(requestId, 1, mobileOperator);
                                    }
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Ban soan tin sai cu phap.Hay soan " + commandCode +
                                            " <Cuphap> <NgayThangSinh> gui8584." +
                                            "Vi du " + commandCode + " NQ 2011" + enter + "DT 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        }
                    } else if (subCode1.equals("TP")) {
                        commandId = "XEM";

                        String ddMM = infoTmp.substring(commandCode.length());
                        ddMM = ddMM.substring(subCode1.length());

                        ddMM = ddMM.replaceAll("o", "0");
                        ddMM = ddMM.replaceAll("O", "0");
                        ddMM = StringTool.getContentOnlyNumber(ddMM);

                        if (ddMM.length() > 4) {
                            ddMM = ddMM.substring(0, 4);
                        }
                        if ((ddMM.length() == 0) || (ddMM.length() == 1)) {
                            if (status != 0) {
                                if (commandCode.equalsIgnoreCase("HM") || commandCode.equalsIgnoreCase("QM")) {
                                    alert =
                                            "Ban soan tin sai cu phap " + commandCode + " TP <NgayThangSinh> gui8484.Vi du " +
                                            commandCode +
                                            " TP 2011.\nDT 1900561577";
                                } else {
                                    alert =
                                            "Ban soan tin sai cu phap " + commandCode + " TP <NgayThangSinh> gui8384.Vi du " +
                                            commandCode +
                                            " TP 2011.\nDT 1900561577";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " TP<==" + serviceId + ":WrongFormat:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        } else {
                            //2-3-4
                            boolean trueFormat = false;
                            if (ddMM.length() == 2) {
                                String firstDigit = ddMM.substring(0, 1);
                                String secondDigit = ddMM.substring(1, 2);
                                if ((Integer.parseInt(firstDigit) > 0) && (Integer.parseInt(secondDigit) > 0)) {
                                    ddMM = "0" + firstDigit + "-" + "0" + secondDigit;
                                    trueFormat = true;
                                }
                            } else if (ddMM.length() == 3) {
                                String firstDigit = ddMM.substring(0, 1);
                                String secondDigit = ddMM.substring(1, 2);
                                String thirdDigit = ddMM.substring(2, 3);

                                if ((Integer.parseInt(firstDigit) > 0) && (Integer.parseInt(secondDigit + thirdDigit) <= 12) &&
                                        (Integer.parseInt(secondDigit + thirdDigit) > 0)) {
                                    // XYZ -> 0X-YZ
                                    ddMM = "0" + firstDigit + "-" + secondDigit + thirdDigit;
                                    trueFormat = true;
                                } else if ((!trueFormat) && (Integer.parseInt(firstDigit) == 0) && (Integer.parseInt(secondDigit) > 0) &&
                                        (Integer.parseInt(thirdDigit) > 0)) {
                                    //XYZ -> XY-0Z
                                    ddMM = firstDigit + secondDigit + "-" + "0" + thirdDigit;
                                    trueFormat = true;
                                } else if ((!trueFormat) && (Integer.parseInt(firstDigit + secondDigit) > 0) &&
                                        (Integer.parseInt(firstDigit + secondDigit) <= 31) &&
                                        (Integer.parseInt(thirdDigit) > 0)) {
                                    //XYZ -> XY-0Z
                                    ddMM = firstDigit + secondDigit + "-" + "0" + thirdDigit;
                                    trueFormat = true;
                                }
                            } else if (ddMM.length() == 4) {
                                if ((Integer.parseInt(ddMM.substring(0, 2)) > 0) && (Integer.parseInt(ddMM.substring(0, 2)) <= 31) &&
                                        (Integer.parseInt(ddMM.substring(2, 4)) > 0) && (Integer.parseInt(ddMM.substring(2, 4)) <= 12)) {
                                    ddMM = ddMM.substring(0, 2) + "-" + ddMM.substring(2, 4);
                                    trueFormat = true;
                                }
                            }

                            if (trueFormat) {
                                String chiemTinh = Utilities.getCungChiemTinh(ddMM);
                                result = dbTool.getTXTData_2(commandId, subCode1, chiemTinh);
                                if ((result != null) && !result.equals("")) {
//                                    if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) ||
//                                            (Preference.adminPhoneNumber.contains(userId))) {
                                    if ((!commandCode.equals("HM") && (("8384,8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                            (!commandCode.equals("QM") && (("8384,8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                            (Preference.adminPhoneNumber.contains(userId)) ||
                                            (commandCode.endsWith("HM") && (("8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                            (commandCode.endsWith("QM") && (("8484,8584,8684,8784").indexOf(serviceId) != -1))) {
                                        if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                            result = StringTool.splitMsg(result);
                                        }

                                        boolean deductMoney = false;
                                        result = result.replaceAll("!xxx!", "|");
                                        StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                        int stTokenLen = stToken.countTokens();
                                        for (int ii = 0; ii < stTokenLen; ii++) {
                                            String txtSms = stToken.nextToken();
                                            if (!deductMoney) {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                        txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                                deductMoney = true;
                                                MyLogger.log(userId + "<==" + commandCode + " TP<==" + serviceId);
                                            } else {
                                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                        txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                            }
                                            if (ii > 5) {
                                                break;
                                            }
                                        }
                                    } else {
                                        if (commandCode.equalsIgnoreCase("HM") || commandCode.equalsIgnoreCase("QM")) {
                                            alert =
                                                    "Ban gui sai so dich vu,de nhan thong tin theo yeu cau hay soan: " + info +
                                                    " gui8484\nDT ho tro 1900561577";
                                        } else {
                                            alert =
                                                    "Ban gui sai so dich vu,de nhan thong tin theo yeu cau hay soan: " + info +
                                                    " gui8384\nDT ho tro 1900561577";
                                        }
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + " TP<==" + serviceId + ":WrongServiceId:" + info);
                                    }
                                } else {
                                    if (status != 0) {
                                        alert =
                                                "Xin loi ban,hien tai he thong chua y kien cua chuyen gia ve TyPhu voi thong tin ban dua ra.\nDT ho tro 1900561577";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        MyLogger.log(userId + "<==" + commandCode + " TP<==" + serviceId + ":NotFound:" + info);
                                    } else {
                                        dbTool.updateStatus(requestId, 1, mobileOperator);
                                    }
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Ban soan tin sai cu phap,de biet co tro thanh TyPhu duoc khong hay soan: " + commandCode +
                                            " TP <NgayThangSinh> gui8384." +
                                            "Vi du " + commandCode + " TP 2011.\nDT 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " TP<==" + serviceId + ":WrongFormat:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        }
                    } else if (subCode1.equals("CUGIAI") || subCode1.equals("DUONGCUU") || subCode1.equals("KIMNGUU") || subCode1.equals("SONGSINH") ||
                            subCode1.equals("SUTU")) {
                        commandId = "XEM";
                        result = dbTool.getTXTData_3(commandId, subCode1);
                        if ((result != null) && !result.equals("")) {
                            if (!serviceId.endsWith("8184") || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + " CUNG<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin Xem Ten theo yeu cau hay soan " + info +
                                        " gui 8384\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " CUNG<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert = "Tin nhan sai,de biet cac dich vu duoc cung cap cua tong dai 8x84 vui long soan: HELP gui8184\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " CUNG<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("NCA") || subCode1.equals("NCE")) {
                        commandId = "XEM";
                        if (StringTool.isNumberic(subCode2)) {
                            result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                            result = result.trim();
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + " NCA/NCE<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de biet tinh cach cua chang/nang qua net chu soan: " + info +
                                        " gui 8384\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " NCA/NCE<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Thong tin khong co hoac tin nhan sai.Soan: " + commandCode + " NCA <MaSo> gui8384.Hoac " + commandCode +
                                        " NCE <MaSo> gui8384\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " NCA/NCE<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("XXH") || subCode1.equals("XCH") || subCode1.equals("XKD") || subCode1.equals("XKT") || subCode1.equals("XLN") || subCode1.equals("XDT") || subCode1.equals("XGD") || subCode1.equals("XVT") || subCode1.equals("XAT")) {
                        commandId = "XEM";
                        if (subCode1.equals("XKT")) {
                            subCode1 = "XKD";
                        }
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((!subCode2.equals("")) && (result != null) && !result.equals("")) {
                            result = result.trim();
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + ":" + subCode1 + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de biet ngay tot/xau soan: " + info +
                                        " gui 8384" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + ":" + subCode1 + "<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xem ngay tot/xau.Soan: " + commandCode + " <ChuDe> <Thang> gui8384.ChuDe gom: Xuat hanh-XXH,CuoiHoi-XCH," +
                                        "KhaiTruong-XLT,SuaNha-XLN,DongTho-XDT,GiaoDich-XGD.Thang gom 1,2..";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + ":" + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("NA") || subCode1.equals("NU")) {
                        commandId = "XEM";
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((!subCode2.equals("")) && (result != null) && !result.equals("")) {
                            result = result.trim();
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + ":" + subCode1 + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,tim nguoi xong nha hop tuoi soan: " + info +
                                        " gui 8384" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + ":" + subCode1 + "<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Tim nguoi xong nha hop tuoi soan.Soan: " + commandCode + " <NA/NU>  <NamSinh> gui8384." + enter +
                                        "Vidu:Ban la Nu,sinh nam 1972 can tim nguoi xong nha hop tuoi soan: " + commandCode + " NU 1972 gui8384";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + ":" + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("RT")) {
                        infoTmp = infoTmp.substring(commandCode.length()).trim();
                        infoTmp = infoTmp.substring(subCode1.length()).trim();
                        infoTmp = StringTool.getContentOnlyChar(infoTmp);
                        Hashtable htTmp = new Hashtable();
                        for (int j = 0; j < infoTmp.length(); j++) {
                            htTmp.put(infoTmp.subSequence(j, j + 1), infoTmp.subSequence(j, j + 1));
                        }
                        int size = htTmp.size();
                        if (size > 0) {
                            commandId = "XEM";
                            subCode2 = String.valueOf(htTmp.size());
                            result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                            if ((result != null) && !result.equals("")) {
                                result = result.trim();
//                                if ((!serviceId.endsWith("8084") && !serviceId.endsWith("8184") && !serviceId.endsWith("8284")) ||
//                                        (Preference.adminPhoneNumber.contains(userId))) {
                                if ((!commandCode.equals("HM") && (("8384,8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                        (!commandCode.equals("QM") && (("8384,8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                        (Preference.adminPhoneNumber.contains(userId)) ||
                                        (commandCode.endsWith("HM") && (("8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                        (commandCode.endsWith("QM") && (("8484,8584,8684,8784").indexOf(serviceId) != -1))) {
                                    if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                        result = StringTool.splitMsg(result);
                                    }
                                    boolean deductMoney = false;
                                    result = result.replaceAll("!xxx!", "|");
                                    StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                    int stTokenLen = stToken.countTokens();
                                    for (int ii = 0; ii < stTokenLen; ii++) {
                                        String txtSms = stToken.nextToken();
                                        if (!deductMoney) {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                            deductMoney = true;
                                            MyLogger.log(userId + "<==" + commandCode + ":" + subCode1 + "<==" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                        }
                                        if (ii > 5) {
                                            break;
                                        }
                                    }
                                } else {
                                    if (commandCode.equals("HM") || commandCode.equals("QM")) {
                                        alert = "Ban gui sai dau so dich vu.De biet 2ban dang o giai doan nao cua tinh yeu,soan: " + commandCode +
                                                " RT <HoTenBan> <HoTenNguoiAy> gui8484." +
                                                "Cam on da su dung DV";
                                    } else {
                                        alert = "Ban gui sai dau so dich vu.De biet 2ban dang o giai doan nao cua tinh yeu,soan: " + commandCode +
                                                " RT <HoTenBan> <HoTenNguoiAy> gui8384." +
                                                "Cam on da su dung DV";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + ":WrongServiceId " + subCode1 + "<==" + serviceId);
                                }
                            } else {
                                if (status != 0) {
                                    if (commandCode.equals("HM") || commandCode.equals("QM")) {
                                        alert = "De biet 2ban dang o giai doan nao cua tinh yeu,soan: " + commandCode + " RT <HoTenBan> <HoTenNguoiAy> gui8484." +
                                                "Cam on da su dung DV";
                                    } else {
                                        alert = "De biet 2ban dang o giai doan nao cua tinh yeu,soan: " + commandCode + " RT <HoTenBan> <HoTenNguoiAy> gui8384." +
                                                "Cam on da su dung DV";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + ":" + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        } else {
                            if (status != 0) {
                                if (commandCode.equals("HM") || commandCode.equals("QM")) {
                                    alert = "De biet 2ban dang o giai doan nao cua tinh yeu,soan: " + commandCode + " RT <HoTenBan> <HoTenNguoiAy> gui8484." +
                                            "Cam on da su dung DV";
                                } else {
                                    alert = "De biet 2ban dang o giai doan nao cua tinh yeu,soan: " + commandCode + " RT <HoTenBan> <HoTenNguoiAy> gui8384." +
                                            "Cam on da su dung DV";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + ":" + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("XT")) {
                        commandId = "XEM";
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((!subCode2.equals("")) && (result != null) && !result.equals("")) {
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                    (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de xem Tai Van soan: " + info +
                                        " gui 8584\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Thong tin khong co hoac tin nhan sai.Soan: " + commandCode + " XT <NamSinh> gui8584.VD " + commandCode +
                                        " XT 1985 gui8584\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("SDT")) {
                        //XEM SDT 1
                        commandId = "XEM";
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((!subCode2.equals("")) && (result != null) && !result.equals("")) {
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                    (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de biet 'Nguoi ay yeu ban nhieu hay it' soan: " + info +
                                        " gui 8584\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Thong tin khong co hoac tin nhan sai.Soan: " + commandCode + " SDT <MaSo> gui8584.VD " + commandCode +
                                        " SDT 3 gui8584\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (StringTool.isNumberic(subCode1)) {
                        //XEM 1993
                        commandId = "XEM";
                        if ((Integer.parseInt(subCode1) >= 1941) && (Integer.parseInt(subCode1) <= 1999)) {
                            result = dbTool.getTXTData_1(commandId, subCode1);
                            if (subCode2.equals("") && (result != null) && !result.equals("")) {
                                if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) ||
                                        (Preference.adminPhoneNumber.contains(userId))) {
                                    if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                        result = StringTool.splitMsg(result);
                                    }

                                    boolean deductMoney = false;
                                    result = result.replaceAll("!xxx!", "|");
                                    StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                    int stTokenLen = stToken.countTokens();
                                    for (int ii = 0; ii < stTokenLen; ii++) {
                                        String txtSms = stToken.nextToken();
                                        if (!deductMoney) {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                            deductMoney = true;
                                            MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                        }
                                        if (ii > 5) {
                                            break;
                                        }
                                    }
                                } else {
                                    alert =
                                            "Ban gui sai so dich vu,de xem NamSinh soan: " + info +
                                            " gui 8384\nDT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongServiceId:" + info);
                                }
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Thong tin khong co hoac tin nhan sai.Soan: " + commandCode + " <NamSinh> gui8384.VD " + commandCode +
                                            " 1985 gui8384\nDT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Thong tin khong co hoac tin nhan sai.Soan: " + commandCode + " <NamSinh> gui8384.VD " + commandCode +
                                        " 1985 gui8384\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("CT") || subCode1.equals("CG")) {
                        commandId = commandCode;
                        if (subCode1.equals("CT")) {
                            commandId = "SNA";
                        } else if (subCode1.equals("CG")) {
                            commandId = "SNU";
                        }

                        result = dbTool.getTXTData_1(commandId, subCode2);
                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                    (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                        " gui8584\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==GT<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                        "\nDat ten con: " + commandCode + " " + subCode1 + "  <ChuCai> gui8584" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==GT<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("VANG")) {
                        commandId = "GIA";
                        result = dbTool.getTXTData_1(commandId, subCode1);
                        if ((result != null) && !result.equals("")) {
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                    (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin GiaVang theo yeu cau hay soan " + info +
                                        " gui8584\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " VANG<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            alert =
                                    "Xin loi ban,hien tai he thong chua co thong tin Gia VANG ban yeu cau\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Tin nhan sai dinh dang.Ban vua soan: " + info + " gui" + serviceId + enter +
                                    "DT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + ":" + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("MV") || commandCode.equals("MPH") || commandCode.equals("QV") || commandCode.equals("HV")) {
                    commandId = "MV";
                    result = dbTool.getTXTData_1(commandId, subCode1);
                    if ((result != null) && !result.equals("")) {
//                        if (!serviceId.endsWith("8184") || (Preference.adminPhoneNumber.contains(userId))) {
                        if ((!commandCode.equals("HV") && (("8284,8384,8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                (!commandCode.equals("QV") && (("8284,8384,8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                (Preference.adminPhoneNumber.contains(userId)) ||
                                (commandCode.endsWith("HV") && (("8484,8584,8684,8784").indexOf(serviceId) != -1)) ||
                                (commandCode.endsWith("QV") && (("8484,8584,8684,8784").indexOf(serviceId) != -1))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==MV<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            if (commandCode.equals("HV") || commandCode.equals("QV")) {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin MeoVat theo yeu cau hay soan " + info +
                                        " gui 8484\nDT ho tro 1900561577";
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin MeoVat theo yeu cau hay soan " + info +
                                        " gui 8384\nDT ho tro 1900561577";
                            }
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==MV<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Tin nhan sai cu phap,de nhan thong tin ve MeoVat hay soan MV MaSo gui 8384,de biet nhac chuong Hot trong ngay soan HOT gui 8384.\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==MV<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("GIA") || commandCode.equals("NH") || commandCode.equals("BT")) {
                    commandId = "GIA";
                    if (subCode1.equals("TYGIA") || subCode1.equals("TIGIA")) {
                        subCode1 = "TYGIA";
                        //GIA TYGIA
                        result = dbTool.getTXTData_1(commandId, subCode1);
                        if ((result != null) && !result.equals("")) {
                            if ((!serviceId.endsWith("8184")) || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + " TYGIA<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin TyGia theo yeu cau hay soan " + info +
                                        " gui 8384\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " TYGIA<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            alert =
                                    "Xin loi ban,hien tai he thong chua co thong tin ve TyGia ma ban yeu cau\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + " TYGIA<==" + serviceId + ":NotFound:" + info);
                        }
                    } else if (subCode1.equals("DT")) {
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if (!subCode2.equals("") && (result != null) && !result.equals("")) {
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + " DT<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin Gia dien thoai theo yeu cau hay soan " + info +
                                        " gui 8384\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " DT<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Tin nhan sai cu phap,de nhan thong tin Gia DT hay soan GIADT TenHangDT gui8384,trong do TenHangDT(NOKIA,LG,SAMSUNG..)\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " DT<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("VANG")) {
                        result = dbTool.getTXTData_1(commandId, subCode1);
                        if ((result != null) && !result.equals("")) {
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + " VANG<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin GiaVang theo yeu cau hay soan " + info +
                                        " gui8384\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " VANG<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            alert =
                                    "Xin loi ban,hien tai he thong chua co thong tin Gia VANG ban yeu cau\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + " VANG<==" + serviceId + ":NotFound:" + info);
                        }
                    } else if (subCode1.equals("XM")) {
                        result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                        if ((!subCode2.equals("")) && (result != null) && !result.equals("")) {
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + " XM<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin Gia xemay theo yeu cau hay soan: " + info +
                                        " gui 8384\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " XM<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Thong tin khong co hoac tin nhan sai.Soan: GIA XM <TenHang> gui8384.VD GIA XM HONDA gui8384\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " XM<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (subCode1.equals("XOA")) {
                        //Xoa LOGO
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                            result = dbTool.getLogoOTA(new BigDecimal(1)); //ID Code content Logo clear

                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 2, result, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + ":" + serviceId);
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,de xoa logo mang soan: " + info +
                                    " gui 8384\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Thong tin khong co hoac tin nhan sai.Soan: GIA XM <TenHang> de biet gia xemay,GIA VANG de biet gia vang,GIA DT <TenHang> de biet gia DienThoai";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + ":INV<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("XT")) {
                    commandId = "XT";
                    result = dbTool.getTXTData_1(commandId, subCode1);
                    if ((result != null) && !result.equals("")) {
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==XT<XemTenConGiap><==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,xem tai van dau nam soan " + info +
                                    " gui 8384\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==XT<XemTenConGiap><==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Thong tin khong co hoac tin nhan sai.De xem tai van dau nam soan: XT <TenConGiap> gui 8384,VD XT TUAT gui 8384\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==XT<XemTenConGiap><==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("THOITIET") || commandCode.equals("WEA")) {
                    commandId = "THOITIET";
                    String comment = infoTmp.substring(commandCode.length()).trim();
                    subCode1 = comment;
                    result = dbTool.getTXTData_6(commandId, comment);
                    if ((result != null) && !result.equals("")) {
                        if ((!serviceId.endsWith("8184")) || (Preference.adminPhoneNumber.contains(userId))) {
                            result = comment + "\n" + result;
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,nhan thong tin Thoi Tiet soan " + info +
                                    " gui8284\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Thong tin khong co hoac tin nhan sai.De nhan tin du bao thoi tiet soan: " + commandCode + " <TenTinh> gui 8384,vi du " +
                                    commandCode + " HANOI\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("TEEN")) {
                    commandId = "TEEN";
                    subCode1 = infoTmp.substring(commandCode.length()).trim();
                    result = dbTool.getTXTData_3(commandId, subCode1);

                    if ((result != null) && !result.equals("")) {
                        result = result.replaceAll("\r\n", "\n");
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==TEEN<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,de kham pha do HOT cua ban hay soan " + info +
                                    " gui 8384\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==TEEN<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Thong tin khong co hoac tin nhan sai.De kham pha do HOT cua ban soan: TEEN <CUNG> gui 8384,vi du TEEN BACHDUONG\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==TEEN<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("HOA")) {
                    commandId = "HOA";
                    subCode1 = StringTool.getContentNoWhiteLetter(info.substring(commandId.length(), info.length()));
                    result = dbTool.getTXTData_1(commandId, subCode1);
                    if ((result != null) && !result.equals("")) {
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==HOA<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,de biet y nghia cac loai hoa hay soan " + info +
                                    " gui 8384\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==HOA<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        alert =
                                "Thong tin khong co hoac tin nhan sai.De biet y nghia cac loai hoa soan: HOA <TenHoa> gui 8384,vi du HOA LUULY\nDT ho tro 1900561577";
                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                        MyLogger.log(userId + "<==HOA<==" + serviceId + ":NotFound:" + info);
                    }
                } else if (commandCode.equals("UK")) {
                    commandId = "UK";
                    subCode1 = StringTool.getContentNoWhiteLetter(info.substring(commandId.length(), info.length()));
                    result = dbTool.getTXTData_1(commandId, subCode1);
                    if ((result != null) && !result.equals("")) {
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==UK<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,de nhan duoc thong tin theo yeu cau hay soan " + info +
                                    " gui 8384\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==UK<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Thong tin khong co hoac tin nhan sai.Biet Uu khuyet diem cua minh soan: UK <CUNG> gui8384,CUNG:BAOBINH,NHANMA..\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==UK<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("YEU")) {
                    commandId = "YEU";
                    String phoneNumberFriend = subCode2;
                    phoneNumberFriend = Utilities.rebuildUserId(phoneNumberFriend);
                    String mOperator = Utilities.buildMobileOperator(phoneNumberFriend);

                    result = dbTool.getTXTData_1(commandId, subCode1);
                    if ((result != null) && !result.equals("") && (phoneNumberFriend != null) && (!phoneNumberFriend.equals("")) && (mOperator != null) &&
                            (!mOperator.equals(""))) {
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                            alert = "Ban vua gui loi YEU thuong den so dien thoai " + phoneNumberFriend +
                                    ".Cam on ban da su dung dich vu,DT ho tro 1900561577.";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(phoneNumberFriend + "<==YEU<==" + serviceId + "<==" + userId);

                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            alert = userId +
                                    " vua gui loi YEU thuong den ban tu he thong 8x84.De biet cac dich vu cua 8x84 moi soan: Help gui8184!\nDT ho tro 1900561577.";
                            dbTool.sendMTEx(phoneNumberFriend, serviceId, mOperator, commandCode, 0, alert, 0, requestId, subCode1, subCode2, subCode3);
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                dbTool.sendMTEx(phoneNumberFriend, serviceId, mOperator, commandCode, 0, txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,de nhan duoc thong tin theo yeu cau hay soan " + info +
                                    " gui 8384\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==YEU<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Thong tin khong co hoac tin nhan sai.Gui loi yeu thuong soan YEU <DoiTuong> <SdtNguoiNhan>,DoiTuong:Em,Ban,Chong,Vo,BanGai,Me,Bo..\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==YEU<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("CUOI") || commandCode.equals("VUI") || commandCode.equals("SCU") || commandCode.equals("QCUOI") || commandCode.equals("HCUOI")) {
                    commandId = "CUOI";

                    String phoneNumberFriend = subCode1;
                    phoneNumberFriend = Utilities.rebuildUserId(phoneNumberFriend);
                    String mOperator = Utilities.buildMobileOperator(phoneNumberFriend);

                    result = dbTool.getTXTData(commandId);
                    if ((result != null) && !result.equals("")) {
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) ||
                                (commandCode.equals("SCU") && (serviceId.endsWith("8584") || serviceId.endsWith("8684") || serviceId.endsWith("8784"))) ||
                                (commandCode.equals("QCUOI") && (serviceId.endsWith("8484") ||serviceId.endsWith("8584") || serviceId.endsWith("8684") || serviceId.endsWith("8784"))) ||
                                (commandCode.equals("HCUOI") && (serviceId.endsWith("8484") ||serviceId.endsWith("8584") || serviceId.endsWith("8684") || serviceId.endsWith("8784"))) ||
                                (Preference.adminPhoneNumber.contains(userId))) {
                            if ((mOperator == null) || (mOperator.equals(""))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert = "Ban vua gui Chuyen cuoi den so dien thoai " + phoneNumberFriend +
                                        ".Cam on ban da su dung dich vu.DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);

                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    dbTool.sendMTEx(phoneNumberFriend, serviceId, mOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            }
                        } else {
                            if(commandCode.equals("QCUOI")||commandCode.equals("HCUOI")){
                                alert =
                                    "Ban gui sai so dich vu,de nhan duoc thong tin theo yeu cau hay soan " + info +
                                    " gui 8484\nDT ho tro 1900561577";
                            }else{
                                alert =
                                    "Ban gui sai so dich vu,de nhan duoc thong tin theo yeu cau hay soan " + info +
                                    " gui 8384\nDT ho tro 1900561577";
                            }
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Xin loi ban,hien tai he thong chua co noi dung cho Chuyen cuoi.\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("XNT")) {
                    //XNT NGONTAY -> 82XX, NGONTAY = CAI,TRO,UT..
                    commandId = "XNT";
                    result = dbTool.getTXTData_1(commandId, subCode1);
                    if ((result != null) && !result.equals("")) {
                        if (!serviceId.endsWith("8184") || (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==XNT<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,de nhan thong tin Xem Ten theo yeu cau hay soan " + info +
                                    " gui 8384\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==XNT<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Xin loi ban,hien tai he thong chua y kien cua chuyen gia ve XNT voi thong tin ban dua ra.\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==XNT<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("VTV") || commandCode.equals("VCTV")) {
                    commandId = info;
                    result = dbTool.getTXTData(commandId);
                    if ((result != null) && !result.equals("")) {
                        if ((!serviceId.endsWith("8184")) ||
                                (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }

                        } else {
                            alert =
                                    "Ban gui sai so dich vu,de nhan thong tin lich phat song TV hay soan: " + info +
                                    " gui8284.\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Xin loi ban,hien tai he thong chua co thong tin ma ban yeu cau.\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("HOT") || commandCode.equals("TOP")) {
                    commandId = commandCode;
                    boolean unKnow = false;
                    if ((subCode1 != null) && (!subCode1.equals(""))) {
                        if (!subCode1.equalsIgnoreCase("DONAM") && !subCode1.equalsIgnoreCase("DAAM")) {
                            unKnow = true;
                        }
                    } else {
                        subCode1 = "";
                    }

                    if (!unKnow) {
                        if (subCode1.equals("")) {
                            result = dbTool.getTXTData(commandId);
                        } else {
                            result = dbTool.getTXTData_1(commandId, subCode1);
                        }
                        if ((result != null) && !result.equals("")) {
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }

                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin ve TOP va HOT soan " + info +
                                        " gui 8384.\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi ban,hien tai he thong chua co thong tin ma ban yeu cau.Soan TOP hoac HOT gui 8384 de nhan MaSo hinhanh/nhac chuong HOT.\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else { //Process Hot 8123xxx

                        if (status != 0) {
                            result = dbTool.getTXTData(commandId);
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }

                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin ve TOP va HOT soan " + info +
                                        " gui 8384.\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("TN")) {
                    //Trac Nghiem
                    commandId = commandCode;
                    result = dbTool.getTXTData_5(commandId, subCode1, subCode2);
                    if ((result != null) && !result.equals("")) {
                        if (!serviceId.endsWith("8184") || (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }

                        } else {
                            alert =
                                    "Ban gui sai so dich vu,de nhan thong tin Trac nghiem soan " + info +
                                    " gui 8284.\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Xin loi ban,Thong tin ban gui khong ro rang,de nhan thong tin trac nghiem soan TN <MaSo> <TenCung>,Vidu: TN 1036 NHANMA gui 8284.\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("CHUC") || commandCode.equals("STH")) {
                    commandId = "CHUC";
                    if (subCode1.equals("102")) {
                        result = dbTool.getTXTData_1(commandId, subCode1);
                        if ((subCode2.equals("")) && (result != null) && !result.equals("")) {
                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                    (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan Seri loi Chuc soan: " + info +
                                        " gui 8584\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Thong tin khong co hoac tin nhan sai.Soan: " + commandCode + " 102 gui8584.nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " " + subCode1 + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else {
                        String friendReceiver = subCode2;
                        String mobileOperatorFriend = Utilities.buildMobileOperator(subCode2);

                        if (!mobileOperatorFriend.equals("")) {
                            result = dbTool.getTXTData_1(commandId, subCode1);
                            if ((result != null) && !result.equals("")) {
                                if (!serviceId.endsWith("8184") || (Preference.adminPhoneNumber.contains(userId))) {
                                    if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                        result = StringTool.splitMsg(result);
                                    }

                                    result = result.replaceAll("!xxx!", "|");
                                    StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                    int stTokenLen = stToken.countTokens();
                                    alert = "Ban vua gui loi chuc toi " + friendReceiver + ".\nDe duoc tro giup xin goi 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);

                                    alert = "So dien thoai " + userId + " gui loi chuc toi ban qua he thong 8x84.\nDe duoc tro giup xin goi 1900561577";
                                    dbTool.sendMTEx(friendReceiver, serviceId, mobileOperatorFriend, commandCode, 0,
                                            alert, 0, requestId, subCode1, subCode2, subCode3);
                                    for (int ii = 0; ii < stTokenLen; ii++) {
                                        String txtSms = stToken.nextToken();
                                        dbTool.sendMTEx(friendReceiver, serviceId, mobileOperatorFriend, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                        if (ii > 5) {
                                            break;
                                        }
                                    }
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    alert =
                                            "Ban gui sai so dich vu,de de gui loi chuc soan:" + info +
                                            " gui 8384.\nDT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                                }
                            } else {
                                alert =
                                        "Xin loi ban,Thong tin ban gui khong ro rang,de gui loi chuc xin soan:" + commandCode +
                                        " <DoiTuong> SoDTNhan gui 8384,DoiTuong gom EM,ANH,CHI,BAN.\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                            }
                        } else {
                            if (status != 0) {
//                                alert =
//                                        "Xin loi ban,Thong tin ban gui khong ro rang,de gui loi chuc xin soan:CHUC DoiTuong SoDtNhan gui 8384,DoiTuong gom EM,ANH,CHI,BAN.\nDT ho tro 1900561577";
                                alert = "Xin loi,thong tin ban gui khong ro rang.Gui loi chuc,soan:CHUC <DoiTuong> SoDTNhan gui 8384,DoiTuong gom EM,EMYEU,VO,CHI,BAN,BA,COGIAO,CO.DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==CHUC<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    }
                } else if (commandCode.equals("NR")) {
                    commandId = "XEM";

                    result = dbTool.getTXTData_5(commandId, "NR", subCode1);
                    if ((result != null) && !result.equals("")) {
                        result = result.replaceAll("\r\n", "\n");
                        //result = StringTool.getContentOnlyCharEx(result);
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384")) ||
                                (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + " NR/NRN<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            if (commandCode.equals("NR")) {
                                commandCode = "XEM";
                            }
                            alert =
                                    "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info + enter +
                                    "Bi mat not ruoi soan " + commandCode + " NR <MaSo> gui8484,hoac " + commandCode +
                                    " NRN <MaSo> gui8484\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + " NR/NRN<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            if (commandCode.equals("NR")) {
                                commandCode = "XEM";
                            }
                            alert =
                                    "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info + enter +
                                    "Bi mat not ruoi soan " + commandCode + " NR <MaSo> gui8484,hoac " + commandCode +
                                    " NRN <MaSo> gui8484\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==NR<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("NA") || commandCode.equals("NU") || commandCode.equals("KB") || commandCode.equals("QNA") || commandCode.equals("QNU")) {
                    //NA name age-> 8784
                    info = (smsReceiveQueue.getInfo()).trim();
                    //info = commandCode + " " + info.substring(commandCode.length(), info.length());
                    infoTmp = info.substring(commandCode.length(), info.length()).trim();

                    if (commandCode.equals("NA") || commandCode.equals("NU") || commandCode.equals("QNA") || commandCode.equals("QNU")) {
                        int ii = 0;
                        String name = "";
                        while (ii < infoTmp.length()) {
                            char ch = infoTmp.charAt(ii);
                            if (ch >= '0' && ch <= '9') {
                                break;
                            } else {
                                name = name + String.valueOf(ch);
                            }
                            ii++;
                        }

                        String ageStr = infoTmp.substring(ii);
                        int sex = 0;
                        MakeFriendItem makeFriendItem = new MakeFriendItem();
                        makeFriendItem.setFoneNumber(userId);
                        makeFriendItem.setName(name);
                        if (commandCode.equals("NA") || commandCode.equals("QNA")) {
                            sex = 0;
                        } else {
                            sex = 1;
                        }
                        makeFriendItem.setSex(sex);

                        if ((ageStr.length() == 2) && (StringTool.isNumberic(ageStr))) {
                            int age = Integer.parseInt(DateProc.getCurrentYYYY()) - Integer.parseInt(ageStr);
                            makeFriendItem.setYearOfBirth(age);
                            alert = "Ban dang ky KetBan voi: GioiTinh ";
                            if (commandCode.equals("NA") || commandCode.equals("QNA")) {
                                alert = alert + "Nam,";
                            } else {
                                alert = alert + "Nu,";
                            }
                            alert = alert + ageStr + "tuoi\n";
                            alert = alert + "De co them ban moi soan: KB gui8584" + enter;
                            alert = alert + "De HUY tham gia ketban soan: KB HUY gui8184";
                            alert = alert + "Cac SoDT ban nen lam quen:" + enter;

                            if (!dbTool.checkExistInMakeFriend(userId)) {
                                //New member
                                if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                        Preference.adminPhoneNumber.contains(userId)) {
                                    if (dbTool.addMakeFriend(makeFriendItem)) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        String recommend = dbTool.getRecommentMakeFriend(userId, age, sex);
                                        if (recommend.length() > 160 && (recommend.indexOf("!xxx!") == -1)) {
                                            recommend = StringTool.splitMsg(recommend);
                                        }
                                        recommend = recommend.replaceAll("!xxx!", "|");
                                        StringTokenizer stToken = new StringTokenizer(recommend, "|", false);
                                        int stTokenLen = stToken.countTokens();
                                        for (int j = 0; j < stTokenLen; j++) {
                                            String txtSms = stToken.nextToken();
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                            if (j > 5) {
                                                break;
                                            }
                                        }
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                    } else {
                                        MyLogger.log("Add friend false");
                                    }
                                } else {
                                    if (commandCode.equals("QNA") || commandCode.equals("QNU")) {
                                        alert =
                                                "Ban gui sai dau so dich vu,tham gia KetBan neu la NAM soan: QNA <Ten> <Tuoi> gui8584,neu la nu soan: QNU <ten> <tuoi> gui8584.ViDu: QNA Hoang 24 gui8584";
                                    } else {
                                        alert =
                                                "Ban gui sai dau so dich vu,tham gia KetBan neu la NAM soan: NA <Ten> <Tuoi> gui8584,neu la nu soan: NU <ten> <tuoi> gui8584.ViDu: NA Hoang 24 gui8584";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + ":Wrong ServiceId<==" + serviceId);
                                }
                            } else {
                                //Exist member
                                if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                        Preference.adminPhoneNumber.contains(userId)) {
                                    if (dbTool.updateMakeFriend(makeFriendItem, false)) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                        String recommend = dbTool.getRecommentMakeFriend(userId, age, sex);
                                        if (recommend.length() > 160 && (recommend.indexOf("!xxx!") == -1)) {
                                            recommend = StringTool.splitMsg(recommend);
                                        }
                                        recommend = recommend.replaceAll("!xxx!", "|");
                                        StringTokenizer stToken = new StringTokenizer(recommend, "|", false);
                                        int stTokenLen = stToken.countTokens();
                                        for (int j = 0; j < stTokenLen; j++) {
                                            String txtSms = stToken.nextToken();
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                            if (j > 5) {
                                                break;
                                            }
                                        }
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                    }
                                } else {
                                    alert =
                                            "Ban gui sai dau so dich vu,de duoc gioi thieu them ban soan: " + info + " gui8584\nDT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + commandCode + ":Wrong ServiceId<==" + serviceId);
                                }
                            }
                        } else {
                            if (status != 0) {
                                if( commandCode.equals("QNA") || commandCode.equals("QNU")){
                                    alert =
                                        "Ban soan tin sai dinh dang,tham gia ket ban soan neu la NAM soan: QNA <Ten> <Tuoi> gui8584,neu la nu soan: QNU <Ten> <Tuoi> gui8584.ViDu: QNA Hoang 24 gui8584";
                                }else{
                                    alert =
                                        "Ban soan tin sai dinh dang,tham gia ket ban soan neu la NAM soan: NA <Ten> <Tuoi> gui8584,neu la nu soan: NU <Ten> <Tuoi> gui8584.ViDu: NA Hoang 24 gui8584";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (commandCode.equals("KB")) {
                        if (subCode1.equalsIgnoreCase("HUY") && ((subCode2 == null) || (subCode2.equals("")))) {
                            alert =
                                    "Ban da HUY tham gia ket ban tu he thong 8x84.De dang ky lai, soan: NA/NU TEN TUOI gui8584.Cam on ban da quan tam toi dich vu." +
                                    enter +
                                    "Moi thac mac xin goi 1900561577";
                            if (dbTool.removeFriend(userId)) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " HUY<==" + serviceId);
                            }
                        } else if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                Preference.adminPhoneNumber.contains(userId)) {
                            if (dbTool.checkExistInMakeFriend(userId)) {
                                //Exist
                                MakeFriendItem makeFriendItem = dbTool.getMakeFriendItem(userId);
                                alert = "Chung toi se gui toi ban nhung so dien thoai ma ban nen lam quen.De biet cac dich vu khac moi soan: Help gui8184" +
                                        enter +
                                        "Moi thac mac xin goi 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                String recommend = dbTool.getRecommentMakeFriend(makeFriendItem.getFoneNumber(), makeFriendItem.getYearOfBirth(),
                                        makeFriendItem.getSex());
                                if (recommend.length() > 160 && (recommend.indexOf("!xxx!") == -1)) {
                                    recommend = StringTool.splitMsg(recommend);
                                }
                                recommend = recommend.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(recommend, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int j = 0; j < stTokenLen; j++) {
                                    String txtSms = stToken.nextToken();
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    if (j > 5) {
                                        break;
                                    }
                                }
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                            } else {
                                if (status != 0) {
                                    alert =
                                            "Ban soan tin sai dinh dang,tham gia ket ban soan neu la NAM soan: NA <Ten> <Tuoi> gui8584,neu la nu soan: NU <Ten> <Tuoi> gui8584" +
                                            enter + "ViDu: NA Hoang 24 gui8584";
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                } else {
                                    dbTool.updateStatus(requestId, 1, mobileOperator);
                                }
                            }
                        } else {
                            alert =
                                    "Ban gui sai dau so dich vu,de duoc gioi thieu them ban soan: KB NA neu ban la NAM hoac KB NU neu ban la NU gui8584\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + ":Wrong ServiceId<==" + serviceId);
                        }
                    }
                } else if (commandCode.equals("DO") || commandCode.equals("PN") || commandCode.equals("BOY") || commandCode.equals("GIRL") ||
                        commandCode.equals("ANH") || commandCode.equals("EM")) {
                    MakeFriendItem makeFriendItem = new MakeFriendItem();
                    makeFriendItem.setFoneNumber(userId);

                    if (commandCode.equals("DO") || commandCode.equals("BOY") || commandCode.equals("ANH")) {
                        makeFriendItem.setSex(0);
                        alert = "Chung toi xin gioi thieu voi ban cac so dien thoai cua cac ban Nu ma ban nen lam quen:";
                    } else {
                        alert = "Chung toi xin gioi thieu voi ban cac so dien thoai cua cac ban Nam ma ban nen lam quen:";
                        makeFriendItem.setSex(1);
                    }

                    if (dbTool.checkExistInMakeFriend(userId)) {
                        //Exist member
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                Preference.adminPhoneNumber.contains(userId)) {
                            if (dbTool.updateMakeFriend(makeFriendItem, false)) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                String recommend = dbTool.getRecommentMakeFriend(userId);
                                if (recommend.length() > 160 && (recommend.indexOf("!xxx!") == -1)) {
                                    recommend = StringTool.splitMsg(recommend);
                                }
                                recommend = recommend.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(recommend, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int j = 0; j < stTokenLen; j++) {
                                    String txtSms = stToken.nextToken();
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    if (j > 5) {
                                        break;
                                    }
                                }
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                            }
                        } else {
                            alert =
                                    "Ban gui sai dau so dich vu,de duoc gioi thieu them ban soan: " + info + " gui8584\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + ":Wrong ServiceId<==" + serviceId);
                        }
                    } else {
                        //NA name age-> 8784
                        info = (smsReceiveQueue.getInfo()).trim();
                        //info = commandCode + " " + info.substring(commandCode.length(), info.length());
                        infoTmp = info.substring(commandCode.length(), info.length()).trim();

                        int ii = 0;
                        String name = "";
                        while (ii < infoTmp.length()) {
                            char ch = infoTmp.charAt(ii);
                            if (ch >= '0' && ch <= '9') {
                                break;
                            } else {
                                name = name + String.valueOf(ch);
                            }
                            ii++;
                        }

                        String ageStr = infoTmp.substring(ii);
                        int sex = 0;
                        makeFriendItem.setName(name);
                        if (commandCode.equals("BOY") || commandCode.equals("DO") || commandCode.equals("ANH")) {
                            sex = 0;
                        } else {
                            sex = 1;
                        }
                        makeFriendItem.setSex(sex);
                        if ((ageStr.length() == 2) && (StringTool.isNumberic(ageStr))) {
                            int age = Integer.parseInt(DateProc.getCurrentYYYY()) - Integer.parseInt(ageStr);
                            makeFriendItem.setYearOfBirth(age);
                            alert = "Ban dang ky KetBan voi: GioiTinh ";
                            if (commandCode.equals("BOY") || commandCode.equals("DO")) {
                                alert = alert + "Nam,";
                            } else {
                                alert = alert + "Nu,";
                            }
                            alert = alert + ageStr + "tuoi\n";
                            alert = alert + "De co them ban moi soan: " + commandCode + " gui8584" + enter;
                            alert = alert + "De HUY tham gia ketban soan: KB HUY gui8184";
                            alert = alert + "Cac SoDT ban nen lam quen:" + enter;

                            if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                                    Preference.adminPhoneNumber.contains(userId)) {
                                if (dbTool.addMakeFriend(makeFriendItem)) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    String recommend = dbTool.getRecommentMakeFriend(userId, age, sex);
                                    if (recommend.length() > 160 && (recommend.indexOf("!xxx!") == -1)) {
                                        recommend = StringTool.splitMsg(recommend);
                                    }
                                    recommend = recommend.replaceAll("!xxx!", "|");
                                    StringTokenizer stToken = new StringTokenizer(recommend, "|", false);
                                    int stTokenLen = stToken.countTokens();
                                    for (int j = 0; j < stTokenLen; j++) {
                                        String txtSms = stToken.nextToken();
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                        if (j > 5) {
                                            break;
                                        }
                                    }
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    MyLogger.log("Add friend false");
                                }
                            } else {
                                alert =
                                        "Ban gui sai dau so dich vu,tham gia ket ban soan " + info + " gui8584" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + ":Wrong ServiceId<==" + serviceId);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Ban soan tin sai dinh dang,tham gia ket ban soan neu la NAM soan: NA <Ten> <Tuoi> gui8584 hoac NU <Ten> <Tuoi> gui8584 neu la NU.ViDu: NA Hoang 24 gui8584";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    }
                } else if (commandCode.equals("DM")) {
                    commandId = commandCode;
                    result = dbTool.getTXTData_1(commandId, subCode1);
                    if ((result != null) && !result.equals("")) {
                        result = result.replaceAll("\r\n", "\n");
                        //result = StringTool.getContentOnlyCharEx(result);
                        if (serviceId.endsWith("8784") || (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }

                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                    " gui8784\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                    "\nNhan thong tin soan: " + commandCode + " <MaSo> gui8784" + enter + "DT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("LOV") || commandCode.equals("STL") || commandCode.equals("QOV")) {
                    try {
                        String phoneNumberFriend = StringTool.getObjectString(infoTmp, 1);
                        String phoneNumberFriendOrigin = phoneNumberFriend;
                        phoneNumberFriend = Utilities.rebuildUserId(phoneNumberFriend);
                        String mOperator = Utilities.buildMobileOperator(phoneNumberFriend);
                        if (!mOperator.equals("")) {
                            if ((!serviceId.endsWith("8084") && !serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") &&
                                    !serviceId.endsWith("8484")) ||
                                    Preference.adminPhoneNumber.contains(userId)) {

                                if (!userId.equals(phoneNumberFriend)) {
                                    String msg = infoTmp.substring(phoneNumberFriendOrigin.length() + commandCode.length() + 1).trim();
                                    if (msg.equals("")) {
                                        if (status != 0) {
                                            if (commandCode.equals("LOV") || commandCode.equals("QOV")) {
                                                alert =
                                                        "Ban soan tin thieu Loinhan,vui long soan lai: " + commandCode + " <SDTnhan> <Loinhan> gui8584" + enter +
                                                        "VD: " + commandCode + " 0936474760 Chuc em 1 ngay vui ve,hanh phuc.Anh yeu em!" + enter +
                                                        "DT ho tro 1900561577";
                                            } else {
                                                alert =
                                                        "Ban soan tin thieu Loinhan,vui long soan lai: " + commandCode + " <SDTnhan> <Loinhan> gui8684" + enter +
                                                        "VD: " + commandCode + " 0936474760 Chuc em 1 ngay vui ve,hanh phuc.Anh yeu em!" + enter +
                                                        "DT ho tro 1900561577";
                                            }
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2,
                                                    subCode3);
                                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                                        } else {
                                            dbTool.updateStatus(requestId, 1, mobileOperator);
                                        }
                                    } else {
                                        alert = "Ban vua gui hoa va loi nhan toi so DT " + phoneNumberFriend + enter +
                                                "De XEM DUYEN soan: XEM DUYEN <NamSinhEM> <NamSinhAnh> gui8384";
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);

                                        alert = "So DT " + userId + " gui toi ban hoa va loi nhan:" + msg;
                                        dbTool.sendMTEx(phoneNumberFriend, serviceId, mOperator, commandCode, 0, alert, 0, requestId, subCode1, subCode2,
                                                subCode3);

                                        String codePlowerId = dbTool.getTXTData("LOV");
                                        StringTokenizer tk = new StringTokenizer(codePlowerId, ",");
                                        int ii = tk.countTokens();
                                        Vector tkV = new Vector();
                                        String idFlower = "";
                                        while (tk.hasMoreTokens()) {
                                            tkV.add(tk.nextToken());
                                        }

                                        try {
                                            SecureRandom srd = SecureRandom.getInstance("SHA1PRNG", "SUN");
                                            idFlower = (String) tkV.elementAt(srd.nextInt(ii));
                                            String dataType = "24";
                                            String auth = dbTool.add2PushInfo(phoneNumberFriend, userId, new BigDecimal(idFlower.substring(3)), dataType);
                                            String dataStr = "[PTS] " + userId + " Tang Ban " + http + "push/" + idFlower + ".gif?auth=" + auth;
//                                            String dataStr = "[PTS] " + alert + " Tang Ban " + http + "push/" + idFlower + ".gif?auth=" + auth;
                                            if (!auth.equals("")) {
                                                dbTool.sendMTEx(phoneNumberFriend, serviceId, mOperator, commandCode, 8, dataStr, 0, requestId, subCode1,
                                                        subCode2, subCode3);
                                            }
                                            MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" + userId);
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }
                                    }
                                } else {
                                    String codePlowerId = dbTool.getTXTData("LOV");
                                    StringTokenizer tk = new StringTokenizer(codePlowerId, ",");
                                    int ii = tk.countTokens();
                                    Vector tkV = new Vector();
                                    String idFlower = "";
                                    while (tk.hasMoreTokens()) {
                                        tkV.add(tk.nextToken());
                                    }

                                    try {
                                        SecureRandom srd = SecureRandom.getInstance("SHA1PRNG", "SUN");
                                        idFlower = (String) tkV.elementAt(srd.nextInt(ii));
                                        String dataType = "24";
                                        String auth = dbTool.add2PushInfo(userId, userId, new BigDecimal(idFlower.substring(3)), dataType);
                                        String dataStr = "[PTS] " + idFlower + http + "push/" + idFlower + ".gif?auth=" + auth;
                                        if (!auth.equals("")) {
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 8, dataStr, 1, requestId, subCode1, subCode2,
                                                    subCode3);
                                            MyLogger.log(phoneNumberFriend + "<==" + commandCode + "<==" + serviceId + "<==" + userId);
                                        } else {
                                            System.out.println("Auth in LOV Null");
                                        }
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                }
                            } else {
                                alert =
                                        "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                        " gui8584\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        } else {
                            if (status != 0) {
                                alert =
                                        "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                        "\nTang hoa moi soan: " + commandCode + " <SoDT nhan> <LoiNhan> gui8584" + enter + "DT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        if (status != 0) {
                            alert =
                                    "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info +
                                    "\nTang hoa moi soan: " + commandCode + " <SoDT nhan> <TinNhan> gui8584" + enter + "DT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("AL")) {
                    //Convert from Solar to Lunar
                    // DD MM YY

                    boolean dateFormat = true;
                    //Check format
                    if ((subCode1 == null) || (subCode1.equals("")) || !StringTool.isNumberic(subCode1) || (subCode1.length() > 2)) {
                        dateFormat = false;
                    } else if ((subCode2 == null) || (subCode2.equals("")) || !StringTool.isNumberic(subCode2) || (subCode2.length() > 2)) {
                        dateFormat = false;
                    } else if (((subCode3 != null) && !StringTool.isNumberic(subCode3)) || (subCode3.length() != 4)) {
                        dateFormat = false;
                    }

                    if (!dateFormat) {
                        if (status != 0) {
                            alert =
                                    "Tin nhan khong ro rang,ban vua soan: " + info + enter +
                                    "Muon doi Ngay Duong Lich qua Am Lich soan: " + commandCode + " Ngay/thang/Nam gui8384" + enter + "DT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    } else {
                        if ((!serviceId.endsWith("8084") && !serviceId.endsWith("8184") && !serviceId.endsWith("8284")) ||
                                Preference.adminPhoneNumber.contains(userId)) {
                            int[] lurnar = LuniSolarCalendar.Solar2Lunar(Integer.parseInt(subCode1), Integer.parseInt(subCode2), Integer.parseInt(subCode3));
                            String nhuan = lurnar[3] == 1 ? " nhuan" : "";
                            String canChi = LuniSolarCalendar.CanChi(lurnar[0], lurnar[1], lurnar[2], lurnar[3]);

                            result = "Ngay " + subCode1 + "/" + subCode2 + "/" + subCode3 + " (DuongLich) ~ " + enter +
                                    lurnar[0] + "/" + lurnar[1] + "/" + lurnar[2] + nhuan + " (AmLich) ~ " + canChi;
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, result, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                    " gui8384\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    }
                } else if (commandCode.equals("DL")) {
                    //Convert from Lunar to Sonar
                    // DD MM YY

                    boolean dateFormat = true;
                    //Check format
                    if ((subCode1 == null) || (subCode1.equals("")) || !StringTool.isNumberic(subCode1) || (subCode1.length() > 2)) {
                        dateFormat = false;
                    } else if ((subCode2 == null) || (subCode2.equals("")) || !StringTool.isNumberic(subCode2) || (subCode2.length() > 2)) {
                        dateFormat = false;
                    } else if (((subCode3 != null) && !StringTool.isNumberic(subCode3)) || (subCode3.length() != 4)) {
                        dateFormat = false;
                    }

                    if (!dateFormat) {
                        if (status != 0) {
                            alert =
                                    "Tin nhan khong ro rang,ban vua soan: " + info + enter +
                                    "Muon doi Ngay Am Lich qua Duong Lich soan: " + commandCode + " Ngay/thang/Nam gui8384" + enter + "DT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    } else {
                        if ((!serviceId.endsWith("8084") && !serviceId.endsWith("8184") && !serviceId.endsWith("8284")) ||
                                Preference.adminPhoneNumber.contains(userId)) {
                            int[][] lunarYear = LuniSolarCalendar.LunarYear(Integer.parseInt(subCode3));
                            int[] solar = LuniSolarCalendar.Lunar2Solar(Integer.parseInt(subCode1), Integer.parseInt(subCode2), Integer.parseInt(subCode3),
                                    lunarYear[4][4]);
                            result = "Ngay " + subCode1 + "/" + subCode2 + "/" + subCode3 + " (AmLich) ~ " + enter +
                                    solar[0] + "/" + solar[1] + "/" + solar[2] + " (DuongLich)";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, result, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                        } else {
                            alert =
                                    "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                    " gui8384\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                        }

                    }
                } else if (commandCode.equals("CPA") || commandCode.equals("CPE") || commandCode.equals("SPE") || commandCode.equals("SPA")) {
                    if (commandCode.equals("SPE")) {
                        commandId = "CPE";
                    } else if (commandCode.equals("SPA")) {
                        commandId = "CPA";
                    } else {
                        commandId = commandCode;
                    }

                    if ((subCode1 == null) || (subCode1.equals(""))) {
                        result = dbTool.getTXTData(commandId);
                        if ((result != null) && !result.equals("")) {
                            result = result.replaceAll("\r\n", "\n");
                            //result = StringTool.getContentOnlyCharEx(result);
                            if (serviceId.endsWith("8684") || serviceId.endsWith("8784") || (Preference.adminPhoneNumber.contains(userId))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }
                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            } else {
                                if (commandCode.equals("SPE") || commandCode.equals("SPA")) {
                                    alert =
                                            "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                            " gui8784\nDT ho tro 1900561577";
                                } else {
                                    alert =
                                            "Ban gui sai so dich vu,de nhan thong tin soan:" + info +
                                            " gui8684\nDT ho tro 1900561577";
                                }
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                            }
                        }
                    } else {
                        if (status != 0) {
                            alert =
                                    "Xin loi,thong tin ban dua ra khong ro rang,ban vua soan: " + info + enter +
                                    "Moi soan HELP gui8184 de biet them chi tiet" + enter + "DT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":NotFound:" + info);
                        } else {
                            dbTool.updateStatus(requestId, 1, mobileOperator);
                        }
                    }
                } else if (commandCode.equals("GPRS") || commandCode.equals("GPRSR") || commandCode.equals("GPRST") || commandCode.equals("GPRSV") ||
                        commandCode.equals("TNGP") || commandCode.equals("THGP") || commandCode.equals("DPGP")) {
                    //GPRS,GPRSR,GPRST,GPRSV
                    //TNGP - 8584
                    commandId = "GPRS";
                    result = dbTool.getTXTData_1(commandId, mobileOperator);
                    if ((result != null) && !result.equals("")) {
                        result = result.replaceAll("\r\n", "\n");
//                        if (!serviceId.endsWith("8184") || (Preference.adminPhoneNumber.contains(userId)) ||
//                            ((commandCode.equals("TNGP") || commandCode.equals("THGP")) &&
//                             (serviceId.equals("8584") || serviceId.equals("8684") && serviceId.equals("8784")))) {
                        if ((("TNGP,THGP,DPGP").indexOf(commandCode) == -1) && (("8184,8284").indexOf(serviceId) == -1) ||
                                (("TNGP,THGP,DPGP").indexOf(commandCode) != -1) && (("8184,8284,8384,8484").indexOf(serviceId) == -1) ||
                                (Preference.adminPhoneNumber.contains(userId))) {
                            if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                result = StringTool.splitMsg(result);
                            }
                            boolean deductMoney = false;
                            result = result.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(result, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                    MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                            txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5) {
                                    break;
                                }
                            }
                        } else {
                            if (("TNGP,THGP,DPGP").indexOf(commandCode) != -1) {
                                alert = "Ban gui sai so dich vu,hay soan:" + info + " gui 8584." + enter +
                                        "DT ho tro 1900561577";
                            } else {
                                alert = "Ban gui sai so dich vu,hay soan GPRS gui 8384 de duoc huong dan cai dat GPRS" + enter +
                                        "Soan: GPRSA gui 8784 de duoc cai dat GPRS tu dong.DT ho tro 1900561577";
                            }
                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":WrongServiceId:" + info);
                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                alert =
                        "Xin loi ban,Thong tin ban gui khong ro rang.Soan HOT gui8384 de nhan ma so cac anh Bikini tuyet dep.\nDT ho tro 1900561577";
                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                MyLogger.log(userId + "<==WrongFormat<==" + serviceId + ":" + info);
            }
        } //End for

    }

    public void run() {
        MyLogger.log("DailyServices process Thread Start !");
        while (Main.running) {
            try {
                processRequestQueue("VIETEL");
                processRequestQueue("GPC");
                processRequestQueue("VMS");
                Thread.sleep(3000);
            } catch (Exception ex) {
            }
        }
    }

    public static void main(String[] args) {
        String aa = "ABCDEF";
        MyLogger.log(aa.substring(2));
    }
}
