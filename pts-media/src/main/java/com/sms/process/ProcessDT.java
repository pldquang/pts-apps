package com.sms.process;

import java.util.*;
import com.sms.common.Preference;
import java.math.BigDecimal;
import com.sms.item.*;
import com.sms.common.*;

public class ProcessDT extends Thread {

    DBTool dbTool = null;
    String alert = "";
    char enter = (char) 10;
    BigDecimal id = null;
    BigDecimal requestId = null;
    String userId = "";
    String serviceId = "";
    String commandCode = "";
    String info = "";
    String mobileOperator = "";
    String infoTmp = "";
    String data = "";
    String dataTmp = "";
    String result = "";
    boolean isProcessing = false;

    public ProcessDT() {
        dbTool = new DBTool();
    }

    public void processRequestQueue() {

        isProcessing = true;
        Vector listRequest = dbTool.getRequestFromDTWait();
        DiemthiWaitItem diemthiWaitItem = null;
        System.out.println("Total in queue is: " + listRequest.size());
        Main.schoolCodeHavePoint = dbTool.getSchoolCodeHavePoint();

        for (int i = 0; i < listRequest.size(); i++) {
            try {
                diemthiWaitItem = (DiemthiWaitItem) listRequest.elementAt(i);

                id = diemthiWaitItem.getId();
                requestId = diemthiWaitItem.getRequestId();
                userId = diemthiWaitItem.getUserId();
                serviceId = diemthiWaitItem.getServiceId();
                mobileOperator = diemthiWaitItem.getMobileOperator();
                commandCode = diemthiWaitItem.getCommandCode();
                info = diemthiWaitItem.getInfo().trim();
                System.out.println("Process:" + userId + ":" + info);
                infoTmp = info.substring(commandCode.length(), info.length());

                String data = infoTmp.toUpperCase();
                data = StringTool.getContentOnlyCharAndNumber(data);

                if (commandCode.equals("DT") || commandCode.equals("DC") || commandCode.equals("TNT") || commandCode.equals("SDT") || commandCode.equals("DIEM") || commandCode.equals("DIEMCHUAN")) {
                    //Kiem tra diem co chua
                    boolean isTrueSchoolCodeHavePoin = false;
                    boolean isDiemChuan = false;
                    String schoolCode = "";
                    synchronized (Main.schoolCodeHavePoint) {
                        for (int j = 0; j < Main.schoolCodeHavePoint.size(); j++) {
                            if ((data != null) && (!data.equals("")) && data.startsWith((String) Main.schoolCodeHavePoint.elementAt(j))) {
                                schoolCode = (String) Main.schoolCodeHavePoint.elementAt(j);
                                if (data.substring(schoolCode.length()).length() > 2) {
                                    isTrueSchoolCodeHavePoin = true;
                                } else {
                                    isDiemChuan = true;
                                }
                                break;
                            }
                        }
                    }

                    synchronized (Main.schoolCodeHavePoint) {
                        for (int j = 0; j < Main.schoolCode.size(); j++) {
                            if ((data != null) && (!data.equals("")) && data.equals((String) Main.schoolCode.elementAt(j))) {
                                schoolCode = (String) Main.schoolCode.elementAt(j);
                                isDiemChuan = true;
                                break;
                            }
                        }
                    }

//                    if (isDiemChuan) {
//                        dbTool.updateDiemThiWait(id, schoolCode, 2, data);
//                    } else {
//                        dbTool.updateDiemThiWait(id, schoolCode, 2, data);
//                    }

                    if (!isDiemChuan) {
                        dataTmp = data.substring(schoolCode.length());
                        dataTmp = dataTmp.replaceAll("o", "0");
                        dataTmp = dataTmp.replaceAll("O", "0");
                        data = schoolCode + dataTmp;
                        System.out.print("==>" + data);
                        if (!(alert = dbTool.getDiemThi(data)).equals("")) {
                            alert = alert + enter + "De biet DiemChuan soan: " + commandCode + " <MaTruong> gui8584";
//                            if (dbTool.removeDiemThiWait(id)) {
//                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 0, requestId, "", "", "");
//                                System.out.println(userId + "<==DT<==" + serviceId);
//                            }
                            if (dbTool.updateDiemThiWait(id, schoolCode, 2, data)) {
                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 0, requestId, "", "", "");
                                System.out.println(userId + "<==DT<==" + serviceId);
                            }
                        }
//                        else {
//                            alert =
//                                    "Khong tim thay thong tin diem thi ban yeu cau,moi soan " + commandCode +
//                                    " <SoBD> gui8484,SoBD gom MaTruong,MaKhoi,SoBaoDanh" + enter + "Luu y MaKhoi bao gom ca phan chu va so";
//                            if (dbTool.removeDiemThiWait(id)) {
//                                dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 0, requestId, "", "", "");
//                                System.out.println(userId + "<==DT NotFound<==" + info);
//                            }
//                        }
                    } else {
                        synchronized (Main.schoolStandardMark) {
                            result = (String) Main.schoolStandardMark.get(schoolCode);
                            if ((result != null) && (!result.equals(""))) {
                                if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                    result = StringTool.splitMsg(result);
                                }

                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                if (dbTool.removeDiemThiWait(id)) {
                                    for (int ii = 0; ii < stTokenLen; ii++) {
                                        String txtSms = stToken.nextToken();
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                txtSms, 0, requestId, "", "", "");
                                        if (ii > 5) {
                                            break;
                                        }
                                    }
                                }
                                System.out.println(userId + "<==DTC<==" + serviceId);
                            }
                        }
                    }
                } else if (commandCode.equals("XM")) {
                    if (infoTmp.startsWith("DT") || (StringTool.getObjectString(info, 1) != null && (StringTool.getObjectString(info, 1).equals("DT")))) {
                        data = data.substring(2);

                        //Kiem tra diem co chua
                        boolean isTrueSchoolCodeHavePoin = false;
                        boolean isDiemChuan = false;
                        String schoolCode = "";

                        synchronized (Main.schoolCodeHavePoint) {
                            for (int j = 0; j < Main.schoolCodeHavePoint.size(); j++) {
                                if ((data != null) && (!data.equals("")) && data.startsWith((String) Main.schoolCodeHavePoint.elementAt(j))) {
                                    schoolCode = (String) Main.schoolCodeHavePoint.elementAt(j);
                                    if (data.substring(schoolCode.length()).length() > 2) {
                                        isTrueSchoolCodeHavePoin = true;
                                    } else {
                                        isDiemChuan = true;
                                    }
                                    break;
                                }
                            }
                        }

                        synchronized (Main.schoolCode) {
                            for (int j = 0; j < Main.schoolCode.size(); j++) {
                                if ((data != null) && (!data.equals("")) && data.equals((String) Main.schoolCode.elementAt(j))) {
                                    schoolCode = (String) Main.schoolCode.elementAt(j);
                                    isDiemChuan = true;
                                    break;
                                }
                            }
                        }

                        if (isDiemChuan) {
                            dbTool.updateDiemThiWait(id, schoolCode, 0, data);
                        } else {
                            dbTool.updateDiemThiWait(id, schoolCode, 1, data);
                        }

                        if (!isDiemChuan) {
                            dataTmp = data.substring(schoolCode.length());
                            dataTmp = dataTmp.replaceAll("o", "0");
                            dataTmp = dataTmp.replaceAll("O", "0");
                            data = schoolCode + dataTmp;
                            System.out.print("==>" + data);
                            if (!(alert = dbTool.getDiemThi(data)).equals("")) {
                                alert = alert + enter + "De biet DiemChuan soan: " + commandCode + " <MaTruong> gui8384";
                                if (dbTool.removeDiemThiWait(id)) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 0, requestId, "", "", "");
                                    System.out.println(userId + "<==DT<==" + serviceId);
                                }
                            } else {
                                alert =
                                        "Khong tim thay thong tin diem thi ban yeu cau,moi soan " + commandCode +
                                        " DT <MaTruong,MaKhoi,SoBaoDanh> gui8384" + enter + "Luu y MaKhoi bao gom ca phan chu va so";
                                if (dbTool.removeDiemThiWait(id)) {
                                    dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 0, requestId, "", "", "");
                                    System.out.println(userId + "<==DT NotFound<==" + info);
                                }
                            }
                        } else {
                            synchronized (Main.schoolStandardMark) {
                                result = (String) Main.schoolStandardMark.get(schoolCode);
                                if ((result != null) && (!result.equals(""))) {
                                    if (result.length() > 160 && (result.indexOf("!xxx!") == -1)) {
                                        result = StringTool.splitMsg(result);
                                    }
                                    result = result.replaceAll("!xxx!", "|");
                                    StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                    int stTokenLen = stToken.countTokens();
                                    if (dbTool.removeDiemThiWait(id)) {
                                        for (int ii = 0; ii < stTokenLen; ii++) {
                                            String txtSms = stToken.nextToken();
                                            dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0,
                                                    txtSms, 0, requestId, "", "", "");
                                            if (ii > 5) {
                                                break;
                                            }
                                        }
                                    }
                                    System.out.println(userId + "<==DTC<==" + serviceId);
                                }
                            }
                        }
                    }
                }

                Thread.sleep(1000);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } //endFor
        isProcessing = false;

    }

    public void run() {
        System.out.println("DiemThi process Thread Start !");
        while (Main.running) {
            try {
                if ((DateProc.getCurrentMI() % 10 == 0) && !isProcessing) {
                    processRequestQueue();
                }
                Thread.sleep(50000);
            } catch (Exception ex) {
            }
        }
    }

    public static void main(String[] args) {
        String alert =
                "Khong tim thay thong tin diem thi ban yeu cau,moi soan " + "   " +
                " <SoBD> gui8384,SoBD gom MaTruong,MaKhoi,SoBaoDanh" + " " + "Luu y MaKhoi bao gom ca phan chu va so,vidu DT DQU D1 1234";

        System.out.println(alert.length());
    }
}
