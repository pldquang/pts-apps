package com.sms.process;

import com.sms.common.DateProc;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2008</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class ProcessRefeshData extends Thread{
    DBTool dbTool = null;

    public ProcessRefeshData() {
        dbTool = new DBTool();
    }

    public void processRequestQueue() {
        synchronized(Main.schoolStandardMark) {
            Main.schoolStandardMark = dbTool.getStandardMark();
        }
        synchronized(Main.schoolCodeHavePoint) {
            Main.schoolCodeHavePoint = dbTool.getSchoolCodeHavePoint();
        }

//        System.out.println("schoolStandardMark: " + Main.schoolStandardMark.size());
//        System.out.println("schoolCodeHavePoint: " + Main.schoolCodeHavePoint.size());
        notifyAll();
    }

    public void run() {
        System.out.println("Refesh Data Thread Start !");
        while (Main.running) {
            try {
                if (DateProc.getCurrentMI() % 5 == 0) {
                    processRequestQueue();
                }
                Thread.sleep(5000);
            } catch (Exception ex) {}
        }

    }
}
