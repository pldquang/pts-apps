/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sms.process;

import com.sms.common.DateProc;
import com.sms.item.CkItem;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;

/**
 *
 * @author hungdt
 */
public class ProcessCK extends Thread {

    DBTool dbTool = null;
    String alert = "";
    char enter = (char) 10;
    BigDecimal id = null;
    BigDecimal requestId = null;
    String userId = "";
    String serviceId = "";
    String commandCode = "";
    String info = "";
    String mobileOperator = "";
    String infoTmp = "";
    String data = "";
    String dataTmp = "";
    String result = "";
    boolean isProcessing = false;
    boolean check = false;
    int remain = 0;

    public ProcessCK() {
        dbTool = new DBTool();
    }

    public void processRequestQueue() {
        isProcessing = true;
        Vector listRequest = dbTool.getRequestFromCKWait();
        CkItem ckItem = null;
        Calendar cal = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("EEE");
        int h = cal.get(Calendar.HOUR_OF_DAY);
        String day = "";
        System.out.println("Total in queue CKWait is: " + listRequest.size());
//        String alert = "";
        try {
            day = df.format(cal.getTime());
            System.out.println("D = " + day + " H = " + h);
            if (!day.equalsIgnoreCase("SUN")) {
                if ((h >= 9) && (h <= 11)) {
                    for (int i = 0; i < listRequest.size(); i++) {
                        ckItem = (CkItem) listRequest.elementAt(i);
                        id = ckItem.getId();
                        requestId = ckItem.getRequestId();
                        userId = ckItem.getUserId();
                        serviceId = ckItem.getServiceId();
                        mobileOperator = ckItem.getMobileOperator();
                        commandCode = ckItem.getCommandCode();
                        info = ckItem.getInfo().trim();
                        remain = ckItem.getRemain();
                        System.out.println("Process CK:" + userId + ":" + info);
                        if (info.indexOf("TT") >= 0) {
                            //xu ly TT
                            //neu chua co kq thread stop 5'
                            if (dbTool.checkCKbyHour(String.valueOf(h))) {
                                //tra ket qua
                                alert = dbTool.getCKByCode(info.substring(2).trim());
                                if (!alert.equalsIgnoreCase("")) {
                                    if (dbTool.updateCKWait(id, remain - 1)) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 0, requestId, info, "", "");
                                    }
                                }
                                check = false;
                            } else {
                                //chua co kq
                                check = true;
                            }
                        }
                        if (h == 11) {
                            //xu ly cac truong hop khac
                            //neu chua co kq thread stop 5'
                            if (dbTool.checkCKbyHour(String.valueOf(h))) {
                                //tra ket qua
                                alert = dbTool.getCKByCode(info.substring(2).trim());
                                if (!alert.equalsIgnoreCase("")) {
                                    if (dbTool.updateCKWait(id, remain - 1)) {
                                        dbTool.sendMTEx(userId, serviceId, mobileOperator, commandCode, 0, alert, 0, requestId, info, "", "");
                                    }
                                }
                                check = false;
                            } else {
                                //chua co kq
                                check = true;
                            }
                        }

                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public long syncThread() {
        long ret = 0;
        Calendar now = Calendar.getInstance();
        int h = now.get(Calendar.HOUR_OF_DAY);
        if (check && ((h >= 9) && (h <= 11))) {
            ret = 5 * 60 * 1000;
        } else {
            ret = (60 - now.get(Calendar.MINUTE)) * 60 * 1000;
        }
        return ret;
    }

    /**
     * @param args the command line arguments
     */
    @Override
    public void run() {
        System.out.println("CK process Thread Start !");
        while (Main.running) {
            try {
//                if ((DateProc.getCurrentMI() % 10 == 0) && !isProcessing) {
                processRequestQueue();
//                }
                sleep(syncThread());
            } catch (Exception ex) {
            }
        }
    }

    public static void main(String[] args) {
        String alert =
                "Khong tim thay thong tin chung khoan ban yeu cau,moi soan " + "   " +
                " gui8184. DT ho tro: 1900561577";

        System.out.println(alert.length());
    }
}
