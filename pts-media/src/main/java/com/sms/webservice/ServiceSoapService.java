/**
 * ServiceSoapService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Aug 08, 2005 (11:49:10 PDT) WSDL2Java emitter.
 */

package com.sms.webservice;

public interface ServiceSoapService extends javax.xml.rpc.Service {
    public java.lang.String getViskySOAPInsertAddress();

    public com.sms.webservice.ServiceSoap getViskySOAPInsert() throws javax.xml.rpc.ServiceException;

    public com.sms.webservice.ServiceSoap getViskySOAPInsert(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
