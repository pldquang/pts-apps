/**
 * ServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Aug 08, 2005 (11:49:10 PDT) WSDL2Java emitter.
 */

package com.sms.webservice;

public interface ServiceSoap extends java.rmi.Remote {
    public java.lang.String insertSMS(java.lang.String username, java.lang.String password, SMS[] vt) throws java.rmi.RemoteException;
}
