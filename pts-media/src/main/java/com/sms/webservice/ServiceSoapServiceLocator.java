/**
 * ServiceSoapServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Aug 08, 2005 (11:49:10 PDT) WSDL2Java emitter.
 */

package com.sms.webservice;

public class ServiceSoapServiceLocator extends org.apache.axis.client.Service implements com.sms.webservice.ServiceSoapService {

    public ServiceSoapServiceLocator() {
    }


    public ServiceSoapServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ServiceSoapServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ViskySOAPInsert
//    private java.lang.String ViskySOAPInsert_address = "http://210.245.80.42/ViskyHCM/services/ViskySOAPInsert";
    private java.lang.String ViskySOAPInsert_address = "http://hcm.vimobi.vn/services/ViskySOAPInsert";

    public java.lang.String getViskySOAPInsertAddress() {
        return ViskySOAPInsert_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ViskySOAPInsertWSDDServiceName = "ViskySOAPInsert";

    public java.lang.String getViskySOAPInsertWSDDServiceName() {
        return ViskySOAPInsertWSDDServiceName;
    }

    public void setViskySOAPInsertWSDDServiceName(java.lang.String name) {
        ViskySOAPInsertWSDDServiceName = name;
    }

    public com.sms.webservice.ServiceSoap getViskySOAPInsert() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ViskySOAPInsert_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getViskySOAPInsert(endpoint);
    }

    public com.sms.webservice.ServiceSoap getViskySOAPInsert(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.sms.webservice.ViskySOAPInsertSoapBindingStub _stub = new com.sms.webservice.ViskySOAPInsertSoapBindingStub(portAddress, this);
            _stub.setPortName(getViskySOAPInsertWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setViskySOAPInsertEndpointAddress(java.lang.String address) {
        ViskySOAPInsert_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.sms.webservice.ServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.sms.webservice.ViskySOAPInsertSoapBindingStub _stub = new com.sms.webservice.ViskySOAPInsertSoapBindingStub(new java.net.URL(ViskySOAPInsert_address), this);
                _stub.setPortName(getViskySOAPInsertWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ViskySOAPInsert".equals(inputPortName)) {
            return getViskySOAPInsert();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://210.245.80.42/ViskyHCM/services/ViskySOAPInsert", "ServiceSoapService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://210.245.80.42/ViskyHCM/services/ViskySOAPInsert", "ViskySOAPInsert"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ViskySOAPInsert".equals(portName)) {
            setViskySOAPInsertEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
