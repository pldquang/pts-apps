package com.sms.webservice;
import java.io.*;

public class SMS implements Serializable {
    private String commandName;
    private String fullContent;
    private String phoneNumber;
    private String serviceNumber;
    private String messageTime;
    private String requestID;
    public SMS(String commandName, String fullContent, String phoneNumber,
               String serviceNumber, String messageTime, String requestID) {
        super();
        this.commandName = commandName;
        this.fullContent = fullContent;
        this.phoneNumber = phoneNumber;
        this.serviceNumber = serviceNumber;
        this.messageTime = messageTime;
        this.requestID = requestID;
    }

    public SMS() {
        super();
    }

    public String getCommandName() {
        return commandName;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public String getFullContent() {
        return fullContent;
    }

    public void setFullContent(String fullContent) {
        this.fullContent = fullContent;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }
}
