package com.sms.webservice;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2008</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import javax.xml.namespace.QName;
import org.apache.axis.encoding.ser.BeanSerializerFactory;
import org.apache.axis.encoding.ser.BeanDeserializerFactory;


public class Submit2Visky {
    String user="pts";
    String pass="@8x84#visky#8x84";

    public Submit2Visky() {
    }

    public String send2Visky(String userId,String serviceId,String commandCode,String yyyyMMDDHH24MISS,String idStr) {
        String result = "";
        try {
//            String endpoint = "http://210.245.80.42/ViskyHCM/services/ViskySOAPInsert";
            String endpoint = "http://hcm.vimobi.vn/services/ViskySOAPInsert";
            Service service = new Service();
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(new java.net.URL(endpoint));
            call.setOperationName("insertSMS");
            call.registerTypeMapping(SMS.class,
                                     new QName("http://SMS.model.visky", "SMS"),
                                     BeanSerializerFactory.class,
                                     BeanDeserializerFactory.class);
            SMS[] vt = new SMS[1];
            vt[0]= new SMS(commandCode, commandCode, userId, serviceId, yyyyMMDDHH24MISS, idStr);
            result = (String) call.invoke(new Object[] {user,pass,vt});
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            return result;
        }
    }

    public static void main(String[] args) {
        Submit2Visky submit = new Submit2Visky();
        System.out.println(submit.send2Visky("84916943692","8784","GPRSA","20090824185700","1234"));
    }
}
