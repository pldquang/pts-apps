package com.sms.image;

import java.awt.*;
import java.awt.font.*;
import java.awt.image.BufferedImage;
import com.sun.image.codec.jpeg.*;
import java.io.*;

public class Write2Image {
    public Write2Image() {
    }

    public void test() {
        try {
            BufferedInputStream fis = new BufferedInputStream(new FileInputStream("F:\\programming\\java\\smsNew\\pts\\datatest\\framesize3_.gif"));

            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Image image = toolkit.getImage("c:\\test.gif");
            //Graphics g = image.getGraphics();
            //g.setFont(Font.getFont(Font.FACE_SYSTEM, Font.STYLE_ITALICS, Font.SIZE_MEDIUM));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void addTextJPG(String text, String font, Color color, int size,
                           OutputStream out, int vAlign, int hAlign) {
        try {
            String[] word = new String[2];

            Frame fr = new Frame();

            Image image = Toolkit.getDefaultToolkit().getImage("c:\\fr.gif");

//            Font  F = Font.createFont(Font.PLAIN,new FileInputStream("F:\\JBuilder2006\\jdk1.5\\jre\\lib\\fonts\\Vntfap01.ttf"));
//            Font f = new Font(F.getName(),Font.PLAIN,size);

            Font f = new Font(font, Font.PLAIN, size);



            MediaTracker mt = new MediaTracker(fr);

            mt.addImage(image, 0);
            try {
                mt.waitForID(0);
            } catch (InterruptedException ie) {}
            int width = image.getWidth(null);
            int height = image.getHeight(null);

            BufferedImage bimg = new BufferedImage(width, height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D g = (Graphics2D) bimg.createGraphics();
            g.setFont(f);
            g.setColor(color);
            g.drawImage(image, 0, 0, fr);

            FontMetrics fm = g.getFontMetrics();
            int textWidth = fm.stringWidth(text);

            int x0 = 0;
            int y0 = 0;

            int index = text.lastIndexOf(" ");
            String sTmp = text;
            boolean check = false;
            if (textWidth > width) {
                check = true;
            } while (textWidth > width) {
                index = sTmp.lastIndexOf(" ");
                if (index > 0) {
                    sTmp = text.substring(0, index).trim();
                    textWidth = fm.stringWidth(sTmp);
                } else {
                    break;
                }
            }
            if (check) {
                word[0] = sTmp;
                word[1] = text.substring(sTmp.length()).trim();
                y0 = 2 * (height / (2 * word.length + 1));

                for (int i = 0; i < word.length; i++) {
                    x0 = (width - fm.stringWidth(word[i])) / 2;
                    g.drawString(word[i], x0, y0);
                    y0 = y0 + 2 * size;
                }
            } else {
                x0 = (width - textWidth) / 2 + 1;
                y0 = (height - fm.getHeight()) / 2 + fm.getAscent() - (size / 8);
                g.drawString(text, x0, y0);
            }

//            JPEGImageEncoder en = JPEGCodec.createJPEGEncoder(out);
//            en.encode(bimg);
//            out.close();

//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            GifEncoder encoder = new GifEncoder(bimg, out);
            encoder.encode();
            out.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Write2Image test = new Write2Image();
        try {
//            String t = "C\u1EE7 chu\u1ED1i";
//            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("c:\\text.txt"), "UTF8"));
//            out.write(t);
//            out.close();
//
//            byte[] keke = t.getBytes("UTF-8");
//            System.out.println(keke);

//            test.addTextJPG("\ufeffC\u1ee7 chu\u1ed1i",".TMC-Ong Do",Color.white,25,new FileOutputStream("c:\\test.gif"),15,15);
            test.addTextJPG("C\u1EE7 chu\u1ED1i","UVN Muc Cham",Color.black,90,new FileOutputStream("c:\\test.gif"),15,15);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
