package com.sms.image;

/**
 * <p>Title: MyRubic Server application</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004 VASC</p>
 * <p>Company: </p>
 * @author
 * @version  1.0 Release 1
 */

import java.io.*;
import java.awt.*;

public class GenImageSecuNum {
  public GenImageSecuNum() {
  }
  public String gen(long secuNumber) throws IOException {
    Graphics g = null;

    Frame frame = new Frame();
    frame.addNotify();
    Image image = frame.createImage(140,50);
    g = image.getGraphics();
//    g.setFont(new Font("Arial", Font.PLAIN, 110));
    g.setFont(new Font("Arial", Font.PLAIN, 30));
    g.setColor(new Color(0, 99, 0));
    g.drawString(String.valueOf(secuNumber),8,35);
    g.drawLine(0,5,56,88);
    g.dispose();
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    GifEncoder encoder = new GifEncoder(image, baos);
    encoder.encode();
    baos.flush();


    frame.removeNotify();
    String b64Image = new sun.misc.BASE64Encoder().encode(baos.toByteArray());
    b64Image = b64Image.replaceAll("\r","");
    b64Image = b64Image.replaceAll("\n","");
    return (b64Image);
  }
}
