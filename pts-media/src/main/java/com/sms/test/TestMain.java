/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sms.test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author hungdt
 */
public class TestMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            Calendar cal = Calendar.getInstance();
            DateFormat df = new SimpleDateFormat("EEE");
            System.out.println(df.format(cal.getTime()));
            System.out.println(cal.get(Calendar.HOUR_OF_DAY));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
