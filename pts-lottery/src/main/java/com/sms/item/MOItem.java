package com.sms.item;

import java.math.BigDecimal;

public class MOItem {
    BigDecimal id = null;
    String userId = "";
    String serviceId = "";
    String commandCode = "";
    String mobileOperator = "";
    String info = "";
    String timeReceive = "";//ddmmyyyy
    int responsed = 0;
    int count = 0;

    public MOItem() {
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }
    public BigDecimal getId() {
        return id;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getUserId() {
        return userId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
    public String getServiceId() {
        return serviceId;
    }

    public void setCommandCode(String commandCode) {
        this.commandCode = commandCode;
    }
    public String getCommandCode() {
        return commandCode;
    }

    public void setInfo(String info) {
        this.info = info;
    }
    public String getInfo() {
        return info;
    }

    public void setMobileOperator(String mobileOperator) {
        this.mobileOperator = mobileOperator;
    }
    public String getMobileOperator() {
        return mobileOperator;
    }

    public void setTimeReceive(String timeReceive) {
        this.timeReceive = timeReceive;
    }
    public String getTimeReceive() {
        return timeReceive;
    }

    public void setResponsed(int responsed) {
        this.responsed = responsed;
    }
    public int getResponsed() {
        return responsed;
    }

    public void setCount(int count) {
        this.count = count;
    }
    public int getCount() {
        return count;
    }
}
