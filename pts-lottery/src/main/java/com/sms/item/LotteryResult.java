package com.sms.item;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import java.math.BigDecimal;

public class LotteryResult {
    BigDecimal id = null;
    BigDecimal companyId = null;
    String result = "";
    String resultDate = "";// DD/MM/YY
    int resultStatus = 0;

    public LotteryResult() {
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }
    public BigDecimal getId() {
        return id;
    }

    public void setCompanyId(BigDecimal companyId) {
        this.companyId = companyId;
    }

    public BigDecimal getCompanyId() {
        return companyId;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDate(String resultDate) {
        this.resultDate = resultDate;
    }

    public String getResultDate() {
        return resultDate;
    }

    public void setResultStatus(int resultStatus) {
      this.resultStatus = resultStatus;
    }

    public int getResultStatus() {
      return resultStatus;
    }
}
