package com.sms.item;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import java.math.BigDecimal;

public class LotteryDaily {
    BigDecimal id ;
    String userId = "";
    String serviceId = "";
    String commandCode = "";
    String mobileOperator = "";
    BigDecimal companyId;
    int remain = 0;
    int status = 0;
    String requestDate = ""; // DD/MM/YY
    String responseDate = "";// DD/MM/YY
    BigDecimal requestId;
    String info = "";

    public LotteryDaily() {
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setCommandCode(String commandCode) {
      this.commandCode = commandCode;
    }

    public String getCommandCode() {
      return commandCode;
    }

    public void setMobileOperator(String mobileOperator) {
        this.mobileOperator = mobileOperator;
    }

    public String getMobileOperator() {
        return mobileOperator;
    }

    public void setCompanyId(BigDecimal companyId) {
        this.companyId = companyId;
    }

    public BigDecimal getCompanyId() {
        return companyId;
    }

    public void setRemain(int remain) {
        this.remain = remain;
    }

    public int getRemain() {
        return remain;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        //0: not location; 1: according location MT, MN
        return status;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setResponseDate(String responseDate) {
        this.responseDate = responseDate;
    }

    public String getResponseDate() {
        return responseDate;
    }

    public void setRequestId(BigDecimal requestId) {
        this.requestId = requestId;
    }

    public BigDecimal getRequestId() {
        return requestId;
    }

    public void setInfo(String info) {
        this.info = info;
    }
    public String getInfo() {
        return info;
    }
}
