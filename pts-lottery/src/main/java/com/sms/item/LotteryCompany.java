package com.sms.item;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
import java.math.BigDecimal;

public class LotteryCompany {
  BigDecimal id = null;
  String code = "";
  String name = "";
  String companyDesc = "";

  public LotteryCompany() {
  }

  public void setCompanyId(BigDecimal id) {
    this.id = id;
  }

  public BigDecimal getCompanyId() {
    return id;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setCompanyDesc(String companyDesc) {
    this.companyDesc = companyDesc;
  }

  public String getCompanyDesc() {
    return companyDesc;
  }

}
