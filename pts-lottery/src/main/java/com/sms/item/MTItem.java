package com.sms.item;

import java.math.BigDecimal;

public class MTItem {
    BigDecimal id = null;
    BigDecimal requestId = null;
    String userId = "";
    String serviceId = "";
    String commandCode = "";
    String mobileOperator = "";
    String info = "";
    int messageType = 0;
    int contentType = 0;
    String doneDate = "";

    public MTItem() {
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }
    public BigDecimal getId() {
        return id;
    }

    public void setRequestId(BigDecimal requestId) {
        this.requestId = requestId;
    }
    public BigDecimal getRequestId() {
        return requestId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getUserId() {
        return userId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
    public String getServiceId() {
        return serviceId;
    }

    public void setCommandCode(String commandCode) {
        this.commandCode = commandCode;
    }
    public String getCommandCode() {
        return commandCode;
    }

    public void setMobileOperator(String mobileOperator) {
        this.mobileOperator = mobileOperator;
    }
    public String getMobileOperator() {
        return mobileOperator;
    }

    public void setInfo(String info) {
        this.info = info;
    }
    public String getInfo() {
        return info;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setContentType(int contentType) {
        this.contentType = contentType;
    }

    public int getContentType() {
        return contentType;
    }

    public void setDoneDate(String doneDate) {
        this.doneDate = doneDate;
    }
    public String getDoneDate() {
        return doneDate;
    }
}
