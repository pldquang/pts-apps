package com.sms.common;

/**
 * <p>Title: SMPP Gateway</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: Home</p>
 * @author not attributable
 * @version 1.0
 */
import java.io.*;
import java.util.*;
import java.sql.*;
import java.math.BigDecimal;

import oracle.jdbc.driver.OracleDriver;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utilities {
    static FileOutputStream fout = null;
    static char enter = (char) 10;
    static final boolean VERBOSE = true;
    static char hexChar[] = {
                            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                            'A', 'B', 'C', 'D', 'E', 'F'
    };

    public Utilities() {
    }

    public Connection getDBConnection(String url, String user,
                                      String password) throws SQLException {
        Connection conn = null;
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            conn = DriverManager.getConnection(url, user, password);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return conn;
    }

    public void cleanup(Connection con, PreparedStatement ps) {
        try {
            if (ps != null)
                ps.close();
            if (con != null)
                con.close();
        } catch (Exception e) {}
    }

    public void cleanup(PreparedStatement ps, Statement stmt) {
        try {
            if (ps != null)
                ps.close();
            if (stmt != null)
                stmt.close();
        } catch (Exception e) {}
    }

    public void cleanup(ResultSet rs) {
        try {
            if (rs != null)
                rs.close();
        } catch (Exception e) {}
    }

    public void closeConnection(Connection connection, Statement statement) {
        try {
            if (statement != null)
                statement.close();
            if (connection != null)
                connection.close();
        } catch (SQLException e) {}
    }

    public static java.sql.Timestamp createTimestampFullDDMMYY(String strTime) {
        SimpleDateFormat formatterdv = new SimpleDateFormat("dd/MM/yy");
        java.util.Date date = null;
        try {
            date = formatterdv.parse(strTime);
            return new java.sql.Timestamp(date.getTime());
        } catch (Exception ex) {
        }
        return null;
    }

    public static byte[] getBytes(Object obj) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(obj);
        oos.flush();
        oos.close();
        bos.close();
        byte data[] = bos.toByteArray();
        return data;
    }

    public static String toHexString(byte b[]) {
        StringBuffer sb = new StringBuffer(b.length * 2);
        for (int i = 0; i < b.length; i++) {
            sb.append(hexChar[(b[i] & 0xf0) >>> 4]);
            sb.append(hexChar[b[i] & 0xf]);
        }

        return sb.toString();
    }

    public static byte[] fromHexString(String s) {
        int stringLength = s.length();
        if ((stringLength & 1) != 0)
            throw new IllegalArgumentException(
                    "fromHexString requires an even number of hex characters");
        byte b[] = new byte[stringLength / 2];
        int i = 0;
        for (int j = 0; i < stringLength; j++) {
            int high = charToNibble(s.charAt(i));
            int low = charToNibble(s.charAt(i + 1));
            b[j] = (byte) (high << 4 | low);
            i += 2;
        }

        return b;
    }

    private static int charToNibble(char c) {
        if ('0' <= c && c <= '9')
            return c - 48;
        if ('a' <= c && c <= 'f')
            return (c - 97) + 10;
        if ('A' <= c && c <= 'F')
            return (c - 65) + 10;
        else
            throw new IllegalArgumentException("Invalid hex character: " + c);
    }

    public static double round(double value, int decimalPlace) {
        double power_of_ten;
        for (power_of_ten = 1.0D; decimalPlace-- > 0; power_of_ten *= 10D)
            ;
        return (double) Math.round(value * power_of_ten) / power_of_ten;
    }

    public static double roundEx(double value, int decimalPlace) {
        BigDecimal bigValue = new BigDecimal(value);
        bigValue = bigValue.setScale(decimalPlace, 4);
        return bigValue.doubleValue();
    }

    public static String removePlusSign(String userId) {
        String temp = userId;
        if (temp.startsWith("+"))
            temp = temp.substring(1);
        return temp;
    }

    public static String rebuildServiceId(String serviceId) {
        String temp = serviceId;
        if (temp.startsWith("+"))
            temp = temp.substring(1);
        if (temp.startsWith("84") || temp.startsWith("04"))
            temp = temp.substring(2);
        return temp;
    }

    public static Vector getListCompanyMTToday() {
        Vector v = new Vector();
        GregorianCalendar newCal = new GregorianCalendar();
        int day = newCal.get(Calendar.DAY_OF_WEEK);
        //day = 1 -> CN
        //    = 2 -> T2
        //    = 3 -> T3
        //    = 4 -> T4
        //    = 5 -> T5
        //    = 6 -> T6
        //    = 7 -> T7

        if (day == 1) {
            v.add(new BigDecimal(8)); //Khanh Hoa
            v.add(new BigDecimal(9)); //Kon Tum
        } else if (day == 2) {
            v.add(new BigDecimal(15)); //Thua Thien Hue
            v.add(new BigDecimal(11)); //Phu Yen
        } else if (day == 3) {
            //T3 & MT
            v.add(new BigDecimal(5)); //Daklak
            v.add(new BigDecimal(12)); //Quang Nam
        } else if (day == 4) {
            //T4 & MT
            v.add(new BigDecimal(8)); //Khanh Hoa
            v.add(new BigDecimal(6)); //Da Nang
        } else if (day == 5) {
            //T5 & MT
            v.add(new BigDecimal(4)); //Binh Dinh
            v.add(new BigDecimal(17)); //Quang Tri
            v.add(new BigDecimal(16)); //Quang Binh
        } else if (day == 6) {
            //T6 & MT
            v.add(new BigDecimal(10)); //Ninh Thuan
            v.add(new BigDecimal(7)); //Gia Lai
        } else if (day == 7) {
            //T7 & MT
            v.add(new BigDecimal(6)); //Da Nang
            v.add(new BigDecimal(13)); //Quang Ngai QNI|QNG
            v.add(new BigDecimal(14)); //Dac Nong
        }
        return v;
    }

    public static Vector getListCompanyMTByDayOfWeek(int dayOfWeek) {
        Vector v = new Vector();
//      GregorianCalendar newCal = new GregorianCalendar();
//      int day = newCal.get(Calendar.DAY_OF_WEEK);
        //day = 1 -> CN
        //    = 2 -> T2
        //    = 3 -> T3
        //    = 4 -> T4
        //    = 5 -> T5
        //    = 6 -> T6
        //    = 7 -> T7

        if (dayOfWeek == 1) {
            //CN & MT
            v.add(new BigDecimal(8)); //Khanh Hoa
            v.add(new BigDecimal(9)); //Kon Tum
        } else if (dayOfWeek == 2) {
            //T2 & MT
            v.add(new BigDecimal(15)); //Thua Thien Hue
            v.add(new BigDecimal(11)); //Phu Yen
        } else if (dayOfWeek == 3) {
            //T3 & MT
            v.add(new BigDecimal(5)); //Daklak
            v.add(new BigDecimal(12)); //Quang Nam
        } else if (dayOfWeek == 4) {
            //T4 & MT
            v.add(new BigDecimal(8)); //Khanh Hoa
            v.add(new BigDecimal(6)); //Da Nang
        } else if (dayOfWeek == 5) {
            //T5 & MT
            v.add(new BigDecimal(4)); //Binh Dinh
            v.add(new BigDecimal(17)); //Quang Tri
            v.add(new BigDecimal(16)); //Quang Binh
        } else if (dayOfWeek == 6) {
            //T6 & MT
            v.add(new BigDecimal(10)); //Ninh Thuan
            v.add(new BigDecimal(7)); //Gia Lai
        } else if (dayOfWeek == 7) {
            //T7 & MT
            v.add(new BigDecimal(6)); //Da Nang
            v.add(new BigDecimal(13)); //Quang Ngai QNI|QNG
            v.add(new BigDecimal(14)); //Dac Nong
        }
        return v;
    }

    public static Vector getListCompanyMTByDayOfMonth(int dayOfMonth, int month) {
        Vector v = new Vector();
        int year = Integer.parseInt(DateProc.getCurrentYYYY());
        GregorianCalendar newCal = new GregorianCalendar(year,month-1,dayOfMonth);
        int day = newCal.get(Calendar.DAY_OF_WEEK);
        //day = 1 -> CN
        //    = 2 -> T2
        //    = 3 -> T3
        //    = 4 -> T4
        //    = 5 -> T5
        //    = 6 -> T6
        //    = 7 -> T7
        v = getListCompanyMTByDayOfWeek(day);
        return v;
    }

    public static Vector getListCompanyMNToday() {
        Vector v = new Vector();
        GregorianCalendar newCal = new GregorianCalendar();
        int day = newCal.get(Calendar.DAY_OF_WEEK);
        //day = 1 -> CN
        //    = 2 -> T2
        //    = 3 -> T3
        //    = 4 -> T4
        //    = 5 -> T5
        //    = 6 -> T6
        //    = 7 -> T7

        if (day == 1) {
            //CN & MN
            v.add(new BigDecimal(36)); //Tien Giang
            v.add(new BigDecimal(32)); //Kien Giang
            v.add(new BigDecimal(34)); //Lam Dong - LD
        } else if (day == 2) {
            //T2 & MT
            v.add(new BigDecimal(31)); //Tp.HCM - TPHCM
            v.add(new BigDecimal(29)); //Dong Thap
            v.add(new BigDecimal(26)); //Ca Mau
        } else if (day == 3) {
            //T3 & MN
            v.add(new BigDecimal(23)); //Vung Tau - BRVT
            v.add(new BigDecimal(24)); //Ben Tre - BTE
            v.add(new BigDecimal(21)); //Bac Lieu
        } else if (day == 4) {
            //T4 & MN
            v.add(new BigDecimal(28)); //Dong Nai
            v.add(new BigDecimal(27)); //Can Tho
            v.add(new BigDecimal(35)); //Soc Trang
        } else if (day == 5) {
            //T5 & MN
            v.add(new BigDecimal(37)); //Tay Ninh
            v.add(new BigDecimal(19)); //An Giang
            v.add(new BigDecimal(25)); //Binh Thuan - BTN
        } else if (day == 6) {
            //T6 & MN
            v.add(new BigDecimal(20)); //Binh Duong
            v.add(new BigDecimal(39)); //Vinh Long
            v.add(new BigDecimal(38)); //Tra Vinh
        } else if (day == 7) {
            //T7 & MN
            v.add(new BigDecimal(31)); //tp.HCM - TPHCM
            v.add(new BigDecimal(22)); //Binh Phuoc
            v.add(new BigDecimal(33)); //Long An
            v.add(new BigDecimal(30)); //Hau Giang
        }
        return v;
    }

    public static Vector getListCompanyMNByDayOfWeek(int dayOfWeek) {
        Vector v = new Vector();
//      GregorianCalendar newCal = new GregorianCalendar();
//      int day = newCal.get(Calendar.DAY_OF_WEEK);
        //day = 1 -> CN
        //    = 2 -> T2
        //    = 3 -> T3
        //    = 4 -> T4
        //    = 5 -> T5
        //    = 6 -> T6
        //    = 7 -> T7

        if (dayOfWeek == 1) {
            //CN & MN
            v.add(new BigDecimal(36)); //Tien Giang
            v.add(new BigDecimal(32)); //Kien Giang
            v.add(new BigDecimal(34)); //Lam Dong - LD
        } else if (dayOfWeek == 2) {
            //T2 & MN
            v.add(new BigDecimal(31)); //Tp.HCM - TPHCM
            v.add(new BigDecimal(29)); //Dong Thap
            v.add(new BigDecimal(26)); //Ca Mau
        } else if (dayOfWeek == 3) {
            //T3 & MN
            v.add(new BigDecimal(23)); //Vung Tau - BRVT
            v.add(new BigDecimal(24)); //Ben Tre - BTE
            v.add(new BigDecimal(21)); //Bac Lieu
        } else if (dayOfWeek == 4) {
            //T4 & MN
            v.add(new BigDecimal(28)); //Dong Nai
            v.add(new BigDecimal(27)); //Can Tho
            v.add(new BigDecimal(35)); //Soc Trang
        } else if (dayOfWeek == 5) {
            //T5 & MN
            v.add(new BigDecimal(37)); //Tay Ninh
            v.add(new BigDecimal(19)); //An Giang
            v.add(new BigDecimal(25)); //Binh Thuan - BTN
        } else if (dayOfWeek == 6) {
            //T6 & MN
            v.add(new BigDecimal(20)); //Binh Duong
            v.add(new BigDecimal(39)); //Vinh Long
            v.add(new BigDecimal(38)); //Tra Vinh
        } else if (dayOfWeek == 7) {
            //T7 & MN
            v.add(new BigDecimal(31)); //tp.HCM - TPHCM
            v.add(new BigDecimal(22)); //Binh Phuoc
            v.add(new BigDecimal(33)); //Long An
            v.add(new BigDecimal(30)); //Hau Giang
        }
        return v;
    }

    public static Vector getListCompanyMNByDayOfMonth(int dayOfMonth, int month) {
        Vector v = new Vector();
        int year = Integer.parseInt(DateProc.getCurrentYYYY());
        GregorianCalendar newCal = new GregorianCalendar(year,month -1, dayOfMonth);
        int day = newCal.get(Calendar.DAY_OF_WEEK);
        //day = 1 -> CN
        //    = 2 -> T2
        //    = 3 -> T3
        //    = 4 -> T4
        //    = 5 -> T5
        //    = 6 -> T6
        //    = 7 -> T7
        v = getListCompanyMNByDayOfWeek(day);
        return v;
    }

    public static void bubbleSort(Vector v) {
        //Sort by descending length elements in a Vector
        try {
            boolean isSorted;
            String tmp;
            int numberOfTimesLooped = 0;
            do {
                isSorted = true;
                for (int i = 1; i < v.size() - numberOfTimesLooped; i++) {
                    int len1 = ((String) v.elementAt(i)).length();
                    int len2 = ((String) v.elementAt(i - 1)).length();
                    if (len1 > len2) {
                        tmp = (String) v.elementAt(i);
                        v.remove(i);
                        v.add(i, v.elementAt(i - 1));
                        v.remove(i - 1);
                        v.add(i - 1, tmp);
                        isSorted = false;
                    }
                }
                numberOfTimesLooped++;
            } while (!isSorted);

        } catch (Exception ex) {
        }
    }

    public static String getXSMBOld(String inputStr) {
        inputStr = inputStr.toUpperCase();
        String kq = "";
        try {
            if(inputStr.startsWith("MB"))
                inputStr = inputStr.substring(1);
            //format string inpput
            inputStr = StringTool.getContentFormatLottery(inputStr);

            //Lay giai Dac biet
            int i;
            if((i = inputStr.indexOf("DB")) != -1) {
                kq = kq + inputStr.substring(i, i + 2) + ":" + inputStr.substring(i + 3, inputStr.length());
                inputStr = inputStr.substring(0, i - 1);
            }


            //Lay cac giai con laij
            StringTokenizer strTK = new StringTokenizer(inputStr, ".", false);
            while (strTK.hasMoreElements()) {
                String s = strTK.nextToken();
                if ((s.length() == 1) && StringTool.isNumberic(s) && (Integer.parseInt(s) >= 1) && (Integer.parseInt(s) <= 9)) {
                    if(kq.endsWith("-")) {
                        kq = kq.substring(0,kq.length() - 1);
                    }
                    if(!kq.equals("")) {
                        kq = kq + "\n" + s + ":";
                    } else {
                        kq = s + ":";
                    }
                } else {
                    kq = kq + s + "-";
                }
            }

            if(kq.endsWith("-"))
                kq = kq.substring(0,kq.length()-1);

            kq = "MB " + DateProc.getCurrentDDMM() + "\n" + kq;

        } catch (Exception ex) {
            ex.printStackTrace();
            kq = "ERROR";
        } finally {
            return kq;
        }
    }

    public static String getXSMB(String inputStr) {
        inputStr = inputStr.toUpperCase();
        String kq = "";
        boolean valid = true;
        try {
            if(inputStr.startsWith("MB"))
                inputStr = inputStr.substring(2);
            //format string inpput
            inputStr = StringTool.getContentFormatLottery(inputStr);

            //Lay giai Dac biet
            int i;
            if((i = inputStr.indexOf("DB")) != -1) {
                String bestPrice = inputStr.substring(i + 3, inputStr.length());
                if((bestPrice != null) && (bestPrice.length() == 5) && (StringTool.isNumberic(bestPrice))) {
                    kq = kq + inputStr.substring(i, i + 2) + ":" + inputStr.substring(i + 3, inputStr.length());
                    inputStr = inputStr.substring(0, i - 1);
                } else {
                    valid = false;
                }
            }


            //Lay cac giai con laij
            StringTokenizer strTK = new StringTokenizer(inputStr, ".", false);
            int indexPrice = 0;
            while (strTK.hasMoreElements()) {
                String s = strTK.nextToken();
                if ((s.length() == 1) && StringTool.isNumberic(s) && (Integer.parseInt(s) >= 1) && (Integer.parseInt(s) <= 7)) {

                    indexPrice = Integer.parseInt(s);

                    if(kq.endsWith("-")) {
                        kq = kq.substring(0,kq.length() - 1);
                    }
                    if(!kq.equals("")) {
                        kq = kq + enter + s + ":";
                    } else {
                        kq = s + ":";
                    }
                } else {
                    if(((indexPrice == 1) || (indexPrice == 2) || (indexPrice == 3)) && StringTool.isNumberic(s) && (s.length()==5)) {
                        kq = kq + s + "-";
                    } else if(((indexPrice == 4) || (indexPrice == 5)) && StringTool.isNumberic(s) && (s.length()==4)) {
                        kq = kq + s + "-";
                    } else if((indexPrice == 6) && StringTool.isNumberic(s) && (s.length()==3)) {
                        kq = kq + s + "-";
                    } else if((indexPrice == 7) && StringTool.isNumberic(s) && (s.length()==2)) {
                        kq = kq + s + "-";
                    } else {
                        if(indexPrice != 0) {
                            valid = false;
                        }
                    }
                }
            }

            if(kq.endsWith("-"))
                kq = kq.substring(0,kq.length()-1);

            kq = "MB " + DateProc.getCurrentDDMM() + enter + kq;

            if(!valid) {
                kq = "";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            kq = "";
        } finally {
            return kq;
        }
    }

    public static void main(String[] args) {
        try {
            Utilities test = new Utilities();
            String data = "Mb 1.07893.2.66420.29509.3.06452.77978.06160.09451.86999.44961.4.6917.0390.3829.6883.5.3231.8870.0317.5650.9693.4336.6.409.454.757.7.80.42.52.62!xxx!Mb 1.07893.2.66420.29509.3.06452.77978.06160.09451.86999.44961.4.6917.0390.3829.6883.5.3231.8870.0317.5650.9693.4336.6.409.454.757.7.80.42.52.62.db.53099";
            String result = test.getXSMB(data);
            System.out.println(result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
