package com.sms.common;

/**
 * <p>Title: admin system</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: admin</p>
 *
 * @author admin
 * @version 1.0
 */

import java.util.*;
import java.sql.*;

public class DBPool {
    static LinkedList dbPoolContent = null;
    static LinkedList dbPoolRemote = null;

    public DBPool() {
        dbPoolContent = new LinkedList();
        dbPoolRemote = new LinkedList();
    }

    private static Connection makeDBConnection(String url, String user, String password) {
        Connection conn = null;
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            conn = DriverManager.getConnection(url, user, password);
        } catch (Exception ex) {
            MyLogger.log(ex.getMessage());
            MyLogger.log("makeDBConnection Error, reconnect in " + 10000);
            sleep(10000);
        }
        return conn;
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception ex) {}
    }

    public void initConnect2DBContent() {
        Connection conn = null;
        MyLogger.log("[DBPool Content] initializing...");
        while (dbPoolContent.size() < Preference.pooldb_max_content) {
            for (int i = 0; i < Preference.pooldb_max_content; i++) {
                conn = makeDBConnection(Preference.db_url_content, Preference.db_user_content, Preference.db_password_content);
                if (conn != null) {
                    dbPoolContent.addLast(conn);
                }
            }
            if(dbPoolContent.size() < Preference.pooldb_max_remote) {
                try {
                    MyLogger.log("dbPoolContent size: " + dbPoolContent.size());
                    MyLogger.log("Reconnect to DBContent in 60s...");
                    Thread.sleep(60000);
                } catch (Exception ex) {}
            }
        }
        MyLogger.log("Size of DBPoolContent is: " + dbPoolContent.size() + " connections\n");
    }

    public void initConnect2DBRemote() {
        Connection conn = null;
        MyLogger.log("[DBPool Remote] initializing...");
        while (dbPoolRemote.size() < Preference.pooldb_max_remote) {
            for (int i = 0; i < Preference.pooldb_max_remote; i++) {
                conn = makeDBConnection(Preference.db_url_remote, Preference.db_user_remote, Preference.db_password_remote);
                if (conn != null) {
                    dbPoolRemote.addLast(conn);
                }
            }
            if(dbPoolRemote.size() < Preference.pooldb_max_remote) {
                try {
                    MyLogger.log("Reconnect to DBContent in 60s...");
                    Thread.sleep(60000);
                } catch (Exception ex) {}
            }
        }
        MyLogger.log("Size of DBPoolRemote is: " + dbPoolRemote.size() + " connections\n");
    }

    public static Connection getConnectionContent() {
        Connection conn = null;
        synchronized (dbPoolContent) {
            while (conn == null) {
                try {
                    if (dbPoolContent.isEmpty()) {
                        conn = makeDBConnection(Preference.db_url_content, Preference.db_user_content, Preference.db_password_content);
                    } else {
                        conn = (Connection) dbPoolContent.removeFirst();
                    }
                    conn.setAutoCommit(true);
                } catch (Exception ex) {
                    MyLogger.log(ex.getMessage());
                }

                if (conn == null) {
                    try {
                        MyLogger.log("Get Connect Content is null, retry ...");
                        Thread.sleep(3000);
                    } catch (Exception ex) {}
                }
            } // End while
            return conn;
        }
    }

    public static Connection getConnectionRemote() {
        Connection conn = null;
        synchronized (dbPoolRemote) {
            while (conn == null) {
                try {
                    if (dbPoolRemote.isEmpty()) {
                        conn = makeDBConnection(Preference.db_url_remote, Preference.db_user_remote, Preference.db_password_remote);
                    } else {
                        conn = (Connection) dbPoolRemote.removeFirst();
                    }
                    conn.setAutoCommit(true);
                } catch (Exception ex) {
                    MyLogger.log(ex.getMessage());
                }

                if (conn == null) {
                    try {
                        MyLogger.log("Get Connect Remote is null, retry ...");
                        Thread.sleep(3000);
                    } catch (Exception ex) {}
                }
            }
            return conn;
        }
    }

    public static void putConnection2DBContent(Connection conn) {
        try { // Ignore closed connection
            if (conn == null || conn.isClosed()) {
                MyLogger.log("DBPoolContent putConnection: conn is null or closed: " + conn);
                return;
            }
            if (dbPoolContent.size() >= Preference.pooldb_max_content) {
                conn.close();
                return;
            }
        } catch (SQLException ex) {}

        synchronized (dbPoolContent) {
            dbPoolContent.addLast(conn);
            dbPoolContent.notifyAll();
        }
    }

    public static void putConnection2DBRemote(Connection conn) {
        try { // Ignore closed connection
            if (conn == null || conn.isClosed()) {
                MyLogger.log("DBPoolRemote putConnection: conn is null or closed: " +
                                   conn);
                return;
            }
            if (dbPoolRemote.size() >= Preference.pooldb_max_remote) {
                conn.close();
                return;
            }
        } catch (SQLException ex) {}

        synchronized (dbPoolRemote) {
            dbPoolRemote.addLast(conn);
            dbPoolRemote.notifyAll();
        }
    }

// Remove and close all connections in pool
    public static void releaseAllConnection() {
        //Close dbPoolContent
        synchronized (dbPoolContent) {
            for (Iterator it = dbPoolContent.iterator(); it.hasNext(); ) {
                try {
                    ((Connection) it.next()).close();
                } catch (SQLException e) {
                    MyLogger.log("dbPoolContent release: Cannot close connection! (maybe closed?)");
                }
            }
            dbPoolContent.clear();
        }

        //Close dbPoolRemote
        synchronized (dbPoolRemote) {
            for (Iterator it = dbPoolRemote.iterator(); it.hasNext(); ) {
                try {
                    ((Connection) it.next()).close();
                } catch (SQLException e) {
                    MyLogger.log("dbPoolRemote release: Cannot close connection! (maybe closed?)");
                }
            }
            dbPoolRemote.clear();
        }
    }

    public static void main(String args[]) {
        DBPool pool = new DBPool();
        pool.getConnectionContent();
    }

}
