package com.sms.common;

/**
 * <p>Title: SMPP Gateway</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Home</p>
 *
 * @author not attributable
 * @version 1.0
 */
import java.util.*;
import java.util.Collection;

public class StringTool {
    static final String seperators[] = {
                                       " ", ".", ",", "-", "_", "="
    };

    public StringTool() {
    }

    public static String replaceCharAt(String s, int pos, char c) {
        StringBuffer buf = new StringBuffer(s);
        buf.setCharAt(pos, c);
        return buf.toString();
    }

    public static String removeChar(String s, char c) {
        StringBuffer newString = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            char cur = s.charAt(i);
            if (cur != c)
                newString.append(cur);
        }

        return newString.toString();
    }

    public static String removeCharAt(String s, int pos) {
        StringBuffer buf = new StringBuffer(s.length() - 1);
        buf.append(s.substring(0, pos)).append(s.substring(pos + 1));
        return buf.toString();
    }

    public static Collection parseString(String text, String seperator) {
        Vector vResult = new Vector();
        if (text == null || "".equals(text))
            return vResult;
        String tempStr = text.trim();
        String currentLabel = null;
        for (int index = tempStr.indexOf(seperator); index != -1;
                         index = tempStr.indexOf(seperator)) {
            currentLabel = tempStr.substring(0, index).trim();
            if (!"".equals(currentLabel))
                vResult.addElement(currentLabel);
            tempStr = tempStr.substring(index + 1);
        }

        currentLabel = tempStr.trim();
        if (!"".equals(currentLabel))
            vResult.addElement(currentLabel);
        return vResult;
    }

    public static Collection parseString(String text) {
        Vector vResult = new Vector();
        if (text == null || "".equals(text))
            return vResult;
        String tempStr = text.trim();
        String currentLabel = null;
        for (int index = getNextIndex(tempStr); index != -1;
                         index = getNextIndex(tempStr)) {
            currentLabel = tempStr.substring(0, index).trim();
            if (!"".equals(currentLabel))
                vResult.addElement(currentLabel);
            tempStr = tempStr.substring(index + 1);
        }

        currentLabel = tempStr.trim();
        if (!"".equals(currentLabel))
            vResult.addElement(currentLabel);
        return vResult;
    }

    private static int getNextIndex(String text) {
        int index = 0;
        int newIdx = 0;
        boolean hasOne = false;
        for (int i = 0; i < seperators.length; i++) {
            newIdx = text.indexOf(seperators[i]);
            if (!hasOne) {
                if (newIdx != -1) {
                    index = newIdx;
                    hasOne = true;
                }
            } else
            if (newIdx != -1 && newIdx < index)
                index = newIdx;
        }

        if (!hasOne)
            index = -1;
        return index;
    }

    public static Collection parseStringEx(String text) {
        Vector vResult = new Vector();
        if (text == null || "".equals(text))
            return vResult;
        String tempStr = text.trim();
        char NINE = '9';
        char ZERO = '0';
        char CH_a = 'a';
        char CH_z = 'z';
        char CH_A = 'A';
        char CH_Z = 'Z';
        String currLabel = "";
        char currChar = '\0';
        for (int i = 0; i < tempStr.length(); i++) {
            currChar = tempStr.charAt(i);
            if (currChar >= ZERO && currChar <= NINE ||
                currChar >= CH_a && currChar <= CH_z ||
                currChar >= CH_A && currChar <= CH_Z)
                currLabel = currLabel + currChar;
            else
            if (currLabel.length() > 0) {
                vResult.add(currLabel);
                currLabel = new String("");
            }
        }

        if (currLabel.length() > 0)
            vResult.add(currLabel);
        return vResult;
    }

    public static boolean isNumberic(String sNumber) {
        if (sNumber == null || "".equals(sNumber))
            return false;
        char ch_max = '9';
        char ch_min = '0';
        for (int i = 0; i < sNumber.length(); i++) {
            char ch = sNumber.charAt(i);
            if (ch < ch_min || ch > ch_max)
                return false;
        }

        return true;
    }


    public static String replaceWhiteLetter(String sInput) {
        String strTmp = sInput;
        String sReturn = "";
        boolean flag = true;
        int i = 0;
        while (i < sInput.length() && flag) {
            char ch = sInput.charAt(i);
            if ((ch >= '0' && ch <= '9') ||
                (ch >= 'A' && ch <= 'Z') ||
                (ch >= 'a' && ch <= 'z'))
                flag = false;
            else
                strTmp = sInput.substring(i + 1);
            i++;
        }

        i = strTmp.length() - 1;
        flag = true;
        sReturn = strTmp;
        while (i >= 0 && flag) {
            char ch = strTmp.charAt(i);
            if ((ch >= '0' && ch <= '9') ||
                (ch >= 'A' && ch <= 'Z') ||
                (ch >= 'a' && ch <= 'z'))
                flag = false;
            else
                sReturn = strTmp.substring(0, i);
            i--;
        }
        return sReturn;
    }

    public static int getIndexOfSeparate(String strInput, int type) {
        if (type == 1) {
            for (int i = strInput.length() - 1; i >= 0; i--) {
                char ch = strInput.charAt(i);
                if ((ch < '0') ||
                    (ch > '9' && ch < 'A') ||
                    (ch > 'Z' && ch < 'a') ||
                    (ch > 'z')) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < strInput.length(); i++) {
                char ch = strInput.charAt(i);
                if ((ch < '0') ||
                    (ch > '9' && ch < 'A') ||
                    (ch > 'Z' && ch < 'a') ||
                    (ch > 'z')) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static String getObjectString1(String msg, int index) {
        String sReturn = "";
        String sTmp = replaceWhiteLetter(msg);
        int i = getIndexOfSeparate(sTmp, 0);
        if (i < 0)
            return null;
        sTmp = sTmp.substring(i + 1);
        sTmp = replaceWhiteLetter(sTmp);
        i = getIndexOfSeparate(sTmp, 0);
        switch (index) {
        case 1:
            if (i < 0)
                sReturn = sTmp;
            else
                sReturn = sTmp.substring(0, i);
            break;
        case 2:
            if (i < 0)
                return null;
            sTmp = sTmp.substring(i + 1);
            sTmp = replaceWhiteLetter(sTmp);
            i = getIndexOfSeparate(sTmp, 0);
            if (i < 0)
                sReturn = sTmp;
            else
                sReturn = sTmp.substring(0, i);
            break;
        case 3:
            if (i < 0)
                return null;
            sTmp = sTmp.substring(i + 1);
            sTmp = replaceWhiteLetter(sTmp);
            i = getIndexOfSeparate(sTmp, 0);
            if (i < 0)
                return null;
            sTmp = sTmp.substring(i + 1);
            sTmp = replaceWhiteLetter(sTmp);
            i = getIndexOfSeparate(sTmp, 0);
            if (i < 0)
                sReturn = sTmp;
            else
                sReturn = sTmp.substring(0, i);
            break;
        case 4:
            if (i < 0)
                return null;
            sTmp = sTmp.substring(i + 1);
            sTmp = replaceWhiteLetter(sTmp);
            i = getIndexOfSeparate(sTmp, 0);
            if (i < 0)
                return null;
            sTmp = sTmp.substring(i + 1);
            sTmp = replaceWhiteLetter(sTmp);
            i = getIndexOfSeparate(sTmp, 0);
            if (i < 0)
                return null;
            sTmp = sTmp.substring(i + 1);
            sTmp = replaceWhiteLetter(sTmp);
            i = getIndexOfSeparate(sTmp, 0);
            if (i < 0)
                sReturn = sTmp.toUpperCase();
            else
                sReturn = sTmp.substring(0, i).toUpperCase();
            break;
        }
        return sReturn;
    }

    public static String getObjectString(String msg, int index) {
        String sReturn = "";
        String sTmp = replaceWhiteLetter(msg);
        int i = getIndexOfSeparate(sTmp, 0);
        if (i < 0)
            return null;
        sTmp = sTmp.substring(i + 1);
        sTmp = replaceWhiteLetter(sTmp);
        i = getIndexOfSeparate(sTmp, 0);
        switch (index) {
        case 1:
            if (i < 0)
                sReturn = sTmp;
            else
                sReturn = sTmp.substring(0, i);
            break;
        case 2:
            if (i < 0)
                return null;
            sTmp = sTmp.substring(i + 1);
            sTmp = replaceWhiteLetter(sTmp);
            i = getIndexOfSeparate(sTmp, 0);
            if (i < 0)
                sReturn = sTmp;
            else
                sReturn = sTmp.substring(0, i);
            break;
        case 3:
            if (i < 0)
                return null;
            sTmp = sTmp.substring(i + 1);
            sTmp = replaceWhiteLetter(sTmp);
            i = getIndexOfSeparate(sTmp, 0);
            if (i < 0)
                return null;
            sTmp = sTmp.substring(i + 1);
            sTmp = replaceWhiteLetter(sTmp);
            i = getIndexOfSeparate(sTmp, 0);
            if (i < 0)
                sReturn = sTmp;
            else
                sReturn = sTmp.substring(0, i);
            break;
        case 4:
            if (i < 0)
                return null;
            sTmp = sTmp.substring(i + 1);
            sTmp = replaceWhiteLetter(sTmp);
            i = getIndexOfSeparate(sTmp, 0);
            if (i < 0)
                return null;
            sTmp = sTmp.substring(i + 1);
            sTmp = replaceWhiteLetter(sTmp);
            i = getIndexOfSeparate(sTmp, 0);
            if (i < 0)
                return null;
            sTmp = sTmp.substring(i + 1);
            sTmp = replaceWhiteLetter(sTmp);
            i = getIndexOfSeparate(sTmp, 0);
            if (i < 0)
//          sReturn = sTmp.toUpperCase();
                sReturn = sTmp;
            else
//          sReturn = sTmp.substring(0, i).toUpperCase();
                sReturn = sTmp.substring(0, i);
            break;
        }
        return sReturn;
    }

    public static String getContentNoWhiteLetter1(String sInput) {
        String sReturn = "";
        char[] charr = new char[sInput.length()];
        int i = 0;
        while (i < sInput.length()) {
            char ch = sInput.charAt(i);
            System.out.println("ch: " + ch);
            if ((ch >= '0' && ch <= '9') ||
                (ch >= 'A' && ch <= 'Z') ||
                (ch >= 'a' && ch <= 'z')) {
                sReturn = sReturn + String.valueOf(ch);
            }
            i++;
        }
        return sReturn;
    }

    public static String getContentNoWhiteLetter(String sInput) {
        String sReturn = "";
        char[] charr = new char[sInput.length()];
        int i = 0;
        while (i < sInput.length()) {
            char ch = sInput.charAt(i);
            //System.out.println("ch: " + ch);
            if (ch != ' ') {
                sReturn = sReturn + String.valueOf(ch);
            }
            i++;
        }
        return sReturn;
    }

    public static String getContentFormatLottery(String sInput) { //For
        String sReturn = "";
        char[] charr = new char[sInput.length()];
        int i = 0;
        while (i < sInput.length()) {
            char ch = sInput.charAt(i);
            //System.out.println("ch: " + ch);
            if ((ch == 'D') || (ch == 'B') || (ch == 'M') || (ch == '.') || ((ch >= '0') && (ch <= '9'))) {
                sReturn = sReturn + String.valueOf(ch);
            }
            i++;
        }
        return sReturn;
    }

    public static String splitMsg(String info) {
        String infoTmp = info;
        String content = "";
        int position = 0;

        while (infoTmp.length() >= 160) {
            position = infoTmp.substring(0, 160).lastIndexOf(" ");
            if (content.equals("")) {
                content = infoTmp.substring(0, position);
            } else {
                content = content + "!xxx!" + infoTmp.substring(0, position);
            }
            infoTmp = info.substring(position + 1, info.length());
            info = info.substring(position + 1, info.length());
            //System.out.println(info);
        }
        content = content + "!xxx!" + info;
        return content;
    }

    public static void main(String[] args) {
        StringTool tool = new StringTool();
        String test = "._c.sd  fsf&  ";

        System.out.println(tool.getObjectString("MB 9/10",2));
    }
}
