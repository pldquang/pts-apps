package com.sms.common;

/**
 * <p>Title: SMPP Gateway</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Home</p>
 *
 * @author not attributable
 * @version 1.0
 */
import java.io.IOException;

public class FileCopyException extends IOException{
  public FileCopyException(String msg) {
    super(msg);
  }
}
