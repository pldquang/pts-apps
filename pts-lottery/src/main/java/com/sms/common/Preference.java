package com.sms.common;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import java.util.*;
import java.io.*;

import com.sms.item.LotteryResult;

public class Preference {
    private static Properties properties = new Properties();

    public static String db_driver_remote = "oracle.jdbc.driver.OracleDriver";
    public static String db_url_remote = "jdbc:oracle:thin:@localhost:1521:orcl";
    public static String db_user_remote = "dbremote";
    public static String db_password_remote = "dbremote13%&";
    public static int pooldb_max_remote = 3;

    public static String db_driver_content = "oracle.jdbc.driver.OracleDriver";
    public static String db_url_content = "jdbc:oracle:thin:@localhost:1521:orcl";
    public static String db_user_content = "dbcontent";
    public static String db_password_content = "dbcontent13%&";
    public static int pooldb_max_content = 3;

    public static String helpStr80 = "Dang ky nhan ket qua XoSo,soan: XS<Tinh/Tp> Vidu: XSMB lay kqua Mien Bac hay XSHCM lay kqua TP.HCM.Gui den 8184(lay kq ngay).Gui den 8784 (30 ngay)";
    public static String wrongformat80 = "Tin nhan sai!Dang ky nhan ket qua XoSo,soan: XS<Tinh/Tp> Vidu: XSMB lay kqua Mien Bac hay XSHCM lay kqua TP.HCM.Gui den 8184(lay kq ngay).Gui den 8784 (30 ngay)";

    public static String checkForUpdateStr = "";

    public static Vector listAdmin = new Vector();
    public static Vector listAdminLive = new Vector();
    public static Hashtable lotteryResult = new Hashtable();


    public static Vector commnandCodeLottery = new Vector();
    public static Vector commnandCodeSOICAU = new Vector();
    public static Vector cmdForUpdateResult = new Vector();

    public static int waitingHour = 19;
    public static int waitingMinuteStart = 0;
    public static int waitingMinuteEnd = 45;

    public static int nXS85 = 8;
    public static int nXS87 = 20;

    public static int nXSMT85 = 2;
    public static int nXSMT87 = 7;

    public static int nXSMN85 = 3;
    public static int nXSMN87 = 10;


    public Preference() {
    }

    public static void loadProperties(String fileName) throws IOException {
        System.out.println("Reading configuration file " + fileName + "...");
        FileInputStream propsFile = new FileInputStream(fileName);
        properties.load(propsFile);
        propsFile.close();
        System.out.println("Setting default parameters...");

        db_driver_remote = properties.getProperty("db_driver_remote", db_driver_remote);
        db_url_remote = properties.getProperty("db_url_remote", db_url_remote);
        db_user_remote = properties.getProperty("db_user_remote", db_user_remote);
        db_password_remote = properties.getProperty("db_password_remote", db_password_remote);
        pooldb_max_remote = getIntProperty("pooldb_max_remote", pooldb_max_remote);

        db_driver_content = properties.getProperty("db_driver_content", db_driver_content);
        db_url_content = properties.getProperty("db_url_content", db_url_content);
        db_user_content = properties.getProperty("db_user_content", db_user_content);
        db_password_content = properties.getProperty("db_password_content", db_password_content);
        pooldb_max_content = getIntProperty("pooldb_max_content", pooldb_max_content);


        nXS87 = getIntProperty("number_xs_87",nXS87);
        helpStr80 = properties.getProperty("help80",helpStr80);
        wrongformat80 = properties.getProperty("wrongformat80",wrongformat80);

        getListAdmin();
        getListAdminLive();
        getCommandCodeLottery();
        getCommandCodeSOICAU();
        getCommandCodeUpdate();
        loadnXS();


        //--- Get condition where clause
        for (int i = 0; i < listAdmin.size(); i++) {
            if(checkForUpdateStr.equals("")) {
                checkForUpdateStr =  "USER_ID = '" + listAdmin.elementAt(i) + "'";
            } else {
                checkForUpdateStr = checkForUpdateStr + " OR USER_ID = '" + listAdmin.elementAt(i) + "'";
            }
        }

        for (int i = 0; i < listAdminLive.size(); i++) {
            if(checkForUpdateStr.equals("")) {
                checkForUpdateStr =  "USER_ID = '" + listAdminLive.elementAt(i) + "'";
            } else {
                checkForUpdateStr = checkForUpdateStr + " OR USER_ID = '" + listAdminLive.elementAt(i) + "'";
            }
        }

        checkForUpdateStr = "( " + checkForUpdateStr + " ) AND ( ";
        for (int i = 0; i < cmdForUpdateResult.size(); i++) {
            if(i==0) {
                checkForUpdateStr =  checkForUpdateStr + " COMMAND_CODE = '" + cmdForUpdateResult.elementAt(i) + "'";
            } else {
                checkForUpdateStr = checkForUpdateStr + " OR COMMAND_CODE = '" + cmdForUpdateResult.elementAt(i) + "'";
            }
        }

        checkForUpdateStr  = checkForUpdateStr + " )";
    }

    static byte getByteProperty(String propName, byte defaultValue) {
        return Byte.parseByte(properties.getProperty(propName,
                Byte.toString(defaultValue)).trim());
    }

    static int getIntProperty(String propName, int defaultValue) {
        return Integer.parseInt(properties.getProperty(propName,
                Integer.toString(defaultValue)).trim());
    }


    public static void getListAdmin() {
        String keyStr = properties.getProperty("list_admin");
        StringTokenizer tokenizer = new StringTokenizer(keyStr, ",");
        while (tokenizer.hasMoreTokens()) {
            String key = tokenizer.nextToken();
            listAdmin.add(key);
        }
    }

    public static void getListAdminLive() {
        String keyStr = properties.getProperty("list_admin_live");
        StringTokenizer tokenizer = new StringTokenizer(keyStr, ",");
        while (tokenizer.hasMoreTokens()) {
            String key = tokenizer.nextToken();
            listAdminLive.add(key);
        }
    }

    public static void getCommandCodeLottery() {
        String keyStr = properties.getProperty("command_code_lottery");
        StringTokenizer tokenizer = new StringTokenizer(keyStr, ",");
        while (tokenizer.hasMoreTokens()) {
            String key = tokenizer.nextToken();
            commnandCodeLottery.add(key);
        }
        Utilities.bubbleSort(commnandCodeLottery);//DESC short
    }

    public static void getCommandCodeSOICAU() {
        String keyStr = properties.getProperty("command_code_soicau");
        StringTokenizer tokenizer = new StringTokenizer(keyStr, ",");
        while (tokenizer.hasMoreTokens()) {
            String key = tokenizer.nextToken();
            commnandCodeSOICAU.add(key);
        }
        Utilities.bubbleSort(commnandCodeSOICAU);//DESC short
    }

    public static void getCommandCodeUpdate() {
        String keyStr = properties.getProperty("list_cmd_update");
        StringTokenizer tokenizer = new StringTokenizer(keyStr, ",");
        while (tokenizer.hasMoreTokens()) {
            String key = tokenizer.nextToken();
            cmdForUpdateResult.add(key);
        }
        Utilities.bubbleSort(cmdForUpdateResult);//DESC short
    }

    static void loadnXS() {
        nXS85 = getIntProperty("number_xs_85",8);
        nXS87 = getIntProperty("number_xs_87",20);

        nXSMT85 = getIntProperty("number_xsmt_85",3);
        nXSMT87 = getIntProperty("number_xsmt_87",10);

        nXSMN85 = getIntProperty("number_xsmn_85",2);
        nXSMN87 = getIntProperty("number_xsmn_87",7);
    }

    public static void main(String[] args) {
    }


}
