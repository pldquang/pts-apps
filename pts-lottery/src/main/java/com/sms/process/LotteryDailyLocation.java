package com.sms.process;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
import com.sms.common.*;

public class LotteryDailyLocation extends Thread {

    DBTool dbTool = null;

    public LotteryDailyLocation() {
        dbTool = new DBTool();
    }

    public void processLotteryDailyLocation() {
        dbTool.processLotteryDailyLocation();
    }

    @Override
    public void run() {
        try {
            Thread.sleep(5 * 1000);
        } catch (Exception ex) {
        }

        MyLogger.log("Lottery Daily Location start!");

        while (Main.running) {
            try {
                int HH24 = DateProc.getCurrentHH24();
                processLotteryDailyLocation();
                if ((HH24 >= 16) && (HH24 <= 20)) {
                    Thread.sleep(1000);
                } else {
                    Thread.sleep(60 * 1000);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }//End while
    }
}
