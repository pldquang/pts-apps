package com.sms.process;

import com.sms.common.*;

public class LotteryAlert extends Thread {
  DBTool dbTool = null;
  public LotteryAlert() {
    dbTool = new DBTool();
  }

  public void processAlert() {
    dbTool.alertLotteryDaily();
  }

  public void run() {
    MyLogger.log("Lottery Alert Thread Start!");
    while (Main.running) {
      try {
        int HH24 = DateProc.getCurrentHH24();
        if (HH24 == 16) {
            processAlert();
            Thread.sleep(10*60*1000);
        } else {
          Thread.sleep(15*60*1000);
        }

      } catch (Exception ex) {
        ex.printStackTrace();
      }

    }

  }
}
