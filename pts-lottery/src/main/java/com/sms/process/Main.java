package com.sms.process;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import java.sql.*;
import java.util.*;
import java.math.BigDecimal;

import com.sms.common.Queue;
import com.sms.common.*;
import com.sms.item.*;
import java.io.*;

public class Main extends Thread {

    static Utilities util = null;
    DBTool dbTool = null;
    public static boolean running = false;
    static BufferedReader keyboard;
    public static Hashtable lotteryCompanyHT = null;
    public static Hashtable lotteryCompanyIDHT = null;
    public static Vector lotteryCommandCodeV = null;
    public static Vector lotteryCompanyIdV = null;
    public static Hashtable lotteryResultHT = null;
    public static boolean initResultLotteryOk = false;


    static {
        keyboard = new BufferedReader(new InputStreamReader(System.in));
    }

    public Main() {
        dbTool = new DBTool();
        util = new Utilities();
        lotteryCompanyHT = new Hashtable();
        lotteryCompanyIDHT = new Hashtable();
        lotteryCommandCodeV = new Vector();
        lotteryCompanyIdV = new Vector();
        lotteryResultHT = new Hashtable();
    }

    private void showMenu() {
        String option = "";
        try {
            option = keyboard.readLine();
            if ("Q".equals(option.toUpperCase())) {
                exit();
            }
        } catch (Exception e) {
            MyLogger.log(e.getMessage());
        }
    }

    private static void exit() {
        running = false;
        DBPool.releaseAllConnection();
        MyLogger.log("Stop.");
        System.exit(0);
    }

    @Override
    public void run() {
        while (running) {
            showMenu();
        }
    }

    public void loadLotteryInfo() {
        dbTool.getLotteryCompanyInfo();
        for (int i = 0; i < lotteryCompanyIdV.size(); i++) {
            BigDecimal companyId = (BigDecimal) lotteryCompanyIdV.elementAt(i);
            LotteryResult lotteryResult = new LotteryResult();
            lotteryResultHT.put(companyId, lotteryResult);
        }
        MyLogger.log("Loading Lottery Company Info success!");
    }

    public static void main(String[] args) {
        Main process = new Main();

        try {
            Preference.loadProperties("cfg.properties");

            (new DBPool()).initConnect2DBRemote();
            (new DBPool()).initConnect2DBContent();

            process.loadLotteryInfo();

            running = true;

            (new LotteryResultCare()).start();
            process.start();

            (new LotteryDailyLocation()).start();

            (new LotteryCareGPC()).start();
            (new LotteryDailyCareGPC()).start();

            (new LotteryCareVIETEL()).start();
            (new LotteryDailyCareVIETEL()).start();

            (new LotteryCareVMS()).start();
            (new LotteryDailyCareVMS()).start();

            (new LotteryAlert()).start();

//            (new ProcessLO()).start();
            (new ProcessCAU()).start();

            (new ProcessKM()).start();

            (new CAUWait()).start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
