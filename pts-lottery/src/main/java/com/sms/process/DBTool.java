package com.sms.process;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import java.util.*;
import java.sql.*;
import java.math.BigDecimal;

import com.sms.common.*;
import com.sms.item.*;
import com.sms.process.Main;

public class DBTool {

    private static char enter = (char) 10;
    private static String prefixAD = "Soan XS gui8584 de duoc TTTT 8 giai.De duoc tu van Soi Cau chinh xac nhat,soan: CAU gui8584." + enter;
    private static String endAD = enter + "Dang quay tiep..";
    private static String prefixG2 = "De biet cap so ban<KET>va 10 cap so ve nhieu nhat,it nhat 30 ngay gan day.Soan SOI<soKET>gui8384.VD SOI 68 gui8384" + enter;

//    Connection conn = null;
//    PreparedStatement pstmt = null;
//    ResultSet rs = null;
//    String sqlStr = "";
    public DBTool() {
    }

    public void freeResouceContent(PreparedStatement pstmt, ResultSet rs,
            Connection conn) {
        try {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if ((conn != null) && (!conn.getAutoCommit())) {
                conn.setAutoCommit(true);
            }
            if (conn != null) {
                DBPool.putConnection2DBContent(conn);
            }
        } catch (Exception ex) {
        }
    }

    public void freeResouceRemote(PreparedStatement pstmt, ResultSet rs,
            Connection conn) {
        try {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if ((conn != null) && (!conn.getAutoCommit())) {
                conn.setAutoCommit(true);
            }
            if (conn != null) {
                DBPool.putConnection2DBRemote(conn);
            }
        } catch (Exception ex) {
        }
    }

    protected BigDecimal nextSequenceGW(String name) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection conn = null;
        String sqlStr = null;
        BigDecimal nextId = null;
        try {
            if ((conn = DBPool.getConnectionRemote()) != null) {
                if (conn != null) {
                    sqlStr = "SELECT " + name + ".NEXTVAL FROM DUAL";
                    pstmt = conn.prepareStatement(sqlStr);
                    rs = pstmt.executeQuery();
                    if (rs.next()) {
                        nextId = rs.getBigDecimal(1);
                    }
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        } finally {
            freeResouceRemote(pstmt, rs, conn);
            return nextId;
        }
    }

    protected BigDecimal nextSequence(String name) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection conn = null;
        String sqlStr = null;
        BigDecimal nextId = null;
        try {
            conn = DBPool.getConnectionContent();
            if (conn != null) {
                sqlStr = "SELECT " + name + ".NEXTVAL FROM DUAL";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    nextId = rs.getBigDecimal(1);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return nextId;
        }
    }

    public boolean updateSubCode(String mobileOperator, BigDecimal id, String subCode1, String subCode2, String subCode3) {
        boolean b = false;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            if ((conn = DBPool.getConnectionRemote()) != null) {
                conn.setAutoCommit(false);
                sqlStr = "UPDATE " + mobileOperator + ".SMS_RECEIVE_QUEUE SET SUBCODE1 = ?, SUBCODE2 = ?, SUBCODE3 = ? WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, subCode1);
                pstmt.setString(2, subCode2);
                pstmt.setString(3, subCode3);
                pstmt.setBigDecimal(4, id);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceRemote(pstmt, null, conn);
            return b;
        }
    }

//    public boolean removeSMSReceiveQueue(String mobileOperator, BigDecimal id, String subCode1, String subCode2, String subCode3) {
//        boolean b = false;
//        Connection conn = null;
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        String sqlStr = "";
//        try {
//            if (updateSubCode(mobileOperator, id, subCode1, subCode2, subCode3)) {
//                if ((conn = DBPool.getConnectionRemote()) != null) {
//                    conn.setAutoCommit(false);
//                    sqlStr = "DELETE FROM " + mobileOperator + ".SMS_RECEIVE_QUEUE WHERE ID = ?";
//                    pstmt = conn.prepareStatement(sqlStr);
//                    pstmt.setBigDecimal(1, id);
//                    if (pstmt.executeUpdate() != -1) {
//                        conn.commit();
//                        b = true;
//                    }
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        } finally {
//            freeResouceRemote(pstmt, null, conn);
//            return b;
//        }
//    }
    public boolean removeSMSReceiveQueue(String mobileOperator, BigDecimal id) {
        boolean b = false;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            if ((conn = DBPool.getConnectionRemote()) != null) {
                conn.setAutoCommit(false);
                sqlStr = "DELETE FROM " + mobileOperator + ".SMS_RECEIVE_QUEUE WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, id);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceRemote(pstmt, null, conn);
            return b;
        }
    }

    public boolean sendMTEx(String userId, String serviceId,
            String commandCode,
            String mobileOperator,
            int contentType, String text,
            int messageType, BigDecimal requestId,
            String subCode1, String subCode2, String subCode3) {
        BigDecimal id = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
//        if (text.length() > 160)
//            text = text.substring(0, 160);
        String sqlStr = "";
        boolean b = false;
        try {
            boolean check = false;
            if (messageType == 1) {
                if (removeSMSReceiveQueue(mobileOperator, requestId)) {
                    check = true;
                }
            } else {
                check = true;
            }

            if (check) {
                id = nextSequenceGW(mobileOperator + ".SEQ_SMS_SEND_QUEUE_ID");
                if ((conn = DBPool.getConnectionRemote()) != null) {
                    conn.setAutoCommit(false);
                    if (subCode1.length() > 15) {
                        subCode1 = subCode1.substring(0, 15);
                    }
                    if (subCode2.length() > 15) {
                        subCode2 = subCode2.substring(0, 15);
                    }
                    if (subCode3.length() > 15) {
                        subCode3 = subCode3.substring(0, 15);

//                    if(mobileOperator.equalsIgnoreCase("VIETEL")) {
//                        if(text.indexOf("1900561577") != -1)
//                            text = text.replaceAll("1900561577","19008088");
//                    }
                    }
                    sqlStr = "INSERT INTO " + mobileOperator +
                            ".SMS_SEND_QUEUE (ID, USER_ID, SERVICE_ID, MOBILE_OPERATOR, COMMAND_CODE, CONTENT_TYPE, INFO, MESSAGE_TYPE, REQUEST_ID,SUBCODE1,SUBCODE2,SUBCODE3) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    pstmt = conn.prepareStatement(sqlStr);
                    pstmt.setBigDecimal(1, id);
                    pstmt.setString(2, userId);
                    pstmt.setString(3, serviceId);
                    pstmt.setString(4, mobileOperator);
                    pstmt.setString(5, commandCode);
                    pstmt.setInt(6, contentType);
                    pstmt.setString(7, text);
                    pstmt.setInt(8, messageType);
                    pstmt.setBigDecimal(9, requestId);
                    pstmt.setString(10, subCode1);
                    pstmt.setString(11, subCode2);
                    pstmt.setString(12, subCode3);
                    if (pstmt.executeUpdate() != -1) {
                        conn.commit();
                        b = true;
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            conn.rollback();
        } finally {
            freeResouceRemote(pstmt, null, conn);
            return b;
        }
    }

    /*
    public boolean sendMTEx(String userId, String serviceId,
    String commandCode,
    String mobileOperator,
    int contentType, String text,
    int messageType, BigDecimal requestId,
    String subCode1,String subCode2,String subCode3) {
    BigDecimal id = null;
    Connection conn = null;
    PreparedStatement pstmt = null;
    if (text.length() > 160)
    text = text.substring(0, 160);
    String sqlStr = "";
    boolean b = false;
    try {
    boolean check = false;
    if (messageType == 1) {
    if (removeSMSReceiveQueue(mobileOperator, requestId)) {
    check = true;
    }
    } else {
    check = true;
    }
    
    if (check) {
    id = nextSequenceGW(mobileOperator + ".SEQ_SMS_SEND_QUEUE_ID");
    if ((conn = DBPool.getConnectionRemote()) != null) {
    conn.setAutoCommit(false);
    sqlStr = "INSERT INTO " + mobileOperator +
    ".SMS_SEND_QUEUE (ID, USER_ID, SERVICE_ID, MOBILE_OPERATOR, COMMAND_CODE, CONTENT_TYPE, INFO, MESSAGE_TYPE, REQUEST_ID,SUBCODE1,SUBCODE2,SUBCODE3) " +
    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    
    if(mobileOperator.equalsIgnoreCase("VIETEL")) {
    if(text.indexOf("1900561577") != -1)
    text = text.replaceAll("1900561577","19008088");
    }
    
    pstmt = conn.prepareStatement(sqlStr);
    pstmt.setBigDecimal(1, id);
    pstmt.setString(2, userId);
    pstmt.setString(3, serviceId);
    pstmt.setString(4, mobileOperator);
    pstmt.setString(5, commandCode);
    pstmt.setInt(6, contentType);
    pstmt.setString(7, text);
    pstmt.setInt(8, messageType);
    pstmt.setBigDecimal(9, requestId);
    pstmt.setString(10,subCode1);
    pstmt.setString(11,subCode2);
    pstmt.setString(12,subCode3);
    if (pstmt.executeUpdate() != -1) {
    conn.commit();
    b = true;
    }
    }
    }
    } catch (Exception ex) {
    ex.printStackTrace();
    conn.rollback();
    } finally {
    freeResouceRemote(pstmt, null, conn);
    return b;
    }
    }
     */
//    public boolean sendMT(String userId, String serviceId,
//                          String commandCode,
//                          String mobileOperator,
//                          int contentType, String text,
//                          int messageType, BigDecimal requestId) {
//        BigDecimal id = null;
//        Connection conn = null;
//        PreparedStatement pstmt = null;
//        if (text.length() > 160)
//            text = text.substring(0, 160);
//        String sqlStr = "";
//        boolean b = false;
//        try {
//            boolean check = false;
//            if (messageType == 1) {
//                if (removeSMSReceiveQueue(mobileOperator, requestId)) {
//                    check = true;
//                }
//            } else {
//                check = true;
//            }
//
//            if (check) {
//                id = nextSequenceGW(mobileOperator + ".SEQ_SMS_SEND_QUEUE_ID");
//                if ((conn = DBPool.getConnectionRemote()) != null) {
//                    conn.setAutoCommit(false);
//                    sqlStr = "INSERT INTO " + mobileOperator +
//                             ".SMS_SEND_QUEUE (ID, USER_ID, SERVICE_ID, MOBILE_OPERATOR, COMMAND_CODE, CONTENT_TYPE, INFO, MESSAGE_TYPE, REQUEST_ID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
//                    pstmt = conn.prepareStatement(sqlStr);
//                    pstmt.setBigDecimal(1, id);
//                    pstmt.setString(2, userId);
//                    pstmt.setString(3, serviceId);
//                    pstmt.setString(4, mobileOperator);
//                    pstmt.setString(5, commandCode);
//                    pstmt.setInt(6, contentType);
//                    pstmt.setString(7, text);
//                    pstmt.setInt(8, messageType);
//                    pstmt.setBigDecimal(9, requestId);
//                    if (pstmt.executeUpdate() != -1) {
//                        conn.commit();
//                        b = true;
//                    }
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            conn.rollback();
//        } finally {
//            freeResouceRemote(pstmt, null, conn);
//            return b;
//        }
//    }
    public void getLotteryCompanyInfo() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            if ((conn = DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT ID,CODE,COMPANY_NAME,COMPANY_DESC FROM LOTTERY_COMPANY ORDER BY LENGTH(CODE) DESC";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    BigDecimal id = rs.getBigDecimal(1);
                    String code = rs.getString(2);
                    String name = rs.getString(3);
                    String desc = rs.getString(4);

                    LotteryCompany lotteryCompany = new LotteryCompany();
                    lotteryCompany.setCompanyId(id);
                    lotteryCompany.setCode(code);
                    lotteryCompany.setName(name);
                    lotteryCompany.setCompanyDesc(desc);

                    Main.lotteryCompanyHT.put(code, lotteryCompany);
                    Main.lotteryCompanyIDHT.put(id, lotteryCompany);

                    Main.lotteryCommandCodeV.add(code);
                    if (!Main.lotteryCompanyIdV.contains(id)) {
                        MyLogger.log("getLotteryCompanyInfo: id = " + id);
                        Main.lotteryCompanyIdV.add(id);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
        }
    }

    public Vector getLastestLotteryResult() { //day is DD/MM/YY

        Vector v = new Vector();
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            if ((conn = DBPool.getConnectionContent()) != null) {
                for (int i = 0; i < Main.lotteryCompanyIdV.size(); i++) {
                    BigDecimal companyId = (BigDecimal) Main.lotteryCompanyIdV.elementAt(i);
                    sqlStr = "SELECT RESULT,TO_CHAR(RESULT_DATE,'DD/MM/YY'),RESULT_STATUS FROM LOTTERY_RESULT WHERE COMPANY_ID = ? AND RESULT_DATE = (SELECT MAX(RESULT_DATE) FROM LOTTERY_RESULT WHERE COMPANY_ID = ?)";
                    pstmt = conn.prepareStatement(sqlStr);
                    pstmt.setBigDecimal(1, companyId);
                    pstmt.setBigDecimal(2, companyId);
                    rs = pstmt.executeQuery();
                    if (rs.next()) {
                        LotteryResult lotteryResult = new LotteryResult();
                        lotteryResult.setCompanyId(companyId);
                        lotteryResult.setResult(rs.getString(1));
                        lotteryResult.setResultDate(rs.getString(2));
                        lotteryResult.setResultStatus(rs.getInt(3));
                        v.add(lotteryResult);
                    }
                    freeResouceContent(pstmt, rs, null);
                } //End for

            } //End if(conn != null)

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(null, null, conn);
            return v;
        }
    }

    public LotteryResult getLastestLotteryResult(BigDecimal companyId) { //day is DD/MM/YY

        LotteryResult lotteryResult = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            conn = DBPool.getConnectionContent();
            if (conn != null) {
                sqlStr = "SELECT RESULT,TO_CHAR(RESULT_DATE,'DD/MM/YY'),RESULT_STATUS FROM LOTTERY_RESULT WHERE COMPANY_ID = ? AND RESULT_DATE = (SELECT MAX(RESULT_DATE) FROM LOTTERY_RESULT WHERE COMPANY_ID = ?)";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, companyId);
                pstmt.setBigDecimal(2, companyId);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    lotteryResult = new LotteryResult();
                    lotteryResult.setCompanyId(companyId);
                    lotteryResult.setResult(rs.getString(1));
                    lotteryResult.setResultDate(rs.getString(2));
                    lotteryResult.setResultStatus(rs.getInt(3));
                }
//                freeResouceContent(pstmt, rs, conn);
            } //End if(conn != null)

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return lotteryResult;
        }
    }

    public LotteryResult getLotteryResultToday(BigDecimal companyId) { //day is DD/MM/YY

        LotteryResult lotteryResult = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            conn = DBPool.getConnectionContent();
            if (conn != null) {
                sqlStr = "SELECT RESULT,TO_CHAR(RESULT_DATE,'DD/MM/YY'),RESULT_STATUS FROM LOTTERY_RESULT WHERE COMPANY_ID = ? AND TO_CHAR(RESULT_DATE,'DD/MM/YY') = TO_CHAR(SYSDATE,'DD/MM/YY')";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, companyId);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    lotteryResult = new LotteryResult();
                    lotteryResult.setCompanyId(companyId);
                    lotteryResult.setResult(rs.getString(1));
                    lotteryResult.setResultDate(rs.getString(2));
                    lotteryResult.setResultStatus(rs.getInt(3));
                }
//                freeResouceContent(pstmt, rs, conn);
            } //End if(conn != null)

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return lotteryResult;
        }
    }

    public void updateLottery(String mobileOperator) {
        String subCode1 = "";
        String subCode2 = "";
        String subCode3 = "";
        try {
            Vector v = getRequestUpdateLottery(mobileOperator);
            for (int i = 0; i < v.size(); i++) {
                SmsReceiveQueue smsReceiveQueue = (SmsReceiveQueue) v.elementAt(i);

                BigDecimal id = smsReceiveQueue.getId();
                String userId = smsReceiveQueue.getUserId();
                String serviceId = smsReceiveQueue.getServiceId();
                String info = smsReceiveQueue.getInfo().replaceAll("\r\n", "\n").trim();
                String commandCode = smsReceiveQueue.getCommandCode();

                if (Preference.listAdmin.contains(userId) || Preference.listAdminLive.contains(userId)) {
                    LotteryCompany lotteryCompany = (LotteryCompany) Main.lotteryCompanyHT.get(commandCode);
                    System.out.println("DateProc.getCurrentHH24() = " + DateProc.getCurrentHH24());
                    if ((lotteryCompany != null) && (DateProc.getCurrentHH24() >= 16) && (DateProc.getCurrentHH24() <= 21)) {
                        if (removeSMSReceiveQueue(mobileOperator, id)) {
                            BigDecimal companyId = lotteryCompany.getCompanyId();
                            subCode1 = String.valueOf(companyId);
                            String name = lotteryCompany.getName();
                            String infoTmp = StringTool.replaceWhiteLetter(info.substring(commandCode.length(), info.length()));
                            infoTmp = name + " " + infoTmp;

                            /*
                            if ((companyId.intValue() >= 4) && (companyId.intValue() <= 17)) {
                            //--> MT
                            infoTmp = infoTmp + "\nXSMT gui8784";
                            } else if ((companyId.intValue() >= 19) && (companyId.intValue() <= 39)) {
                            //--> MN
                            infoTmp = infoTmp + "\nXSMN gui8784";
                            }
                             */

                            infoTmp = infoTmp + enter + "CAU " + lotteryCompany.getCode() + " gui8584";

                            String result = "";
                            if (infoTmp.length() <= 160) {
                                result = infoTmp;
                            } else {
                                result = info;
                            }
                            String alert = "";
                            if (companyId.intValue() != 1) {
                                alert = "Update " + name + " OK !";
                                LotteryResult lotteryResult = new LotteryResult();
                                lotteryResult.setCompanyId(companyId);
                                lotteryResult.setResult(result);
                                if (updateLotteryResult(lotteryResult)) {
                                    sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                            0, alert, 1, id, subCode1, subCode2, subCode3);
                                }
                            }

                            if (companyId.intValue() == 1) {
                                if (!Preference.listAdminLive.contains(userId)) {
                                    LotteryResult lotteryResultToday = getLotteryResultToday(companyId);
                                    if (lotteryResultToday != null) {
                                        if (lotteryResultToday.getResult().equals(info)) {
                                            alert = "He thong da ton tai noi dung giong nhu ban vua gui Update noi dung MB.";
                                            sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                                    0, alert, 1, id, subCode1, subCode2, subCode3);
                                            MyLogger.log(alert);
                                        } else {
                                            String infoTmpCheck = info.toUpperCase();
                                            if (infoTmpCheck.indexOf("DB") != -1) {
                                                //Update result MB full
                                                LotteryResult lotteryResult = new LotteryResult();
                                                lotteryResult.setCompanyId(companyId);
                                                lotteryResult.setResult(info);
                                                lotteryResult.setResultStatus(0);
                                                if (updateLotteryResult(lotteryResult)) {
                                                    alert = "Ban vua cap nhat ket qua MB Full.";
                                                    sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                                            0, alert, 1, id, subCode1, subCode2, subCode3);
                                                    MyLogger.log("Update XSMB FULL !!!");
                                                } else {
                                                    alert = "Cap nhat ket qua MB Full co loi. ERROR !!!,de nghi gui lai hoac goi dien toi so 0904185847.";
                                                    sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                                            0, alert, 1, id, subCode1, subCode2, subCode3);
                                                    MyLogger.log("Update XSMB FULL ERROR !!!");
                                                }
                                            } else if ((infoTmpCheck.indexOf("DB") == -1) && (lotteryResultToday.getResultStatus() == 0)) {
                                                alert = "Ban khong the cap nhat vi trong he thong da co kq Full !!";
                                                sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                                        0, alert, 1, id, subCode1, subCode2, subCode3);
                                                MyLogger.log("Can't update the cause: The last result is full");
                                            } else {
                                                int numOfUpdate = lotteryResultToday.getResultStatus() + 1;
                                                LotteryResult lotteryResult = new LotteryResult();
                                                lotteryResult.setCompanyId(companyId);
                                                lotteryResult.setResult(info);
                                                lotteryResult.setResultStatus(numOfUpdate);
                                                if (updateLotteryResult(lotteryResult)) {
                                                    alert = "Ban vua cap nhat ket qua MB lan thu " + String.valueOf(numOfUpdate);
                                                    sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                                            0, alert, 1, id, subCode1, subCode2, subCode3);
                                                    MyLogger.log("Update XSMB lan thu: " + String.valueOf(numOfUpdate));
                                                } else {
                                                    alert = "Ban vua cap nhat ket qua MB lan thu " + String.valueOf(numOfUpdate) +
                                                            " co loi. ERROR !!!,de nghi gui lai hoac goi dien toi so 0904185847.";
                                                    sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                                            0, alert, 1, id, subCode1, subCode2, subCode3);
                                                    MyLogger.log("Update XSMB lan thu " + String.valueOf(numOfUpdate) + " ERROR !!!");
                                                }
                                            }
                                        }
                                    } else {
                                        //Chua co kq trong csdl, insert new row
                                        LotteryResult lotteryResult = new LotteryResult();
                                        lotteryResult.setCompanyId(companyId); // is 1

                                        lotteryResult.setResult(info);
                                        String infoTmpCheck = info.toUpperCase();
                                        if (infoTmpCheck.indexOf("DB") == -1) {
                                            lotteryResult.setResultStatus(1); //Default in the first update not full

                                            alert = "Ban vua cap nhat XSMB lan thu 1";
                                        } else {
                                            lotteryResult.setResultStatus(0); //Result lottery is full

                                            alert = "Ban vua cap nhat XSMB voi FULL cac giai.";
                                        }
                                        if (updateLotteryResult(lotteryResult)) {
                                            sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                                    0, alert, 1, id, subCode1, subCode2, subCode3);
                                            MyLogger.log(alert);
                                        }
                                    }
                                } else { //For TangBatHo

                                    LotteryResult lotteryResultToday = getLotteryResultToday(companyId);
                                    String resultRequireUpdate = Utilities.getXSMB(info.substring(2));
                                    if ((resultRequireUpdate != null) && (!resultRequireUpdate.trim().equals(""))) {
                                        if (lotteryResultToday != null) {
                                            if (lotteryResultToday.getResult().equals(resultRequireUpdate)) {
                                                alert = "He thong da ton tai noi dung giong nhu ban vua gui Update noi dung MB.";
                                                sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                                        0, alert, 1, id, subCode1, subCode2, subCode3);
                                                MyLogger.log(alert);
                                            } else {
                                                String infoTmpCheck = resultRequireUpdate.toUpperCase();
                                                if (infoTmpCheck.indexOf("DB") != -1) {
                                                    //Update result MB full
                                                    LotteryResult lotteryResult = new LotteryResult();
                                                    lotteryResult.setCompanyId(companyId);
                                                    lotteryResult.setResult(resultRequireUpdate);
                                                    lotteryResult.setResultStatus(0);
                                                    if (updateLotteryResult(lotteryResult)) {
                                                        alert = "Ban vua cap nhat ket qua MB Full.";
                                                        sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                                                0, alert, 1, id, subCode1, subCode2, subCode3);
                                                        MyLogger.log("Update XSMB FULL !!!");
                                                    } else {
                                                        alert = "Cap nhat ket qua MB Full co loi. ERROR !!!,de nghi gui lai hoac goi dien toi so 01697221652.";
                                                        sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                                                0, alert, 1, id, subCode1, subCode2, subCode3);
                                                        MyLogger.log("Update XSMB FULL ERROR !!!");
                                                    }
                                                } else if ((infoTmpCheck.indexOf("DB") == -1) && (lotteryResultToday.getResultStatus() == 0)) {
                                                    alert = "Ban khong the cap nhat vi trong he thong da co kq Full !!";
                                                    sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                                            0, alert, 1, id, subCode1, subCode2, subCode3);
                                                    MyLogger.log("Can't update the cause: The last result is full");
                                                } else {
                                                    resultRequireUpdate = resultRequireUpdate + "\nConTiep..";
                                                    prefixG2 = getTXTData_1("XS", "G2") + enter;
                                                    if ((prefixG2 + resultRequireUpdate).length() <= 160) {
                                                        resultRequireUpdate = prefixG2 + resultRequireUpdate;
//                                                        hungdt
                                                    }
                                                    int numOfUpdate = lotteryResultToday.getResultStatus() + 1;
                                                    LotteryResult lotteryResult = new LotteryResult();
                                                    lotteryResult.setCompanyId(companyId);
                                                    lotteryResult.setResult(resultRequireUpdate);
                                                    lotteryResult.setResultStatus(numOfUpdate);
                                                    if (updateLotteryResult(lotteryResult)) {
                                                        alert = "Ban vua cap nhat ket qua MB lan thu " + String.valueOf(numOfUpdate);
                                                        sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                                                0, alert, 1, id, subCode1, subCode2, subCode3);
                                                        MyLogger.log("Update XSMB lan thu: " + String.valueOf(numOfUpdate));
                                                    } else {
                                                        alert = "Ban vua cap nhat ket qua MB lan thu " + String.valueOf(numOfUpdate) +
                                                                " co loi.ERROR !!!,de nghi gui lai hoac goi dien toi so 01697221652.";
                                                        sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                                                0, alert, 1, id, subCode1, subCode2, subCode3);
                                                        MyLogger.log("Update XSMB lan thu " + String.valueOf(numOfUpdate) + " ERROR !!!");
                                                    }
                                                }
                                            }
                                        } else {
                                            //Chua co kq trong csdl, insert new row
                                            String resultRequireUpdateOrign = resultRequireUpdate;
                                            prefixAD = getTXTData_1("XS", "G1") + enter;
                                            if ((prefixAD + resultRequireUpdate + endAD).length() < 160) {
                                                resultRequireUpdate = prefixAD + resultRequireUpdate + endAD;
                                            }
//                                            if (!resultRequireUpdate.startsWith("Soan XS gui8584 de duoc TTTT 8 giai.")) {
//                                                if ((prefixG2 + resultRequireUpdate).length() < 160) {
//                                                    resultRequireUpdate = prefixG2 + resultRequireUpdate;
//                                                }
//                                            }
                                            LotteryResult lotteryResult = new LotteryResult();
                                            lotteryResult.setCompanyId(companyId); // is 1

                                            String infoTmpCheck = resultRequireUpdate.toUpperCase();
                                            if (infoTmpCheck.indexOf("DB") == -1) {
                                                lotteryResult.setResult(resultRequireUpdate);
                                                lotteryResult.setResultStatus(1); //Default in the first update not full

                                                alert = "Ban vua cap nhat XSMB lan thu 1";
                                            } else {
                                                lotteryResult.setResult(resultRequireUpdateOrign);
                                                lotteryResult.setResultStatus(0); //Result lottery is full

                                                alert = "Ban vua cap nhat XSMB voi FULL cac giai.";
                                            }
                                            if (updateLotteryResult(lotteryResult)) {
                                                sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                                        0, alert, 1, id, subCode1, subCode2, subCode3);
                                                MyLogger.log(alert);
                                            }
                                        }
                                    } else {
                                        alert = "Noi dung ket qua XS co loi.Cap nhat that bai !!";
                                        sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, id, subCode1, subCode2, subCode3);
                                        MyLogger.log(alert);
                                    }
                                }
                            } //End update XSMB

                        } //end if (removeSMSReceiveQueue(id))

                    } else { //end  if (lotteryCommandCode != null)

                        if (removeSMSReceiveQueue(mobileOperator, id)) {
                            String alert = "Ma nhap XOSO sai hoac thoi gian nhap khong hop le !!";
                            sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                    0, alert, 1, id, subCode1, subCode2, subCode3);
                            MyLogger.log("Update XS wrong Code or Invalid hour !!");
                        }
                    }
                } else {
                    if (removeSMSReceiveQueue(mobileOperator, id)) {
                        String alert = "Ban gui sai dau so dich vu,moi soan HELP gui8184 de biet cac dich vu cung cap boi 8x84" + enter +
                                "Cam on ban su dung dich vu!";
                        sendMTEx(userId, serviceId, commandCode, mobileOperator,
                                0, alert, 1, id, subCode1, subCode2, subCode3);
                        MyLogger.log("Not in list Admin FoneNumber");
                    }
                }
            } //End while

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
//            freeResouceRemote(pstmt, rs, conn);
        }
    }

//    public void updateLotteryEX(String mobileOperator) {
//        Connection conn = null;
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        String sqlStr = "";
//        try {
//
//            if ((conn = DBPool.getConnectionRemote()) != null) {
//                sqlStr = "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,INFO,COMMAND_CODE FROM " + mobileOperator +
//                         ".SMS_RECEIVE_QUEUE WHERE SERVICE_ID = '8084'";
//
//                pstmt = conn.prepareStatement(sqlStr);
//                rs = pstmt.executeQuery();
//                while (rs.next()) {
//                    BigDecimal id = rs.getBigDecimal(1);
//                    String userId = rs.getString(2);
//                    String serviceId = rs.getString(3);
//                    String info = rs.getString(5).replaceAll("\r\n", "\n").trim();
//                    String commandCode = rs.getString(6);
//                    if (Preference.listAdmin.contains(userId) || Preference.listAdminLive.contains(userId)) {
//                        LotteryCompany lotteryCompany = (LotteryCompany) Main.lotteryCompanyHT.get(commandCode);
//                        if ((lotteryCompany != null) && (DateProc.getCurrentHH24() >= 16) && (DateProc.getCurrentHH24() <= 21)) {
//                            if (removeSMSReceiveQueue(mobileOperator, id)) {
//                                BigDecimal companyId = lotteryCompany.getCompanyId();
//                                String name = lotteryCompany.getName();
//                                String infoTmp = StringTool.replaceWhiteLetter(info.substring(commandCode.length(), info.length()));
//                                infoTmp = name + " " + infoTmp;
//
//                                if ((companyId.intValue() >= 4) && (companyId.intValue() <= 17)) {
//                                    //--> MT
//                                    infoTmp = infoTmp + "\nXSMT gui 8384";
//                                } else if ((companyId.intValue() >= 19) && (companyId.intValue() <= 39)) {
//                                    //--> MN
//                                    infoTmp = infoTmp + "\nXSMN gui 8384";
//                                }
//                                String result = "";
//                                if (infoTmp.length() <= 160) {
//                                    result = infoTmp;
//                                } else {
//                                    result = info;
//                                }
//                                String alert = "";
//                                if (companyId.intValue() != 1) {
//                                    alert = "Update " + name + " OK !";
//                                    LotteryResult lotteryResult = new LotteryResult();
//                                    lotteryResult.setCompanyId(companyId);
//                                    lotteryResult.setResult(result);
//                                    if (updateLotteryResult(lotteryResult)) {
//                                        sendMT(userId, serviceId, commandCode, mobileOperator,
//                                               0, alert, 1, id);
//                                    }
//                                }
//
//                                if (companyId.intValue() == 1) {
//                                    if (!Preference.listAdminLive.contains(userId)) {
//                                        LotteryResult lotteryResultToday = getLotteryResultToday(companyId);
//                                        if (lotteryResultToday != null) {
//                                            if (lotteryResultToday.getResult().equals(info)) {
//                                                alert = "He thong da ton tai noi dung giong nhu ban vua gui Update noi dung MB.";
//                                                sendMT(userId, serviceId, commandCode, mobileOperator,
//                                                        0, alert, 1, id);
//                                                MyLogger.log(alert);
//                                            } else {
//                                                String infoTmpCheck = info.toUpperCase();
//                                                if (infoTmpCheck.indexOf("DB") != -1) {
//                                                    //Update result MB full
//                                                    LotteryResult lotteryResult = new LotteryResult();
//                                                    lotteryResult.setCompanyId(companyId);
//                                                    lotteryResult.setResult(info);
//                                                    lotteryResult.setResultStatus(0);
//                                                    if (updateLotteryResult(lotteryResult)) {
//                                                        alert = "Ban vua cap nhat ket qua MB Full.";
//                                                        sendMT(userId, serviceId, commandCode, mobileOperator,
//                                                                0, alert, 1, id);
//                                                        MyLogger.log("Update XSMB FULL !!!");
//                                                    } else {
//                                                        alert = "Cap nhat ket qua MB Full co loi. ERROR !!!,de nghi gui lai hoac goi dien toi so 0904185847.";
//                                                        sendMT(userId, serviceId, commandCode, mobileOperator,
//                                                                0, alert, 1, id);
//                                                        MyLogger.log("Update XSMB FULL ERROR !!!");
//                                                    }
//                                                } else if ((infoTmpCheck.indexOf("DB") == -1) && (lotteryResultToday.getResultStatus() == 0)) {
//                                                    alert = "Ban khong the cap nhat vi trong he thong da co kq Full !!";
//                                                    sendMT(userId, serviceId, commandCode, mobileOperator,
//                                                            0, alert, 1, id);
//                                                    MyLogger.log("Can't update the cause: The last result is full");
//                                                } else {
//                                                    int numOfUpdate = lotteryResultToday.getResultStatus() + 1;
//                                                    LotteryResult lotteryResult = new LotteryResult();
//                                                    lotteryResult.setCompanyId(companyId);
//                                                    lotteryResult.setResult(info);
//                                                    lotteryResult.setResultStatus(numOfUpdate);
//                                                    if (updateLotteryResult(lotteryResult)) {
//                                                        alert = "Ban vua cap nhat ket qua MB lan thu " + String.valueOf(numOfUpdate);
//                                                        sendMT(userId, serviceId, commandCode, mobileOperator,
//                                                                0, alert, 1, id);
//                                                        MyLogger.log("Update XSMB lan thu: " + String.valueOf(numOfUpdate));
//                                                    } else {
//                                                        alert = "Ban vua cap nhat ket qua MB lan thu " + String.valueOf(numOfUpdate) +
//                                                                " co loi. ERROR !!!,de nghi gui lai hoac goi dien toi so 0904185847.";
//                                                        sendMT(userId, serviceId, commandCode, mobileOperator,
//                                                                0, alert, 1, id);
//                                                        MyLogger.log("Update XSMB lan thu " + String.valueOf(numOfUpdate) + " ERROR !!!");
//                                                    }
//                                                }
//                                            }
//                                        } else {
//                                            //Chua co kq trong csdl, insert new row
//                                            LotteryResult lotteryResult = new LotteryResult();
//                                            lotteryResult.setCompanyId(companyId); // is 1
//                                            lotteryResult.setResult(info);
//                                            String infoTmpCheck = info.toUpperCase();
//                                            if (infoTmpCheck.indexOf("DB") == -1) {
//                                                lotteryResult.setResultStatus(1); //Default in the first update not full
//                                                alert = "Ban vua cap nhat XSMB lan thu 1";
//                                            } else {
//                                                lotteryResult.setResultStatus(0); //Result lottery is full
//                                                alert = "Ban vua cap nhat XSMB voi FULL cac giai.";
//                                            }
//                                            if (updateLotteryResult(lotteryResult)) {
//                                                sendMT(userId, serviceId, commandCode, mobileOperator,
//                                                        0, alert, 1, id);
//                                                MyLogger.log(alert);
//                                            }
//                                        }
//                                    } else { //For TangBatHo
//                                        LotteryResult lotteryResultToday = getLotteryResultToday(companyId);
//                                        String resultRequireUpdate = Utilities.getXSMB(info.substring(2));
//                                        if (lotteryResultToday != null) {
//                                            if (lotteryResultToday.getResult().equals(resultRequireUpdate)) {
//                                                alert = "He thong da ton tai noi dung giong nhu ban vua gui Update noi dung MB.";
//                                                sendMT(userId, serviceId, commandCode, mobileOperator,
//                                                        0, alert, 1, id);
//                                                MyLogger.log(alert);
//                                            } else {
//                                                String infoTmpCheck = resultRequireUpdate.toUpperCase();
//                                                if (infoTmpCheck.indexOf("DB") != -1) {
//                                                    //Update result MB full
//                                                    LotteryResult lotteryResult = new LotteryResult();
//                                                    lotteryResult.setCompanyId(companyId);
//                                                    lotteryResult.setResult(resultRequireUpdate);
//                                                    lotteryResult.setResultStatus(0);
//                                                    if (updateLotteryResult(lotteryResult)) {
//                                                        alert = "Ban vua cap nhat ket qua MB Full.";
//                                                        sendMT(userId, serviceId, commandCode, mobileOperator,
//                                                                0, alert, 1, id);
//                                                        MyLogger.log("Update XSMB FULL !!!");
//                                                    } else {
//                                                        alert = "Cap nhat ket qua MB Full co loi. ERROR !!!,de nghi gui lai hoac goi dien toi so 01697221652.";
//                                                        sendMT(userId, serviceId, commandCode, mobileOperator,
//                                                                0, alert, 1, id);
//                                                        MyLogger.log("Update XSMB FULL ERROR !!!");
//                                                    }
//                                                } else if ((infoTmpCheck.indexOf("DB") == -1) && (lotteryResultToday.getResultStatus() == 0)) {
//                                                    alert = "Ban khong the cap nhat vi trong he thong da co kq Full !!";
//                                                    sendMT(userId, serviceId, commandCode, mobileOperator,
//                                                            0, alert, 1, id);
//                                                    MyLogger.log("Can't update the cause: The last result is full");
//                                                } else {
//                                                    resultRequireUpdate = resultRequireUpdate + "\nConTiep..";
//                                                    int numOfUpdate = lotteryResultToday.getResultStatus() + 1;
//                                                    LotteryResult lotteryResult = new LotteryResult();
//                                                    lotteryResult.setCompanyId(companyId);
//                                                    lotteryResult.setResult(resultRequireUpdate);
//                                                    lotteryResult.setResultStatus(numOfUpdate);
//                                                    if (updateLotteryResult(lotteryResult)) {
//                                                        alert = "Ban vua cap nhat ket qua MB lan thu " + String.valueOf(numOfUpdate);
//                                                        sendMT(userId, serviceId, commandCode, mobileOperator,
//                                                                0, alert, 1, id);
//                                                        MyLogger.log("Update XSMB lan thu: " + String.valueOf(numOfUpdate));
//                                                    } else {
//                                                        alert = "Ban vua cap nhat ket qua MB lan thu " + String.valueOf(numOfUpdate) +
//                                                                " co loi.ERROR !!!,de nghi gui lai hoac goi dien toi so 01697221652.";
//                                                        sendMT(userId, serviceId, commandCode, mobileOperator,
//                                                                0, alert, 1, id);
//                                                        MyLogger.log("Update XSMB lan thu " + String.valueOf(numOfUpdate) + " ERROR !!!");
//                                                    }
//                                                }
//                                            }
//                                        } else {
//                                            //Chua co kq trong csdl, insert new row
//                                            String resultRequireUpdateOrign = resultRequireUpdate;
//                                            if ((prefixAD + resultRequireUpdate + endAD).length() < 160) {
//                                                resultRequireUpdate = prefixAD + resultRequireUpdate + endAD;
//                                            }
//                                            LotteryResult lotteryResult = new LotteryResult();
//                                            lotteryResult.setCompanyId(companyId); // is 1
//                                            String infoTmpCheck = resultRequireUpdate.toUpperCase();
//                                            if (infoTmpCheck.indexOf("DB") == -1) {
//                                                lotteryResult.setResult(resultRequireUpdate);
//                                                lotteryResult.setResultStatus(1); //Default in the first update not full
//                                                alert = "Ban vua cap nhat XSMB lan thu 1";
//                                            } else {
//                                                lotteryResult.setResult(resultRequireUpdateOrign);
//                                                lotteryResult.setResultStatus(0); //Result lottery is full
//                                                alert = "Ban vua cap nhat XSMB voi FULL cac giai.";
//                                            }
//                                            if (updateLotteryResult(lotteryResult)) {
//                                                sendMT(userId, serviceId, commandCode, mobileOperator,
//                                                        0, alert, 1, id);
//                                                MyLogger.log(alert);
//                                            }
//                                        }
//                                    }
//                                } //End update XSMB
//                            } //end if (removeSMSReceiveQueue(id))
//                        } else { //end  if (lotteryCommandCode != null)
//                            if (removeSMSReceiveQueue(mobileOperator, id)) {
//                                String alert = "Ma nhap XOSO sai hoac thoi gian nhap khong hop le !!";
//                                sendMT(userId, serviceId, commandCode, mobileOperator,
//                                       0, alert, 1, id);
//                                MyLogger.log("Update XS wrong Code or Invalid hour !!");
//                            }
//                        }
//                    } else {
//                        if (removeSMSReceiveQueue(mobileOperator, id)) {
//                            String alert = "Ban gui sai dau so dich vu,moi soan HELP gui8184 de biet cac dich vu cung cap boi 8x84" + enter +
//                                           "Cam on ban su dung dich vu!";
//                            sendMT(userId, serviceId, commandCode, mobileOperator,
//                                   0, alert, 1, id);
//                            MyLogger.log("Not in list Admin FoneNumber");
//                        }
//                    }
//                } //End while
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        } finally {
//            freeResouceRemote(pstmt, rs, conn);
//        }
//    }
    public void getRequestHELP(String mobileOperator) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";

        String subCode1 = "";
        String subCode2 = "";
        String subCode3 = "";
        try {
            if ((conn = DBPool.getConnectionRemote()) != null) {
                sqlStr = "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,INFO,COMMAND_CODE FROM " + mobileOperator +
                        ".SMS_RECEIVE_QUEUE WHERE SERVICE_ID = '8084'";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    BigDecimal id = rs.getBigDecimal(1);
                    String userId = rs.getString(2);
                    String serviceId = rs.getString(3);
                    //String mobileOperator = rs.getString(4);
                    String info = rs.getString(5);
                    String commandCode = rs.getString(6);
                    if (removeSMSReceiveQueue(mobileOperator, id)) {
                        if (commandCode.equals("HELP")) {
                            sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, Preference.helpStr80, 1, id, subCode1, subCode2, subCode3);
                        } else {
                            sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, Preference.wrongformat80, 1, id, subCode1, subCode2, subCode3);
                        }
                    } //end if (removeSMSReceiveQueue(id))

                } //End while

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceRemote(pstmt, rs, conn);
        }
    }

    public boolean updateLotteryResult(LotteryResult lotteryResult) {
        boolean b = false;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            conn = DBPool.getConnectionContent();
            if ((conn != null) && (lotteryResult.getResult() != null) &&
                    (!lotteryResult.getResult().equals(""))) {
                conn.setAutoCommit(false);

                removeLotteryResult(lotteryResult.getCompanyId());

//                BigDecimal id = nextSequence("LOTTERY_RESULT_ID_SEQ");
                sqlStr = "INSERT INTO LOTTERY_RESULT(ID,COMPANY_ID,RESULT,RESULT_DATE,RESULT_STATUS) VALUES(LOTTERY_RESULT_ID_SEQ.NEXTVAL,?,?,SYSDATE,?)";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, lotteryResult.getCompanyId());
                if (lotteryResult.getResult().length() > 160) {
                    pstmt.setString(2, lotteryResult.getResult().substring(0, 160));
                } else {
                    pstmt.setString(2, lotteryResult.getResult());
                }
                pstmt.setInt(3, lotteryResult.getResultStatus());
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public Vector getRequestFromQueue(String mobileOperator, Vector commandCodeLottery) {
        Vector v = new Vector();
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";

        try {
            String condition = "";
            int size = commandCodeLottery.size();
            for (int i = 0; i < size; i++) {
                if (condition.equals("")) {
                    condition = condition + "COMMAND_CODE = '" + commandCodeLottery.elementAt(i) + "'";
                } else {
                    condition = condition + " OR COMMAND_CODE = '" + commandCodeLottery.elementAt(i) + "'";
                }
            }
            condition = "(" + condition + ")" + " AND SERVICE_ID <> '8084'";
            condition = condition +
                    " AND ((STATUS = 0 ) OR (STATUS = 2) OR (TRUNC((86400*(SYSDATE-RECEIVE_TIME))/60)-60*(TRUNC(((86400*(SYSDATE-RECEIVE_TIME))/60)/60)) >= 3))";

            if ((conn = DBPool.getConnectionRemote()) != null) {
//                sqlStr = "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,INFO,COMMAND_CODE,STATUS FROM " + mobileOperator +
//                         ".SMS_RECEIVE_QUEUE WHERE SERVICE_ID <> ? AND (" +
//                         condition +
//                         ")";
                sqlStr = "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,INFO,COMMAND_CODE,STATUS FROM " + mobileOperator +
                        ".SMS_RECEIVE_QUEUE WHERE " + condition;
//                System.out.println(sqlStr);
                pstmt = conn.prepareStatement(sqlStr);
//                pstmt.setString(1, "8084");
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    SmsReceiveQueue receiveQueue = new SmsReceiveQueue();
                    receiveQueue.setId(rs.getBigDecimal(1));
                    receiveQueue.setUserId(rs.getString(2));
                    receiveQueue.setServiceId(rs.getString(3));
                    receiveQueue.setMobileOperator(rs.getString(4));
                    receiveQueue.setInfo(rs.getString(5));
                    receiveQueue.setCommandCode(rs.getString(6));
                    receiveQueue.setStatus(rs.getInt(7));
                    v.add(receiveQueue);
                } // end while

            }
        } catch (Exception ex) {
            ex.printStackTrace();
//            Main.rebuildConnection2PoolDBRemote();
        } finally {
            freeResouceRemote(pstmt, rs, conn);
            return v;
        }
    }

    public Vector getRequestUpdateLottery(String mobileOperator) {
        Vector v = new Vector();
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";

        try {
            if ((conn = DBPool.getConnectionRemote()) != null) {
                sqlStr = "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,INFO,COMMAND_CODE FROM " + mobileOperator +
                        ".SMS_RECEIVE_QUEUE WHERE SERVICE_ID = '8084' AND " + Preference.checkForUpdateStr;
//                sqlStr = "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,INFO,COMMAND_CODE FROM " + mobileOperator +
//                         ".SMS_RECEIVE_QUEUE WHERE SERVICE_ID = '8084'";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    SmsReceiveQueue receiveQueue = new SmsReceiveQueue();
                    receiveQueue.setId(rs.getBigDecimal(1));
                    receiveQueue.setUserId(rs.getString(2));
                    receiveQueue.setServiceId(rs.getString(3));
                    receiveQueue.setMobileOperator(rs.getString(4));
                    receiveQueue.setInfo(rs.getString(5));
                    receiveQueue.setCommandCode(rs.getString(6));
                    v.add(receiveQueue);
                } // end while

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceRemote(pstmt, rs, conn);
            return v;
        }
    }

    public String getResultByDate(BigDecimal companyId, String dateTime) {
        //DateTime: dd/mm/yy
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";

        String result = "";
        try {
            conn = DBPool.getConnectionContent();
            if (conn != null) {
                sqlStr =
                        "SELECT RESULT FROM LOTTERY_RESULT WHERE TO_CHAR(RESULT_DATE,'DD/MM/YY') = ? AND COMPANY_ID = ? AND RESULT_STATUS = 0";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, dateTime);
                pstmt.setBigDecimal(2, companyId);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString(1);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public Vector getResultByDateLocation(BigDecimal companyId, String ddMM) {

        Vector v = new Vector();
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        String result = "";
        String day = ddMM.substring(0, 2);
        String month = ddMM.substring(2, 4);
        try {
            conn = DBPool.getConnectionContent();
            String condition = "";
            if (conn != null) {
                if (companyId.intValue() == 2) {
                    Vector vMT = Utilities.getListCompanyMTByDayOfMonth(Integer.parseInt(day), Integer.parseInt(month));
                    for (int i = 0; i < vMT.size(); i++) {
                        condition = " COMPANY_ID = " + (BigDecimal) vMT.elementAt(i) + " OR" + condition;
                    }
                } else if (companyId.intValue() == 3) {
                    Vector vMN = Utilities.getListCompanyMNByDayOfMonth(Integer.parseInt(day), Integer.parseInt(month));
                    for (int i = 0; i < vMN.size(); i++) {
                        condition = " COMPANY_ID = " + (BigDecimal) vMN.elementAt(i) + " OR" + condition;
                    }
                }

                if (condition.endsWith("OR")) {
                    condition = condition.substring(1, condition.length() - 2);
                }
                sqlStr =
                        "SELECT RESULT FROM LOTTERY_RESULT WHERE TO_CHAR(RESULT_DATE,'DDMM') = ? AND (" + condition + ")";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, ddMM);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    result = rs.getString(1);
                    v.add(result);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return v;
        }
    }

    public boolean removeLotteryDaily(String userId, String serviceId, BigDecimal companyId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        boolean b = false;
        try {
            conn = DBPool.getConnectionContent();
            if (conn != null) {
                conn.setAutoCommit(false);
                sqlStr = "DELETE FROM LOTTERY_DAILY WHERE USER_ID = ? AND SERVICE_ID = ? AND COMPANY_ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, userId);
                pstmt.setString(2, serviceId);
                pstmt.setBigDecimal(3, companyId);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public boolean removeLotteryDaily(BigDecimal id) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        boolean b = false;
        try {
            conn = DBPool.getConnectionContent();
            if (conn != null) {
                conn.setAutoCommit(false);
                sqlStr = "DELETE FROM LOTTERY_DAILY WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, id);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public boolean removeLotteryDailyNotNeedAlert() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        boolean b = false;
        try {
            conn = DBPool.getConnectionContent();
            if (conn != null) {
                conn.setAutoCommit(false);
                sqlStr = "DELETE FROM LOTTERY_DAILY WHERE REMAIN = 0 AND STATUS = 1";
                pstmt = conn.prepareStatement(sqlStr);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public boolean removeLotteryDailyLocation(String userId, String serviceId, BigDecimal companyId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        boolean b = false;
        try {
            conn = DBPool.getConnectionContent();
            if (conn != null) {
                conn.setAutoCommit(false);
                sqlStr = "DELETE FROM LOTTERY_DAILY_LOCATION WHERE USER_ID = ? AND SERVICE_ID = ? AND COMPANY_ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, userId);
                pstmt.setString(2, serviceId);
                pstmt.setBigDecimal(3, companyId);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public boolean removeLotteryDailyLocation(BigDecimal id) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        boolean b = false;
        try {
            conn = DBPool.getConnectionContent();
            if (conn != null) {
                conn.setAutoCommit(false);
                sqlStr = "DELETE FROM LOTTERY_DAILY_LOCATION WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, id);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public boolean removeLotteryResult(BigDecimal companyId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        boolean b = false;
        try {
            conn = DBPool.getConnectionContent();
            if (conn != null) {
                conn.setAutoCommit(false);
                sqlStr = "DELETE FROM LOTTERY_RESULT WHERE COMPANY_ID = ? AND TO_CHAR(RESULT_DATE,'DDMMYY')=TO_CHAR(SYSDATE,'DDMMYY')";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, companyId);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public boolean add2LotteryDaily(LotteryDaily lotteryDaily) {
        boolean b = false;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            removeLotteryDaily(lotteryDaily.getUserId(), lotteryDaily.getServiceId(), lotteryDaily.getCompanyId());
            conn = DBPool.getConnectionContent();
            if ((conn != null) && (!lotteryDaily.getUserId().equals("")) && (!lotteryDaily.getServiceId().equals("")) &&
                    (!lotteryDaily.getCompanyId().equals(""))) {
                conn.setAutoCommit(false);
                BigDecimal id = nextSequence("LOTTERY_DAILY_ID_SEQ");
                sqlStr =
                        "INSERT INTO LOTTERY_DAILY(ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMPANY_ID,REMAIN,REQUEST_ID,COMMAND_CODE,STATUS,INFO,RESPONSE_DATE) " +
                        "VALUES(?,?,?,?,?,?,?,?,?,?,?)";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, id);
                pstmt.setString(2, lotteryDaily.getUserId());
                pstmt.setString(3, lotteryDaily.getServiceId());
                pstmt.setString(4, lotteryDaily.getMobileOperator());
                pstmt.setBigDecimal(5, lotteryDaily.getCompanyId());
                pstmt.setInt(6, lotteryDaily.getRemain());
                pstmt.setBigDecimal(7, lotteryDaily.getRequestId());
                pstmt.setString(8, lotteryDaily.getCommandCode());
                pstmt.setInt(9, lotteryDaily.getStatus());
                pstmt.setString(10, lotteryDaily.getInfo());
                pstmt.setTimestamp(11, Utilities.createTimestampFullDDMMYY(lotteryDaily.getResponseDate()));

                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public boolean add2LotteryDailyLocation(LotteryDaily lotteryDaily) {
        boolean b = false;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            removeLotteryDailyLocation(lotteryDaily.getUserId(), lotteryDaily.getServiceId(), lotteryDaily.getCompanyId());
            conn = DBPool.getConnectionContent();
            if ((conn != null) && (!lotteryDaily.getUserId().equals("")) && (!lotteryDaily.getServiceId().equals("")) &&
                    (!lotteryDaily.getCompanyId().equals(""))) {
                conn.setAutoCommit(false);
                BigDecimal id = nextSequence("LOTTERY_DAILY_LOCATION_ID_SEQ");
                sqlStr = "INSERT INTO LOTTERY_DAILY_LOCATION(ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMPANY_ID,REMAIN,RESPONSE_DATE,REQUEST_ID,COMMAND_CODE) " +
                        "VALUES(?,?,?,?,?,?,?,?,?)";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, id);
                pstmt.setString(2, lotteryDaily.getUserId());
                pstmt.setString(3, lotteryDaily.getServiceId());
                pstmt.setString(4, lotteryDaily.getMobileOperator());
                pstmt.setBigDecimal(5, lotteryDaily.getCompanyId());
                pstmt.setInt(6, lotteryDaily.getRemain());
                pstmt.setTimestamp(7, Utilities.createTimestampFullDDMMYY(lotteryDaily.getResponseDate()));
                pstmt.setBigDecimal(8, lotteryDaily.getRequestId());
                pstmt.setString(9, lotteryDaily.getCommandCode());
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public Vector getLotteryDaily(String mobileOperator, BigDecimal companyId, int resultStatus) {
        Vector v = new Vector();
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            conn = DBPool.getConnectionContent();
            if (conn != null) {
                if (resultStatus != 0) {
                    sqlStr = "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMPANY_ID,TO_CHAR(RESPONSE_DATE,'DD/MM/YY'),REMAIN,REQUEST_ID,COMMAND_CODE FROM LOTTERY_DAILY WHERE COMPANY_ID = ? AND REMAIN >= 2 AND MOBILE_OPERATOR =? AND SERVICE_ID <> '8784' AND (RESPONSE_DATE IS NULL OR TO_CHAR(RESPONSE_DATE,'DD/MM/YY') <> ? OR STATUS <> ?)";
//                    sqlStr = "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMPANY_ID,TO_CHAR(RESPONSE_DATE,'DD/MM/YY'),REMAIN,REQUEST_ID,COMMAND_CODE FROM LOTTERY_DAILY WHERE COMPANY_ID = ? AND ((REMAIN >= 2) OR ((REMAIN = 2) AND (SERVICE_ID=2))) AND MOBILE_OPERATOR =? AND SERVICE_ID <> '8784' AND (RESPONSE_DATE IS NULL OR TO_CHAR(RESPONSE_DATE,'DD/MM/YY') <> ? OR STATUS <> ?)";
//                } else if (resultStatus == 1) {
//                    sqlStr = "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMPANY_ID,TO_CHAR(RESPONSE_DATE,'DD/MM/YY'),REMAIN,REQUEST_ID,COMMAND_CODE FROM LOTTERY_DAILY WHERE COMPANY_ID = ? AND ((REMAIN >= 2) OR ((REMAIN = 2) AND (SERVICE_ID=2))) AND MOBILE_OPERATOR =? AND SERVICE_ID <> '8784' AND (RESPONSE_DATE IS NULL OR TO_CHAR(RESPONSE_DATE,'DD/MM/YY') <> ? OR STATUS <> ?)";
                } else {
//                    sqlStr = "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMPANY_ID,TO_CHAR(RESPONSE_DATE,'DD/MM/YY'),REMAIN,REQUEST_ID,COMMAND_CODE FROM LOTTERY_DAILY WHERE COMPANY_ID = ? AND REMAIN >= 1 AND MOBILE_OPERATOR =? AND (((RESPONSE_DATE IS NULL OR TO_CHAR(RESPONSE_DATE,'DD/MM/YY') <> ?) AND (SERVICE_ID = '8584' OR SERVICE_ID ='8784') ) OR (SERVICE_ID <> '8584' AND SERVICE_ID <> '8784'))";
//                    sqlStr = "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMPANY_ID,TO_CHAR(RESPONSE_DATE,'DD/MM/YY'),REMAIN,REQUEST_ID,COMMAND_CODE FROM LOTTERY_DAILY WHERE COMPANY_ID = ? AND REMAIN >= 1 AND MOBILE_OPERATOR =? AND ((SERVICE_ID <> '8584' AND SERVICE_ID <> '8784') OR (RESPONSE_DATE IS NULL OR (TO_CHAR(RESPONSE_DATE,'DD/MM/YY') <> ?)))";
//                    sqlStr = "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMPANY_ID,TO_CHAR(RESPONSE_DATE,'DD/MM/YY'),REMAIN,REQUEST_ID,COMMAND_CODE FROM LOTTERY_DAILY WHERE COMPANY_ID = ? AND REMAIN >= 1 AND MOBILE_OPERATOR =? AND (RESPONSE_DATE IS NULL OR (TO_CHAR(RESPONSE_DATE,'DD/MM/YY') <> ? OR SERVICE_ID <> '8784'))";
//                    sqlStr = "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMPANY_ID,TO_CHAR(RESPONSE_DATE,'DD/MM/YY'),REMAIN,REQUEST_ID,COMMAND_CODE FROM LOTTERY_DAILY WHERE COMPANY_ID = ? AND REMAIN >= 1 AND MOBILE_OPERATOR =? AND (RESPONSE_DATE IS NULL OR (TO_CHAR(RESPONSE_DATE,'DD/MM/YY') <> ? OR SERVICE_ID <> '8784')) AND (RESPONSE_DATE IS NULL OR (TO_CHAR(RESPONSE_DATE,'DD/MM/YY') <> ? OR STATUS <> 0 OR SERVICE_ID <> '8584'))";
                    sqlStr = "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMPANY_ID,TO_CHAR(RESPONSE_DATE,'DD/MM/YY'),REMAIN,REQUEST_ID,COMMAND_CODE FROM LOTTERY_DAILY WHERE COMPANY_ID = ? AND REMAIN >= 1 AND MOBILE_OPERATOR =? AND (RESPONSE_DATE IS NULL OR (TO_CHAR(RESPONSE_DATE,'DD/MM/YY') <> ? OR STATUS <> 0)) ORDER BY SERVICE_ID";
                }
                //System.out.println(sqlStr);
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, companyId);
                pstmt.setString(2, mobileOperator);
                if (resultStatus != 0) {
                    pstmt.setString(3, DateProc.getCurrentDDMMYY());
                    pstmt.setInt(4, resultStatus);
                } else {
                    pstmt.setString(3, DateProc.getCurrentDDMMYY());
//                    pstmt.setString(4, DateProc.getCurrentDDMMYY());
                }
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    LotteryDaily lotteryDaily = new LotteryDaily();
                    lotteryDaily.setId(rs.getBigDecimal(1));
                    lotteryDaily.setUserId(rs.getString(2));
                    lotteryDaily.setServiceId(rs.getString(3));
                    lotteryDaily.setMobileOperator(rs.getString(4));
                    lotteryDaily.setCompanyId(rs.getBigDecimal(5));
                    lotteryDaily.setResponseDate(rs.getString(6));
                    lotteryDaily.setRemain(rs.getInt(7));
                    lotteryDaily.setRequestId(rs.getBigDecimal(8));
                    lotteryDaily.setCommandCode(rs.getString(9));
                    v.add(lotteryDaily);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return v;
        }
    }

    public boolean updateRemain(LotteryDaily lotteryDaily, int resultStatus) {
        boolean b = false;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            BigDecimal id = lotteryDaily.getId();
            String serviceId = lotteryDaily.getServiceId();
            int remain = lotteryDaily.getRemain();

//            String userId = lotteryDaily.getUserId();
//            String mobileOperator = lotteryDaily.getMobileOperator();
//            BigDecimal companyId = lotteryDaily.getCompanyId();
//            String responseDate = lotteryDaily.getResponseDate();
//            BigDecimal requestId = lotteryDaily.getRequestId();

            if ((conn = DBPool.getConnectionContent()) != null) {
                conn.setAutoCommit(false);
                if ((remain == 1) && (!serviceId.equals("8784"))) {
                    removeLotteryDaily(id);
                    b = true;
                } else {
                    //System.out.println("serviceId: " + serviceId);
                    sqlStr = "UPDATE LOTTERY_DAILY SET REMAIN = REMAIN - 1, RESPONSE_DATE = SYSDATE, STATUS = ? WHERE ID = ?";
                    pstmt = conn.prepareStatement(sqlStr);
                    pstmt.setInt(1, resultStatus);
                    pstmt.setBigDecimal(2, id);
                    if (pstmt.executeUpdate() != -1) {
                        conn.commit();
                        b = true;
                    }
                }
            /*
            if ((resultStatus == 0)) {
            if (!serviceId.endsWith("8784")) {
            removeLotteryDaily(id);
            b = true;
            } else {
            sqlStr = "UPDATE LOTTERY_DAILY SET REMAIN = REMAIN - 1, RESPONSE_DATE = SYSDATE WHERE ID = ?";
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setBigDecimal(1, id);
            if (pstmt.executeUpdate() != -1) {
            conn.commit();
            b = true;
            }
            }
            } else { //Only XSMB now
            if (serviceId.endsWith("8784")) {
            sqlStr = "UPDATE LOTTERY_DAILY SET REMAIN = REMAIN - 1, RESPONSE_DATE = SYSDATE WHERE ID = ?";
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setBigDecimal(1, id);
            if (pstmt.executeUpdate() != -1) {
            conn.commit();
            b = true;
            }
            } else {
            sqlStr = "UPDATE LOTTERY_DAILY SET REMAIN = REMAIN - 1, RESPONSE_DATE = SYSDATE, STATUS = ? WHERE ID = ?";
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, resultStatus);
            pstmt.setBigDecimal(2, id);
            if (pstmt.executeUpdate() != -1) {
            conn.commit();
            b = true;
            }
            }
            }
             */
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public void alertLotteryDaily() {
        Connection conn = null;
        PreparedStatement pstmt1 = null;
        PreparedStatement pstmt2 = null;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        String sqlStr = "";

        String subCode1 = "";
        String subCode2 = "";
        String subCode3 = "";

        try {
            conn = DBPool.getConnectionContent();
            if (conn != null) {

                removeLotteryDailyNotNeedAlert();

                //In LotteryDaily
                sqlStr =
                        "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMMAND_CODE,COMPANY_ID,REQUEST_ID FROM LOTTERY_DAILY WHERE REMAIN = 0 AND SERVICE_ID='8784'";
                pstmt1 = conn.prepareStatement(sqlStr);
                rs1 = pstmt1.executeQuery();
                while (rs1.next()) {
                    BigDecimal id = rs1.getBigDecimal(1);
                    String userId = rs1.getString(2);
                    String serviceId = rs1.getString(3);
                    String mobileOperator = rs1.getString(4);
                    String commandCode = rs1.getString(5);
                    BigDecimal companyId = rs1.getBigDecimal(6);
                    BigDecimal requestId = rs1.getBigDecimal(7);
                    String alert = "";
                    subCode1 = String.valueOf(companyId);
                    if (removeLotteryDaily(id)) {
                        if (companyId.intValue() == 1) {
                            alert = "Thoi han nhan kq xo so MienBac cua ban da het,de nhan ket qua XS MienBac trong " + Preference.nXS87 +
                                    " ngay lien tiep soan XSMB gui 8784.";
                            sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3);
                        } else if (companyId.intValue() == 2) {
                            alert = "Thoi han nhan kq xo so MienTrung cua ban da het,de nhan kq xo so MienTrung trong " + Preference.nXSMT87 +
                                    " ngay lien tiep soan XSMT gui 8784.";
                            sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3);
                        } else if (companyId.intValue() == 3) {
                            alert = "Thoi han nhan kq xo so MienNam cua ban da het,de nhan kq xo so MienNam trong " + Preference.nXSMN87 +
                                    " ngay lien tiep soan XSMN gui 8784.";
                            sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3);
                        } else {
                            LotteryCompany lotteryCompany = (LotteryCompany) Main.lotteryCompanyIDHT.get(companyId);
                            alert = "Thoi han nhan kq xo so " + lotteryCompany.getCompanyDesc() + " cua ban da het,de nhan kq xo so " +
                                    lotteryCompany.getCompanyDesc() + " trong " + Preference.nXS87 +
                                    " ngay lien tiep soan XS" + lotteryCompany.getCode() + " gui 8784.";
                            sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3);
                        }
                    }
                }

                //In LotteryLocationDaily
                sqlStr = "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMMAND_CODE,COMPANY_ID,REQUEST_ID FROM LOTTERY_DAILY_LOCATION WHERE REMAIN = 0";
                pstmt2 = conn.prepareStatement(sqlStr);
                rs2 = pstmt2.executeQuery();
                while (rs2.next()) {
                    BigDecimal id = rs2.getBigDecimal(1);
                    String userId = rs2.getString(2);
                    String serviceId = rs2.getString(3);
                    String mobileOperator = rs2.getString(4);
                    String commandCode = rs2.getString(5);
                    BigDecimal companyId = rs2.getBigDecimal(6);
                    BigDecimal requestId = rs2.getBigDecimal(7);
                    String alert = "";
                    if (removeLotteryDailyLocation(id)) {
                        if (companyId.intValue() == 2) {
                            alert = "Thoi han nhan kq xo so MienTrung cua ban da het,de nhan kq xo so MienTrung trong " + Preference.nXSMT87 +
                                    " ngay lien tiep soan XSMT gui 8784.";
                            sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3);
                        } else if (companyId.intValue() == 3) {
                            alert = "Thoi han nhan kq xo so MienNam cua ban da het,de nhan kq xo so MienNam trong " + Preference.nXSMN87 +
                                    " ngay lien tiep soan XSMN gui 8784.";
                            sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt1, rs1, conn);
            freeResouceContent(pstmt2, rs2, null);
        }
    }

    public void processLotteryDailyLocation() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";

        try {
            conn = DBPool.getConnectionContent();
            if (conn != null) {
                sqlStr = "SELECT ID,USER_ID,SERVICE_ID,COMPANY_ID,COMMAND_CODE,MOBILE_OPERATOR,REQUEST_ID FROM LOTTERY_DAILY_LOCATION WHERE REMAIN >= 1 AND (RESPONSE_DATE IS NULL OR TO_CHAR(RESPONSE_DATE,'DD/MM/YY') <> ?)";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, DateProc.getCurrentDDMMYY());
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    BigDecimal id = rs.getBigDecimal(1);
                    String userId = rs.getString(2);
                    String serviceId = rs.getString(3);
                    BigDecimal companyId = rs.getBigDecimal(4);
                    String commandCode = rs.getString(5);
                    String mobileOperator = rs.getString(6);
                    BigDecimal requestId = rs.getBigDecimal(7);
                    if (companyId.intValue() == 2) {
                        Vector v = Utilities.getListCompanyMTToday();
                        if (updateRemainLotteryDailyLocation(id)) {
                            for (int i = 0; i < v.size(); i++) {
                                BigDecimal companyIdToday = (BigDecimal) v.elementAt(i);

                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setCompanyId(companyIdToday);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setRemain(1);
                                lotteryDaily.setStatus(1); //Not need alert

                                lotteryDaily.setRequestId(requestId);
                                add2LotteryDaily(lotteryDaily);
                            }
                        }
                    } else if (companyId.intValue() == 3) {
                        Vector v = Utilities.getListCompanyMNToday();
                        if (updateRemainLotteryDailyLocation(id)) {
                            for (int i = 0; i < v.size(); i++) {
                                BigDecimal companyIdToday = (BigDecimal) v.elementAt(i);

                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setCompanyId(companyIdToday);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setRemain(1);
                                lotteryDaily.setStatus(1); //Not need alert

                                lotteryDaily.setRequestId(requestId);
                                add2LotteryDaily(lotteryDaily);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
        }
    }

    public boolean updateRemainLotteryDailyLocation(BigDecimal id) {
        boolean b = false;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            conn = DBPool.getConnectionContent();
            if (conn != null) {
                conn.setAutoCommit(false);
                sqlStr = "UPDATE LOTTERY_DAILY_LOCATION SET REMAIN = REMAIN - 1, RESPONSE_DATE = SYSDATE WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, id);
                if (pstmt.executeUpdate() != -1) {
                    b = true;
                    conn.commit();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public Vector getListResult(BigDecimal companyId) {
        Vector v = new Vector();
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            conn = DBPool.getConnectionContent();
            if (conn != null) {
                sqlStr =
                        "SELECT RESULT FROM (SELECT RESULT FROM LOTTERY_RESULT WHERE COMPANY_ID = ? AND RESULT_STATUS = 0 ORDER BY ID DESC) WHERE ROWNUM <= 30";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, companyId);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    String result = rs.getString(1);
                    v.add(result);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return v;
        }

    }

    public String getSOI(String commandCode) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";

        String result = "";
        try {
            if ((conn = DBPool.getConnectionContent()) != null) {
                conn.setAutoCommit(false);
                sqlStr = "SELECT DATA FROM FB_DATA WHERE COMMAND_CODE =? AND ACTIVE = 1 ORDER BY ID DESC";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, commandCode); //Get SOI commandCode

                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString(1);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public boolean removeLotteryDaily(String userId, BigDecimal companyId) {
        boolean b = false;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            if ((conn = DBPool.getConnectionContent()) != null) {
                conn.setAutoCommit(false);
                sqlStr = "DELETE FROM LOTTERY_DAILY WHERE USER_ID = ? AND COMPANY_ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, userId);
                pstmt.setBigDecimal(2, companyId);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public boolean updateStatus(BigDecimal id, int status, String mobileOperator) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        String sqlStr = "";
        boolean b = false;
        try {
            if ((conn = DBPool.getConnectionRemote()) != null) {
                sqlStr = "UPDATE " + mobileOperator + ".SMS_RECEIVE_QUEUE SET STATUS = ? WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setInt(1, status);
                pstmt.setBigDecimal(2, id);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceRemote(pstmt, null, conn);
            return b;
        }
    }

    public String getCurrentPrizeOfLottery(BigDecimal companyId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";

        String result = "";
        try {
            if ((conn = DBPool.getConnectionContent()) != null) {
                conn.setAutoCommit(false);
                sqlStr = "SELECT RESULT FROM LOTTERY_RESULT WHERE COMPANY_ID = ? AND TO_CHAR(RESULT_DATE,'DD/MM/YY')=? AND RESULT_STATUS = 0";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, companyId);
                pstmt.setString(2, DateProc.getCurrentDDMMYY());
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString(1);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public Hashtable getListUserIdEndWithDBPrize(String mobileOperator, String dbPrize, String previousLotteryResultTime, String currentLotteryResultTime) {
//        previousTime: DD/MM/YY HH24:MI:SS
//        currentTime : DD/MM/YY HH24:MI:SS
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";

        Hashtable ht = new Hashtable();
        MOItem moItem = null;

        BigDecimal id = null;
        String userId = "";
        String serviceId = "";
        String commandCode = "";
        String info = "";
        String receiveTime = "";



        try {
            if ((conn = DBPool.getConnectionRemote()) != null) {
                String cmdSoiCau = "COMMAND_CODE not in(";
                for (int i = 0; i < Preference.commnandCodeSOICAU.size(); i++) {
//                    if (cmdSoiCau.equals("")) {
//                        cmdSoiCau = " COMMAND_CODE <> '" + (String) Preference.commnandCodeSOICAU.elementAt(i) + "' ";
//                    } else {
//                        cmdSoiCau = cmdSoiCau + "AND COMMAND_CODE <> '" + (String) Preference.commnandCodeSOICAU.elementAt(i) + "' ";
//                    }
                    cmdSoiCau += "'" + (String) Preference.commnandCodeSOICAU.elementAt(i) + "',";
                }
                if (cmdSoiCau.endsWith(",")) {
                    cmdSoiCau = cmdSoiCau.substring(0, cmdSoiCau.length() - 1);
                }
                cmdSoiCau = cmdSoiCau + ") AND ";
//                sqlStr = "SELECT DISTINCT(USER_ID) FROM " + mobileOperator + ".SMS_RECEIVE_LOG PARTITION(SMS_RECEIVE_LOG_P" + DateProc.getCurrentYYYY() +
//                         DateProc.getCurrentMM() + ") " +
//                         "WHERE (SERVICE_ID='8484' OR SERVICE_ID='8584' OR SERVICE_ID='8684') AND SUBCODE1='1' AND RECEIVE_TIME BETWEEN TO_DATE('" +
//                         DateProc.getPreviousDate() + " 19:24:30'" + ",'DD/MM/YY HH24:MI:SS') " +
//                         "AND TO_DATE('" + DateProc.getCurrentDDMMYY() + " 19:24:30'" + ",'DD/MM/YY HH24:MI:SS')";

//                sqlStr = "SELECT ID,USER_ID,SERVICE_ID,COMMAND_CODE,INFO,TO_CHAR(RECEIVE_TIME,'DD/MM/YY HH24:MI:SS') " +
//                         "FROM " + mobileOperator + ".SMS_RECEIVE_LOG PARTITION(SMS_RECEIVE_LOG_P" + DateProc.getCurrentYYYY() +
//                         DateProc.getCurrentMM() + ") " +
//                         "WHERE (SERVICE_ID='8484' OR SERVICE_ID='8584' OR SERVICE_ID='8684') AND SUBCODE1='1' AND RECEIVE_TIME BETWEEN TO_DATE('" +
//                         previousLotteryResultTime + "','DD/MM/YY HH24:MI:SS') " +
//                         "AND TO_DATE('" + currentLotteryResultTime + "','DD/MM/YY HH24:MI:SS')";

//                sqlStr = "SELECT REQUEST_ID,USER_ID,SERVICE_ID,COMMAND_CODE,INFO,TO_CHAR(SUBMIT_DATE,'DD/MM/YY HH24:MI:SS') " +
//                         "FROM " + mobileOperator + ".SMS_SEND_LOG PARTITION(SMS_SEND_LOG_P" + DateProc.getCurrentYYYY() +
//                         DateProc.getCurrentMM() + ") " +
//                         "WHERE MESSAGE_TYPE = 1 AND (SERVICE_ID='8484' OR SERVICE_ID='8584' OR SERVICE_ID='8684') AND SUBCODE1='1' AND SUBMIT_DATE BETWEEN TO_DATE('" +
//                         previousLotteryResultTime + "','DD/MM/YY HH24:MI:SS') " +
//                         "AND TO_DATE('" + currentLotteryResultTime + "','DD/MM/YY HH24:MI:SS')";

                sqlStr = "SELECT REQUEST_ID,USER_ID,SERVICE_ID,COMMAND_CODE,INFO,TO_CHAR(SUBMIT_DATE,'DD/MM/YY HH24:MI:SS') " +
                        "FROM " + mobileOperator + ".SMS_SEND_LOG PARTITION(SMS_SEND_LOG_P" + DateProc.getCurrentYYYY() +
                        DateProc.getCurrentMM() + ") " +
                        "WHERE USER_ID like '84%" + dbPrize + "' and " + cmdSoiCau + " MESSAGE_TYPE = 1 AND (SERVICE_ID='8484' OR SERVICE_ID='8584' OR SERVICE_ID='8684') AND SUBCODE1='1' AND SUBMIT_DATE BETWEEN TO_DATE('" +
                        previousLotteryResultTime + "','DD/MM/YY HH24:MI:SS') " +
                        "AND TO_DATE('" + currentLotteryResultTime + "','DD/MM/YY HH24:MI:SS')";
                System.out.println(sqlStr);
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    id = rs.getBigDecimal(1);
                    userId = rs.getString(2);
                    serviceId = rs.getString(3);
                    commandCode = rs.getString(4);
                    info = rs.getString(5);
                    receiveTime = rs.getString(6);

                    if (userId.endsWith(dbPrize)) {
                        moItem = new MOItem();
                        moItem.setId(id);
                        moItem.setUserId(userId);
                        moItem.setServiceId(serviceId);
                        moItem.setCommandCode(commandCode);
                        moItem.setInfo(info);
                        moItem.setTimeReceive(receiveTime);
                        moItem.setMobileOperator(mobileOperator);

                        ht.put(userId, moItem);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceRemote(pstmt, rs, conn);
            return ht;
        }
    }

    public String getTimeWhenLotteryIsFull(String dayTime, BigDecimal companyId) {
        String result = ""; //DD/MM/YY HH24:MI:SS

        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            if ((conn = DBPool.getConnectionContent()) != null) {
                sqlStr =
                        "SELECT TO_CHAR(RESULT_DATE,'DD/MM/YY HH24:MI:SS') FROM LOTTERY_RESULT WHERE TO_CHAR(RESULT_DATE,'DD/MM/YY')=? AND COMPANY_ID =? AND RESULT_STATUS = 0";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, dayTime);
                pstmt.setBigDecimal(2, companyId);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString(1);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public String Update2PromotionalLottery(MOItem moItem) {
        String result = ""; //DD/MM/YY HH24:MI:SS

        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            if ((conn = DBPool.getConnectionContent()) != null) {
                sqlStr = "INSERT INTO PROMOTIONAL_LOTTERY(ID,USER_ID,SERVICE_ID,COMMAND_CODE,INFO,MOBILE_OPERATOR,COMPANY_ID,RECEIVE_TIME,REQUEST_ID) " +
                        "VALUES(PROMOTIONAL_LOTTERY_ID_SEQ.NEXTVAL,?,?,?,?,?,?,TO_DATE('" + moItem.getTimeReceive() + "','DD/MM/YY HH24:MI:SS'),?)";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, moItem.getUserId());
                pstmt.setString(2, moItem.getServiceId());
                pstmt.setString(3, moItem.getCommandCode());
                pstmt.setString(4, moItem.getInfo());
                pstmt.setString(5, moItem.getMobileOperator());
                pstmt.setInt(6, 1);
                pstmt.setBigDecimal(7, moItem.getId());
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return result;
        }
    }

    public String getTXTData_1(String commandId, String param1) {
        String result = "";
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT CONTENT FROM " +
                        "(SELECT CONTENT FROM TXT_DATA A " +
                        "WHERE COMMAND_ID=? AND PARAM1=? AND NVL(ACTIVE,' ') ='Y' ORDER BY DBMS_RANDOM.VALUE) " +
                        "WHERE ROWNUM = 1";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setString(1, commandId);
                pstmt.setString(2, param1);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString("CONTENT");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return result;
        }
    }

    public String getSpam() {
        String result = "";
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "SELECT CONTENT FROM SPAMDATA WHERE ACTIVE<>'1' and ROWNUM < 2";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    result = rs.getString("CONTENT");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
        }
        return result;
    }

    public static void main(String[] args) {
        //System.out.println("abc\rdfsd");
//        System.out.println(Utilities.createTimestampFullDDMMYY(""));
        DBTool dBTool = new DBTool();
        dBTool.getListUserIdEndWithDBPrize("GPC", "99", "04/06/10 19:21:22", "05/06/10 19:25:54");

    }

    public boolean add2CauDaily(CauItem lotteryDaily) {
        boolean b = false;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            conn = DBPool.getConnectionContent();
            if ((conn != null) && (!lotteryDaily.getUserId().equals("")) && (!lotteryDaily.getServiceId().equals("")) &&
                    (lotteryDaily.getCompanyId() != 0)) {
                conn.setAutoCommit(false);
                BigDecimal id = nextSequence("CAU_DAILY_ID_SEQ");
                sqlStr =
                        "INSERT INTO CAU_DAILY(ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMPANY_ID,REMAIN,REQUEST_ID,COMMAND_CODE,STATUS,INFO,RESPONSE_DATE) " +
                        "VALUES(?,?,?,?,?,?,?,?,?,?,?)";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, id);
                pstmt.setString(2, lotteryDaily.getUserId());
                pstmt.setString(3, lotteryDaily.getServiceId());
                pstmt.setString(4, lotteryDaily.getMobileOperator());
                pstmt.setInt(5, lotteryDaily.getCompanyId());
                pstmt.setInt(6, lotteryDaily.getRemain());
                pstmt.setBigDecimal(7, lotteryDaily.getRequestId());
                pstmt.setString(8, lotteryDaily.getCommandCode());
                pstmt.setInt(9, lotteryDaily.getStatus());
                pstmt.setString(10, lotteryDaily.getInfo());
                pstmt.setTimestamp(11, Utilities.createTimestampFullDDMMYY(lotteryDaily.getResponseDate()));

                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }

    public Vector getRequestFromCAUWait() {
        Vector v = new Vector();
        CauItem cauItem = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlStr = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr =
                        "SELECT ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,INFO,COMMAND_CODE,REQUEST_ID,REMAIN " +
                        "FROM CAU_DAILY WHERE REMAIN > 0";
                pstmt = conn.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    cauItem = new CauItem();
                    cauItem.setId(rs.getBigDecimal(1));
                    cauItem.setUserId(rs.getString(2));
                    cauItem.setServiceId(rs.getString(3));
                    cauItem.setMobileOperator(rs.getString(4));
                    cauItem.setInfo(rs.getString(5));
                    cauItem.setCommandCode(rs.getString(6));
                    cauItem.setRequestId(rs.getBigDecimal(7));
                    cauItem.setRemain(rs.getInt(8));
                    v.add(cauItem);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return v;
        }
    }

    public boolean updateCauWait(BigDecimal id, int remain) {
        boolean b = false;
        Connection conn = null;
        PreparedStatement pstmt = null;
        String sqlStr = "";
        try {
            if ((conn = (Connection) DBPool.getConnectionContent()) != null) {
                sqlStr = "UPDATE CAU_DAILY SET REMAIN =? WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setInt(1, remain);
                pstmt.setBigDecimal(2, id);
                if (pstmt.executeUpdate() != -1) {
                    conn.commit();
                    b = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, null, conn);
            return b;
        }
    }
}
