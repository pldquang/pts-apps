package com.sms.process;

import java.util.*;
import java.util.regex.*;
import java.math.BigDecimal;

import com.sms.item.SmsReceiveQueue;
import com.sms.common.*;
import com.sms.item.LotteryCompany;

public class ProcessLO extends Thread {
    DBTool dbTool = null;
    Hashtable xxNumHT = null;
    static Hashtable xxNumHTDB = null;

    SmsReceiveQueue smsReceiveQueue = null;
    BigDecimal requestId = null;
    String userId = "";
    String serviceId = "";
    int status = 0;
    String commandCode = "";
    String info = "";
    String tmp = "";

    String subCode1 = "1";
    String subCode2 = "";
    String subCode3 = "";

    public ProcessLO() {
        dbTool = new DBTool();
        xxNumHT = new Hashtable();
    }

    public String getLO(Vector listResult, String number) {
        Matcher matcher = null;
        String inputStr = "";
        String newLine = "\n";
        String msg = "";
        Pattern p = Pattern.compile(":|-");
        int count = 0;

        for (int i = 0; i <= 99; i++) {
            if (i < 10) {
                xxNumHT.put("0" + String.valueOf(i), new Integer(0));
            } else {
                xxNumHT.put(String.valueOf(i), new Integer(0));
            }
        }

        for (int i = 0; i < listResult.size(); i++) {
            inputStr = (String) listResult.elementAt(i);
            matcher = p.matcher(inputStr);
            String tmp = matcher.replaceAll(" ");

            StringTokenizer token = new StringTokenizer(tmp, " ");
            int tokens = token.countTokens();
            String tkStr = "";
            String xxLast = "";
            for (int j = 0; j < tokens; j++) {
                tkStr = token.nextToken();
                try {
                    if ((tkStr.length() >= 2)) {
                        xxLast = tkStr.substring(tkStr.length() - 2, tkStr.length());
                        Integer.parseInt(xxLast);
                        System.out.println(xxLast);
                        if (xxNumHT.containsKey(xxLast)) {
                            Integer num = (Integer) xxNumHT.get(xxLast);
                            xxNumHT.remove(xxLast);
                            xxNumHT.put(xxLast, num + 1);
                        } else {
                            xxNumHT.put(xxLast, 1);
                        }
                    }
                } catch (Exception ex) {}

                if (!number.equals("")) {
                    try {
                        int data = Integer.parseInt(tkStr);
                        if (data >= 10) {
                            if (tkStr.endsWith(number)) {
                                count++;
                            }
                        }
                    } catch (Exception ex) {}
                }
            }
        } //End for

        //Put keys and values in to an arraylist using entryset
        ArrayList myArrayList = new ArrayList(xxNumHT.entrySet());
        //Sort the values based on values first and then keys.
        Collections.sort(myArrayList, new MyComparator());

        msg = "Thong ke 60ngay gan day:";
        if ((number != null) && !number.equals("")) {
            msg = msg + newLine + "Cap so " + number + " ve " + count + " lan.";
        }

        //Show sorted results
        Iterator itr = myArrayList.iterator();
        String key = "";
        int value = 0;
        int cnt = 0;
        while (itr.hasNext()) {
            Map.Entry e = (Map.Entry) itr.next();
            key = (String) e.getKey();
            value = Integer.parseInt(e.getValue().toString());
            if (cnt <= 4) {
                if (cnt == 0) {
                    msg = msg + newLine + "5 cap so ve nhieu nhat la " + key + ":" + value + "lan";
                } else {
                    msg = msg + "," + key + ":" + value;
                }
            }

            if (cnt >= 95) {
                if (cnt == 95) {
                    msg = msg + newLine + "5 cap so ve it nhat la " + key + ":" + value + "lan";
                } else {
                    msg = msg + "," + key + ":" + value;
                }
            }
            cnt++;
        }
        return msg;
    }

    public void processRequestQueue(String mobileOperator) {
        Vector listRequest = dbTool.getRequestFromQueue(mobileOperator, Preference.commnandCodeSOICAU);
        try {
            for (int i = 0; i < listRequest.size(); i++) {
                smsReceiveQueue = (SmsReceiveQueue) listRequest.elementAt(i);
                requestId = smsReceiveQueue.getId();
                userId = smsReceiveQueue.getUserId();
                serviceId = smsReceiveQueue.getServiceId();
                status = smsReceiveQueue.getStatus();
                commandCode = smsReceiveQueue.getCommandCode();
                info = (smsReceiveQueue.getInfo()).trim().toUpperCase();

                tmp = commandCode + " " + info.substring(commandCode.length(), info.length());
                String xxStr = "";
                int numXX = 0;
                String msg = "";
                tmp = tmp.substring(commandCode.length()).trim().toUpperCase();
                tmp = StringTool.getContentNoWhiteLetter(tmp);

                LotteryCompany lotteryCompany = null;
                BigDecimal companyId = null;

                if((tmp != null) && !tmp.equals("")) {
                    lotteryCompany = (LotteryCompany) Main.lotteryCompanyHT.get(tmp);
                    if(lotteryCompany != null) {
                        companyId = lotteryCompany.getCompanyId();
                    }
                } else {
                    lotteryCompany = (LotteryCompany) Main.lotteryCompanyHT.get("MB");
                    if(lotteryCompany != null) {
                        companyId = lotteryCompany.getCompanyId();
                    }
                }

                if ((companyId != null) && (companyId.intValue() == 1)) { //Is MienBac
                    if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284")) ||
                        Preference.listAdmin.contains(userId)) {
                        try {
                            xxStr = StringTool.getObjectString(tmp, 1);
                            if (xxStr.length() > 2)
                                xxStr = xxStr.substring(0, 2);
                            try {
                                numXX = Integer.parseInt(xxStr);
                                msg = getLO(dbTool.getListResult(new BigDecimal(1)), String.valueOf(numXX));
                            } catch (Exception ex) {
                                msg = dbTool.getSOI("CAU"); //CAU is defaul value in db
                                if (msg.equals("")) {
                                    msg = getLO(dbTool.getListResult(new BigDecimal(1)), "");
                                }
                            }
                        } catch (Exception ex) {
                            msg = dbTool.getSOI("CAU"); //CAU is defaul value in db
                            if (msg.equals("")) {
                                msg = getLO(dbTool.getListResult(new BigDecimal(1)), "");
                            }
                        }

                        if (dbTool.removeSMSReceiveQueue(mobileOperator, requestId)) {
                            if (msg.length() > 160 && (msg.indexOf("!xxx!") == -1)) {
                                msg = StringTool.splitMsg(msg);
                            }

                            boolean deductMoney = false;
                            msg = msg.replaceAll("!xxx!", "|");
                            StringTokenizer stToken = new StringTokenizer(msg, "|", false);
                            int stTokenLen = stToken.countTokens();
                            for (int ii = 0; ii < stTokenLen; ii++) {
                                String txtSms = stToken.nextToken();
                                if (txtSms.startsWith("\n"))
                                    txtSms = txtSms.substring(1);
                                if (!deductMoney) {
                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0,
                                                  txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0,
                                                  txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                }
                                if (ii > 5)
                                    break;
                            }
                            System.out.println(userId + "<==" + serviceId + " " + commandCode);
                        }
                    } else {
                        if (dbTool.removeSMSReceiveQueue(mobileOperator, requestId)) {
                            String alert = "De nhan duoc thong tin soi cau xin soan " + commandCode + " gui 8584, hoac " + commandCode +
                                           " ConSo ma ban muon soi gui 8584\nDT ho tro 1900561577";
                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                            System.out.println(userId + "<==" + serviceId + " " + commandCode);
                        }
                    }
                } else {
                    if (status != 0) {
                        if ((!serviceId.endsWith("8184") && !serviceId.endsWith("8284") && !serviceId.endsWith("8384") && !serviceId.endsWith("8484")) ||
                            Preference.listAdmin.contains(userId)) {

                            msg = dbTool.getSOI("CAU"); //CAU is defaul value in db
                            if (msg.equals("")) {
                                msg = getLO(dbTool.getListResult(new BigDecimal(1)), "");
                            }

                            if (dbTool.removeSMSReceiveQueue(mobileOperator, requestId)) {
                                if (msg.length() > 160 && (msg.indexOf("!xxx!") == -1)) {
                                    msg = StringTool.splitMsg(msg);
                                }

                                boolean deductMoney = false;
                                msg = msg.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(msg, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (txtSms.startsWith("\n"))
                                        txtSms = txtSms.substring(1);
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5)
                                        break;
                                }
                                System.out.println(userId + "<==" + serviceId + " " + commandCode);
                            }

                        } else {
                            if (dbTool.removeSMSReceiveQueue(mobileOperator, requestId)) {
                                String alert = "De nhan duoc thong tin soi cau xin soan " + commandCode + " gui 8584, hoac " + commandCode +
                                               " ConSo ma ban muon soi gui 8584\nDT ho tro 1900561577";
                                dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                System.out.println(userId + "<==" + serviceId + " " + commandCode);
                            }
                        }
                    } else {
                        dbTool.updateStatus(requestId, 1, mobileOperator);
                    }
                }
            } //End for
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void run() {
        while (Main.running) {
            try {
                processRequestQueue("VIETEL");
                processRequestQueue("GPC");
                processRequestQueue("VMS");
                Thread.sleep(5000);
            } catch (Exception ex) {}
        }
    }

    public static void main(String[] args) {
        ProcessLO processLO = new ProcessLO();
        String a = "ab" + (char) 10 + "c";
        System.out.println(a.length());
    }
}
