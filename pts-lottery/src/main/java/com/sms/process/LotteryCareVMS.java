package com.sms.process;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import java.util.*;
import java.math.BigDecimal;

import com.sms.item.*;
import com.sms.common.*;
import com.sms.common.Preference;
import com.sms.common.DateProc;
import com.sms.item.LotteryResult;

/*
A là thời gian từ 19h27 (sau khi có kết quả đầy đủ) đến trước 16h00 ngày hôm sau
B là thời gian từ 16h00 hàng ngày đến trước 19h13 (trước giải1)
C là thời gian từ 19h13 đến 19h27 (Trong thời gian quay kết quả)
 */
public class LotteryCareVMS extends Thread {

    DBTool dbTool = null;
    static final String findNotResult =
            "KQ se gui cho ban ngay khi co.Chuc ban may man!De nhan KQ " + Preference.nXS87 +
            " ngay lien tiep soan XSMB gui 8784,gui 8584 nhan tung giai lien tiep.";
    //String xsLive = "Ban se duoc TuongThuatTrucTiep tung giai XoSo MienBac tu truong quay TangBatHo\nDT ho tro 1900561577";
    public String xsCho = "Hien nay chua co ket qua XS chinh thuc.He thong se gui kq xo so toi ban ngay khi co kq.DT ho tro 1900561577";
    public String xs87 = "Ban se nhan dc ket qua xs trong " + Preference.nXS87 + " ngay lien tiep.Chuc ban may man!DT ho tro 1900561577";
    public String notYetResult = "He thong hien tai chua co ket qua xo so.Hay soan XSMB gui 8784 de nhan kqxs Mien bac " + Preference.nXS87 +
            " ngay lien tiep.";
    public String wrongMsg = "Tin nhan sai,de nhan KQ xs Mien bac " + Preference.nXS87 +
            " ngay lien tiep soan XSMB gui 8784,gui 8584 nhan TTTT tung giai\nDT ho tro 1900561577";
    public String wrongMTMsg = "De nhan KQ xs MienTrung  " + Preference.nXSMT87 +
            " ngay lien tiep soan XSMT gui 8784,gui 8384 nhan kq moi nhat.DT ho tro 1900561577";
    public String wrongMNMsg = "De nhan KQ xs MienNam " + Preference.nXSMN87 +
            " ngay lien tiep soan XSMN gui 8784,gui 8384 nhan kq moi nhat.DT ho tro 1900561577";
    public String operator = "VMS";
    public String notResult =
            "He thong khong tim thay ket qua xs ma ban yeu cau,de nhan KQ xs Mien bac " + Preference.nXS87 +
            " ngay lien tiep soan XSMB gui 8784,gui 8584 nhan tung giai lien tiep.";
    static char enter = (char) 10;
    String alert = "";
    SmsReceiveQueue smsReceiveQueue = null;
    String userId = "";
    String serviceId = "";
    String commandCode = "";
    int status = 0;
    String info = "";
    String mobileOperator = "";
    BigDecimal requestId = null;

    public LotteryCareVMS() {
        dbTool = new DBTool();
    }

    public void processLottery() {
        int remain = 0;
        String currentDDMMYY = "";
        String currentDDMM = "";
        int HH24 = 0;
        int MI = 0;
        boolean trueFormat = false;
        boolean dateFormat = false;
        boolean isNoParam = false;
        boolean deductMoney = false;

        String currCode = "";
        String temp = "";
        String tmp1 = "";
        String tmp2 = "";

        String subCode1 = "";
        String subCode2 = "";
        String subCode3 = "";

        String code1 = "";
        String code2 = "";
        String code3 = "";

        String dd = "";
        String mm = "";
        String yy = DateProc.getCurrentYY();

        currentDDMMYY = DateProc.getCurrentDDMMYY(); //DD/MM/YY

        currentDDMM = DateProc.getCurrentDDMM(); //DD/MM/YY

        HH24 = DateProc.getCurrentHH24();
        MI = DateProc.getCurrentMI();

        //Get all MO request companyId
        Vector listRequest = new Vector();
        listRequest = dbTool.getRequestFromQueue(operator, Preference.commnandCodeLottery);
        for (int ii = 0; ii < listRequest.size(); ii++) {
            smsReceiveQueue = (SmsReceiveQueue) listRequest.elementAt(ii);
            userId = smsReceiveQueue.getUserId();
            serviceId = smsReceiveQueue.getServiceId();
            status = smsReceiveQueue.getStatus();
            commandCode = smsReceiveQueue.getCommandCode();
            info = smsReceiveQueue.getInfo();
            mobileOperator = smsReceiveQueue.getMobileOperator();
            requestId = smsReceiveQueue.getId();

            try {
                temp = StringTool.replaceWhiteLetter(info).toUpperCase();
                if (temp.equals("XS") || temp.equals("SX")) {
                    temp = "XSMB";
                }
                if (temp.startsWith("MB") || temp.startsWith("XMB") || temp.startsWith("SMB")) {
                    tmp1 = "MB" + StringTool.replaceWhiteLetter(temp.substring(commandCode.length(), temp.length()));
                } else if (temp.startsWith("XMT") || temp.startsWith("SMT") || temp.startsWith("HGT") || temp.startsWith("QMT")) {
                    tmp1 = "MT" + StringTool.replaceWhiteLetter(temp.substring(commandCode.length(), temp.length()));
                } else if (temp.startsWith("XMN") || temp.startsWith("SMN") || temp.startsWith("HGN") || temp.startsWith("QMN")) {
                    if (!temp.startsWith("XMNR")) {
                        tmp1 = "MN" + StringTool.replaceWhiteLetter(temp.substring(commandCode.length(), temp.length()));
                    } else {
                        dbTool.updateStatus(requestId, 1, mobileOperator);
                        continue;
                    }
                } else if (temp.equals(commandCode)) {
                    tmp1 = "MB";
                } else {
//                    tmp1 = StringTool.replaceWhiteLetter(temp.substring(commandCode.length(), temp.length()));
                    temp = StringTool.replaceWhiteLetter(temp);
//                    tmp1 = StringTool.getContentNoWhiteLetter(temp.substring(commandCode.length(), temp.length()));
                    tmp1 = StringTool.replaceWhiteLetter(temp.substring(commandCode.length(), temp.length()));
//                    tmp1 = StringTool.replaceWhiteLetter(tmp1);
                }
            } catch (Exception ex) {
            }

            try {
                if (StringTool.isNumberic(tmp1.substring(0, 1))) {
                    tmp1 = "MB" + " " + tmp1;
                }
            } catch (Exception ex) {
            }

            for (int i = 0; i < Main.lotteryCommandCodeV.size(); i++) {
                currCode = (String) Main.lotteryCommandCodeV.elementAt(i);
                if (tmp1.startsWith(currCode)) {
                    tmp2 = currCode + " " + StringTool.replaceWhiteLetter(tmp1.substring(currCode.length(), tmp1.length()));
                    trueFormat = true;
                    break;
                } //Start with code

            } //End for earch Code Lottery

            if (trueFormat) {
                code1 = StringTool.getObjectString(tmp2, 1); // DD or CHO

                code2 = StringTool.getObjectString(tmp2, 2); // MM

                code3 = StringTool.getObjectString(tmp2, 3); // YY or YYYY

                LotteryCompany lotteryCompany = (LotteryCompany) Main.lotteryCompanyHT.get(currCode);
                BigDecimal companyId = lotteryCompany.getCompanyId();

                LotteryResult lotteryResult = (LotteryResult) Main.lotteryResultHT.get(companyId);
                String result = lotteryResult.getResult();
                int result_status = lotteryResult.getResultStatus();
                if (commandCode.equals("ST")) {
                    result = result.replaceAll("CAU gui8584.", "SC gui8584.");
                    result = result.replaceAll("XS.", "ST");
                }
                int resultStatus = lotteryResult.getResultStatus();
                String resultDate = lotteryResult.getResultDate();

                // DD-MM-YY
                try {

                    if ((code1 == null) || (code1.equals(""))) {
                        isNoParam = true;
                    } else {
                        if (StringTool.isNumberic(code1) && (code1.length() <= 2)) {
                            if (code1.length() == 1) {
                                code1 = "0" + code1;
                                dateFormat = true;
                            } else if (Integer.parseInt(code1) > 31) {
                                dateFormat = false;
                            } else {
                                dateFormat = true;
                            }
                        } else {
                            dateFormat = false;
                        }

                        if (dateFormat) {
                            if (StringTool.isNumberic(code2) && (code2.length() <= 2)) {
                                if (code2.length() == 1) {
                                    code2 = "0" + code2;
                                    dateFormat = true;
                                } else if (Integer.parseInt(code2) > 12) {
                                    dateFormat = false;
                                } else {
                                    dateFormat = true;
                                }
                            } else {
                                dateFormat = false;
                            }
                        }

                        if (dateFormat) {
                            if ((code3 != null) && (!code3.equals(""))) {
                                if (StringTool.isNumberic(code3) && ((code3.length() == 2) || (code3.length() == 4))) {
                                    if (code3.length() == 2) {
                                        code3 = "20" + code3;
                                        dateFormat = true;
                                    } else if (Integer.parseInt(code3) > Integer.parseInt(DateProc.getCurrentYYYY())) {
                                        dateFormat = false;
                                    } else {
                                        dateFormat = true;
                                    }
                                } else {
                                    dateFormat = false;
                                }
                            }
                        }
                    }
                    if (result_status == 0) {
                        String spam = dbTool.getSpam();
                        if (!spam.equalsIgnoreCase("")) {
                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, spam, 0, requestId, subCode1, subCode2, subCode3);
                        }
                    }
                } catch (Exception ex) {
                    dateFormat = false;
                }

                if (dateFormat) {
                    dd = code1;
                    mm = code2;
                }

                if (companyId.intValue() == 1) { //Is MienBac

                    subCode1 = "1"; // Code of MB is 1

                    if (serviceId.endsWith("8184")) {
                        if (isNoParam) {
                            if ((resultDate.equals(currentDDMMYY) && (resultStatus == 0)) || (HH24 >= 0 && HH24 < 16)) {
                                // In A time distance
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB:<==" + serviceId);
                                }
                            } else if ((resultStatus != 0) || (HH24 >= 16)) {
                                // In B or C time distance
                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                lotteryDaily.setRemain(1);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setRequestId(requestId);
                                dbTool.add2LotteryDaily(lotteryDaily);
//                                alert = "Hien nay he thong chua co ket qua MB day du ngay " + currentDDMM +
//                                        ",he thong se gui ban ngay khi co kq chinh thuc" +
//                                        enter + "Tuong thuat truc tiep tung giai soan: " + commandCode + " gui8584";
//                                alert = "Ban da dang ky thanh cong KQXSMB " + currentDDMM +
//                                        ". De biet cap so ban <KET> va 10 cap so ve nhieu nhat va it nhat trong 60 ngay gan day nhat." +
//                                        "Soan: SOI <capso KET> gui8384.";
                                alert = dbTool.getTXTData_1("XS", "CHO");
                                if (!alert.equalsIgnoreCase("")) {
                                    alert = alert.replaceAll("!yyy!", currentDDMM);
                                } else {
                                    alert = "Ban da dang ky KQXSMB " + currentDDMM +
                                            ". De biet cap so ban<KET>va 10 cap so ve nhieu nhat va it nhat trong 30 ngay gan day." +
                                            "Soan: SOI <soKET>gui8384.VD: SOI 68 gui 8384";
                                }
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":Waiting:" + serviceId);
                                }
                            } else {
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB:<==" + serviceId);
                                }
                            }
                        } else if (code1.equals("CHO")) {
                            if (resultDate.equals(currentDDMMYY) && (resultStatus == 0)) {
                                // In A time distance
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB:<==" + serviceId);
                                }
                            } else {
                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                lotteryDaily.setRemain(1);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setRequestId(requestId);
                                dbTool.add2LotteryDaily(lotteryDaily);
                                alert = "Hien nay he thong chua co ket qua MB day du ngay " + currentDDMM +
                                        ",he thong se gui ban ngay khi co kq chinh thuc" +
                                        enter + "Tuong thuat truc tiep tung giai soan: " + commandCode + " gui8584";
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":Waiting:" + serviceId);
                                }
                            }
                        } else if (code1.equals("HUY") || code1.equals("XOA")) {
                            alert = "Ban vua huy bo nhan ket MienBac tu he thong 8x84.DT ho tro 1900561577";
                            if (dbTool.removeLotteryDaily(userId, new BigDecimal(1))) { //CompanyId of MB is 1

                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":RemoveFromDaily:MB:<==" + serviceId);
                                }
                            }
                        } else if (dateFormat) {
                            if (currentDDMMYY.equals(dd + "/" + mm + "/" + yy)) {
                                if (resultDate.equals(currentDDMMYY) && (resultStatus == 0)) {
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + commandCode + ":MB:<==" + serviceId);
                                    }
                                } else {
                                    //Add to live
                                    LotteryDaily lotteryDaily = new LotteryDaily();
                                    lotteryDaily.setUserId(userId);
                                    lotteryDaily.setServiceId(serviceId);
                                    lotteryDaily.setCommandCode(commandCode);
                                    lotteryDaily.setMobileOperator(mobileOperator);
                                    lotteryDaily.setCompanyId(companyId);
                                    lotteryDaily.setRemain(1);
                                    lotteryDaily.setInfo(info);
                                    lotteryDaily.setRequestId(requestId);
                                    dbTool.add2LotteryDaily(lotteryDaily);

                                    alert = "Ban dang ky nhan kqMienBac ngay " + currentDDMM + " thanh cong.De SOI CAU ngay " + currentDDMM +
                                            " moi ban soan SOI gui 8584" + enter + "CHUC MAY MAN";
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                            subCode3)) {
                                        MyLogger.log(userId + "<==" + commandCode + "<=Waiting<=" + serviceId);
                                    }
                                }
                            } else {
                                String resultRequestByDate = dbTool.getResultByDate(companyId, dd + "/" + mm + "/" + yy);
                                if (!resultRequestByDate.equals("")) {
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, resultRequestByDate, 1, requestId, subCode1,
                                            subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + commandCode + ":MB:<==" + serviceId);
                                    }
                                } else {
                                    if (status != 0) {
                                        alert = "Ket qua Xo so MienBac ngay ban yeu cau khong co trong he thong" + enter +
                                                "Soan " + commandCode + " Ngay/thang gui8184,vidu: " + commandCode + " 25/10 gui8184.DT ho tro 1900561577";
                                        if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                                subCode3)) {
                                            MyLogger.log(userId + "<==" + commandCode + ":MB:Not Found<==" + serviceId);
                                        }
                                    } else {
                                        dbTool.updateStatus(requestId, 1, mobileOperator);
                                    }
                                }
                            }
                        } else {
                            if (status != 0) {
                                alert = "Tin nhan sai dinh dang.Soan " + commandCode + " gui8184 lay kq MienBac.Soan " + commandCode +
                                        " Ngay/thang gui8184 lay kq theo ngay,vidu: " + commandCode + " 25/10 gui8184.DT ho tro 1900561577";
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                        subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB:Not Found<==" + serviceId);
                                }
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (serviceId.endsWith("8284") || serviceId.endsWith("8384") || serviceId.endsWith("8484") || serviceId.endsWith("8584") ||
                            serviceId.endsWith("8684")) {
                        if (isNoParam) {
                            if ((resultDate.equals(currentDDMMYY) && (resultStatus == 0)) || (HH24 >= 0 && HH24 < 16)) {
                                // In A time distance
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB:<==" + serviceId);
                                }

                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                if (serviceId.endsWith("8284")) {
                                    remain = 1;
                                } else if (serviceId.endsWith("8384")) {
                                    remain = 2;
                                }
                                if (serviceId.endsWith("8484") || serviceId.endsWith("8584") || serviceId.endsWith("8684")) {
                                    remain = 8;
                                }
                                lotteryDaily.setRemain(remain);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setStatus(resultStatus);
                                lotteryDaily.setRequestId(requestId);
                                lotteryDaily.setResponseDate(resultDate);
                                dbTool.add2LotteryDaily(lotteryDaily);

                                if (HH24 >= 19 && HH24 <= 23) {
                                    //Return result of next day
                                    if (serviceId.equals("8484") || serviceId.equals("8584") || serviceId.equals("8684")) {
                                        alert = "Ban da dang ky TuongThuat kqMienBac ngay " + DateProc.getNextDateDDMM() + enter +
                                                "Ban duoc TuongThuat kqMienBac mien phi 5ngay lien tiep neu 2 so cuoi DT cua ban trung voi 2 so cuoi giai DB";
                                    } else {
                                        alert = "Ban se duoc TuongThuat kqMienBac ngay " + DateProc.getNextDateDDMM() +
                                                " trong thoi gian som nhat.De xem thong tin SOI CAU moi soan: CAU gui8584.CHUC MAY MAN";
                                    }
                                } else {
                                    if (serviceId.equals("8484") || serviceId.equals("8584") || serviceId.equals("8684")) {
                                        alert = "Ban da dang ky TuongThuat kqMienBac ngay " + DateProc.getCurrentDDMM() + enter +
                                                "Ban duoc TuongThuat kqMienBac mien phi 5ngay lien tiep neu 2 so cuoi DT cua ban trung voi 2 so cuoi giai DB.";
                                    } else {
                                        alert = "Ban se nhan duoc TuongThuat kqMienBac ngay " + currentDDMM +
                                                " trong thoi gian som nhat.De xem thong tin SOI CAU moi soan: CAU gui8584.CHUC MAY MAN";
                                    }
                                }
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB&Waiting<==" + serviceId);
                                }
                            } else if (!resultDate.equals(currentDDMMYY) && (HH24 >= 16)) {
                                // In B or C time distance
                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                if (serviceId.endsWith("8284")) {
//                                    remain = 1;
                                    remain = 2;
                                } else if (serviceId.endsWith("8384")) {
                                    remain = 2;
                                }
                                if (serviceId.endsWith("8484") || serviceId.endsWith("8584") || serviceId.endsWith("8684")) {
                                    remain = 8;
                                }
                                lotteryDaily.setRemain(remain);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setStatus(resultStatus);
                                lotteryDaily.setRequestId(requestId);
                                dbTool.add2LotteryDaily(lotteryDaily);

                                if (serviceId.equals("8484") || serviceId.equals("8584") || serviceId.equals("8684")) {
//                                    alert = "Ban da dang ky TuongThuat kqMienBac ngay " + currentDDMM + enter +
//                                            "Ban duoc TuongThuat kqMienBac mien phi 5ngay lien tiep neu 2 so cuoi DT cua ban trung voi 2 so cuoi giai DB.";
                                    alert = dbTool.getTXTData_1("XS", "TT");
                                    if (!alert.equalsIgnoreCase("")) {
                                        alert = alert.replaceAll("!yyy!", DateProc.getCurrentDDMM());
                                    } else {
                                        alert = "Ban da dang ky TuongThuat kqMienBac ngay " + DateProc.getCurrentDDMM() + enter +
                                                "Ban duoc TuongThuat kqMienBac mien phi 5ngay lien tiep neu 2 so cuoi DT cua ban trung voi 2 so cuoi giai DB.";
                                    }
                                } else {
                                    alert = "Ban se nhan duoc TuongThuat kqMienBac ngay " + currentDDMM +
                                            " trong thoi gian som nhat.De xem thong tin SOI CAU moi soan: CAU gui8584.CHUC MAY MAN";
                                }
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB Wait:<==" + serviceId);
                                }
                            } else if ((resultStatus != 0) && (HH24 >= 16)) {
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB Live:<==" + serviceId);
                                }
                                // In B or C time distance
                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                if (serviceId.endsWith("8284")) {
                                    remain = 1;
                                } else if (serviceId.endsWith("8384")) {
                                    remain = 2;
                                }
                                if (serviceId.endsWith("8484") || serviceId.endsWith("8584") || serviceId.endsWith("8684")) {
                                    remain = 8;
                                }
                                lotteryDaily.setRemain(remain);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setStatus(resultStatus);
                                lotteryDaily.setResponseDate(resultDate);
                                lotteryDaily.setRequestId(requestId);
                                dbTool.add2LotteryDaily(lotteryDaily);

//                                if (serviceId.equals("8484") || serviceId.equals("8584") || serviceId.equals("8684")) {
//                                    alert = "Ban da dang ky TuongThuat kqMienBac ngay " + currentDDMM + enter +
//                                            "Ban duoc TuongThuat kqMienBac mien phi 5ngay lien tiep neu 2 so cuoi DT cua ban trung voi 2 so cuoi giai DB.";
//                                } else {
//                                    alert = "Ban se nhan duoc TuongThuat kqMienBac ngay " + currentDDMM +
//                                            " trong thoi gian som nhat.De xem thong tin SOI CAU moi soan: CAU gui8584.CHUC MAY MAN";
//                                }
//                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3)) {
//                                    MyLogger.log(userId + "<==" + commandCode + ":MB Wait:<==" + serviceId);
//                                }
                            } else {
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB:<==" + serviceId);
                                }

                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                if (serviceId.endsWith("8284")) {
                                    remain = 1;
                                } else if (serviceId.endsWith("8384")) {
                                    remain = 2;
                                }
                                if (serviceId.endsWith("8484") || serviceId.endsWith("8584") || serviceId.endsWith("8684")) {
                                    remain = 8;
                                }
                                lotteryDaily.setRemain(remain);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setStatus(resultStatus);
                                lotteryDaily.setRequestId(requestId);
                                lotteryDaily.setResponseDate(resultDate);
                                dbTool.add2LotteryDaily(lotteryDaily);
                                if (HH24 >= 19 && HH24 <= 23) {
                                    //Return result of next day
                                    if (serviceId.equals("8484") || serviceId.equals("8584") || serviceId.equals("8684")) {
                                        alert = "Ban da dang ky TuongThuat kqMienBac ngay " + DateProc.getCurrentDDMM() + enter +
                                                "Ban duoc TuongThuat kqMienBac mien phi 5ngay lien tiep neu 2 so cuoi DT cua ban trung voi 2 so cuoi giai DB.";
                                    } else {
                                        alert = "Ban se duoc TuongThuat kqMienBac ngay " + DateProc.getCurrentDDMM() +
                                                " trong thoi gian som nhat.De xem thong tin SOI CAU moi ban soan: CAU gui8584.CHUC MAY MAN";
                                    }
                                } else {
                                    if (serviceId.equals("8484") || serviceId.equals("8584") || serviceId.equals("8684")) {
                                        alert = "Ban da dang ky TuongThuat kqMienBac ngay " + DateProc.getNextDateDDMM() + enter +
                                                "Ban duoc TuongThuat kqMienBac mien phi 5ngay lien tiep neu 2 so cuoi DT cua ban trung voi 2 so cuoi giai DB.";
                                    } else {
                                        alert = "Ban se nhan duoc TuongThuat kqMienBac ngay " + DateProc.getNextDateDDMM() +
                                                " trong thoi gian som nhat.De xem thong tin SOI CAU moi ban soan: CAU gui8584.CHUC MAY MAN";
                                    }
                                }
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":Waiting:" + serviceId);
                                }
                            }
                        } else if (code1.equals("CHO")) {
                            if (resultDate.equals(currentDDMMYY) && (resultStatus == 0)) {
                                // In A time distance
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB:<==" + serviceId);
                                }

                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                if (serviceId.endsWith("8284")) {
                                    remain = 1;
                                } else if (serviceId.endsWith("8384")) {
                                    remain = 2;
                                }
                                if (serviceId.endsWith("8484") || serviceId.endsWith("8584") || serviceId.endsWith("8684")) {
                                    remain = 8;
                                }
                                lotteryDaily.setRemain(remain);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setStatus(resultStatus);
                                lotteryDaily.setRequestId(requestId);
                                lotteryDaily.setResponseDate(resultDate);
                                dbTool.add2LotteryDaily(lotteryDaily);

                                if (HH24 >= 19 && HH24 <= 23) {
                                    //Return result of next day
                                    if (serviceId.equals("8484") || serviceId.equals("8584") || serviceId.equals("8684")) {
                                        alert = "Ban da dang ky TuongThuat kqMienBac ngay " + DateProc.getNextDateDDMM() + enter +
                                                "Ban duoc TuongThuat kqMienBac mien phi 5ngay lien tiep neu 2 so cuoi DT cua ban trung voi 2 so cuoi giai DB";
                                    } else {
                                        alert = "Ban se duoc TuongThuat kqMienBac ngay " + DateProc.getNextDateDDMM() +
                                                " trong thoi gian som nhat.De xem thong tin SOI CAU moi soan: CAU gui8584.CHUC MAY MAN";
                                    }
                                } else {
                                    if (serviceId.equals("8484") || serviceId.equals("8584") || serviceId.equals("8684")) {
                                        alert = "Ban da dang ky TuongThuat kqMienBac ngay " + DateProc.getCurrentDDMM() + enter +
                                                "Ban duoc TuongThuat kqMienBac mien phi 5ngay lien tiep neu 2 so cuoi DT cua ban trung voi 2 so cuoi giai DB.";
                                    } else {
                                        alert = "Ban se nhan duoc TuongThuat kqMienBac ngay " + DateProc.getCurrentDDMM() +
                                                " trong thoi gian som nhat.De xem thong tin SOI CAU moi soan: CAU gui8584.CHUC MAY MAN";
                                    }
                                }
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB&Waiting<==" + serviceId);
                                }
                            } else {
                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                if (serviceId.endsWith("8284")) {
                                    remain = 1;
                                } else if (serviceId.endsWith("8384")) {
                                    remain = 2;
                                }
                                if (serviceId.endsWith("8484") || serviceId.endsWith("8584") || serviceId.endsWith("8684")) {
                                    remain = 8;
                                }
                                lotteryDaily.setRemain(remain);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setRequestId(requestId);
                                dbTool.add2LotteryDaily(lotteryDaily);
                                alert = "Hien nay he thong chua co ket qua MB day du ngay " + currentDDMM +
                                        ",he thong se gui ban ngay khi co kq chinh thuc" +
                                        enter + "Tuong thuat truc tiep tung giai soan: " + commandCode + " gui8584";
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":Waiting:" + serviceId);
                                }
                            }
                        } else if (code1.equals("HUY") || code1.equals("XOA")) {
                            alert = "Ban vua huy bo nhan ket MienBac tu he thong 8x84.DT ho tro 1900561577";
                            if (dbTool.removeLotteryDaily(userId, new BigDecimal(1))) { //CompanyId of MB is 1

                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":RemoveFromDaily:MB:<==" + serviceId);
                                }
                            }
                        } else if (dateFormat) {
                            if (currentDDMMYY.equals(dd + "/" + mm + "/" + yy)) {
                                if (resultDate.equals(currentDDMMYY) && (resultStatus == 0)) {
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + commandCode + ":MB:<==" + serviceId);
                                    }
                                    LotteryDaily lotteryDaily = new LotteryDaily();
                                    lotteryDaily.setUserId(userId);
                                    lotteryDaily.setServiceId(serviceId);
                                    lotteryDaily.setCommandCode(commandCode);
                                    lotteryDaily.setMobileOperator(mobileOperator);
                                    lotteryDaily.setCompanyId(companyId);
                                    if (serviceId.endsWith("8284")) {
                                        remain = 1;
                                    } else if (serviceId.endsWith("8384")) {
                                        remain = 2;
                                    }
                                    if (serviceId.endsWith("8484") || serviceId.endsWith("8584") || serviceId.endsWith("8684")) {
                                        remain = 8;
                                    }
                                    lotteryDaily.setRemain(remain);
                                    lotteryDaily.setInfo(info);
                                    lotteryDaily.setStatus(resultStatus);
                                    lotteryDaily.setRequestId(requestId);
                                    lotteryDaily.setResponseDate(resultDate);
                                    dbTool.add2LotteryDaily(lotteryDaily);

                                    if (HH24 >= 19 && HH24 <= 23) {
                                        if (serviceId.equals("8484") || serviceId.equals("8584") || serviceId.equals("8684")) {
                                            alert = "Ban da dang ky TuongThuat kqMienBac ngay " + DateProc.getNextDateDDMM() + enter +
                                                    "Ban duoc TuongThuat kqMienBac mien phi 5ngay lien tiep neu 2 so cuoi DT cua ban trung voi 2 so cuoi giai DB";
                                        } else {
                                            alert = "Ban se duoc TuongThuat kqMienBac ngay " + DateProc.getNextDateDDMM() +
                                                    " trong thoi gian som nhat.De xem thong tin SOI CAU moi soan: CAU gui8584.CHUC MAY MAN";
                                        }
                                    } else {
                                        //Return result of next day
                                        if (serviceId.equals("8484") || serviceId.equals("8584") || serviceId.equals("8684")) {
                                            alert = "Ban da dang ky TuongThuat kqMienBac ngay " + DateProc.getCurrentDDMM() + enter +
                                                    "Ban duoc TuongThuat kqMienBac mien phi 5ngay lien tiep neu 2 so cuoi DT cua ban trung voi 2 so cuoi giai DB.";
//                                            alert = dbTool.getTXTData_1("XS", "TT");
//                                            if (!alert.equalsIgnoreCase("")) {
//                                                alert = alert.replaceAll("!yyy!", DateProc.getCurrentDDMM());
//                                            } else {
//                                                alert = "Ban da dang ky TuongThuat kqMienBac ngay " + DateProc.getCurrentDDMM() + enter +
//                                                        "Ban duoc TuongThuat kqMienBac mien phi 5ngay lien tiep neu 2 so cuoi DT cua ban trung voi 2 so cuoi giai DB.";
//                                            }
                                        } else {
                                            alert = "Ban se nhan duoc TuongThuat kqMienBac ngay " + currentDDMM +
                                                    " trong thoi gian som nhat.De xem thong tin SOI CAU moi soan: CAU gui8584.CHUC MAY MAN";
                                        }
                                    }
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + commandCode + ":MB&Waiting<==" + serviceId);
                                    }
                                } else {
                                    //Add to live
                                    LotteryDaily lotteryDaily = new LotteryDaily();
                                    lotteryDaily.setUserId(userId);
                                    lotteryDaily.setServiceId(serviceId);
                                    lotteryDaily.setCommandCode(commandCode);
                                    lotteryDaily.setMobileOperator(mobileOperator);
                                    lotteryDaily.setCompanyId(companyId);
                                    if (serviceId.endsWith("8284")) {
                                        remain = 1;
                                    } else if (serviceId.endsWith("8384")) {
                                        remain = 2;
                                    }
                                    if (serviceId.endsWith("8484") || serviceId.endsWith("8584") || serviceId.endsWith("8684")) {
                                        remain = 8;
                                    }
                                    lotteryDaily.setRemain(remain);
                                    lotteryDaily.setInfo(info);
                                    lotteryDaily.setRequestId(requestId);
                                    dbTool.add2LotteryDaily(lotteryDaily);

                                    alert = "Ban dang ky nhan kqMienBac ngay " + currentDDMM + " thanh cong.De SOI CAU ngay " + currentDDMM +
                                            " moi ban soan: CAU gui8584" + enter + "CHUC MAY MAN";
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                            subCode3)) {
                                        MyLogger.log(userId + "<==" + commandCode + "<=Waiting<=" + serviceId);
                                    }
                                }
                            } else {
                                String resultRequestByDate = dbTool.getResultByDate(companyId, dd + "/" + mm + "/" + yy);
                                if (!resultRequestByDate.equals("")) {
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, resultRequestByDate, 1, requestId, subCode1,
                                            subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + commandCode + ":MB:<==" + serviceId);
                                    }

                                    //Add to live
                                    LotteryDaily lotteryDaily = new LotteryDaily();
                                    lotteryDaily.setUserId(userId);
                                    lotteryDaily.setServiceId(serviceId);
                                    lotteryDaily.setCommandCode(commandCode);
                                    lotteryDaily.setMobileOperator(mobileOperator);
                                    lotteryDaily.setCompanyId(companyId);
                                    if (serviceId.endsWith("8284")) {
                                        remain = 1;
                                    } else if (serviceId.endsWith("8384")) {
                                        remain = 2;
                                    }
                                    if (serviceId.endsWith("8484") || serviceId.endsWith("8584") || serviceId.endsWith("8684")) {
                                        remain = 8;
                                    }
                                    lotteryDaily.setRemain(remain);
                                    lotteryDaily.setInfo(info);
                                    lotteryDaily.setResponseDate(dd + "/" + mm + "/" + yy);
                                    lotteryDaily.setRequestId(requestId);
                                    dbTool.add2LotteryDaily(lotteryDaily);

                                    alert = "Ban dang ky nhan kqMienBac ngay " + currentDDMM + " thanh cong.De SOI CAU ngay " + currentDDMM +
                                            " moi ban soan: CAU gui8584" + enter + "CHUC MAY MAN";
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2,
                                            subCode3)) {
                                        MyLogger.log(userId + "<==" + commandCode + "<=Waiting<=" + serviceId);
                                    }
                                } else {
                                    if (status != 0) {
                                        alert = "Ket qua Xo so MienBac ngay ban yeu cau khong co trong he thong" + enter +
                                                "Soan " + commandCode + " Ngay/thang gui8184,vidu: " + commandCode + " 25/10 gui8184.DT ho tro 1900561577";
                                        if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                                subCode3)) {
                                            MyLogger.log(userId + "<==" + commandCode + ":MB:Not Found<==" + serviceId);
                                        }
                                    } else {
                                        dbTool.updateStatus(requestId, 1, mobileOperator);
                                    }
                                }
                            }
                        } else {
                            if (status != 0) {
                                alert = "Tin nhan sai dinh dang.Soan " + commandCode + " gui8184 lay kq MienBac.Soan " + commandCode +
                                        " Ngay/thang gui8184 lay kq theo ngay,vidu: " + commandCode + " 25/10 gui8184.DT ho tro 1900561577";
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                        subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB:Not Found<==" + serviceId);
                                }
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else if (serviceId.endsWith("8784")) {
                        if (isNoParam) {
                            if ((resultDate.equals(currentDDMMYY) && (resultStatus == 0)) || (HH24 >= 0 && HH24 < 16)) {
                                // In A time distance
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB:<==" + serviceId);
                                }
                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                lotteryDaily.setRemain(Preference.nXS87 - 1);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setStatus(resultStatus);
                                lotteryDaily.setRequestId(requestId);
                                lotteryDaily.setResponseDate(resultDate);
                                dbTool.add2LotteryDaily(lotteryDaily);
                                alert = "Ban da dang ky nhan ketqua XoSo " + Preference.nXS87 + "ngay lien tiep thanh cong." +
                                        "De SOI CAU ngay " + currentDDMM +
                                        " moi ban soan: CAU gui8584" + enter + "CHUC MAY MAN";
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB&Waiting<==" + serviceId);
                                }
                            } else if ((resultStatus != 0) || (HH24 >= 16)) {
                                // In B or C time distance
                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                lotteryDaily.setRemain(Preference.nXS87);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setRequestId(requestId);
                                lotteryDaily.setResponseDate(resultDate);
                                dbTool.add2LotteryDaily(lotteryDaily);

                                alert = "Ban da dang ky nhan ketqua XoSo " + Preference.nXS87 + "ngay lien tiep thanh cong." +
                                        "De SOI CAU ngay " + currentDDMM +
                                        " moi ban soan: CAU gui8584" + enter + "CHUC MAY MAN";
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":Waiting:" + serviceId);
                                }
                            } else {
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB:<==" + serviceId);
                                }
                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                lotteryDaily.setRemain(Preference.nXS87 - 1);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setRequestId(requestId);
                                lotteryDaily.setResponseDate(resultDate);
                                dbTool.add2LotteryDaily(lotteryDaily);

                                alert = "Ban da dang ky nhan ketqua XoSo " + Preference.nXS87 + " ngay lien tiep thanh cong." +
                                        "De SOI CAU ngay " + currentDDMM +
                                        " moi ban soan: CAU gui8584" + enter + "CHUC MAY MAN";
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":Add2Daily:" + serviceId);
                                }
                            }
                        } else if (code1.equals("CHO")) {
                            if (resultDate.equals(currentDDMMYY) && (resultStatus == 0)) {
                                // In A time distance
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB:<==" + serviceId);
                                }

                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                lotteryDaily.setRemain(Preference.nXS87 - 1);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setStatus(resultStatus);
                                lotteryDaily.setRequestId(requestId);
                                lotteryDaily.setResponseDate(resultDate);
                                dbTool.add2LotteryDaily(lotteryDaily);

                                alert = "Ban da dang ky nhan ketqua XoSo " + Preference.nXS87 + " ngay lien tiep thanh cong." +
                                        "De SOI CAU ngay " + currentDDMM +
                                        " moi ban soan: CAU gui8584" + enter + "CHUC MAY MAN";
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":Add2Daily:" + serviceId);
                                }
                            } else {
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB:<==" + serviceId);
                                }
                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                lotteryDaily.setRemain(Preference.nXS87);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setRequestId(requestId);
                                dbTool.add2LotteryDaily(lotteryDaily);
                                alert = "Ban da dang ky nhan ketqua XoSo " + Preference.nXS87 + " ngay lien tiep thanh cong." +
                                        "De SOI CAU ngay " + currentDDMM +
                                        " moi ban soan: CAU gui8584" + enter + "CHUC MAY MAN";
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":Waiting:" + serviceId);
                                }
                            }
                        } else if (code1.equals("HUY") || code1.equals("XOA")) {
                            alert = "Ban vua huy bo nhan ket MienBac tu he thong 8x84.DT ho tro 1900561577";
                            if (dbTool.removeLotteryDaily(userId, new BigDecimal(1))) { //CompanyId of MB is 1

                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":RemoveFromDaily:MB:<==" + serviceId);
                                }
                            }
                        } else if (dateFormat) {
                            if (currentDDMMYY.equals(dd + "/" + mm + "/" + yy)) {
                                if (resultDate.equals(currentDDMMYY) && (resultStatus == 0)) {
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + commandCode + ":MB:<==" + serviceId);
                                    }
                                    LotteryDaily lotteryDaily = new LotteryDaily();
                                    lotteryDaily.setUserId(userId);
                                    lotteryDaily.setServiceId(serviceId);
                                    lotteryDaily.setCommandCode(commandCode);
                                    lotteryDaily.setMobileOperator(mobileOperator);
                                    lotteryDaily.setCompanyId(companyId);
                                    lotteryDaily.setRemain(Preference.nXS87 - 1);
                                    lotteryDaily.setInfo(info);
                                    lotteryDaily.setStatus(resultStatus);
                                    lotteryDaily.setRequestId(requestId);
                                    lotteryDaily.setResponseDate(resultDate);
                                    dbTool.add2LotteryDaily(lotteryDaily);
                                    alert = "Ban da dang ky nhan ketqua XoSo " + Preference.nXS87 + " ngay lien tiep thanh cong." +
                                            "De SOI CAU ngay " + currentDDMM +
                                            " moi ban soan: CAU gui8584" + enter + "CHUC MAY MAN";
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + commandCode + ":Waiting:" + serviceId);
                                    }
                                } else {
                                    //Add to live
                                    LotteryDaily lotteryDaily = new LotteryDaily();
                                    lotteryDaily.setUserId(userId);
                                    lotteryDaily.setServiceId(serviceId);
                                    lotteryDaily.setCommandCode(commandCode);
                                    lotteryDaily.setMobileOperator(mobileOperator);
                                    lotteryDaily.setCompanyId(companyId);
                                    lotteryDaily.setRemain(Preference.nXS87);
                                    lotteryDaily.setInfo(info);
                                    lotteryDaily.setRequestId(requestId);
                                    dbTool.add2LotteryDaily(lotteryDaily);

                                    alert = "Ban da dang ky nhan ketqua XoSo " + Preference.nXS87 + " ngay lien tiep thanh cong." +
                                            "De SOI CAU ngay " + currentDDMM +
                                            " moi ban soan: CAU gui8584" + enter + "CHUC MAY MAN";
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + commandCode + ":Waiting:" + serviceId);
                                    }
                                }
                            } else {
                                String resultRequestByDate = dbTool.getResultByDate(companyId, dd + "/" + mm + "/" + yy);
                                if (!resultRequestByDate.equals("")) {
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, resultRequestByDate, 1, requestId, subCode1,
                                            subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + commandCode + ":MB:<==" + serviceId);
                                    }

                                    //Add to live
                                    LotteryDaily lotteryDaily = new LotteryDaily();
                                    lotteryDaily.setUserId(userId);
                                    lotteryDaily.setServiceId(serviceId);
                                    lotteryDaily.setCommandCode(commandCode);
                                    lotteryDaily.setMobileOperator(mobileOperator);
                                    lotteryDaily.setCompanyId(companyId);
                                    lotteryDaily.setRemain(Preference.nXS87);
                                    lotteryDaily.setRemain(remain);
                                    lotteryDaily.setInfo(info);
                                    lotteryDaily.setRequestId(requestId);
                                    dbTool.add2LotteryDaily(lotteryDaily);

                                    alert = "Ban da dang ky nhan ketqua XoSo " + Preference.nXS87 + " ngay lien tiep thanh cong." +
                                            "De SOI CAU ngay " + currentDDMM +
                                            " moi ban soan: CAU gui8584" + enter + "CHUC MAY MAN";
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + commandCode + ":Add2Daily:" + serviceId);
                                    }
                                } else {
                                    if (status != 0) {
                                        alert = "Ket qua Xo so MienBac ngay ban yeu cau khong co trong he thong" + enter +
                                                "Soan " + commandCode + " Ngay/thang gui8184,vidu: " + commandCode + " 25/10 gui8184.DT ho tro 1900561577";
                                        if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                                subCode3)) {
                                            MyLogger.log(userId + "<==" + commandCode + ":MB:Not Found<==" + serviceId);
                                        }
                                    } else {
                                        dbTool.updateStatus(requestId, 1, mobileOperator);
                                    }
                                }
                            }
                        } else {
                            if (status != 0) {
                                alert = "Tin nhan sai dinh dang.Soan " + commandCode + " gui8184 lay kq MienBac.Soan " + commandCode +
                                        " Ngay/thang gui8184 lay kq theo ngay,vidu: " + commandCode + " 25/10 gui8184.DT ho tro 1900561577";
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                        subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MB:Not Found<==" + serviceId);
                                }
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    }
                } else if (companyId.intValue() == 2) { //Is MienTrung

                    subCode1 = "2";

                    if (((commandCode.equals("XS") && (serviceId.equals("8184") || serviceId.equals("8284"))) ||
                            ((commandCode.equals("SMT") || commandCode.equals("XMT") || commandCode.equals("HGT") || commandCode.equals("QMT")) &&
                            (serviceId.equals("8184") || serviceId.equals("8284") || serviceId.equals("8384")) || serviceId.equals("8484"))) &&
                            !Preference.listAdmin.contains(userId)) {

                        if (commandCode.equals("XMT")) {
                            wrongMTMsg = "De nhan ket qua xs MienTrung moi nhat soan XMT gui 8584,gui 8784 de nhan kq xo so MienTrung " +
                                    Preference.nXSMT87 +
                                    " ngay lien tiep,dien thoai ho tro 1900561577.";
                        } else if (commandCode.equals("SMT")) {
                            wrongMTMsg = "De nhan ket qua xs MienTrung moi nhat soan SMT gui 8584,gui 8784 de nhan kq xo so MienTrung " +
                                    Preference.nXSMT87 +
                                    " ngay lien tiep,dien thoai ho tro 1900561577.";
                        } else if (commandCode.equals("HGT")) {
                            wrongMTMsg = "De nhan ket qua xs MienTrung moi nhat soan HGT gui 8584,gui 8784 de nhan kq xo so MienTrung " +
                                    Preference.nXSMT87 +
                                    " ngay lien tiep,dien thoai ho tro 1900561577.";
                        } else if (commandCode.equals("QMT")) {
                            wrongMTMsg = "De nhan ket qua xs MienTrung moi nhat soan QMT gui 8584,gui 8784 de nhan kq xo so MienTrung " +
                                    Preference.nXSMT87 +
                                    " ngay lien tiep,dien thoai ho tro 1900561577.";
                        } else {
                            wrongMTMsg = "De nhan ket qua xs MienTrung moi nhat soan XSMT gui 8384,gui 8784 de nhan kq xo so MienTrung " +
                                    Preference.nXSMT87 +
                                    " ngay lien tiep,dien thoai ho tro 1900561577.";
                        }
                        if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, wrongMTMsg, 1, requestId, subCode1, subCode2, subCode3)) {
                            MyLogger.log(userId + "<==Wrong ServiceId:" + serviceId);
                        }

                    } else if (serviceId.endsWith("8784")) {
                        LotteryDaily lotteryDaily = new LotteryDaily();
                        lotteryDaily.setUserId(userId);
                        lotteryDaily.setServiceId(serviceId);
                        lotteryDaily.setMobileOperator(mobileOperator);
                        lotteryDaily.setCommandCode(commandCode);
                        lotteryDaily.setCompanyId(companyId);
                        lotteryDaily.setRequestId(requestId);
                        lotteryDaily.setInfo(info);

                        //Send current result for this daily
                        boolean haveResultToday = false;
                        Vector getListCompanyMTToDay = Utilities.getListCompanyMTToday();
                        for (int i = 0; i < getListCompanyMTToDay.size(); i++) {
                            companyId = (BigDecimal) getListCompanyMTToDay.elementAt(i);
                            lotteryResult = (LotteryResult) Main.lotteryResultHT.get(companyId);
                            result = lotteryResult.getResult();
                            resultDate = lotteryResult.getResultDate();

                            result = result.replaceAll("XSMT", commandCode);

                            if (commandCode.equals("SMT")) {
                                result = result.replaceAll("CAU", "SC");
                            }

                            if ((!result.equals("")) && (resultDate.equals(currentDDMMYY))) {
                                haveResultToday = true;
                                break;
                            }
                        }

                        if (haveResultToday) {
                            if (commandCode.equals("SMT")) {
                                lotteryDaily.setRemain(6); //6 days

                            } else {
                                lotteryDaily.setRemain(Preference.nXSMT87 - 1);
                            }
                            lotteryDaily.setResponseDate(currentDDMMYY);
                        } else {
                            if (commandCode.equals("SMT")) {
                                lotteryDaily.setRemain(7); //7 days

                            } else {
                                lotteryDaily.setRemain(Preference.nXSMT87);
                            }
                        }

                        if (dbTool.add2LotteryDailyLocation(lotteryDaily)) {
                            if (commandCode.equals("SMT")) {
                                alert = "Ban se nhan duoc ket qua xo so " + lotteryCompany.getCompanyDesc() + " trong 7" +
                                        " ngay lien tiep.Chuc ban may man.";
                            } else {
                                alert = "Ban se nhan duoc ket qua xo so " + lotteryCompany.getCompanyDesc() + " trong " + Preference.nXSMT87 +
                                        " ngay lien tiep.Chuc ban may man.";
                            }
                            if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3)) {
                                MyLogger.log(userId + "<==Add2DailyLocation:MT:" + serviceId);
                            }
                        }

                        if (haveResultToday) { //One Company lottery have result

                            for (int i = 0; i < getListCompanyMTToDay.size(); i++) {
                                companyId = (BigDecimal) getListCompanyMTToDay.elementAt(i);
                                lotteryResult = (LotteryResult) Main.lotteryResultHT.get(companyId);
                                result = lotteryResult.getResult();
                                resultDate = lotteryResult.getResultDate();

                                result = result.replaceAll("XSMT", commandCode);
                                if ((!result.equals("")) && (resultDate.equals(currentDDMMYY))) {
                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 0, requestId, subCode1, subCode2, subCode3);
                                } else {
                                    lotteryDaily.setCompanyId(companyId);
                                    dbTool.add2LotteryDaily(lotteryDaily);
                                }
                            }
                        } else {
                            //Send current result for this daily
                            GregorianCalendar newCal = new GregorianCalendar();
                            int day = newCal.get(Calendar.DAY_OF_WEEK);
                            if (day == 1) {
                                day = 7;
                            } else {
                                day = day - 1;
                            }
                            Vector getListCompanyMTbyDay = Utilities.getListCompanyMTByDayOfWeek(day);
                            for (int i = 0; i < getListCompanyMTbyDay.size(); i++) {
                                companyId = (BigDecimal) getListCompanyMTbyDay.elementAt(i);
                                lotteryResult = (LotteryResult) Main.lotteryResultHT.get(companyId);
                                result = lotteryResult.getResult();

                                result = result.replaceAll("XSMT", commandCode);
                                if (commandCode.equals("SMT")) {
                                    result = result.replaceAll("CAU", "SC");
                                }

                                if (!result.equals("")) {
                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 0, requestId, subCode1, subCode2, subCode3);
                                }
                            }
                        }
                    } else { //8284,8384,8484,8584,8684

                        if (isNoParam) {
                            if ((HH24 >= 14) && (HH24 <= 19)) {
                                Vector vListMTToday = Utilities.getListCompanyMTToday();
                                deductMoney = false;
                                for (int i = 0; i < vListMTToday.size(); i++) {
                                    companyId = (BigDecimal) vListMTToday.elementAt(i);
                                    lotteryResult = (LotteryResult) Main.lotteryResultHT.get(companyId);
                                    result = lotteryResult.getResult();
                                    resultStatus = lotteryResult.getResultStatus();
                                    resultDate = lotteryResult.getResultDate();

                                    result = result.replaceAll("XSMT", commandCode);
                                    if (commandCode.equals("SMT")) {
                                        result = result.replaceAll("CAU", "SC");
                                    }

                                    if (resultDate.equals(currentDDMMYY)) {
                                        if (!deductMoney) {
                                            if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2,
                                                    subCode3)) {
                                                deductMoney = true;
                                            }
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 0, requestId, subCode1, subCode2,
                                                    subCode3);
                                        }
                                    } else {
                                        LotteryDaily lotteryDaily = new LotteryDaily();
                                        lotteryDaily.setUserId(userId);
                                        lotteryDaily.setServiceId(serviceId);
                                        lotteryDaily.setCommandCode(commandCode);
                                        lotteryDaily.setMobileOperator(mobileOperator);
                                        lotteryDaily.setCompanyId(companyId);
                                        lotteryDaily.setRemain(1);
                                        lotteryDaily.setInfo(info);
                                        lotteryDaily.setRequestId(requestId);
                                        dbTool.add2LotteryDaily(lotteryDaily);
                                    }
                                } //End for

                                if (!deductMoney) {
                                    if (commandCode.equals("SMT")) {
                                        alert = "Hien nay he thong chua co ket qua xo so cac tinh " +
                                                lotteryCompany.getCompanyDesc() +
                                                ".Ban se nhan duoc ngay khi he thong co kq chinh thuc.Soi cau,soan: SC TenTinh gui8584.Chuc ban may man!";
                                    } else {
                                        alert = "Hien nay he thong chua co ket qua xo so cac tinh " +
                                                lotteryCompany.getCompanyDesc() +
                                                ".Ban se nhan duoc ngay khi he thong co kq chinh thuc.Soi cau,soan: CAU TenTinh gui8584.Chuc ban may man!";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                }
                                MyLogger.log(userId + "<==" + serviceId);
                            } else {
                                GregorianCalendar newCal = new GregorianCalendar();
                                int day = newCal.get(Calendar.DAY_OF_WEEK);
                                if ((HH24 >= 0) && (HH24 <= 15)) {
                                    if (day == 1) {
                                        day = 7;
                                    } else {
                                        day = day - 1;
                                    }
                                }
                                Vector getListCompanyMTByDayOfWeek = Utilities.getListCompanyMTByDayOfWeek(day);
                                deductMoney = false;
                                for (int i = 0; i < getListCompanyMTByDayOfWeek.size(); i++) {
                                    companyId = (BigDecimal) getListCompanyMTByDayOfWeek.elementAt(i);
                                    lotteryResult = (LotteryResult) Main.lotteryResultHT.get(companyId);
                                    result = lotteryResult.getResult();

                                    result = result.replaceAll("XSMT", commandCode);
                                    if (commandCode.equals("SMT")) {
                                        result = result.replaceAll("CAU", "SC");
                                    }

                                    if (!result.equals("")) {
                                        if (!deductMoney) {
                                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2,
                                                    subCode3);
                                            deductMoney = true;
                                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 0, requestId, subCode1, subCode2,
                                                    subCode3);
                                        }
                                    }
                                } //End for

                            }
                        } else if (dateFormat) {
                            if (currentDDMMYY.equals(dd + "/" + mm + "/" + yy)) {
                                Vector vListMTToday = Utilities.getListCompanyMTToday();
                                deductMoney = false;
                                for (int i = 0; i < vListMTToday.size(); i++) {
                                    companyId = (BigDecimal) vListMTToday.elementAt(i);
                                    lotteryResult = (LotteryResult) Main.lotteryResultHT.get(companyId);
                                    result = lotteryResult.getResult();
                                    resultStatus = lotteryResult.getResultStatus();
                                    resultDate = lotteryResult.getResultDate();

                                    result = result.replaceAll("XSMT", commandCode);
                                    if (commandCode.equals("SMT")) {
                                        result = result.replaceAll("CAU", "SC");
                                    }

                                    if (resultDate.equals(currentDDMMYY)) {
                                        if (!deductMoney) {
                                            if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2,
                                                    subCode3)) {
                                                deductMoney = true;
                                                MyLogger.log(userId + "<==" + commandCode + " MT:" + dd + ":" + mm + "<==" + serviceId);
                                            }
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 0, requestId, subCode1, subCode2,
                                                    subCode3);
                                        }
                                    } else {
                                        LotteryDaily lotteryDaily = new LotteryDaily();
                                        lotteryDaily.setUserId(userId);
                                        lotteryDaily.setServiceId(serviceId);
                                        lotteryDaily.setCommandCode(commandCode);
                                        lotteryDaily.setMobileOperator(mobileOperator);
                                        lotteryDaily.setCompanyId(companyId);
                                        lotteryDaily.setRemain(1);
                                        lotteryDaily.setInfo(info);
                                        lotteryDaily.setRequestId(requestId);
                                        dbTool.add2LotteryDaily(lotteryDaily);
                                    }
                                } //End for

                                if (!deductMoney) {
                                    if (commandCode.equals("SMT")) {
                                        alert = "Hien nay he thong chua co ket qua xo so cac tinh " +
                                                lotteryCompany.getCompanyDesc() +
                                                ".Ban se nhan duoc ngay khi he thong co kq chinh thuc.Soi cau,soan: SC TenTinh gui8584.Chuc ban may man!";
                                    } else {
                                        alert = "Hien nay he thong chua co ket qua xo so cac tinh " +
                                                lotteryCompany.getCompanyDesc() +
                                                ".Ban se nhan duoc ngay khi he thong co kq chinh thuc.Soi cau,soan: CAU TenTinh gui8584.Chuc ban may man!";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==XSMT " + dd + ":" + mm + " Waiting<==" + serviceId);
                                }
                            } else {
                                Vector v = dbTool.getResultByDateLocation(companyId, dd + mm);
                                deductMoney = false;
                                if (v.size() > 0) {
                                    for (int i = 0; i < v.size(); i++) {
                                        String resultRequestByDate = (String) v.elementAt(i);

                                        resultRequestByDate = resultRequestByDate.replaceAll("XSMT", commandCode);

                                        if (!deductMoney) {
                                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, resultRequestByDate, 1, requestId, subCode1,
                                                    subCode2, subCode3);
                                            deductMoney = true;
                                            MyLogger.log(userId + "<==XSMT " + dd + ":" + mm + "<==" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, resultRequestByDate, 0, requestId, subCode1,
                                                    subCode2, subCode3);
                                        }
                                    } //End for

                                } else {
                                    alert = "Hien nay he khong co ket qua xo so cac tinh " + lotteryCompany.getCompanyDesc() +
                                            " ma ban yeu cau.DT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                }
                            }
                        } else {
                            if (status != 0) {
                                if (commandCode.equals("XMT")) {
                                    alert = "Ket qua Xo so MienTrung ngay ban yeu cau khong co trong he thong" + enter +
                                            "Soan " + commandCode + " Ngay/thang gui8384,vidu: " + commandCode + " 25/10 gui8384.DT ho tro 1900561577";
                                } else if (commandCode.equals("SMT")) {
                                    alert = "Ket qua Xo so MienTrung ngay ban yeu cau khong co trong he thong" + enter +
                                            "Soan " + commandCode + " Ngay/thang gui8584,vidu: " + commandCode + " 25/10 gui8584.DT ho tro 1900561577";
                                } else if (commandCode.equals("HGT")) {
                                    alert = "Ket qua Xo so MienTrung ngay ban yeu cau khong co trong he thong" + enter +
                                            "Soan " + commandCode + " Ngay/thang gui8584,vidu: " + commandCode + " 25/10 gui8584.DT ho tro 1900561577";
                                } else if (commandCode.equals("QMT")) {
                                    alert = "Ket qua Xo so MienTrung ngay ban yeu cau khong co trong he thong" + enter +
                                            "Soan " + commandCode + " Ngay/thang gui8584,vidu: " + commandCode + " 25/10 gui8584.DT ho tro 1900561577";
                                } else {
                                    alert = "Ket qua Xo so MienTrung ngay ban yeu cau khong co trong he thong" + enter +
                                            "Soan " + commandCode + "MT Ngay/thang gui8384,vidu: " + commandCode + "MT 25/10 gui8384.DT ho tro 1900561577";
                                }
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                        subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MT:Not Found<==" + serviceId);
                                }
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } //End MT

                } else if (companyId.intValue() == 3) { //Is MienNam

                    subCode1 = "3";
//                    if ((serviceId.endsWith("8084") || serviceId.endsWith("8184") || serviceId.endsWith("8284")) && !Preference.listAdmin.contains(userId)) {
//                        if (commandCode.equals("XMN")) {
//                            wrongMNMsg = "De nhan ket qua xs MienNam moi nhat soan XMN gui 8384,gui 8784 de nhan kq xo so MienNam " + Preference.nXSMN87 +
//                                         " ngay lien tiep,dien thoai ho tro 1900561577.";
//                        } else {
//                            wrongMNMsg = "De nhan ket qua xs MienNam moi nhat soan XSMN gui 8384,gui 8784 de nhan kq xo so MienNam " + Preference.nXSMN87 +
//                                         " ngay lien tiep,dien thoai ho tro 1900561577.";
//                        }
//                        if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, wrongMNMsg, 1, requestId, subCode1, subCode2, subCode3)) {
//                            MyLogger.log(userId + "<==Wrong ServiceId:" + serviceId);
//                        }
//                    }

                    if (((commandCode.equals("XS") && (serviceId.equals("8184") || serviceId.equals("8284"))) ||
                            ((commandCode.equals("SMN") || commandCode.equals("XMN")) &&
                            (serviceId.equals("8184") || serviceId.equals("8284") || serviceId.equals("8384") || serviceId.equals("8484")))) &&
                            !Preference.listAdmin.contains(userId)) {
                        if (commandCode.equals("XMN")) {
                            wrongMNMsg = "De nhan ket qua xs MienNam moi nhat soan XMN gui 8584,gui 8784 de nhan kq xo so MienNam " + Preference.nXSMN87 +
                                    " ngay lien tiep,dien thoai ho tro 1900561577.";
                        } else if (commandCode.equals("HGN")) {
                            wrongMNMsg = "De nhan ket qua xs MienNam moi nhat soan HGN gui 8584,gui 8784 de nhan kq xo so MienNam " + Preference.nXSMN87 +
                                    " ngay lien tiep,dien thoai ho tro 1900561577.";
                        } else if (commandCode.equals("QMN")) {
                            wrongMNMsg = "De nhan ket qua xs MienNam moi nhat soan QMN gui 8584,gui 8784 de nhan kq xo so MienNam " + Preference.nXSMN87 +
                                    " ngay lien tiep,dien thoai ho tro 1900561577.";
                        }
                        if (commandCode.equals("SMN")) {
                            wrongMNMsg = "De nhan ket qua xs MienNam moi nhat soan SMN gui 8584,gui 8784 de nhan kq xo so MienNam 7" +
                                    " ngay lien tiep,dien thoai ho tro 1900561577.";
                        } else {
                            wrongMNMsg = "De nhan ket qua xs MienNam moi nhat soan XSMN gui 8384,gui 8784 de nhan kq xo so MienNam " + Preference.nXSMN87 +
                                    " ngay lien tiep,dien thoai ho tro 1900561577.";
                        }
                        if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, wrongMNMsg, 1, requestId, subCode1, subCode2, subCode3)) {
                            MyLogger.log(userId + "<==Wrong ServiceId:" + serviceId);
                        }
                    } else if (serviceId.endsWith("8784")) {
                        LotteryDaily lotteryDaily = new LotteryDaily();
                        lotteryDaily.setUserId(userId);
                        lotteryDaily.setServiceId(serviceId);
                        lotteryDaily.setMobileOperator(mobileOperator);
                        lotteryDaily.setCommandCode(commandCode);
                        lotteryDaily.setCompanyId(companyId);
                        lotteryDaily.setRequestId(requestId);
                        lotteryDaily.setInfo(info);

                        //Send current result for this daily
                        boolean haveResultToday = false;
                        Vector getListCompanyMNToDay = Utilities.getListCompanyMNToday();
                        for (int i = 0; i < getListCompanyMNToDay.size(); i++) {
                            companyId = (BigDecimal) getListCompanyMNToDay.elementAt(i);
                            lotteryResult = (LotteryResult) Main.lotteryResultHT.get(companyId);
                            result = lotteryResult.getResult();
                            resultDate = lotteryResult.getResultDate();

                            result = result.replaceAll("XSMN", commandCode);
                            if (commandCode.equals("SMN")) {
                                result = result.replaceAll("CAU", "SC");
                            }

                            if ((!result.equals("")) && (resultDate.equals(currentDDMMYY))) {
                                haveResultToday = true;
                                break;
                            }
                        }

                        if (haveResultToday) {
                            if (commandCode.equals("SMN")) {
                                lotteryDaily.setRemain(6);
                            } else {
                                lotteryDaily.setRemain(Preference.nXSMN87 - 1);
                            }
                            lotteryDaily.setResponseDate(currentDDMMYY);
                        } else {
                            if (commandCode.equals("SMN")) {
                                lotteryDaily.setRemain(7);
                            } else {
                                lotteryDaily.setRemain(Preference.nXSMN87);
                            }
                        }
                        if (dbTool.add2LotteryDailyLocation(lotteryDaily)) {
                            xs87 = "Ban se nhan duoc ket qua xo so MienNam trong " + Preference.nXSMN87 + " ngay lien tiep.Chuc ban may man.";
                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, xs87, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==Add2DailyLocation:MN:" + serviceId);
                        }

                        if (haveResultToday) { //One Company lottery have result

                            for (int i = 0; i < getListCompanyMNToDay.size(); i++) {
                                companyId = (BigDecimal) getListCompanyMNToDay.elementAt(i);
                                lotteryResult = (LotteryResult) Main.lotteryResultHT.get(companyId);
                                result = lotteryResult.getResult();
                                resultDate = lotteryResult.getResultDate();

                                result = result.replaceAll("XSMN", commandCode);
                                if (commandCode.equals("SMN")) {
                                    result = result.replaceAll("CAU", "SC");
                                }

                                if ((!result.equals("")) && (resultDate.equals(currentDDMMYY))) {
                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 0, requestId, subCode1, subCode2, subCode3);
                                } else {
                                    lotteryDaily.setCompanyId(companyId);
                                    dbTool.add2LotteryDaily(lotteryDaily);
                                }
                            }
                        } else {
                            //Send current result for this daily
                            GregorianCalendar newCal = new GregorianCalendar();
                            int day = newCal.get(Calendar.DAY_OF_WEEK);
                            if (day == 1) {
                                day = 7;
                            } else {
                                day = day - 1;
                            }
                            Vector getListCompanyMNbyDay = Utilities.getListCompanyMNByDayOfWeek(day);
                            for (int i = 0; i < getListCompanyMNbyDay.size(); i++) {
                                companyId = (BigDecimal) getListCompanyMNbyDay.elementAt(i);
                                lotteryResult = (LotteryResult) Main.lotteryResultHT.get(companyId);
                                result = lotteryResult.getResult();

                                result = result.replaceAll("XSMN", commandCode);
                                if (commandCode.equals("SMN")) {
                                    result = result.replaceAll("CAU", "SC");
                                }

                                if (!result.equals("")) {
                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 0, requestId, subCode1, subCode2, subCode3);
                                }
                            }
                        }
                    } else { //8284,8384,8484,8584,8684

                        if (isNoParam) {
                            if ((HH24 >= 14) && (HH24 <= 18)) {
                                Vector vListMNToday = Utilities.getListCompanyMNToday();
                                deductMoney = false;
                                for (int i = 0; i < vListMNToday.size(); i++) {
                                    companyId = (BigDecimal) vListMNToday.elementAt(i);
                                    lotteryResult = (LotteryResult) Main.lotteryResultHT.get(companyId);
                                    result = lotteryResult.getResult();
                                    resultStatus = lotteryResult.getResultStatus();
                                    resultDate = lotteryResult.getResultDate();

                                    result = result.replaceAll("XSMN", commandCode);
                                    if (commandCode.equals("SMN")) {
                                        result = result.replaceAll("CAU", "SC");
                                    }

                                    if (resultDate.equals(currentDDMMYY)) {
                                        if (!deductMoney) {
                                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2,
                                                    subCode3);
                                            deductMoney = true;
                                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 0, requestId, subCode1, subCode2,
                                                    subCode3);
                                        }
                                    } else {
                                        LotteryDaily lotteryDaily = new LotteryDaily();
                                        lotteryDaily.setUserId(userId);
                                        lotteryDaily.setServiceId(serviceId);
                                        lotteryDaily.setCommandCode(commandCode);
                                        lotteryDaily.setMobileOperator(mobileOperator);
                                        lotteryDaily.setCompanyId(companyId);
                                        lotteryDaily.setRemain(1);
                                        lotteryDaily.setInfo(info);
                                        lotteryDaily.setRequestId(requestId);
                                        dbTool.add2LotteryDaily(lotteryDaily);
                                    }
                                } //End for

                                if (!deductMoney) {
                                    if (commandCode.equals("SMN")) {
                                        alert = "Hien nay he thong chua co ket qua xo so cac tinh " +
                                                lotteryCompany.getCompanyDesc() +
                                                ".Ban se nhan duoc ngay khi he thong co kq chinh thuc.Soi cau,soan: SC TenTinh gui8584.Chuc ban may man!";
                                    } else {
                                        alert = "Hien nay he thong chua co ket qua xo so cac tinh " +
                                                lotteryCompany.getCompanyDesc() +
                                                ".Ban se nhan duoc ngay khi he thong co kq chinh thuc.Soi cau,soan: CAU TenTinh gui8584.Chuc ban may man!";
                                    }

                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    deductMoney = true;
                                }
                                MyLogger.log(userId + "<==" + serviceId);
                            } else {
                                GregorianCalendar newCal = new GregorianCalendar();
                                int day = newCal.get(Calendar.DAY_OF_WEEK);
                                if ((HH24 >= 0) && (HH24 <= 15)) {
                                    if (day == 1) {
                                        day = 7;
                                    } else {
                                        day = day - 1;
                                    }
                                }
                                Vector getListCompanyMNByDayOfWeek = Utilities.getListCompanyMNByDayOfWeek(day);
                                deductMoney = false;
                                for (int i = 0; i < getListCompanyMNByDayOfWeek.size(); i++) {
                                    companyId = (BigDecimal) getListCompanyMNByDayOfWeek.elementAt(i);
                                    lotteryResult = (LotteryResult) Main.lotteryResultHT.get(companyId);
                                    result = lotteryResult.getResult();

                                    result = result.replaceAll("XSMN", commandCode);
                                    if (commandCode.equals("SMN")) {
                                        result = result.replaceAll("CAU", "SC");
                                    }

                                    if (!result.equals("")) {
                                        if (!deductMoney) {
                                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2,
                                                    subCode3);
                                            deductMoney = true;
                                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 0, requestId, subCode1, subCode2,
                                                    subCode3);
                                        }
                                    }
                                } //End for

                            }
                        } else if (dateFormat) {
                            if (currentDDMMYY.equals(dd + "/" + mm + "/" + yy)) {
                                Vector vListMNToday = Utilities.getListCompanyMNToday();
                                deductMoney = false;
                                for (int i = 0; i < vListMNToday.size(); i++) {
                                    companyId = (BigDecimal) vListMNToday.elementAt(i);
                                    lotteryResult = (LotteryResult) Main.lotteryResultHT.get(companyId);
                                    result = lotteryResult.getResult();
                                    resultStatus = lotteryResult.getResultStatus();
                                    resultDate = lotteryResult.getResultDate();

                                    result = result.replaceAll("XSMN", commandCode);
                                    if (commandCode.equals("SMN")) {
                                        result = result.replaceAll("CAU", "SC");
                                    }

                                    if (resultDate.equals(currentDDMMYY)) {
                                        if (!deductMoney) {
                                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2,
                                                    subCode3);
                                            deductMoney = true;
                                            MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 0, requestId, subCode1, subCode2,
                                                    subCode3);
                                        }
                                    } else {
                                        LotteryDaily lotteryDaily = new LotteryDaily();
                                        lotteryDaily.setUserId(userId);
                                        lotteryDaily.setServiceId(serviceId);
                                        lotteryDaily.setCommandCode(commandCode);
                                        lotteryDaily.setMobileOperator(mobileOperator);
                                        lotteryDaily.setCompanyId(companyId);
                                        lotteryDaily.setRemain(1);
                                        lotteryDaily.setInfo(info);
                                        lotteryDaily.setRequestId(requestId);
                                        dbTool.add2LotteryDaily(lotteryDaily);
                                    }
                                } //End for

                                if (!deductMoney) {
                                    if (commandCode.equals("SMN")) {
                                        alert = "Hien nay he thong chua co ket qua xo so cac tinh " +
                                                lotteryCompany.getCompanyDesc() +
                                                ".Ban se nhan duoc ngay khi he thong co kq chinh thuc.Soi cau,soan: SC TenTinh gui8584.Chuc ban may man!";
                                    } else {
                                        alert = "Hien nay he thong chua co ket qua xo so cac tinh " +
                                                lotteryCompany.getCompanyDesc() +
                                                ".Ban se nhan duoc ngay khi he thong co kq chinh thuc.Soi cau,soan: CAU TenTinh gui8584.Chuc ban may man!";
                                    }
                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                }
                                MyLogger.log(userId + "<==" + serviceId);
                            } else {
                                Vector v = dbTool.getResultByDateLocation(companyId, dd + mm);
                                deductMoney = false;
                                if (v.size() > 0) {
                                    for (int i = 0; i < v.size(); i++) {
                                        String resultRequestByDate = (String) v.elementAt(i);

                                        if (!deductMoney) {
                                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, resultRequestByDate, 1, requestId,
                                                    subCode1,
                                                    subCode2,
                                                    subCode3);
                                            deductMoney = true;
                                            MyLogger.log(userId + "<==" + commandCode + " " + dd + ":" + mm + "<==" + serviceId);
                                        } else {
                                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, resultRequestByDate, 0, requestId,
                                                    subCode1,
                                                    subCode2,
                                                    subCode3);
                                        }
                                    } //End for

                                } else {
                                    alert = "Hien nay he khong co ket qua xo so cac tinh " + lotteryCompany.getCompanyDesc() +
                                            " ma ban yeu cau.DT ho tro 1900561577";
                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==XSMN " + dd + ":" + mm + " Not Found<==" + serviceId);
                                }
                            }
                        } else {
                            if (status != 0) {
                                if (commandCode.equals("XMN")) {
                                    alert = "Ket qua Xo so MienNam ngay ban yeu cau khong co trong he thong" + enter +
                                            "Soan " + commandCode + " Ngay/thang gui8384,vidu: " + commandCode + " 25/10 gui8384.DT ho tro 1900561577";
                                } else if (commandCode.equals("SMN")) {
                                    alert = "Ket qua Xo so MienNam ngay ban yeu cau khong co trong he thong" + enter +
                                            "Soan " + commandCode + " Ngay/thang gui8584,vidu: " + commandCode + " 25/10 gui8584.DT ho tro 1900561577";
                                } else if (commandCode.equals("HGN")) {
                                    alert = "Ket qua Xo so MienNam ngay ban yeu cau khong co trong he thong" + enter +
                                            "Soan " + commandCode + " Ngay/thang gui8584,vidu: " + commandCode + " 25/10 gui8584.DT ho tro 1900561577";
                                } else if (commandCode.equals("QMN")) {
                                    alert = "Ket qua Xo so MienNam ngay ban yeu cau khong co trong he thong" + enter +
                                            "Soan " + commandCode + " Ngay/thang gui8584,vidu: " + commandCode + " 25/10 gui8584.DT ho tro 1900561577";
                                } else {
                                    alert = "Ket qua Xo so MienNam ngay ban yeu cau khong co trong he thong" + enter +
                                            "Soan " + commandCode + "MN Ngay/thang gui8384,vidu: " + commandCode + "MN 25/10 gui8384.DT ho tro 1900561577";
                                }
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                        subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":MN:Not Found<==" + serviceId);
                                }
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } //End MN

                } else { //Other companyId not in(1,2,3)

                    subCode1 = String.valueOf(companyId);
                    if (serviceId.endsWith("8184") || serviceId.endsWith("8284")) {
                        if (isNoParam) {
                            if (!Utilities.getListCompanyMNToday().contains(companyId) && !Utilities.getListCompanyMTToday().contains(companyId)) {
                                dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + "<==" + serviceId);
                            } else {
                                if (resultDate.equals(currentDDMMYY)) {
                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + "<==" + serviceId);
                                } else if (!resultDate.equals(currentDDMMYY) && (HH24 >= 0)) {
                                    //Add to wait
                                    LotteryDaily lotteryDaily = new LotteryDaily();
                                    lotteryDaily.setUserId(userId);
                                    lotteryDaily.setServiceId(serviceId);
                                    lotteryDaily.setCommandCode(commandCode);
                                    lotteryDaily.setMobileOperator(mobileOperator);
                                    lotteryDaily.setCompanyId(companyId);
                                    lotteryDaily.setRemain(1);
                                    lotteryDaily.setInfo(info);
                                    lotteryDaily.setRequestId(requestId);
                                    dbTool.add2LotteryDaily(lotteryDaily);

                                    alert = "Chung toi se gui ban kq xoso " + lotteryCompany.getCompanyDesc() + " ngay khi he thong co." +
                                            enter +
                                            "Soi Cau soan: CAU " + lotteryCompany.getCode() + " gui 8584";

                                    if (commandCode.equals("ST")) {
                                        alert = alert.replaceAll("CAU", "SC");
                                    }

                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":Wait<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + "<==" + serviceId);
                                }
                            }
                        } else if (code1.equals("CHO")) {
                            if (resultDate.equals(currentDDMMYY)) {
                                dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + "<==" + serviceId);
                            } else {
                                //Add to wait
                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                lotteryDaily.setRemain(1);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setRequestId(requestId);
                                dbTool.add2LotteryDaily(lotteryDaily);

                                alert = "Chung toi se gui ban kq xoso " + lotteryCompany.getCompanyDesc() + " ngay khi he thong co." +
                                        enter +
                                        "Soi Cau soan: CAU " + lotteryCompany.getCode() + " gui 8584";

                                if (commandCode.equals("ST")) {
                                    alert = alert.replaceAll("CAU", "SC");
                                }

                                dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":Wait<==" + serviceId);
                            }
                        } else if (code1.equals("HUY") || code1.equals("XOA")) {
                            alert = "Ban vua huy bo nhan ket qua xo so " + lotteryCompany.getCompanyDesc() + " tu he thong 8x84.DT ho tro 1900561577";
                            if (dbTool.removeLotteryDaily(userId, companyId)) {
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":DelFromDaily:" + lotteryCompany.getCompanyDesc() + ":<==" + serviceId);
                                }
                            }
                        } else if (dateFormat) {
                            if (currentDDMMYY.equals(dd + "/" + mm + "/" + yy)) {
                                if (resultDate.equals(currentDDMMYY) && (resultStatus == 0)) {
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + "<==" + serviceId);
                                    }
                                } else {
                                    //Add to wait
                                    LotteryDaily lotteryDaily = new LotteryDaily();
                                    lotteryDaily.setUserId(userId);
                                    lotteryDaily.setServiceId(serviceId);
                                    lotteryDaily.setCommandCode(commandCode);
                                    lotteryDaily.setMobileOperator(mobileOperator);
                                    lotteryDaily.setCompanyId(companyId);
                                    lotteryDaily.setRemain(1);
                                    lotteryDaily.setInfo(info);
                                    lotteryDaily.setRequestId(requestId);
                                    dbTool.add2LotteryDaily(lotteryDaily);

                                    alert = "Chung toi se gui ban kq xoso " + lotteryCompany.getCompanyDesc() + " ngay khi he thong co." +
                                            enter +
                                            "Soi Cau soan: CAU " + lotteryCompany.getCode() + " gui 8584";

                                    if (commandCode.equals("ST")) {
                                        alert = alert.replaceAll("CAU", "SC");
                                    }

                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":Wait<==" + serviceId);
                                }
                            } else {
                                String resultRequestByDate = dbTool.getResultByDate(companyId, dd + "/" + mm + "/" + yy);
                                if (!resultRequestByDate.equals("")) {
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, resultRequestByDate, 1, requestId, subCode1,
                                            subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + "<==" + serviceId);
                                    }
                                } else if (Utilities.getListCompanyMNByDayOfMonth(Integer.parseInt(dd), Integer.parseInt(mm)).contains(companyId) ||
                                        Utilities.getListCompanyMTByDayOfMonth(Integer.parseInt(dd), Integer.parseInt(mm)).contains(companyId)) {
                                    //Add to wait
                                    LotteryDaily lotteryDaily = new LotteryDaily();
                                    lotteryDaily.setUserId(userId);
                                    lotteryDaily.setServiceId(serviceId);
                                    lotteryDaily.setCommandCode(commandCode);
                                    lotteryDaily.setMobileOperator(mobileOperator);
                                    lotteryDaily.setCompanyId(companyId);
                                    lotteryDaily.setRemain(1);
                                    lotteryDaily.setInfo(info);
                                    lotteryDaily.setRequestId(requestId);
                                    dbTool.add2LotteryDaily(lotteryDaily);

                                    alert = "Chung toi se gui ban kq xoso " + lotteryCompany.getCompanyDesc() + " ngay khi he thong co." +
                                            enter +
                                            "Soi Cau soan: CAU " + lotteryCompany.getCode() + " gui 8584";

                                    if (commandCode.equals("ST")) {
                                        alert = alert.replaceAll("CAU", "SC");
                                    }

                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":Wait<==" + serviceId);
                                } else {
                                    if (status != 0) {
//                                        alert = "Ket qua Xo so " + lotteryCompany.getCompanyDesc() + " ngay " + dd + "/" + mm + " khong co trong he thong" +
//                                                enter +
//                                                "Soan " + commandCode + lotteryCompany.getCode() + " Ngay/thang gui8284,vidu: " + commandCode +
//                                                lotteryCompany.getCode() + " 25/10 gui8284.DT ho tro 1900561577";

                                        alert = result; //Get result in nearest

                                        if (commandCode.equals("ST")) {
                                            alert = alert.replaceAll("CAU", "SC");
                                        }
                                        if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                                subCode3)) {
                                            MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":NotFound<==" + serviceId);
                                        }
                                    } else {
                                        dbTool.updateStatus(requestId, 1, mobileOperator);
                                    }
                                }
                            }
                        } else {
                            if (status != 0) {
                                alert = "Tin nhan sai dinh dang,ban vua gui " + info + " toi " + serviceId + enter +
                                        "Soan " + commandCode + " <MaTinh> gui8184 nhan ket qua XoSo.DT ho tro 1900561577";
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                        subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":WrongFormat<==" + serviceId);
                                }
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    } else {
                        //83,84,85,86,87
                        if (isNoParam) {
                            if (!Utilities.getListCompanyMNToday().contains(companyId) && !Utilities.getListCompanyMTToday().contains(companyId)) {
                                dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + "<==" + serviceId);

                                //Add to daily
                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                int remainTmp = 0;
                                if (serviceId.equals("8384")) {
                                    remainTmp = 1;
                                } else if (serviceId.equals("8484")) {
                                    remainTmp = 2;
                                } else if (serviceId.equals("8584")) {
                                    remainTmp = 4;
                                } else if (serviceId.equals("8684")) {
                                    remainTmp = 5;
                                } else if (serviceId.equals("8784")) {
                                    remainTmp = 7;
                                }
                                lotteryDaily.setRemain(remainTmp);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setResponseDate(currentDDMMYY);
                                lotteryDaily.setRequestId(requestId);
                                dbTool.add2LotteryDaily(lotteryDaily);

                                alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
                                        "Soi cau soan: CAU " + lotteryCompany.getCode() + " gui 8584";

                                if (commandCode.equals("ST")) {
                                    alert = alert.replaceAll("CAU", "SC");
                                }

                                dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2,
                                        subCode3);
                                MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":Add2Daily<==" + serviceId);
                            } else {
                                if (resultDate.equals(currentDDMMYY)) {
                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + "<==" + serviceId);

                                    //Add to daily
                                    LotteryDaily lotteryDaily = new LotteryDaily();
                                    lotteryDaily.setUserId(userId);
                                    lotteryDaily.setServiceId(serviceId);
                                    lotteryDaily.setCommandCode(commandCode);
                                    lotteryDaily.setMobileOperator(mobileOperator);
                                    lotteryDaily.setCompanyId(companyId);
                                    int remainTmp = 0;
                                    if (serviceId.equals("8384")) {
                                        remainTmp = 1;
                                    } else if (serviceId.equals("8484")) {
                                        remainTmp = 2;
                                    } else if (serviceId.equals("8584")) {
                                        remainTmp = 4;
                                    } else if (serviceId.equals("8684")) {
                                        remainTmp = 5;
                                    } else if (serviceId.equals("8784")) {
                                        remainTmp = 7;
                                    }
                                    lotteryDaily.setRemain(remainTmp);
                                    lotteryDaily.setInfo(info);
                                    lotteryDaily.setResponseDate(currentDDMMYY);
                                    lotteryDaily.setRequestId(requestId);
                                    dbTool.add2LotteryDaily(lotteryDaily);

                                    alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
                                            "Soi cau soan: CAU " + lotteryCompany.getCode() + " gui 8584";

                                    if (commandCode.equals("ST")) {
                                        alert = alert.replaceAll("CAU", "SC");
                                    }

                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2,
                                            subCode3);
                                    MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":Add2Daily<==" + serviceId);
                                } else if (!resultDate.equals(currentDDMMYY) && (HH24 >= 16)) {
                                    //Add to daily
                                    LotteryDaily lotteryDaily = new LotteryDaily();
                                    lotteryDaily.setUserId(userId);
                                    lotteryDaily.setServiceId(serviceId);
                                    lotteryDaily.setCommandCode(commandCode);
                                    lotteryDaily.setMobileOperator(mobileOperator);
                                    lotteryDaily.setCompanyId(companyId);
                                    int remainTmp = 0;
                                    if (serviceId.equals("8384")) {
                                        remainTmp = 1;
                                    } else if (serviceId.equals("8484")) {
                                        remainTmp = 2;
                                    } else if (serviceId.equals("8584")) {
                                        remainTmp = 4;
                                    } else if (serviceId.equals("8684")) {
                                        remainTmp = 5;
                                    } else if (serviceId.equals("8784")) {
                                        remainTmp = 7;
                                    }
                                    lotteryDaily.setRemain(remainTmp);
                                    lotteryDaily.setInfo(info);
                                    lotteryDaily.setRequestId(requestId);
                                    dbTool.add2LotteryDaily(lotteryDaily);

//                                    alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
//                                            "Lay kq cac tinh MienTrung soan: XSMT gui8384" + enter +
//                                            "Lay kq cac tinh MienNam soan: XSMN gui8384";

                                    alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
                                            "Soi cau soan: CAU " + lotteryCompany.getCode() + " gui 8584";

                                    if (commandCode.equals("ST")) {
                                        alert = alert.replaceAll("CAU", "SC");
                                    }

                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                            subCode3);
                                    MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":Add2Daily<==" + serviceId);
                                } else {
                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3);
                                    MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + "<==" + serviceId);

                                    //Add to daily
                                    LotteryDaily lotteryDaily = new LotteryDaily();
                                    lotteryDaily.setUserId(userId);
                                    lotteryDaily.setServiceId(serviceId);
                                    lotteryDaily.setCommandCode(commandCode);
                                    lotteryDaily.setMobileOperator(mobileOperator);
                                    lotteryDaily.setCompanyId(companyId);
                                    int remainTmp = 0;
                                    if (serviceId.equals("8384")) {
                                        remainTmp = 1;
                                    } else if (serviceId.equals("8484")) {
                                        remainTmp = 2;
                                    } else if (serviceId.equals("8584")) {
                                        remainTmp = 4;
                                    } else if (serviceId.equals("8684")) {
                                        remainTmp = 5;
                                    } else if (serviceId.equals("8784")) {
                                        remainTmp = 7;
                                    }
                                    lotteryDaily.setRemain(remainTmp);
                                    lotteryDaily.setInfo(info);
                                    lotteryDaily.setResponseDate(currentDDMMYY);
                                    lotteryDaily.setRequestId(requestId);
                                    dbTool.add2LotteryDaily(lotteryDaily);

//                                    alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
//                                            "Lay kq cac tinh MienTrung soan: XSMT gui8384" + enter +
//                                            "Lay kq cac tinh MienNam soan: XSMN gui8384";

                                    alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
                                            "Soi cau soan: CAU " + lotteryCompany.getCode() + " gui 8584";

                                    if (commandCode.equals("ST")) {
                                        alert = alert.replaceAll("CAU", "SC");
                                    }

                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2,
                                            subCode3);
                                    MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":Add2Daily<==" + serviceId);
                                }
                            }
                        } else if (code1.equals("CHO")) {
                            if (resultDate.equals(currentDDMMYY)) {
                                dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + "<==" + serviceId);

                                //Add to daily
                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                int remainTmp = 0;
                                if (serviceId.equals("8384")) {
                                    remainTmp = 1;
                                } else if (serviceId.equals("8484")) {
                                    remainTmp = 2;
                                } else if (serviceId.equals("8584")) {
                                    remainTmp = 4;
                                } else if (serviceId.equals("8684")) {
                                    remainTmp = 5;
                                } else if (serviceId.equals("8784")) {
                                    remainTmp = 7;
                                }
                                lotteryDaily.setRemain(remainTmp);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setResponseDate(currentDDMMYY);
                                lotteryDaily.setRequestId(requestId);
                                dbTool.add2LotteryDaily(lotteryDaily);

//                                alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
//                                        "Lay kq cac tinh MienTrung soan: XSMT gui8384" + enter +
//                                        "Lay kq cac tinh MienNam soan: XSMN gui8384";

                                alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
                                        "Soi cau soan: CAU " + lotteryCompany.getCode() + " gui 8584";

                                if (commandCode.equals("ST")) {
                                    alert = alert.replaceAll("CAU", "SC");
                                }

                                dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2,
                                        subCode3);
                                MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":Add2Daily<==" + serviceId);
                            } else {
                                //Add to daily
                                LotteryDaily lotteryDaily = new LotteryDaily();
                                lotteryDaily.setUserId(userId);
                                lotteryDaily.setServiceId(serviceId);
                                lotteryDaily.setCommandCode(commandCode);
                                lotteryDaily.setMobileOperator(mobileOperator);
                                lotteryDaily.setCompanyId(companyId);
                                int remainTmp = 0;
                                if (serviceId.equals("8384")) {
                                    remainTmp = 1;
                                } else if (serviceId.equals("8484")) {
                                    remainTmp = 2;
                                } else if (serviceId.equals("8584")) {
                                    remainTmp = 4;
                                } else if (serviceId.equals("8684")) {
                                    remainTmp = 5;
                                } else if (serviceId.equals("8784")) {
                                    remainTmp = 7;
                                }
                                lotteryDaily.setRemain(remainTmp);
                                lotteryDaily.setInfo(info);
                                lotteryDaily.setRequestId(requestId);
                                dbTool.add2LotteryDaily(lotteryDaily);

//                                alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
//                                        "Lay kq cac tinh MienTrung soan: XSMT gui8384" + enter +
//                                        "Lay kq cac tinh MienNam soan: XSMN gui8384";

                                alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
                                        "Soi cau soan: CAU " + lotteryCompany.getCode() + " gui 8584";

                                if (commandCode.equals("ST")) {
                                    alert = alert.replaceAll("CAU", "SC");
                                }

                                dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                        subCode3);
                                MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":Add2Daily<==" + serviceId);
                            }
                        } else if (code1.equals("HUY") || code1.equals("XOA")) {
                            alert = "Ban vua huy bo nhan ket qua xo so " + lotteryCompany.getCompanyDesc() + " tu he thong 8x84.DT ho tro 1900561577";
                            if (dbTool.removeLotteryDaily(userId, companyId)) {
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2, subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":DelFromDaily:" + lotteryCompany.getCompanyDesc() + ":<==" + serviceId);
                                }
                            }
                        } else if (dateFormat) {
                            if (currentDDMMYY.equals(dd + "/" + mm + "/" + yy)) {
                                if (resultDate.equals(currentDDMMYY) && (resultStatus == 0)) {
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + "<==" + serviceId);

                                        //Add to daily
                                        LotteryDaily lotteryDaily = new LotteryDaily();
                                        lotteryDaily.setUserId(userId);
                                        lotteryDaily.setServiceId(serviceId);
                                        lotteryDaily.setCommandCode(commandCode);
                                        lotteryDaily.setMobileOperator(mobileOperator);
                                        lotteryDaily.setCompanyId(companyId);
                                        int remainTmp = 0;
                                        if (serviceId.equals("8384")) {
                                            remainTmp = 1;
                                        } else if (serviceId.equals("8484")) {
                                            remainTmp = 2;
                                        } else if (serviceId.equals("8584")) {
                                            remainTmp = 4;
                                        } else if (serviceId.equals("8684")) {
                                            remainTmp = 5;
                                        } else if (serviceId.equals("8784")) {
                                            remainTmp = 7;
                                        }
                                        lotteryDaily.setRemain(remainTmp);
                                        lotteryDaily.setInfo(info);
                                        lotteryDaily.setResponseDate(currentDDMMYY);
                                        lotteryDaily.setRequestId(requestId);
                                        dbTool.add2LotteryDaily(lotteryDaily);

//                                        alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
//                                                "Lay kq cac tinh MienTrung soan: XSMT gui8384" + enter +
//                                                "Lay kq cac tinh MienNam soan: XSMN gui8384";

                                        alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
                                                "Soi cau soan: CAU " + lotteryCompany.getCode() + " gui 8584";

                                        if (commandCode.equals("ST")) {
                                            alert = alert.replaceAll("CAU", "SC");
                                        }

                                        dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2,
                                                subCode3);
                                        MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":Add2Daily<==" + serviceId);
                                    }
                                } else {
                                    //Add to daily
                                    LotteryDaily lotteryDaily = new LotteryDaily();
                                    lotteryDaily.setUserId(userId);
                                    lotteryDaily.setServiceId(serviceId);
                                    lotteryDaily.setCommandCode(commandCode);
                                    lotteryDaily.setMobileOperator(mobileOperator);
                                    lotteryDaily.setCompanyId(companyId);
                                    int remainTmp = 0;
                                    if (serviceId.equals("8384")) {
                                        remainTmp = 1;
                                    } else if (serviceId.equals("8484")) {
                                        remainTmp = 2;
                                    } else if (serviceId.equals("8584")) {
                                        remainTmp = 4;
                                    } else if (serviceId.equals("8684")) {
                                        remainTmp = 5;
                                    } else if (serviceId.equals("8784")) {
                                        remainTmp = 7;
                                    }
                                    lotteryDaily.setRemain(remainTmp);
                                    lotteryDaily.setInfo(info);
                                    lotteryDaily.setRequestId(requestId);
                                    dbTool.add2LotteryDaily(lotteryDaily);

//                                    alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
//                                            "Lay kq cac tinh MienTrung soan: XSMT gui8384" + enter +
//                                            "Lay kq cac tinh MienNam soan: XSMN gui8384";

                                    alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
                                            "Soi cau soan: CAU " + lotteryCompany.getCode() + " gui 8584";

                                    if (commandCode.equals("ST")) {
                                        alert = alert.replaceAll("CAU", "SC");
                                    }

                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                            subCode3);
                                    MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":Add2Daily<==" + serviceId);
                                }
                            } else {
                                String resultRequestByDate = dbTool.getResultByDate(companyId, dd + "/" + mm + "/" + yy);
                                if (!resultRequestByDate.equals("")) {
                                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, resultRequestByDate, 1, requestId, subCode1,
                                            subCode2, subCode3)) {
                                        MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + "<==" + serviceId);

                                        //Add to daily
                                        LotteryDaily lotteryDaily = new LotteryDaily();
                                        lotteryDaily.setUserId(userId);
                                        lotteryDaily.setServiceId(serviceId);
                                        lotteryDaily.setCommandCode(commandCode);
                                        lotteryDaily.setMobileOperator(mobileOperator);
                                        lotteryDaily.setCompanyId(companyId);
                                        int remainTmp = 0;
                                        if (serviceId.equals("8384")) {
                                            remainTmp = 1;
                                        } else if (serviceId.equals("8484")) {
                                            remainTmp = 2;
                                        } else if (serviceId.equals("8584")) {
                                            remainTmp = 4;
                                        } else if (serviceId.equals("8684")) {
                                            remainTmp = 5;
                                        } else if (serviceId.equals("8784")) {
                                            remainTmp = 7;
                                        }
                                        lotteryDaily.setRemain(remainTmp);
                                        lotteryDaily.setInfo(info);
                                        lotteryDaily.setResponseDate(currentDDMMYY);
                                        lotteryDaily.setRequestId(requestId);
                                        dbTool.add2LotteryDaily(lotteryDaily);

//                                        alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
//                                                "Lay kq cac tinh MienTrung soan: XSMT gui8384" + enter +
//                                                "Lay kq cac tinh MienNam soan: XSMN gui8384";

                                        alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
                                                "Soi cau soan: CAU " + lotteryCompany.getCode() + " gui 8584";

                                        if (commandCode.equals("ST")) {
                                            alert = alert.replaceAll("CAU", "SC");
                                        }

                                        dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2,
                                                subCode3);
                                        MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":Add2Daily<==" + serviceId);
                                    }
                                } else if (Utilities.getListCompanyMNByDayOfMonth(Integer.parseInt(dd), Integer.parseInt(mm)).contains(companyId) ||
                                        Utilities.getListCompanyMTByDayOfMonth(Integer.parseInt(dd), Integer.parseInt(mm)).contains(companyId)) {
                                    //Add to daily
                                    LotteryDaily lotteryDaily = new LotteryDaily();
                                    lotteryDaily.setUserId(userId);
                                    lotteryDaily.setServiceId(serviceId);
                                    lotteryDaily.setCommandCode(commandCode);
                                    lotteryDaily.setMobileOperator(mobileOperator);
                                    lotteryDaily.setCompanyId(companyId);
                                    int remainTmp = 0;
                                    if (serviceId.equals("8384")) {
                                        remainTmp = 1;
                                    } else if (serviceId.equals("8484")) {
                                        remainTmp = 2;
                                    } else if (serviceId.equals("8584")) {
                                        remainTmp = 4;
                                    } else if (serviceId.equals("8684")) {
                                        remainTmp = 5;
                                    } else if (serviceId.equals("8784")) {
                                        remainTmp = 7;
                                    }
                                    lotteryDaily.setRemain(remainTmp);
                                    lotteryDaily.setInfo(info);
                                    lotteryDaily.setRequestId(requestId);
                                    dbTool.add2LotteryDaily(lotteryDaily);

//                                    alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
//                                            "Lay kq cac tinh MienTrung soan: XSMT gui8384" + enter +
//                                            "Lay kq cac tinh MienNam soan: XSMN gui8384";

                                    alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
                                            "Soi cau soan: CAU " + lotteryCompany.getCode() + " gui 8584";

                                    if (commandCode.equals("ST")) {
                                        alert = alert.replaceAll("CAU", "SC");
                                    }

                                    dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                            subCode3);
                                    MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":Add2Daily<==" + serviceId);
                                } else {
                                    if (status != 0) {
//                                        alert = "Ket qua Xo so " + lotteryCompany.getCompanyDesc() + " ngay " + dd + "/" + mm + " khong co trong he thong" +
//                                                enter +
//                                                "Soan " + commandCode + lotteryCompany.getCode() + " Ngay/thang gui8184,vidu: " + commandCode +
//                                                lotteryCompany.getCode() + " 25/10 gui8184.DT ho tro 1900561577";

                                        alert = result;
                                        if (commandCode.equals("ST")) {
                                            alert = alert.replaceAll("CAU", "SC");
                                        }

                                        if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                                subCode3)) {
                                            MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":NotFound<==" + serviceId);
                                        }

                                        //Add to daily
                                        LotteryDaily lotteryDaily = new LotteryDaily();
                                        lotteryDaily.setUserId(userId);
                                        lotteryDaily.setServiceId(serviceId);
                                        lotteryDaily.setCommandCode(commandCode);
                                        lotteryDaily.setMobileOperator(mobileOperator);
                                        lotteryDaily.setCompanyId(companyId);
                                        int remainTmp = 0;
                                        if (serviceId.equals("8384")) {
                                            remainTmp = 1;
                                        } else if (serviceId.equals("8484")) {
                                            remainTmp = 2;
                                        } else if (serviceId.equals("8584")) {
                                            remainTmp = 4;
                                        } else if (serviceId.equals("8684")) {
                                            remainTmp = 5;
                                        } else if (serviceId.equals("8784")) {
                                            remainTmp = 7;
                                        }
                                        lotteryDaily.setRemain(remainTmp);
                                        lotteryDaily.setInfo(info);
                                        lotteryDaily.setResponseDate(currentDDMMYY);
                                        lotteryDaily.setRequestId(requestId);
                                        dbTool.add2LotteryDaily(lotteryDaily);

                                        alert = "Ban se nhan duoc " + remainTmp + " ngay ket qua xoso " + lotteryCompany.getCompanyDesc() + enter +
                                                "Soi cau soan: CAU " + lotteryCompany.getCode() + " gui 8584";

                                        if (commandCode.equals("ST")) {
                                            alert = alert.replaceAll("CAU", "SC");
                                        }

                                        dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 0, requestId, subCode1, subCode2,
                                                subCode3);
                                        MyLogger.log(userId + "<==" + lotteryCompany.getCompanyDesc() + ":Add2Daily<==" + serviceId);

                                    } else {
                                        dbTool.updateStatus(requestId, 1, mobileOperator);
                                    }
                                }
                            }
                        } else {
                            if (status != 0) {
                                alert = "Tin nhan sai dinh dang,ban vua gui " + info + " toi " + serviceId + enter +
                                        "Soan " + commandCode + " <MaTinh> gui8184 nhan ket qua XoSo.DT ho tro 1900561577";
                                if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                                        subCode3)) {
                                    MyLogger.log(userId + "<==" + commandCode + ":WrongFormat<==" + serviceId);
                                }
                            } else {
                                dbTool.updateStatus(requestId, 1, mobileOperator);
                            }
                        }
                    }
                }

            } else {
                if (status != 0) {
                    alert = "Tin nhan sai dinh dang,de nhan ket qua xo so soan: " + commandCode + " <MaTinh> gui 8184 " + enter +
                            "Vidu: " + commandCode + "MB gui8184.DT ho tro 1900561577";
                    if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, alert, 1, requestId, subCode1, subCode2,
                            subCode3)) {
                        MyLogger.log(userId + "<==" + commandCode + ":WrongFormat:" + info + "<==" + serviceId);
                    }
                } else {
                    dbTool.updateStatus(requestId, 1, mobileOperator);
                }
            } //End default MB

        } //End of for earch request sms

    }

    public void run() {
        try {
            Thread.sleep(5 * 1000);
        } catch (Exception ex) {
        }
        MyLogger.log("Lottery " + operator + " Care start !");
        while (Main.running) {
            if (Main.initResultLotteryOk) {
                try {
                    int HH24 = DateProc.getCurrentHH24();
                    processLottery();
                    if ((HH24 >= 16) && (HH24 <= 21)) {
                        Thread.sleep(500);
                    } else {
                        Thread.sleep(2000);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
