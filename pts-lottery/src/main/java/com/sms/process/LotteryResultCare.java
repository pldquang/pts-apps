package com.sms.process;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

import java.util.*;
import java.sql.*;
import java.math.BigDecimal;

import com.sms.common.DateProc;
import com.sms.common.*;
import com.sms.item.*;

public class LotteryResultCare extends Thread {
    public static Hashtable resultLottery = new Hashtable();
    DBTool dbTool = null;

    public LotteryResultCare() {
        dbTool = new DBTool();
    }

    public void processRequestUpdateLotteryResult() {
        dbTool.updateLottery("VIETEL");
        dbTool.updateLottery("GPC");
        dbTool.updateLottery("VMS");
    }

    public void lotteryResultUpdateCare() {
        Vector v = dbTool.getLastestLotteryResult();
        for (int i = 0; i < v.size(); i++) {
            LotteryResult lotteryResultNew = (LotteryResult) v.elementAt(i);
            BigDecimal companyId = lotteryResultNew.getCompanyId();
            String resultNew = lotteryResultNew.getResult();
            LotteryResult lotteryResultInMemory = (LotteryResult) Main.lotteryResultHT.get(companyId);
            String resultInMemory = lotteryResultInMemory.getResult();
            if (!resultInMemory.equals(resultNew)) {
                synchronized (Main.lotteryResultHT) {
                    Main.lotteryResultHT.remove(companyId);
                    Main.lotteryResultHT.put(companyId, lotteryResultNew);
                    if (companyId.intValue() != 1) {
                        MyLogger.log("Update Lottery: " + ((LotteryCompany) Main.lotteryCompanyIDHT.get(companyId)).getCode());
                    } else {
                        MyLogger.log("Update Lottery: MB");
                    }
                }
            }
        } //End for
    }


    public void run() {
        MyLogger.log("LotteryResult care start!");
        try {
            Thread.sleep(5 * 1000);
        } catch (Exception ex) {

        } while (Main.running) {
            processRequestUpdateLotteryResult();
            lotteryResultUpdateCare();
            if(!Main.initResultLotteryOk) {
                Main.initResultLotteryOk = true;
            }
            int HH24 = DateProc.getCurrentHH24();
            if ((HH24 >= 16) && (HH24 <= 21)) {
                try {
                    Thread.sleep(400);
                } catch (Exception ex) {}
            } else {
                try {
                    Thread.sleep(400);
//                    Thread.sleep(3 * 60 * 1000);
                } catch (Exception ex) {}
            }
        } //End while
    }
}
