package com.sms.process;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import java.io.*;
import java.util.*;
import java.math.BigDecimal;

import com.sms.item.*;
import com.sms.common.DateProc;
import com.sms.common.MyLogger;

public class ProcessKM extends Thread {

    DBTool dbTool = null;
    Properties properties = null;
    static String fileCfg = "promotional.properties";
    String subCode1 = "1";
    String subCode2 = "";
    String subCode3 = "";

    public ProcessKM() {
        dbTool = new DBTool();
        properties = new Properties();
    }

    public String getLastCheck(String key) {
        String result = "";
        try {
            FileInputStream propsFile = new FileInputStream(fileCfg);
            properties.load(propsFile);
            propsFile.close();
            result = properties.getProperty(key);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public boolean setLastCheck(String valueGPC, String valueVIETEL, String valueVMS) {
        boolean result = false;
        try {
            FileOutputStream propsFile = new FileOutputStream(fileCfg);
            Properties properties = new Properties();
            properties.setProperty("GPC", valueGPC);
            properties.setProperty("VIETEL", valueVIETEL);
            properties.setProperty("VMS", valueVMS);
            properties.store(propsFile, null);
            propsFile.flush();
            propsFile.close();
            result = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public String getCurrentDBPrize() {
        String result = "";
        try {
            String resultCurrent = dbTool.getCurrentPrizeOfLottery(new BigDecimal(1));
            resultCurrent = resultCurrent.toUpperCase();
            int i = resultCurrent.indexOf("DB:");
            result = resultCurrent.substring(i + 3, i + 8);
            result = result.substring(result.length() - 2);
            System.out.println("result:" + result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public void add2Daily(Hashtable ht, String mobileOperator, String db) {
        MOItem moItem = null;
        LotteryDaily lotteryDaily = null;

        String userId = "";
        String serviceId = "";
        String commandCode = "";
        String info = "";
        String receiveTime = "";

        String alert = "Chuc mung ban da trung thuong chuong trinh Khuyen mai XS \"Nhan 1 duoc 5\" cua TD 8x84." +
                "Ban se duoc tuong thuat KQXSMB mien phi 05 ngay lien tiep.Chuc ban may man!";

        Enumeration enume = ht.keys();
        while (enume.hasMoreElements()) {
            userId = (String) enume.nextElement();
            moItem = (MOItem) ht.get(userId);

            lotteryDaily = new LotteryDaily();
            lotteryDaily.setUserId(userId);
            lotteryDaily.setCommandCode(moItem.getCommandCode());
            lotteryDaily.setServiceId(moItem.getServiceId());
            lotteryDaily.setMobileOperator(moItem.getMobileOperator());
            lotteryDaily.setCompanyId(new BigDecimal(1));//MB
            lotteryDaily.setInfo(moItem.getInfo());
            lotteryDaily.setRequestDate(DateProc.getCurrentDDMMYY());
            lotteryDaily.setResponseDate(DateProc.getCurrentDDMMYY());
            lotteryDaily.setRequestId(moItem.getId());
            lotteryDaily.setRemain(40);//5ngay Truc tiep

            if (dbTool.add2LotteryDaily(lotteryDaily)) {
                dbTool.sendMTEx(userId, moItem.getServiceId(), moItem.getCommandCode(), moItem.getMobileOperator(), 0, alert, 0, moItem.getId(), subCode1, subCode2, subCode3);
                System.out.println(userId + "<==Prize:" + db);
                dbTool.Update2PromotionalLottery(moItem);
            }
        }

    }

    public void process(String mobileOperator) {
        System.out.println("Process prize for " + mobileOperator + "...");
        String dbPrize = getCurrentDBPrize();
        String previousResultTime = dbTool.getTimeWhenLotteryIsFull(DateProc.getPreviousDate(), new BigDecimal(1));
        String currentResultTime = dbTool.getTimeWhenLotteryIsFull(DateProc.getCurrentDDMMYY(), new BigDecimal(1));

        String valueGPC = getLastCheck("GPC");
        String valueVIETEL = getLastCheck("VIETEL");
        String valueVMS = getLastCheck("VMS");

        try {
            Hashtable ht = dbTool.getListUserIdEndWithDBPrize(mobileOperator, dbPrize, previousResultTime, currentResultTime);

            add2Daily(ht, mobileOperator, dbPrize);

            if (mobileOperator.equals("GPC")) {
                valueGPC = DateProc.getCurrentDDMMYY();
            }
            if (mobileOperator.equals("VIETEL")) {
                valueVIETEL = DateProc.getCurrentDDMMYY();
            }
            if (mobileOperator.equals("VMS")) {
                valueVMS = DateProc.getCurrentDDMMYY();
            }


            setLastCheck(valueGPC, valueVIETEL, valueVMS);
        } catch (Exception ex) {
        }

    }

    public void run() {
        try {
            Thread.sleep(60000);
        } catch (Exception ex) {
        }
        MyLogger.log("ProcessKM start!");
        while (Main.running) {
            try {
                if ((DateProc.getCurrentHH24() == 19) && (DateProc.getCurrentMI() >= 35) && (DateProc.getCurrentMI() <= 50)) {
                    String currentDDMMYY = DateProc.getCurrentDDMMYY();
                    if (!getLastCheck("GPC").equals(currentDDMMYY)) {
                        process("GPC");
                    }

                    if (!getLastCheck("VMS").equals(currentDDMMYY)) {
                        process("VMS");
                    }

                    if (!getLastCheck("VIETEL").equals(currentDDMMYY)) {
                        process("VIETEL");
                    }

                    Thread.sleep(30000);
                } else {
                    Thread.sleep(30000);
                }
            } catch (Exception ex) {
            }
        }
    }

    public static void main(String[] args) {
        ProcessKM test = new ProcessKM();
        test.setLastCheck("promotional.properties", "VMS", "testing");
    }
}
