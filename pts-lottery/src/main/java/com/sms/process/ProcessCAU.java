package com.sms.process;

import java.util.*;
import java.util.regex.*;
import java.math.BigDecimal;
import java.util.Hashtable;

import com.sms.item.*;
import com.sms.common.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class ProcessCAU extends Thread {

    DBTool dbTool = null;
    Hashtable xxNumHT = null;
    static Hashtable xxNumHTDB = null;
    SmsReceiveQueue smsReceiveQueue = null;
    BigDecimal requestId = null;
    String userId = "";
    String serviceId = "";
    int status = 0;
    String commandCode = "";
    String info = "";
    String tmp = "";
    static char enter = (char) 10;

    public ProcessCAU() {
        dbTool = new DBTool();
        xxNumHT = new Hashtable();
    }

    public boolean isNumber(String value) {
        boolean ret = false;
        try {
            int tmp = Integer.parseInt(value);
            if (tmp >= 0) {
                ret = true;
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return ret;
    }

//    public static void main(String a[]){
//        ProcessCAU p = new ProcessCAU();
//        System.out.println(p.isNumber("06/08"));
//    }
    public String getCAU(Vector listResult, String number) {
        Matcher matcher = null;
        String inputStr = "";
        String newLine = "\n";
        String msg = "";
        Pattern p = Pattern.compile(":|-");
        int count = 0;

        for (int i = 0; i <= 99; i++) {
            if (i < 10) {
                xxNumHT.put("0" + String.valueOf(i), new Integer(0));
            } else {
                xxNumHT.put(String.valueOf(i), new Integer(0));
            }
        }

        for (int i = 0; i < listResult.size(); i++) {
            inputStr = (String) listResult.elementAt(i);
            matcher = p.matcher(inputStr);
            String tmp = matcher.replaceAll(" ").replaceAll("\n", " ");

            StringTokenizer token = new StringTokenizer(tmp, " ");
            int tokens = token.countTokens();
            String tkStr = "";
            String xxLast = "";
            for (int j = 0; j < tokens; j++) {
                tkStr = token.nextToken();
                try {
                    if (isNumber(tkStr)) {
                        if ((tkStr.length() >= 2)) {
                            xxLast = tkStr.substring(tkStr.length() - 2, tkStr.length());
                            Integer.parseInt(xxLast);
//                        System.out.println(xxLast);
                            if (xxNumHT.containsKey(xxLast)) {
                                Integer num = (Integer) xxNumHT.get(xxLast);
                                xxNumHT.remove(xxLast);
                                xxNumHT.put(xxLast, num + 1);
                            } else {
                                xxNumHT.put(xxLast, 1);
                            }
                        }
                    }
                } catch (Exception ex) {
                }

                if (!number.equals("")) {
                    try {
                        int data = Integer.parseInt(tkStr);
//                        if (data >= 10) {
                        if (tkStr.endsWith(number)) {
                            count++;
                        }
//                        }
                    } catch (Exception ex) {
                    }
                }
            }
        } //End for

//        for (int i = 0; i < xxNumHT.size(); i++) {
//            if (i < 10) {
//                System.out.println("0" + i + ": " + xxNumHT.get("0" + i));
//            } else {
//                System.out.println(i + ": " + xxNumHT.get(i));
//            }
//        }
        //Put keys and values in to an arraylist using entryset
        ArrayList myArrayList = new ArrayList(xxNumHT.entrySet());
        //Sort the values based on values first and then keys.
        Collections.sort(myArrayList, new MyComparator());

        msg = "Thong ke 30ngay gan day:";
        if ((number != null) && !number.equals("")) {
            msg = msg + newLine + "Cap so " + number + " ve " + count + " lan.";
        }

        //Show sorted results
        Iterator itr = myArrayList.iterator();
        String key = "";
        int value = 0;
        int cnt = 0;
        while (itr.hasNext()) {
            Map.Entry e = (Map.Entry) itr.next();
            key = (String) e.getKey();
            value = Integer.parseInt(e.getValue().toString());
            if (cnt <= 4) {
                if (cnt == 0) {
                    msg = msg + newLine + "5 cap so ve nhieu nhat la " + key + ":" + value + "lan";
                } else {
                    msg = msg + "," + key + ":" + value;
                }
            }

            if (cnt >= 95) {
                if (cnt == 95) {
                    msg = msg + newLine + "5 cap so ve it nhat la " + key + ":" + value + "lan";
                } else {
                    msg = msg + "," + key + ":" + value;
                }
            }
            cnt++;
        }
        return msg;
    }

    public void process(String mobileOperator) {
        Calendar cal = Calendar.getInstance();
        int h = cal.get(Calendar.HOUR_OF_DAY);
        Vector listRequest = dbTool.getRequestFromQueue(mobileOperator, Preference.commnandCodeSOICAU);
        try {
            for (int i = 0; i < listRequest.size(); i++) {
                smsReceiveQueue = (SmsReceiveQueue) listRequest.elementAt(i);
                requestId = smsReceiveQueue.getId();
                userId = smsReceiveQueue.getUserId();
                serviceId = smsReceiveQueue.getServiceId();
                status = smsReceiveQueue.getStatus();
                commandCode = smsReceiveQueue.getCommandCode();
                info = (smsReceiveQueue.getInfo()).trim().toUpperCase();
                tmp = commandCode + " " + info.substring(commandCode.length(), info.length());
                tmp = tmp.toUpperCase();

                String subCode1 = "";
                String subCode2 = "";
                String subCode3 = "";

                String code1 = "";
                String code2 = "";
                String code3 = "";

                String result = "";

                code1 = StringTool.getObjectString(tmp, 1);

                LotteryCompany lotteryCompany = null;
                BigDecimal companyId = null;
                boolean getCAUXX = false;

                if ((code1 == null) || (code1.equals(""))) {
                    //Defaul is CAU MB
                    lotteryCompany = (LotteryCompany) Main.lotteryCompanyHT.get("MB");
                } else if (StringTool.isNumberic(code1) && (code1.length() == 2)) {
                    //For MB only
                    getCAUXX = true;
                    lotteryCompany = (LotteryCompany) Main.lotteryCompanyHT.get("MB");
                } else {
                    lotteryCompany = (LotteryCompany) Main.lotteryCompanyHT.get(code1);
                }

                if (getCAUXX) {
                    //Get
                    result = getCAU(dbTool.getListResult(new BigDecimal(1)), String.valueOf(code1));
                    subCode1 = "1";
                    subCode1 = "xx";
                } else if (lotteryCompany != null) {
                    companyId = lotteryCompany.getCompanyId();
                    result = dbTool.getTXTData_1("CAU", String.valueOf(companyId));
                    subCode1 = String.valueOf(companyId);
                }
                String cmpId = String.valueOf(companyId);
                if (!result.equals("")) {
                    if (commandCode.equalsIgnoreCase("SOI")) {
                        if ((!serviceId.equals("8084") && !serviceId.equals("8184") && !serviceId.equals("8284")) || (Preference.listAdmin.contains(userId))) {
                            boolean deductMoney = false;
                            if (((h >= 20) || (h < 8)) && (cmpId.equals("1")) && ((code1 == null) || (code1.equals("")))) {
                                result = dbTool.getTXTData_1("SCW", "W");
                                if ((result == null) || (result.equalsIgnoreCase(""))) {
                                    result = "Hien tai he thong dang cap nhat CAUMB.De co thoi gian nghien cuu va du doan duoc chinh xac,chung toi se gui ket qua cho ban vao 8h sang hang ngay.Xin cam on!";
                                }
                                CauItem cauItem = new CauItem();
                                cauItem.setCommandCode(commandCode);
                                cauItem.setCompanyId(1);
                                cauItem.setInfo(subCode1 + " " + subCode2);
                                cauItem.setMobileOperator(mobileOperator);
                                cauItem.setRemain(1);
                                cauItem.setRequestDate(requestId.toString());
                                cauItem.setRequestId(requestId);
                                cauItem.setServiceId(serviceId);
                                cauItem.setUserId(userId);
                                cauItem.setStatus(1);
                                dbTool.add2CauDaily(cauItem);
                                dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0,
                                        result, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " add Wait " + lotteryCompany.getCompanyDesc() + "<==" + serviceId);
                            } else {
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + " " + lotteryCompany.getCompanyDesc() + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            }
                        } else {
                            result = "Ban gui sai dau so dich vu,hay soan: " + info + " gui8584.";
                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0,
                                    result, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + " " + lotteryCompany.getCompanyDesc() + "<==WrongServiceId" + serviceId);
                        }
                    } else {
                        if ((!serviceId.equals("8084") && !serviceId.equals("8184") && !serviceId.equals("8284") && !serviceId.equals("8384") && !serviceId.equals("8484")) || (Preference.listAdmin.contains(userId))) {
                            if (((h >= 20) || (h < 8)) && (cmpId.equals("1"))) {
                                result = dbTool.getTXTData_1("SCW", "W");
                                if ((result == null) || (result.equalsIgnoreCase(""))) {
                                    result = "Hien tai he thong dang cap nhat CAUMB.De co thoi gian nghien cuu va du doan duoc chinh xac,chung toi se gui ket qua cho ban vao 8h sang hang ngay.Xin cam on!";
                                }
                                CauItem cauItem = new CauItem();
                                cauItem.setCommandCode(commandCode);
                                cauItem.setCompanyId(1);
                                cauItem.setInfo(subCode1 + " " + subCode2);
                                cauItem.setMobileOperator(mobileOperator);
                                cauItem.setRemain(1);
                                cauItem.setRequestDate(requestId.toString());
                                cauItem.setRequestId(requestId);
                                cauItem.setServiceId(serviceId);
                                cauItem.setUserId(userId);
                                cauItem.setStatus(1);
                                dbTool.add2CauDaily(cauItem);

                                dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0,
                                        result, 1, requestId, subCode1, subCode2, subCode3);
                                MyLogger.log(userId + "<==" + commandCode + " add Wait " + lotteryCompany.getCompanyDesc() + "<==" + serviceId);
                            } else {
                                boolean deductMoney = false;
                                result = result.replaceAll("!xxx!", "|");
                                StringTokenizer stToken = new StringTokenizer(result, "|", false);
                                int stTokenLen = stToken.countTokens();
                                for (int ii = 0; ii < stTokenLen; ii++) {
                                    String txtSms = stToken.nextToken();
                                    if (!deductMoney) {
                                        dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0,
                                                txtSms, 1, requestId, subCode1, subCode2, subCode3);
                                        deductMoney = true;
                                        MyLogger.log(userId + "<==" + commandCode + " " + lotteryCompany.getCompanyDesc() + "<==" + serviceId);
                                    } else {
                                        dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0,
                                                txtSms, 0, requestId, subCode1, subCode2, subCode3);
                                    }
                                    if (ii > 5) {
                                        break;
                                    }
                                }
                            }
                        } else {
                            result = "Ban gui sai dau so dich vu,hay soan: " + info + " gui8584.";
                            dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0,
                                    result, 1, requestId, subCode1, subCode2, subCode3);
                            MyLogger.log(userId + "<==" + commandCode + " " + lotteryCompany.getCompanyDesc() + "<==WrongServiceId" + serviceId);
                        }
                    }
                } else {
                    if (status != 0) {
                        result = "De SOI CAU soan: " + commandCode + " <MaTinh> gui8584." + enter +
                                "Vidu: " + commandCode + " MB gui8584 lay soi CAU MienBac," + commandCode + " TG gui8584 lay CAU TienGiang" + enter +
                                "DT ho tro 1900561577";
                        if (dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 1, requestId, subCode1, subCode2,
                                subCode3)) {
                            MyLogger.log(userId + "<==" + commandCode + "WrongForamt<==" + serviceId + ":" + info);
                        }
                    } else {
                        dbTool.updateStatus(requestId, 1, mobileOperator);
                    }
                }
            } //End for

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void run() {
        while (Main.running) {
            try {
                process("VIETEL");
                process("GPC");
                process("VMS");
                Thread.sleep(5000);
            } catch (Exception ex) {
            }
        }
    }
}
