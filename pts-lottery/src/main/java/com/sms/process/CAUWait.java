/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sms.process;

import com.sms.common.MyLogger;
import com.sms.item.CauItem;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 *
 * @author hungdt
 */
public class CAUWait extends Thread {
    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */
    DBTool dbTool = null;
    String alert = "";
    char enter = (char) 10;
    BigDecimal id = null;
    BigDecimal requestId = null;
    String userId = "";
    String serviceId = "";
    String commandCode = "";
    String info = "";
    String mobileOperator = "";
    String infoTmp = "";
    String data = "";
    String dataTmp = "";
    String result = "";
    int companyId = 0;
    boolean isProcessing = false;
    boolean check = false;
    int remain = 0;

    public CAUWait() {
        dbTool = new DBTool();
    }

    public void processRequestQueue() {
        isProcessing = true;

        CauItem cauItem = null;
        Calendar cal = Calendar.getInstance();
        int h = cal.get(Calendar.HOUR_OF_DAY);
        try {
            if (h == 8) {
                Vector listRequest = dbTool.getRequestFromCAUWait();
                System.out.println("Total in queue CKWait is: " + listRequest.size());
                for (int i = 0; i < listRequest.size(); i++) {
                    cauItem = (CauItem) listRequest.elementAt(i);
                    id = cauItem.getId();
                    requestId = cauItem.getRequestId();
                    userId = cauItem.getUserId();
                    serviceId = cauItem.getServiceId();
                    mobileOperator = cauItem.getMobileOperator();
                    commandCode = cauItem.getCommandCode();
                    info = cauItem.getInfo().trim();
                    remain = cauItem.getRemain();
                    companyId = cauItem.getCompanyId();
                    System.out.println("Process CauWait:" + userId + ":" + info);
                    result = dbTool.getTXTData_1("CAU", String.valueOf(companyId));
                    result = result.replaceAll("!xxx!", "|");
                    StringTokenizer stToken = new StringTokenizer(result, "|", false);
                    int stTokenLen = stToken.countTokens();
                    for (int ii = 0; ii < stTokenLen; ii++) {
                        String txtSms = stToken.nextToken();
                        dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0,
                                txtSms, 0, requestId, "", "", "");
                        MyLogger.log(userId + "<==" + commandCode + " wait " + "<==" + serviceId);
                        if (ii > 5) {
                            break;
                        }
                    }
                    dbTool.updateCauWait(id, remain - 1);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public long syncThread() {
        long ret = 0;
        Calendar now = Calendar.getInstance();
        int h = now.get(Calendar.HOUR_OF_DAY);
        ret = (60 - now.get(Calendar.MINUTE)) * 60 * 1000;
        return ret;
    }

    /**
     * @param args the command line arguments
     */
    @Override
    public void run() {
        System.out.println("CauWait process Thread Start !");
        while (Main.running) {
            try {
                processRequestQueue();
                sleep(syncThread());
            } catch (Exception ex) {
            }
        }
    }
}
