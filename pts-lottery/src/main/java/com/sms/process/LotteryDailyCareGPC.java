package com.sms.process;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import java.util.*;
import java.math.BigDecimal;

import com.sms.common.*;
import com.sms.item.*;

public class LotteryDailyCareGPC extends Thread {

    DBTool dbTool = null;
    String operator = "";
    BigDecimal companyId = null;
    LotteryResult lotteryResult = null;
    ;
    String resultDate = "";
    String result = "";
    int resultStatus = 0;
    LotteryDaily lotteryDaily = null;
    String userId = "";
    String mobileOperator = "";
    String serviceId = "";
    String commandCode = "";
    BigDecimal requestId = null;
    ;
    String subCode1 = "";
    String subCode2 = "";
    String subCode3 = "";

    public LotteryDailyCareGPC() {
        operator = "GPC";
        dbTool = new DBTool();
    }

    public void processLotteryDaily() {
        Vector v = new Vector(Main.lotteryResultHT.keySet());
        String currentDDMMYY = DateProc.getCurrentDDMMYY(); //DD/MM/YY

        Vector lotteryDailyList = new Vector();
        for (int i = 0; i < v.size(); i++) {
            companyId = (BigDecimal) v.elementAt(i);
            lotteryResult = (LotteryResult) Main.lotteryResultHT.get(companyId);
            resultDate = lotteryResult.getResultDate();
            result = lotteryResult.getResult();
            resultStatus = lotteryResult.getResultStatus();
            subCode1 = String.valueOf(companyId);
            if (resultDate.equals(currentDDMMYY)) {
                lotteryDailyList = dbTool.getLotteryDaily(operator, companyId, resultStatus);
                for (int ii = 0; ii < lotteryDailyList.size(); ii++) {
                    lotteryDaily = (LotteryDaily) lotteryDailyList.elementAt(ii);
                    userId = lotteryDaily.getUserId();
                    mobileOperator = lotteryDaily.getMobileOperator();
                    serviceId = lotteryDaily.getServiceId();
                    commandCode = lotteryDaily.getCommandCode();
                    requestId = lotteryDaily.getRequestId();
                    if (dbTool.updateRemain(lotteryDaily, resultStatus)) {

                        if (commandCode.equals("ST") || commandCode.equals("SMN") || commandCode.equals("SMT")) {
                            result = result.replaceAll("CAU", "SC");
                        }

                        dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, result, 0, requestId, subCode1, subCode2, subCode3);
                        if (companyId.intValue() == 1) {
                            MyLogger.log("MienBac:" + serviceId + "==>" + userId);
                        } else {
                            LotteryCompany lotteryCompany = (LotteryCompany) Main.lotteryCompanyIDHT.get(companyId);
                            MyLogger.log(lotteryCompany.getCompanyDesc() + ":" + serviceId + "==>" + userId);
                        }
                        if (resultStatus == 0) {
                            String spam = dbTool.getSpam();
                            if (!spam.equalsIgnoreCase("")) {
                                dbTool.sendMTEx(userId, serviceId, commandCode, mobileOperator, 0, spam, 0, requestId, subCode1, subCode2,
                                        subCode3);
                                MyLogger.log(userId + "<==" + commandCode + "<==" + serviceId + ":send Spam");
                            }
                        }
                    }
                }
            }
        } //End for

    }

    @Override
    public void run() {
        try {
            Thread.sleep(5 * 1000);
        } catch (Exception ex) {
        }
        MyLogger.log("LotteryDaily " + operator + " Care start!");
        while (Main.running) {
            processLotteryDaily();
            int HH24 = DateProc.getCurrentHH24();
            if ((HH24 >= 16) && (HH24 <= 21)) {
                try {
                    Thread.sleep(500);
                } catch (Exception ex) {
                }
            } else {
                try {
                    Thread.sleep(5 * 1000);
                } catch (Exception ex) {
                }
            }
        }
    }
}
