/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.services.hn;

import com.pts.common.Common;
import com.pts.common.Validator;
import org.apache.log4j.Logger;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

/**
 *
 * @author hungdt
 */
public class HNMOProcessor {

    private static Logger logger = Logger.getLogger(HNMOProcessor.class);

    public String[] parseSMSHNContent(String smsContent, String cmdCode) {
        String content[] = new String[3];
        for (int i = 0; i < 3; i++) {
            content[i] = "";
        }
        String strPattern = "";
        try {
            cmdCode = cmdCode.trim();
            if (cmdCode.equalsIgnoreCase("HN")) {
                strPattern = "([Hh][Nn])\\D*(\\d*)\\D*(\\d*)\\D*";
                content[0] = "HN";
                smsContent = smsContent.substring(2).trim();
                if (smsContent.indexOf(" ") > 0) {
                    content[1] = smsContent.substring(0, smsContent.indexOf(" "));
                    content[2] = smsContent.substring(smsContent.indexOf(" ")).trim();
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }

                if (smsContent.lastIndexOf(" ") > 0) {
                    content[2] = smsContent.substring(smsContent.lastIndexOf(" ")).trim();
                    if (Validator.validPhoneNumber(content[2])) {
                        content[1] = smsContent.substring(0, smsContent.lastIndexOf(" "));
                    } else {
                        content[1] = smsContent;
                        content[2] = "";
                    }
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }
                //System.out.println(content[1].length());
                if (content[1].length() == 3) {
                    if (content[1].substring(1, 2).equalsIgnoreCase(" ")) {
                        content[1] = content[1].substring(0, 1) + content[1].substring(2, 3);
                    }
                }
            } else if (cmdCode.equalsIgnoreCase("TNH")) {
                strPattern = "([Tt][Nn][Hh])\\D*(\\d*)\\D*(\\d*)\\D*";
                content[0] = "TNH";
                smsContent = smsContent.substring(3).trim();
                if (smsContent.indexOf(" ") > 0) {
                    content[1] = smsContent.substring(0, smsContent.indexOf(" "));
                    content[2] = smsContent.substring(smsContent.indexOf(" ")).trim();
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }

                if (smsContent.lastIndexOf(" ") > 0) {
                    content[2] = smsContent.substring(smsContent.lastIndexOf(" ")).trim();
                    if (Validator.validPhoneNumber(content[2])) {
                        content[1] = smsContent.substring(0, smsContent.lastIndexOf(" "));
                    } else {
                        content[1] = smsContent;
                        content[2] = "";
                    }
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }
                //System.out.println(content[1].length());
                if (content[1].length() == 3) {
                    if (content[1].substring(1, 2).equalsIgnoreCase(" ")) {
                        content[1] = content[1].substring(0, 1) + content[1].substring(2, 3);
                    }
                }
            } else if (cmdCode.equalsIgnoreCase("SH")) {
                strPattern = "([Ss][Hh])\\D*(\\d*)\\D*(\\d*)\\D*";
                content[0] = "SH";
                smsContent = smsContent.substring(2).trim();
                if (smsContent.indexOf(" ") > 0) {
                    content[1] = smsContent.substring(0, smsContent.indexOf(" "));
                    content[2] = smsContent.substring(smsContent.indexOf(" ")).trim();
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }

                if (smsContent.lastIndexOf(" ") > 0) {
                    content[2] = smsContent.substring(smsContent.lastIndexOf(" ")).trim();
                    if (Validator.validPhoneNumber(content[2])) {
                        content[1] = smsContent.substring(0, smsContent.lastIndexOf(" "));
                    } else {
                        content[1] = smsContent;
                        content[2] = "";
                    }
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }
                //System.out.println(content[1].length());
                if (content[1].length() == 3) {
                    if (content[1].substring(1, 2).equalsIgnoreCase(" ")) {
                        content[1] = content[1].substring(0, 1) + content[1].substring(2, 3);
                    }
                }
            } else if (cmdCode.equalsIgnoreCase("BNH")) {
                strPattern = "([Bn][Nn][Hh])\\D*(\\d*)\\D*(\\d*)\\D*";
                content[0] = "BNH";
                smsContent = smsContent.substring(3).trim();
                if (smsContent.indexOf(" ") > 0) {
                    content[1] = smsContent.substring(0, smsContent.indexOf(" "));
                    content[2] = smsContent.substring(smsContent.indexOf(" ")).trim();
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }

                if (smsContent.lastIndexOf(" ") > 0) {
                    content[2] = smsContent.substring(smsContent.lastIndexOf(" ")).trim();
                    if (Validator.validPhoneNumber(content[2])) {
                        content[1] = smsContent.substring(0, smsContent.lastIndexOf(" "));
                    } else {
                        content[1] = smsContent;
                        content[2] = "";
                    }
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }
                //System.out.println(content[1].length());
                if (content[1].length() == 3) {
                    if (content[1].substring(1, 2).equalsIgnoreCase(" ")) {
                        content[1] = content[1].substring(0, 1) + content[1].substring(2, 3);
                    }
                }
            } else if (cmdCode.equalsIgnoreCase("DPH")) {
                strPattern = "([Dd][Pp][Hh])\\D*(\\d*)\\D*(\\d*)\\D*";
                content[0] = "DPH";
                smsContent = smsContent.substring(3).trim();
                if (smsContent.indexOf(" ") > 0) {
                    content[1] = smsContent.substring(0, smsContent.indexOf(" "));
                    content[2] = smsContent.substring(smsContent.indexOf(" ")).trim();
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }

                if (smsContent.lastIndexOf(" ") > 0) {
                    content[2] = smsContent.substring(smsContent.lastIndexOf(" ")).trim();
                    if (Validator.validPhoneNumber(content[2])) {
                        content[1] = smsContent.substring(0, smsContent.lastIndexOf(" "));
                    } else {
                        content[1] = smsContent;
                        content[2] = "";
                    }
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }
                //System.out.println(content[1].length());
                if (content[1].length() == 3) {
                    if (content[1].substring(1, 2).equalsIgnoreCase(" ")) {
                        content[1] = content[1].substring(0, 1) + content[1].substring(2, 3);
                    }
                }
            } else {
                return content;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("{com.pts.services.hn.HNProcessor}parseSMSContent@" + ex.toString());
        }
        return content;
    }

    public String getRandFile(String dir) {
        String ret = "";
        File f = new File(dir);
        File[] listFile = f.listFiles();
        ret = listFile[Common.getRandomInteger(0, listFile.length - 1)].getName();
        if (!ret.endsWith(".jpg")) {
            ret = getRandFile(dir);
//            System.out.println("dequy");
        }
        return ret;
    }

    public void CreateImgWithBg(String filebg, String strdata, String output) {
//         set width,height
        int width = 176, height = 230;
        try {
//            read bg
            BufferedImage imgbg = ImageIO.read(new File(filebg));
//            Create image
            BufferedImage image = new BufferedImage(width,
                    height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
//            g.setColor(Color.BLACK);
            g.setBackground(Color.WHITE);
            g.drawImage(imgbg, 0, 0, null);
            //kiem tra text
            if (strdata.length() == 2) {
                if (filebg.endsWith("1.jpg") || filebg.endsWith("5.jpg") || filebg.endsWith("7.jpg") || filebg.endsWith("8.jpg")) {
                    g.setColor(Color.RED);
                    // setFont
                    g.setFont(new Font("VNI-Butlong", Font.LAYOUT_RIGHT_TO_LEFT, 40));
                    // addText
                    g.drawString(strdata.substring(0, 1), 5, 60);
                    g.setColor(Color.BLUE);
                    // setFont
                    g.setFont(new Font("VNI-Kun", Font.LAYOUT_RIGHT_TO_LEFT, 40));
                    // addText
//                    g.drawString(strdata.substring(1, 2), 30, 110);
                    g.drawString(strdata.substring(1, 2), 30, 80);
                } else if (filebg.endsWith("2.jpg") || filebg.endsWith("6.jpg")) {
                    g.setColor(Color.RED);
                    // setFont
                    g.setFont(new Font("VNI-Butlong", Font.LAYOUT_RIGHT_TO_LEFT, 40));
                    // addText
                    g.drawString(strdata.substring(0, 1), 15, 60);
                    g.setColor(Color.BLUE);
                    // setFont
                    g.setFont(new Font("VNI-Kun", Font.LAYOUT_RIGHT_TO_LEFT, 40));
                    // addText
                    g.drawString(strdata.substring(1, 2), 75, 60);
//                    g.drawString(strdata.substring(1, 2), 20, 60);
                } else if (filebg.endsWith("3.jpg")) {
                    g.setColor(Color.RED);
                    // setFont
                    g.setFont(new Font("VNI-Butlong", Font.LAYOUT_RIGHT_TO_LEFT, 25));
                    // addText
                    g.drawString(strdata.substring(0, 1), 15, 95);
                    g.setColor(Color.BLUE);
                    // setFont
                    g.setFont(new Font("VNI-Kun", Font.LAYOUT_RIGHT_TO_LEFT, 35));
                    // addText
                    g.drawString(strdata.substring(1, 2), 110, 130);
                } else if (filebg.endsWith("4.jpg")) {
                    g.setColor(Color.RED);
                    // setFont
                    g.setFont(new Font("VNI-Butlong", Font.LAYOUT_RIGHT_TO_LEFT, 35));

                    // addText
                    g.drawString(strdata.substring(0, 1), 30, 45);
                    g.setColor(Color.BLUE);
                    // setFont
                    g.setFont(new Font("VNI-Kun", Font.LAYOUT_RIGHT_TO_LEFT, 35));
                    // addText
                    g.drawString(strdata.substring(1, 2), 50, 60);
                } else if (filebg.endsWith("9.jpg")) {
                    g.setColor(Color.RED);
                    // setFont
                    g.setFont(new Font("VNI-Butlong", Font.LAYOUT_RIGHT_TO_LEFT, 50));

                    // addText
                    g.drawString(strdata.substring(0, 1), 75, 115);
                    g.setColor(Color.BLUE);
                    // setFont
                    g.setFont(new Font("VNI-Kun", Font.LAYOUT_RIGHT_TO_LEFT, 50));
                    // addText
                    g.drawString(strdata.substring(1, 2), 50, 165);
                } else if (filebg.endsWith("10.jpg")) {
                    g.setColor(Color.RED);
                    // setFont
                    g.setFont(new Font("VNI-Butlong", Font.LAYOUT_RIGHT_TO_LEFT, 50));

                    // addText
                    g.drawString(strdata, 30, 115);
//                    g.setColor(Color.BLUE);
//                    // setFont
//                    g.setFont(new Font("VNI-Kun", Font.LAYOUT_RIGHT_TO_LEFT, 50));
//                    // addText
//                    g.drawString(strdata.substring(1, 2), 50, 165);
                } else if (filebg.endsWith("11.jpg")) {
                    g.setColor(Color.RED);
                    // setFont
                    g.setFont(new Font("VNI-Butlong", Font.LAYOUT_RIGHT_TO_LEFT, 50));

                    // addText
                    g.drawString(strdata.substring(0, 1), 25, 115);
                    g.setColor(Color.BLUE);
                    // setFont
                    g.setFont(new Font("VNI-Kun", Font.LAYOUT_RIGHT_TO_LEFT, 50));
                    // addText
                    g.drawString(strdata.substring(1, 2), 45, 145);
                } else {
                    g.setColor(Color.RED);
                    // setFont
                    g.setFont(new Font("VNI-Butlong", Font.LAYOUT_RIGHT_TO_LEFT, 40));
                    // addText
                    g.drawString(strdata.substring(0, 1), 5, 60);
                    g.setColor(Color.BLUE);
                    // setFont
                    g.setFont(new Font("VNI-Kun", Font.LAYOUT_RIGHT_TO_LEFT, 40));
                    // addText
                    g.drawString(strdata.substring(1, 2), 10, 80);
                }
            } else {
                if (filebg.endsWith("2.jpg") || filebg.endsWith("4.jpg") || filebg.endsWith("5.jpg") || filebg.endsWith("6.jpg") || filebg.endsWith("7.jpg") || filebg.endsWith("9.jpg") || filebg.endsWith("10.jpg")) {
                    g.setColor(Color.RED);
                    // setFont
                    g.setFont(new Font("Tahoma", Font.LAYOUT_RIGHT_TO_LEFT, 30));
                    // addText
                    g.drawString(strdata, (int) Math.floor(((12 - strdata.length())) / 2) * 6 + 2, 40);
                } else if (filebg.endsWith("11.jpg")) {
                    g.setColor(Color.RED);
                    // setFont
                    g.setFont(new Font("Tahoma", Font.LAYOUT_RIGHT_TO_LEFT, 25));
                    // addText
                    g.drawString(strdata, (int) Math.floor(((12 - strdata.length())) / 2) * 6 + 2, 220);
                } else {
                    g.setColor(Color.RED);
                    g.setFont(new Font("Tahoma", Font.LAYOUT_RIGHT_TO_LEFT, 30));
                    // addText
                    g.drawString(strdata, (int) Math.floor(((12 - strdata.length())) / 2) * 6 + 2, 95);
                }
            }
//            g.drawString(strdata, 0, height / 4);

            g.dispose();

            // write to file
            Iterator writers = ImageIO.getImageWritersByFormatName("jpg");
            ImageWriter writer = (ImageWriter) writers.next();
            if (writer == null) {
                throw new RuntimeException("jpg not supported?!");
            }
            ImageOutputStream out = ImageIO.createImageOutputStream(
                    new File(output));
            writer.setOutput(out);
            writer.write(image);
            out.close(); // close flushes buffer

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public ArrayList toArray(String str) {
        String tmp = "";
        ArrayList ret = new ArrayList();
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == ' ') {
                ret.add(tmp);
                tmp = "";
            } else {
                tmp += str.charAt(i);
            }
            if (i == str.length() - 1) {
                ret.add(tmp);
            }
        }

        return ret;
    }

//    public static void main(String a[]) {
//        HNMOProcessor hn = new HNMOProcessor();
//        String[] as = hn.parseSMSHNContent("TNHh Pggd 0916943692", "TNH");
//        for (int i = 0; i < as.length; i++) {
//            System.out.println(as[i]);
//        }
//        String dir = "D:/Pts/25_05/2";
//        for (int i = 0; i < 20; i++) {
//            System.out.println(hn.getRandFile(dir));
//        }
//        hn.CreateImgWithBg("D:/Pts/25_05/2/4.jpg", "HP", "D:/Pts/25_05/2/4_1.jpg");
//    }
}
