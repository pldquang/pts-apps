/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.services.hn;

import com.pts.common.CharactorProcess;
import com.pts.common.Validator;
import com.pts.process.Constants;
import com.pts.process.ContentAbstract;
import com.pts.process.MsgObject;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author hungdt
 */
public class HN extends ContentAbstract {

    private static Logger logger = Logger.getLogger(HN.class);

    @Override
    protected Collection getMessages(MsgObject msgObject, String keyword) throws Exception {
        Collection messages;
        String moCome;
        MsgObject msg, msgTang;
        String userId;
        String phoneNumber;
        String strOperator;
        Properties conf = new Properties();
        String mtValue = "";
        String content = "";
        String bg = "", out = "", filename = "";
        CharactorProcess chrt = new CharactorProcess();
        try {
            conf.load(new FileInputStream("ptsServices.properties"));
            out = conf.getProperty("pts.hn.push.dir");
            messages = new ArrayList();
            msg = new MsgObject(msgObject);
            HNMOProcessor hn = new HNMOProcessor();
            String strServiceID = msgObject.getServiceId();
            strServiceID = strServiceID.substring(1, 2);
            moCome = msgObject.getUsertext();
            userId = msgObject.getUserId();
            //Noi dung tao file
            logger.info("User info:" + msgObject.getUserId() + "@" + msgObject.getUsertext());
            String output[] = hn.parseSMSHNContent(moCome, msgObject.getCommandCode());
            if (msgObject.getServiceId().equalsIgnoreCase("8084") || msgObject.getServiceId().equalsIgnoreCase("8284")) {
                if (msgObject.getCommandCode().equalsIgnoreCase("SH")) {
                    msgObject.setUsertext("Ban soan tin sai so dich vu,de nhan hinh nen theo yeu cau,soan tin: SH <Chucaitenban> <Chucaiten nguoiay> gui 8584. \r\nDT ho tro: 1900561577.");
                } else if (msgObject.getCommandCode().equalsIgnoreCase("BNH")) {
                    msgObject.setUsertext("Ban soan tin sai so dich vu,de nhan hinh nentheo yeu cau,soan tin: BNH <Chucaitenban> <Chucaiten nguoiay> gui 8584. DT ho tro: 1900561577.");
                } else {
                    msgObject.setUsertext("Ban soan tin sai so dich vu,de nhan Hinh Nen,soan tin: HN " + output[1] + " gui 8484.De gui tang,soan: HN <Noidung> <SDTnhan> gui 8484 \r\nDT ho tro: 1900561577.");
                }
                msgObject.setContentType(0);
                msgObject.setMessageType(1);
                messages.add(msgObject);
                return messages;
            }
            if (msgObject.getServiceId().equalsIgnoreCase("8184")) {
                if (!Constants.isWhiteList(msgObject.getUserId())) {
                    if (msgObject.getCommandCode().equalsIgnoreCase("SH")) {
                        msgObject.setUsertext("Ban soan tin sai so dich vu,de nhan hinh nen theo yeu cau,soan tin: SH <Chucaitenban> <Chucaiten nguoiay> gui 8584. \r\nDT ho tro: 1900561577.");
                    } else if (msgObject.getCommandCode().equalsIgnoreCase("BNH")) {
                        msgObject.setUsertext("Ban soan tin sai so dich vu,de nhan hinh nentheo yeu cau,soan tin: BNH <Chucaitenban> <Chucaiten nguoiay> gui 8584. DT ho tro: 1900561577.");
                    } else {
                        msgObject.setUsertext("Ban soan tin sai so dich vu,de nhan Hinh Nen,soan tin: HN " + output[1] + " gui 8484.De gui tang,soan: HN <Noidung> <SDTnhan> gui 8484 \r\nDT ho tro: 1900561577.");
                    }
                    msgObject.setContentType(0);
                    msgObject.setMessageType(1);
                    messages.add(msgObject);
                    return messages;
                }
            }

            //Truong hop gui tang
            if (!output[2].equalsIgnoreCase("") && output[2] != null) {
                if (!Validator.validPhoneNumber(output[2])) {
                    msg.setUsertext("Ban da nhap sai so thue bao duoc tang");
                    msg.setMessageType(0);
                    messages.add(msg);
                    //Thiet lam ban tin SPAM
                    return messages;
                } else {
                    phoneNumber = output[2];
                    if (phoneNumber.startsWith("0")) {//neu so thue bao co dang 0xxxxxxxxx
                        //thay so "0" bang 84

                        phoneNumber = "84" + phoneNumber.substring(1);
                    }
                    strOperator = Validator.getOperator(phoneNumber);
                    //ban tin cho nguoi tang
                    msg.setUsertext("Ban vua gui tang Hinh Nen den so dien thoai " + output[2] + ".\r\nDT ho tro: 1900561577.");

                    msg.setMessageType(1);//tru tien nguoi tang

                }
                messages.add(msg);
                //ban tin cho nguoi dc tang
                msgTang = new MsgObject(msgObject);
                msgTang.setUserId(phoneNumber);
                msgTang.setMobileOperator(strOperator);
                //tao file hn
                content = output[1];
                filename = msgObject.getUserId() + Calendar.getInstance().getTimeInMillis() + ".jpg";
                if (output[1].length() == 2) {
                    content = content.toUpperCase();
                    bg = conf.getProperty("pts.hn.source.dir") + "/2/";
                } else {
                    bg = conf.getProperty("pts.hn.source.dir") + "/m/";
                }
                hn.CreateImgWithBg(bg + hn.getRandFile(bg), chrt.ConvertUnicode2VNI(chrt.convertString(content)), out + "/" + filename);
                mtValue = "[" + content + "] So dien thoai " + msgObject.getUserId() + " tang ban :" + "http://222.252.16.196/download/" + filename;
                logger.info("{com.pts.services.hn.HN}Text send to user: " + mtValue + "$");
                msgTang.setUsertext(mtValue);
                msgTang.setContentType(8);
                messages.add(msgTang);
                return messages;
            }
            //Truong hop ko gui tang
            content = output[1];
            filename = msgObject.getUserId() + Calendar.getInstance().getTimeInMillis() + ".jpg";
            if (output[1].length() == 2) {
                content = content.toUpperCase();
                bg = conf.getProperty("pts.hn.source.dir") + "/2/";
            } else {
                bg = conf.getProperty("pts.hn.source.dir") + "/m/";
            }
            hn.CreateImgWithBg(bg + hn.getRandFile(bg), chrt.ConvertUnicode2VNI(chrt.convertString(content)), out + "/" + filename);
            mtValue = "[" + content + "] :" + "http://222.252.16.196/download/" + filename;
            msg.setMessageType(1);

            msg.setUsertext(mtValue);
            msg.setContentType(8);
            messages.add(msg);
            logger.info("{com.pts.services.hn.HN}Text send to user: " + mtValue + "$");
//            //Thiet lam ban tin SPAM
            return messages;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
            return null;
        }
    }
}
