/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.services.logo;

import com.pts.common.Validator;
import java.util.Random;
import org.apache.log4j.Logger;

/**
 *
 * @author hungdt
 */
public class LGMOProcessor {

    private static Logger logger = Logger.getLogger(LGMOProcessor.class);

//    public String[] parseSMSLGContent(String smsContent, String cmdCode) {
//        String content[] = new String[4];
//        for (int i = 0; i < 4; i++) {
//            content[i] = "";
//        }
//        String strPattern = "";
//        try {
//            cmdCode = cmdCode.trim();
//            if (cmdCode.equalsIgnoreCase("LG")) {
//                strPattern = "([Ll][Oo][Gg][Oo])\\D*(\\d*)\\D*(\\d*)\\D*";
//            } else {
//                return content;
//            }
//            content[0] = "LG";
//            smsContent = smsContent.substring(2).trim();
//            if (smsContent.indexOf(" ") > 0) {
//                content[1] = smsContent.substring(0, smsContent.indexOf(" "));
//                smsContent = smsContent.substring(smsContent.indexOf(" ")).trim();
//            } else {
//                content[1] = "0";
//                content[2] = smsContent;
//            }
//            if (smsContent.indexOf(" ") > 0) {
//                if (smsContent.lastIndexOf(" ") > 0) {
//                    content[3] = smsContent.substring(smsContent.lastIndexOf(" ")).trim();
//                    if (Validator.validPhoneNumber(content[3])) {
//                        content[2] = smsContent.substring(0, smsContent.lastIndexOf(" "));
//                    } else {
//                        content[2] = smsContent;
//                        content[3] = "";
//                    }
//                } else {
//                    content[2] = smsContent;
//                    content[3] = "";
//                }
//
//            } else {
//                content[2] = smsContent;
//                content[3] = "";
//            }
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            logger.error("{com.pts.services.logo.LOGOProcessor}parseSMSContent@" + ex.toString());
//        }
//        return content;
//    }
    public String[] parseSMSLGContent(String smsContent, String cmdCode) {
        String content[] = new String[3];
        for (int i = 0; i < 3; i++) {
            content[i] = "";
        }
        String strPattern = "";
        try {
            cmdCode = cmdCode.trim();
            if (cmdCode.equalsIgnoreCase("LG")) {
                strPattern = "([Ll][Gg])\\D*(\\d*)\\D*(\\d*)\\D*";
                content[0] = "LG";
                smsContent = smsContent.substring(2).trim();
                if (smsContent.indexOf(" ") > 0) {
                    content[1] = smsContent.substring(0, smsContent.indexOf(" "));
                    content[2] = smsContent.substring(smsContent.indexOf(" ")).trim();
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }

                if (smsContent.lastIndexOf(" ") > 0) {
                    content[2] = smsContent.substring(smsContent.lastIndexOf(" ")).trim();
                    if (Validator.validPhoneNumber(content[2])) {
                        content[1] = smsContent.substring(0, smsContent.lastIndexOf(" "));
                    } else {
                        content[1] = smsContent;
                        content[2] = "";
                    }
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }

            } else if (cmdCode.equalsIgnoreCase("TNL")) {
                strPattern = "([Tt][Nn][Ll])\\D*(\\d*)\\D*(\\d*)\\D*";
                content[0] = "TNL";
                smsContent = smsContent.substring(3).trim();
                if (smsContent.indexOf(" ") > 0) {
                    content[1] = smsContent.substring(0, smsContent.indexOf(" "));
                    content[2] = smsContent.substring(smsContent.indexOf(" ")).trim();
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }

                if (smsContent.lastIndexOf(" ") > 0) {
                    content[2] = smsContent.substring(smsContent.lastIndexOf(" ")).trim();
                    if (Validator.validPhoneNumber(content[2])) {
                        content[1] = smsContent.substring(0, smsContent.lastIndexOf(" "));
                    } else {
                        content[1] = smsContent;
                        content[2] = "";
                    }
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }
            } else if (cmdCode.equalsIgnoreCase("SG")) {
                strPattern = "([Ss][Gg])\\D*(\\d*)\\D*(\\d*)\\D*";
                content[0] = "SG";
                smsContent = smsContent.substring(2).trim();
                if (smsContent.indexOf(" ") > 0) {
                    content[1] = smsContent.substring(0, smsContent.indexOf(" "));
                    content[2] = smsContent.substring(smsContent.indexOf(" ")).trim();
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }

                if (smsContent.lastIndexOf(" ") > 0) {
                    content[2] = smsContent.substring(smsContent.lastIndexOf(" ")).trim();
                    if (Validator.validPhoneNumber(content[2])) {
                        content[1] = smsContent.substring(0, smsContent.lastIndexOf(" "));
                    } else {
                        content[1] = smsContent;
                        content[2] = "";
                    }
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }
            } else if (cmdCode.equalsIgnoreCase("BNG")) {
                strPattern = "([Bb][Nn][Gg])\\D*(\\d*)\\D*(\\d*)\\D*";
                content[0] = "BNG";
                smsContent = smsContent.substring(3).trim();
                if (smsContent.indexOf(" ") > 0) {
                    content[1] = smsContent.substring(0, smsContent.indexOf(" "));
                    content[2] = smsContent.substring(smsContent.indexOf(" ")).trim();
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }

                if (smsContent.lastIndexOf(" ") > 0) {
                    content[2] = smsContent.substring(smsContent.lastIndexOf(" ")).trim();
                    if (Validator.validPhoneNumber(content[2])) {
                        content[1] = smsContent.substring(0, smsContent.lastIndexOf(" "));
                    } else {
                        content[1] = smsContent;
                        content[2] = "";
                    }
                } else {
                    content[1] = smsContent;
                    content[2] = "";
                }
            }  else {
                return content;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("{com.pts.services.logo.LOGOProcessor}parseSMSContent@" + ex.toString());
        }
        return content;
    }

    public String getFont() {
        String ret = "VNI-Yahoo";
        String font[] = {"VNI-Butlong",
            "VNI-Yahoo", "VNI-Souvir"
        };
//        if ((id > 0) && (id <= font.length)) {
//            ret = font[id - 1];
//        }
        int item = getRandomInteger(0, font.length - 1);
        ret = font[item];
        return ret;
    }

    public boolean checkInfo(String info) {
        boolean ret = false;
        int t;
        for (int i = 0; i < info.length(); i++) {
            t = (int) info.charAt(i);
//            if ((t == 32) || (t == 47) || ((t >= 48) && (t <= 57)) || ((t >= 65) && (t <= 122))) {
//                ret += info.charAt(i);
//            }
            if ((t < 32) || (t > 126)) {
                ret = true;
                break;
            }
        }
        return ret;
    }

    private int getRandomInteger(int aStart, int aEnd) {
        if (aStart > aEnd) {
            throw new IllegalArgumentException("Start cannot exceed End.");
        }
        //get the range, casting to long to avoid overflow problems
        long range = (long) aEnd - (long) aStart + 1;
        Random aRandom = new Random();
        // compute a fraction of the range, 0 <= frac < range
        long fraction = (long) (range * aRandom.nextDouble());
        int randomNumber = (int) (fraction + aStart);
        return randomNumber;
    }

    /*public boolean  pastString(String str){
    boolean ret = false;
    char t;
    int code = 0;
    try{
    for(int  i = 0 ; i < str.length() ; i++){
    t = str.charAt(i);
    code = (int) t;
    if(code < 0) || (code > 126 )){
    ret = true;
    break;
    }
    }
    }catch(Exception ex){
    logger.error(ex);
    
    }
    return ret;
    }*/
//    public static void main(String a[]) {
//        LGMOProcessor lg = new LGMOProcessor();
//        String[] t = lg.parseSMSLGContent("SG h p ", "SG");
//        for (int i = 0; i < t.length; i++) {
//            System.out.println(t[i]);
//        }
////        System.out.println("r = " + lg.checkInfo("đ"));
//    }
}
