/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.services.logo;

import com.pts.common.CharactorProcess;
import com.pts.common.ImageProcess;
import com.pts.common.Validator;
import com.pts.process.Constants;
import com.pts.process.ContentAbstract;
import com.pts.process.DBPool;
import com.pts.process.MsgObject;
import com.pts.process.OperatorLogo;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author hungdt
 */
public class Logo extends ContentAbstract {

    private static Logger logger = Logger.getLogger(Logo.class);

    @Override
    protected Collection getMessages(MsgObject msgObject, String keyword) throws Exception {
        Collection messages;
        String moCome;
        MsgObject msg, msgTang;
        String userId;
        String phoneNumber;
        String strOperator;
        Properties conf = new Properties();
        try {
            conf.load(new FileInputStream("ptsServices.properties"));
            messages = new ArrayList();
            msg = new MsgObject(msgObject);
            LGMOProcessor ltmo = new LGMOProcessor();
            String strServiceID = msgObject.getServiceId();
            strServiceID = strServiceID.substring(1, 2);
            moCome = msgObject.getUsertext();
            userId = msgObject.getUserId();
            logger.info("User info:" + msgObject.getUserId() + "@" + msgObject.getUsertext());
            String output[] = ltmo.parseSMSLGContent(moCome, msgObject.getCommandCode());
            if (msgObject.getServiceId().equalsIgnoreCase("8084") || msgObject.getServiceId().equalsIgnoreCase("8284")) {
                if (msgObject.getCommandCode().equalsIgnoreCase("SG")) {
                    msgObject.setUsertext("Ban soan tin sai so dich vu,de nhan Logo,soan tin: SG " + output[1] + " gui 8584.De gui tang,soan: SG <Noidung> <SDTnhan> gui 8584 \r\nDT ho tro: 1900561577.");
                } else if (msgObject.getCommandCode().equalsIgnoreCase("BNG")) {
                    msgObject.setUsertext("Ban soan tin sai so dich vu,de nhan Logo theo yeu cau,soan tin: BNG <Noidung> gui 8584. DT ho tro: 1900561577.");
                } else {
                    msgObject.setUsertext("Ban soan tin sai so dich vu,de nhan Logo,soan tin: LG " + output[1] + " gui 8484.De gui tang,soan: LG <Noidung> <SDTnhan> gui 8484 \r\nDT ho tro: 1900561577.");
                }
                msgObject.setContentType(0);
                msgObject.setMessageType(1);
                messages.add(msgObject);
                return messages;
            }
            if (msgObject.getServiceId().equalsIgnoreCase("8184")) {
                if (!Constants.isWhiteList(msgObject.getUserId())) {
                    if (msgObject.getCommandCode().equalsIgnoreCase("SG")) {
                        msgObject.setUsertext("Ban soan tin sai so dich vu,de nhan Logo,soan tin: SG " + output[1] + " gui 8584.De gui tang,soan: SG <Noidung> <SDTnhan> gui 8584 \r\nDT ho tro: 1900561577.");
                    } else if (msgObject.getCommandCode().equalsIgnoreCase("BNG")) {
                        msgObject.setUsertext("Ban soan tin sai so dich vu,de nhan Logo theo yeu cau,soan tin: BNG <Noidung> gui 8584. DT ho tro: 1900561577.");
                    } else {
                        msgObject.setUsertext("Ban soan tin sai so dich vu,de nhan Logo,soan tin: LG " + output[1] + " gui 8484.De gui tang,soan: LG <Noidung> <SDTnhan> gui 8484 \r\nDT ho tro: 1900561577.");
                    }
                    msgObject.setContentType(0);
                    msgObject.setMessageType(1);
                    messages.add(msgObject);
                    return messages;
                }
            }
            //Noi dung tao file

//            if (output[1].startsWith("821")) {
//                msgObject.setCommandCode("INV");
//                DBPool dBPool = new DBPool();
//                dBPool.insert2INV(msgObject);
//                return null;
//            }
            //kiem tra tieng viet
            if (ltmo.checkInfo(output[1])) {
//                msgObject.setCommandCode("INV");
//                DBPool dBPool = new DBPool();
//                dBPool.insert2INV(msgObject);
//                return null;
                msgObject.setUsertext("Ban da nhap sai noi dung. Noi dung nhap phai go tieng viet khong dau kieu telext. \r\nDT ho tro: 1900561577.");
                msgObject.setContentType(0);
                msgObject.setMessageType(1);
                messages.add(msgObject);
                return messages;
            }
            //Truong hop gui tang
            if (!output[2].equalsIgnoreCase("") && output[2] != null) {
                if (!Validator.validPhoneNumber(output[2])) {
                    msg.setUsertext("Ban da nhap sai so thue bao duoc tang");
                    msg.setMessageType(0);
                    messages.add(msg);
                    //Thiet lam ban tin SPAM
                    return messages;
                } else {
                    phoneNumber = output[2];
                    if (phoneNumber.startsWith("0")) {//neu so thue bao co dang 0xxxxxxxxx
                        //thay so "0" bang 84

                        phoneNumber = "84" + phoneNumber.substring(1);
                    }
                    strOperator = Validator.getOperator(phoneNumber);
                    //ban tin cho nguoi tang
                    msg.setUsertext("Ban vua gui tang Logo den so dien thoai " + output[2] + ".\r\nDT ho tro: 1900561577.");

                    msg.setMessageType(1);//tru tien nguoi tang

                }
                messages.add(msg);
                //ban tin cho nguoi dc tang
                msgTang = new MsgObject(msgObject);
                msgTang.setUserId(phoneNumber);
                msgTang.setMobileOperator(strOperator);
                //tao file ota
                ImageProcess processImg = new ImageProcess();
                CharactorProcess cp = new CharactorProcess();
                String font = ltmo.getFont();
                String prifex = conf.getProperty("pts.logo.dir");
                String filename = phoneNumber + "_" + Calendar.getInstance().getTime().getTime() + ".png";
                String fileota = filename.substring(0, filename.length() - 3) + "otb";
                processImg.CreateImg(cp.ConvertUnicode2VNI(cp.convertString(output[1])), font, prifex + filename);
                Thread.sleep(100);
                File f = new File(prifex + filename);
                if (!f.isFile()) {
                    Thread.sleep(10);
                } else {
                    processImg.ConvertImage2Otb(prifex + filename, prifex + fileota);
                }
                msgTang.setMessageType(0);
                OperatorLogo lg = new OperatorLogo(prifex + fileota);
                lg.encode();
                String mtValue = lg.getEncoded().getHexDump();
                msgTang.setUsertext(mtValue);
                msgTang.setContentType(2);
                messages.add(msgTang);
//                    //Thiet lam ban tin SPAM
                return messages;
            }
            //Truong hop ko gui tang
            //tao file ota
            ImageProcess processImg = new ImageProcess();
            CharactorProcess cp = new CharactorProcess();
            String font = ltmo.getFont();
            String prifex = conf.getProperty("pts.logo.dir");
            String filename = userId + "_" + Calendar.getInstance().getTime().getTime() + ".png";
            String fileota = filename.substring(0, filename.length() - 3) + "otb";
            processImg.CreateImg(cp.ConvertUnicode2VNI(cp.convertString(output[1])), font, prifex + filename);
            Thread.sleep(100);
            File f = new File(prifex + filename);
            if (!f.isFile()) {
                Thread.sleep(10);
            } else {
                processImg.ConvertImage2Otb(prifex + filename, prifex + fileota);
            }

            msg.setMessageType(1);

            OperatorLogo lg = new OperatorLogo(prifex + fileota);
            lg.encode();
            String mtValue = lg.getEncoded().getHexDump();
            msg.setUsertext(mtValue);
            msg.setContentType(2);
            messages.add(msg);
            logger.info("{com.pts.services.logo.Logo}Text send to user: " + mtValue + "$");
//            //Thiet lam ban tin SPAM
            return messages;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
