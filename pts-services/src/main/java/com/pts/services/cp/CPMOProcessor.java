/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.services.cp;

import com.pts.common.Validator;
import com.pts.db.pool.ConnectionDB;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;

/**
 *
 * @author hungdt
 */
public class CPMOProcessor {

    private static Logger logger = Logger.getLogger(CPMOProcessor.class);
    private Connection conn = null;
    ResultSet rs = null;
    PreparedStatement pstmt = null;
    ConnectionDB db = new ConnectionDB();

    public CPMOProcessor() {
        if (conn == null) {
            conn = db.getConnectionContent();
        }
    }

    public void freeResouceContent(PreparedStatement pstmt, ResultSet rs, Connection conn) {
        try {
            if (pstmt != null) {
                pstmt.close();
            }
            if (rs != null) {
                rs.close();
            }
            if (conn != null) {
                conn.setAutoCommit(true);
            }
        } catch (Exception ex) {
        }
    }

    public String[] parseSMSContent(String smsContent, String cmdCode) {
        String content[] = new String[3];
        for (int i = 0; i < 3; i++) {
            content[i] = "";
        }
        try {
            cmdCode = cmdCode.trim();
            content[0] = cmdCode.toUpperCase();
            smsContent = smsContent.substring(cmdCode.length()).trim();
            if (smsContent.indexOf(" ") > 0) {
                content[1] = smsContent.substring(0, smsContent.lastIndexOf(" "));
                content[2] = smsContent.substring(smsContent.lastIndexOf(" ")).trim();
                if (!Validator.validPhoneNumber(content[2])) {
                    content[1] = content[1] + content[2];
                    content[2] = "";
                }
            } else {
                content[1] = smsContent;
            }
            content[1] = content[1].replaceAll(" ", "");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
//        for (int i = 0; i < content.length; i++) {
//            System.out.print(content[i] + " ");
//        }
        return content;
    }

    public String getContent(String cat) {
        String ret = "";
//        ConnectionDB cn = new ConnectionDB();
        String strSQL = "select * from CLIP where ID =?";
        String strSQLClip = "select * from (Select * from txt_data where command_id='CP' and param1=? order by dbms_random.value) where  rownum<2";
        PreparedStatement pstmt1 = null;
        ResultSet rs1 = null;
        try {
            if (conn != null) {
                pstmt = conn.prepareStatement(strSQLClip);
                pstmt.setString(1, cat.toUpperCase());
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    pstmt1 = conn.prepareStatement(strSQL);
                    pstmt1.setBigDecimal(1, new BigDecimal(rs.getString("CONTENT").substring(3)));
                    rs1 = pstmt1.executeQuery();
                    if (rs1.next()) {
                        ret = rs.getString("CONTENT") + "@" + rs1.getString("FILENAME");
                    }
                }
            }
            logger.info(cat + " -> " + ret);
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            freeResouceContent(pstmt, rs, conn);
            freeResouceContent(pstmt1, rs1, conn);
//            cn.freeConnectionContent();
        }
        //idFull@name
        return ret;
    }

    public String add2PushInfo(String receiver, String sender, BigDecimal contentId, String dataType) {
        String sqlStr = "";
        long random = (long) (Math.random() * 10000000);
        String auth = "";
//        ConnectionDB cn = new ConnectionDB();
        try {
            if (conn != null) {
                if (checkContentMedia(dataType, contentId)) {
                    auth = contentId.toString() + random;
                    sqlStr = "INSERT INTO INFO_PUSH(ID,CONTENT_ID,SENDER,RECEIVER,AUTH,DATA_TYPE) VALUES(INFO_PUSH_ID_SEQ.NEXTVAL,?,?,?,?,?)";
                    pstmt = conn.prepareStatement(sqlStr);
                    pstmt.setBigDecimal(1, contentId);
                    pstmt.setString(2, sender);
                    pstmt.setString(3, receiver);
                    pstmt.setString(4, auth);
                    pstmt.setString(5, dataType);
                    pstmt.executeUpdate();
                    conn.commit();
                }
            } else {
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
//            cn.freeConnectionContent();
            try {
                if (conn != null) {
                    conn.close();
                }
                db.freeConnectionContent();
            } catch (Exception ex) {
            }
            return auth;
        }
    }

    public boolean checkContentMedia(String dataType, BigDecimal contentId) {
        boolean b = false;
        String tableName = "";
        String dataField = "";
        String sqlStr = "";
        try {
            if (conn != null) {
                if (dataType.equals("12")) {
                    tableName = "RINGTONE";
                    dataField = "POLY_MID";
                } else if (dataType.equals("14")) {
                    tableName = "RINGTONE";
                    dataField = "WORD_AMR";
                } else if (dataType.equals("24")) {
                    tableName = "PHOTO";
                    dataField = "CONTENT_SIZE1";
                } else if (dataType.equals("31")) {
                    tableName = "JAVAGAME";
                    dataField = "CONTENT_JAR";
                } else if (dataType.equals("32")) {
                    tableName = "THEME";
                    dataField = "CONTENT_JAR";
                } else if (dataType.equals("41")) {
                    tableName = "CLIP";
                    dataField = "CONTENT_3GP";
                }

                sqlStr = "SELECT " + dataField + " FROM " + tableName + " WHERE ID = ?";
                pstmt = conn.prepareStatement(sqlStr);
                pstmt.setBigDecimal(1, contentId);
                rs = pstmt.executeQuery();
                byte[] data = null;
                if (rs.next()) {
                    Blob dataBlob = rs.getBlob(1);
                    InputStream is = dataBlob.getBinaryStream();
                    data = new byte[(int) dataBlob.length()];
                    is.read(data);
                    is.close();
                }
                if ((data != null) && (data.length > 0)) {
                    b = true;
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            freeResouceContent(pstmt, rs, conn);
            return b;
        }

    }
//    public static void main(String args[]) {
//        CPMOProcessor moProcessor = new CPMOProcessor();
//        String[] test = moProcessor.parseSMSContent("CP haihuoc 0916943692", "CP");
//        for (int i = 0; i < test.length; i++) {
//            System.out.println(" t [" + i + "]=" + test[i]);
//        }
//    }
}
