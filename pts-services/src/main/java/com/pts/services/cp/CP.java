/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.services.cp;

import com.pts.common.Validator;
import com.pts.process.Constants;
import com.pts.process.ContentAbstract;
import com.pts.process.MsgObject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.log4j.Logger;

/**
 *
 * @author hungdt
 */
public class CP extends ContentAbstract {

    private static Logger logger = Logger.getLogger(CP.class);

    @Override
    protected Collection getMessages(MsgObject msgObject, String keyword) throws Exception {
        Collection messages;
        String moCome;
        MsgObject msg, msgTang;
        String userId;
        String phoneNumber;
        String strOperator;
//        Properties conf = new Properties();
        String mtValue = "";
        String content = "";
        try {
//            conf.load(new FileInputStream("ptsServices.properties"));
            messages = new ArrayList();
            msg = new MsgObject(msgObject);
            CPMOProcessor hn = new CPMOProcessor();
            String strServiceID = msgObject.getServiceId();
            strServiceID = strServiceID.substring(1, 2);
            moCome = msgObject.getUsertext();
            userId = msgObject.getUserId();
            //Noi dung tao file
            logger.info("User info:" + msgObject.getUserId() + "@" + msgObject.getUsertext());
            String output[] = hn.parseSMSContent(moCome, msgObject.getCommandCode());
            if (msgObject.getServiceId().equalsIgnoreCase("8084") || msgObject.getServiceId().equalsIgnoreCase("8284")) {
                if (msgObject.getCommandCode().equalsIgnoreCase("SP")) {
                    msgObject.setUsertext("Ban soan tin sai so dich vu,de tai clip theo yeu cau,soan tin: SP <Chude> gui 8584.Cac chu de gom:Haihuoc,batngo,Langman,Hotgirl. DT ho tro: 1900561577.");
                } else if (msgObject.getCommandCode().equalsIgnoreCase("BNP")) {
                    msgObject.setUsertext("Ban soan tin sai so dich vu,de tai Clip theo yeu cau,soan tin: BNP <Chude> gui 8584.Cac chu de gom:Haihuoc,batngo,Langman,Hotgirl. DT ho tro: 1900561577.");
                } else if (msgObject.getCommandCode().equalsIgnoreCase("TNCP")) {
                    msgObject.setUsertext("Ban soan tin sai so dich vu,de tai clip,soan tin: TNCP <Chude> gui 8584. Cac chu de gom: Haihuoc, langman, Hotgirl, batngo. DT ho tro: 1900561577");
                } else {
                    msgObject.setUsertext("Ban gui sai so dich vu, de tai clip theo yeu cau,soan tin: CP " + output[1] + " gui8584.\r\nDT ho tro: 1900561577");
                }
                msgObject.setContentType(0);
                msgObject.setMessageType(1);
                messages.add(msgObject);
                logger.info(msgObject.getUsertext() + " -> " + "Ban da gui tin nhan sai dau so.");
                return messages;
            }
            if (msgObject.getServiceId().equalsIgnoreCase("8184")) {
                if (!Constants.isWhiteList(msgObject.getUserId())) {
                    if (msgObject.getCommandCode().equalsIgnoreCase("SP")) {
                        msgObject.setUsertext("Ban soan tin sai so dich vu,de tai clip theo yeu cau,soan tin: SP <Chude> gui 8584.Cac chu de gom:Haihuoc,batngo,Langman,Hotgirl. DT ho tro: 1900561577.");
                    } else if (msgObject.getCommandCode().equalsIgnoreCase("BNP")) {
                        msgObject.setUsertext("Ban soan tin sai so dich vu,de tai Clip theo yeu cau,soan tin: BNP <Chude> gui 8584.Cac chu de gom:Haihuoc,batngo,Langman,Hotgirl. DT ho tro: 1900561577.");
                    } else if (msgObject.getCommandCode().equalsIgnoreCase("TNCP")) {
                        msgObject.setUsertext("Ban soan tin sai so dich vu,de tai clip,soan tin: TNCP <Chude> gui 8584. Cac chu de gom: Haihuoc, langman, Hotgirl, batngo. DT ho tro: 1900561577");
                    } else {
                        msgObject.setUsertext("Ban gui sai so dich vu, de tai clip theo yeu cau,soan tin: CP " + output[1] + " gui8584.\r\nDT ho tro: 1900561577");
                    }
                    msgObject.setContentType(0);
                    msgObject.setMessageType(1);
                    messages.add(msgObject);
                    logger.info(msgObject.getUsertext() + " -> " + "Ban da gui tin nhan sai dau so.");
                    return messages;
                }
            }
            //Truong hop gui tang
            if (!output[2].equalsIgnoreCase("") && output[2] != null) {
                if (!Validator.validPhoneNumber(output[2])) {
                    msg.setUsertext("Ban da nhap sai so thue bao duoc tang");
                    msg.setMessageType(0);
                    messages.add(msg);
                    //Thiet lam ban tin SPAM
                    logger.info(msgObject.getUsertext() + " -> " + "Ban da nhap sai so thue bao duoc tang");
                    return messages;
                } else {
                    phoneNumber = output[2];
                    if (phoneNumber.startsWith("0")) {//neu so thue bao co dang 0xxxxxxxxx
                        //thay so "0" bang 84

                        phoneNumber = "84" + phoneNumber.substring(1);
                    }
                    strOperator = Validator.getOperator(phoneNumber);

                    //ban tin cho nguoi dc tang
                    msgTang = new MsgObject(msgObject);
                    msgTang.setUserId(phoneNumber);
                    msgTang.setMobileOperator(strOperator);
                    //tao file hn
                    content = hn.getContent(output[1]);
                    String name = "";
                    String idFull = "";
                    if (content.indexOf("@") > 0) {
                        idFull = content.substring(0, content.indexOf("@"));
                        name = content.substring(content.indexOf("@") + 1);
                        mtValue = "[PTS] " + idFull + "  So dien thoai " + msgObject.getUserId() + " tang ban :" + "http://222.252.16.196/push/" + name + "?auth=" + hn.add2PushInfo(phoneNumber, msgObject.getUserId(), new BigDecimal(idFull.substring(3)), idFull.substring(1, 3));
                        logger.info("{com.pts.services.cp.CP}Text send to user: " + mtValue + "$");
                        msgTang.setUsertext(mtValue);
                        msgTang.setContentType(8);
                        messages.add(msgTang);
                        //ban tin cho nguoi tang
                        msg.setUsertext("Ban vua gui tang video Clip den so dien thoai " + output[2] + ".\r\nDT ho tro: 1900561577.");
                        msg.setMessageType(1);//tru tien nguoi tang
                        msg.setContentType(0);
                        messages.add(msg);
                    } else {
                        //ban tin cho nguoi tang
                        msg.setUsertext("Ban soan tin sai chu de: De tang Clip theo yeu cau,soan tin: CP Chude SDTnhan gui 8584. Cac chude gom: Haihuoc,Batngo,Hotgirl.\r\nDT ho tro: 1900561577.");
                        msg.setMessageType(1);//tru tien nguoi tang
                        msg.setContentType(0);
                        logger.info("{com.pts.services.cp.CP}Text send to user: Ban da nhap sai noi dung.$");
                        messages.add(msg);
                    }
                    return messages;
                }
            }
            //Truong hop ko gui tang
            content = hn.getContent(output[1]);
            String name = "";
            String idFull = "";
            if (content.indexOf("@") > 0) {
                idFull = content.substring(0, content.indexOf("@"));
                name = content.substring(content.indexOf("@") + 1);
                mtValue = "[PTS] " + idFull + ":" + "http://222.252.16.196/push/" + name + "?auth=" + hn.add2PushInfo(msgObject.getUserId(), msgObject.getUserId(), new BigDecimal(idFull.substring(3)), idFull.substring(1, 3));
                logger.info("{com.pts.services.cp.CP}Text send to user: " + mtValue + "$");
                msg.setUsertext(mtValue);
                msg.setContentType(8);
            } else {
                mtValue = "Ban soan tin sai chu de: De tai Clip theo yeu cau,soan tin: CP Chude gui 8584. Cac chude gom: Haihuoc,Batngo,Hotgirl.\r\nDT ho tro: 1900561577.";
                msg.setUsertext(mtValue);
                msg.setContentType(0);
            }
            msg.setMessageType(1);
            messages.add(msg);
            logger.info("{com.pts.services.cp.CP}Text send to user: " + mtValue + "$");
            return messages;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
            return null;
        }
    }
}
