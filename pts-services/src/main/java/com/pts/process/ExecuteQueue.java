/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.process;

import com.pts.db.pool.ConnectionDB;
import com.pts.ptsMain;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author hungdt
 */
public class ExecuteQueue extends Thread {

    private static Logger logger = Logger.getLogger(ExecuteQueue.class);
    int threadID;
    MsgQueue queue;
    BigDecimal AM;
    Connection con;
    int TIME_DELAY_EXECUTE = 5000;
    Properties conf;

    public Properties getConf() {
        return conf;
    }

    public void setConf(Properties conf) {
        this.conf = conf;
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public ExecuteQueue(MsgQueue queue, int threadID) {
        this.threadID = 0;
        this.queue = null;
        AM = new BigDecimal(-1D);
        this.queue = queue;
        this.threadID = threadID;
    }

    @Override
    public void run() {
        while (ptsMain.processData) {
            try {
                MsgObject msgObject = null;
                String process_result = "";
                try {
                    sleep(1500L);
                } catch (InterruptedException interruptedexception) {
                }
                process_result = "";
                try {
                    msgObject = (MsgObject) queue.remove();
                    if (Constants.commandCode.length > 0) {
                        logger.info("keyword=" + msgObject.getCommandCode());
                        process_result = processQueueMsg(msgObject, msgObject.getCommandCode());
//                    queueLog.add(new MsgObject(msgObject.getUserId(), serviceId, msgObject.getMobileOperator(), keyword.getKeyword(), msgObject.getUsertext(), msgObject.getTimestamp(), msgObject.getResponded(), "", "", "", "", "", null, 1, msgObject.getContentType(), msgObject.getMessageType(), msgObject.getRequestId(), msgObject.getCpId()));
                    }
                } catch (Exception ex) {
                    logger.error("Execute queue. Ex:" + ex.toString());
                    ex.printStackTrace();
                    queue.add(msgObject);
                }
                sleep(TIME_DELAY_EXECUTE);
            } catch (InterruptedException ex) {
                logger.error(ex);
            }
        }
    }

    private String processQueueMsg(MsgObject msgObject, String keyword) {
        ContentAbstract delegate = null;
        try {
            String classname = "";
            if (!keyword.equals("")) {
                classname = getClassName(keyword);
                logger.info("processQueueMsg:" + msgObject.getUserId() + "@" + msgObject.getUsertext() + "@" + msgObject.getRequestId().toString() + "@" + classname);
                Class delegateClass = Class.forName(classname);
                Object delegateObject = delegateClass.newInstance();
                delegate = (ContentAbstract) delegateObject;
                if ((getCon() == null) || getCon().isClosed()) {
                    ConnectionDB cn = new ConnectionDB();
                    setCon(cn.getConnectionRemote(getConf()));
                }
                delegate.setCon(getCon());
                delegate.start(msgObject, keyword);
            }
        } catch (Exception e) {
            logger.error("processQueueMsg:" + msgObject.getUserId() + "@" + msgObject.getUsertext() + "@" + msgObject.getRequestId().toString() + "@" + e.toString());
            return msgObject.getUserId() + ":" + e.toString();
        }
        return "OK";
    }

    public String getClassName(String commandCode) {
        String classname = "";
        for (int i = 0; i < Constants.commandCode.length; i++) {
            if (Constants.commandCode[i].equalsIgnoreCase(commandCode)) {
                classname = Constants.listClass[i];
                break;
            }
        }
        return classname;
    }
}
