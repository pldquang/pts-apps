/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.process;

import com.pts.db.pool.ConnectionDB;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author hungdt
 */
public class DBPool {

    private static Logger logger = Logger.getLogger(DBPool.class);

    public boolean insert2INV(MsgObject msgObject) {
        boolean ret = false;
        PreparedStatement pstmt = null;
        Connection con = null;
        String insert = "INSERT INTO " + msgObject.getMobileOperator() + ".sms_receive_queue(ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMMAND_CODE,INFO,STATUS) values(?,?,?,?,?,?,?)";
        logger.info("INSERT TO QUEUE INV " + msgObject.getUserId() + "@" + msgObject.getUsertext());
        ConnectionDB cn = new ConnectionDB();
        Properties conf = new Properties();
        try {
            conf.load(new FileInputStream("ptsServices.properties"));
            con = cn.getConnectionRemote(conf);
            pstmt = con.prepareStatement(insert);
            pstmt.setBigDecimal(1, msgObject.getRequestId());
            pstmt.setString(2, msgObject.getUserId());
            pstmt.setString(3, msgObject.getServiceId());
            pstmt.setString(4, msgObject.getMobileOperator());
            pstmt.setString(5, msgObject.getCommandCode());
            pstmt.setString(6, msgObject.getUsertext());
            pstmt.setInt(7, msgObject.getStatus());
            pstmt.executeUpdate();
            ret = true;
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
            }
            cn.freeConnectionRemote();
        }
        return ret;
    }
}
