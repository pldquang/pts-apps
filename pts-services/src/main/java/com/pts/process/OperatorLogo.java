/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.process;

import java.io.FileInputStream;
import java.io.IOException;
import org.smpp.util.ByteBuffer;

/**
 *
 * @author hungdt
 */
public class OperatorLogo {

    public void setOTB(byte b[]) {
        OTB = b;
    }

    public byte[] getOTB() {
        return OTB;
    }

    public ByteBuffer getEncoded() {
        return encoded;
    }

    public boolean encode()
            throws Exception {
        ByteBuffer buffer = new ByteBuffer();
        buffer.appendBytes(OTB);
        encoded = buffer;
        return true;
    }

    public OperatorLogo() {
        OTB = null;
        encoded = null;
    }

    public OperatorLogo(String text, byte data[]) {
        OTB = null;
        encoded = null;
        OTB = data;
    }

    public OperatorLogo(String filename)
            throws Exception {
        OTB = null;
        encoded = null;
        if (filename == null) {
            throw new Exception("File name is not set");
        }
        filename = filename.toLowerCase();
        try {
            ByteBuffer buf = loadByteBuffer(filename);
            setOTB(buf.getBuffer());
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public ByteBuffer loadByteBuffer(String fileName)
            throws IOException {
        FileInputStream is = new FileInputStream(fileName);
        byte data[] = new byte[is.available()];
        is.read(data);
        is.close();
        return new ByteBuffer(data);
    }

//    public static void main(String args[]) {
//        try {
//            OperatorLogo pic = new OperatorLogo("C:/temp/b2.otb");
//            pic.encode();
//            System.out.println(pic.getEncoded().getHexDump());
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
    static final int LOGO_WIDTH_DEFAULT = 72;
    static final int LOGO_HEIGHT_DEFAULT = 14;
    private byte OTB[];
    private ByteBuffer encoded;
}
