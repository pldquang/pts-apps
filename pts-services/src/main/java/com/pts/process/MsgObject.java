/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.process;

import java.math.BigDecimal;

/**
 *
 * @author hungdt
 */
public class MsgObject {

    private String userId;
    private String serviceId;
    private String mobileOperator;
    private String commandCode;
    private String subcode1;
    private String subcode2;
    private String subcode3;
    private int status;
    private BigDecimal requestId;
    private String usertext;
    private int contentType;
    private int messageType;

    public MsgObject() {
    }

    public MsgObject(BigDecimal requestId, String userId, String serviceId, String mobileOperator, String commandCode, String usertext, int status) {
        this.requestId = requestId;
        this.userId = userId;
        this.serviceId = serviceId;
        this.mobileOperator = mobileOperator;
        this.commandCode = commandCode;
        this.usertext = usertext;
        this.status = status;
        this.subcode1 = "";
        this.subcode2 = "";
        this.subcode3 = "";
    }

    public MsgObject(MsgObject msgObject) {
        this.userId = msgObject.getUserId();
        this.serviceId = msgObject.getServiceId();
        this.commandCode = msgObject.getCommandCode();
        this.mobileOperator = msgObject.getMobileOperator();
        this.contentType = msgObject.getContentType();
        this.messageType = msgObject.getMessageType();
        this.requestId = msgObject.getRequestId();
        this.usertext = msgObject.getUsertext();
        this.contentType = msgObject.getContentType();
        this.messageType = msgObject.getMessageType();
        this.subcode1 = msgObject.getSubcode1();
        this.subcode2 = msgObject.getSubcode2();
        this.subcode3 = msgObject.getSubcode3();
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getSubcode1() {
        return subcode1;
    }

    public void setSubcode1(String subcode1) {
        this.subcode1 = subcode1;
    }

    public String getSubcode2() {
        return subcode2;
    }

    public void setSubcode2(String subcode2) {
        this.subcode2 = subcode2;
    }

    public String getSubcode3() {
        return subcode3;
    }

    public void setSubcode3(String subcode3) {
        this.subcode3 = subcode3;
    }

    public String getUsertext() {
        return usertext;
    }

    public void setUsertext(String usertext) {
        this.usertext = usertext;
    }

    public String getCommandCode() {
        return commandCode;
    }

    public void setCommandCode(String commandCode) {
        this.commandCode = commandCode;
    }

    public int getContentType() {
        return contentType;
    }

    public void setContentType(int contentType) {
        this.contentType = contentType;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public String getMobileOperator() {
        return mobileOperator;
    }

    public void setMobileOperator(String mobileOperator) {
        this.mobileOperator = mobileOperator;
    }

    public BigDecimal getRequestId() {
        return requestId;
    }

    public void setRequestId(BigDecimal requestId) {
        this.requestId = requestId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
