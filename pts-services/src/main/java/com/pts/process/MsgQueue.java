/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.process;

import java.util.Vector;

/**
 *
 * @author hungdt
 */
public class MsgQueue {

    protected Vector queue;

    public MsgQueue() {
        queue = new Vector();
    }

    public Object remove() {
        synchronized (queue) {
            Object item;
            while (queue.isEmpty()) {
                try {
                    queue.wait();
                } catch (InterruptedException interruptedexception) {
                }
            }
            item = queue.firstElement();
            queue.removeElement(item);
            return item;
        }
    }

    public void add(Object item) {
        synchronized (queue) {
            queue.addElement(item);
            queue.notify();
        }
    }

    public long getSize() {
        return (long) queue.size();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public void setVector(Vector v) {
        queue = v;
    }

    public Vector getVector() {
        return queue;
    }
}
