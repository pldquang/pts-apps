/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.process;

import com.pts.db.pool.ConnectionDB;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author hungdt
 */
public abstract class ContentAbstract {

    private static Logger logger = Logger.getLogger(ContentAbstract.class);
    private Connection con;

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public ContentAbstract() {
    }

    public void start(MsgObject msgObject, String keyword)
            throws Exception {
        try {
            Collection messages = getMessages(msgObject, keyword);
            if (messages != null) {
                Iterator iter = messages.iterator();
                for (int i = 1; iter.hasNext(); i++) {
                    MsgObject msgMT = (MsgObject) iter.next();
                    sendMT(msgMT);
                }
            } else {
                logger.info("ContentAbstract@start:" + msgObject.getUserId() + "@TO" + msgObject.getServiceId() + "@" + msgObject.getUsertext() + ": LOST MESSAGE");
            }
        } catch (Exception e) {
            logger.error("ContentAbstract@start:" + msgObject.getUserId() + "@TO" + msgObject.getServiceId() + "@" + msgObject.getUsertext() + ": LOST MESSAGE" + e.toString());
        }
    }

    protected abstract Collection getMessages(MsgObject msgObject, String keyword)
            throws Exception;

    public BigDecimal sendMT(MsgObject msgObject) {
        BigDecimal bigdecimal = new BigDecimal(-1D);
        PreparedStatement statement = null;
        String sSQLInsert = "";
        logger.info("add2SMSSendQueue:" + msgObject.getUserId() + "@" + msgObject.getUsertext());
        String tablename = msgObject.getMobileOperator() + ".sms_send_queue";
        sSQLInsert = "insert into " + tablename + "(ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMMAND_CODE,CONTENT_TYPE,INFO,MESSAGE_TYPE,REQUEST_ID,subcode1,subcode2,subcode3)" + " values(?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
//            removeSMSFormReciveQueue(msgObject.getMobileOperator(), msgObject.getRequestId(), con);
            if ((con == null) || con.isClosed()) {
                ConnectionDB cn = new ConnectionDB();
                Properties conf = new Properties();
                conf.load(new FileInputStream("ptsServices.properties"));
                con = cn.getConnectionRemote(conf);
            }
            bigdecimal = nextSequence(msgObject.getMobileOperator() + ".SEQ_SMS_SEND_QUEUE_ID");
//            System.out.println("bigdecimal = " + bigdecimal);
            statement = con.prepareStatement(sSQLInsert);
            statement.setBigDecimal(1, bigdecimal);
            statement.setString(2, msgObject.getUserId());
            statement.setString(3, msgObject.getServiceId());
            statement.setString(4, msgObject.getMobileOperator());
            statement.setString(5, msgObject.getCommandCode());
            statement.setInt(6, msgObject.getContentType());
            statement.setString(7, msgObject.getUsertext());
            statement.setInt(8, msgObject.getMessageType());
            statement.setBigDecimal(9, msgObject.getRequestId());
            statement.setString(10, msgObject.getSubcode1());
            statement.setString(11, msgObject.getSubcode2());
            statement.setString(12, msgObject.getSubcode3());

            if (statement.executeUpdate() == 1) {
                bigdecimal = msgObject.getRequestId();
            } else {
                logger.error("add2SMSSendQueue:" + msgObject.getUserId() + ":" + msgObject.getUsertext() + ":statement.executeUpdate failed");
                bigdecimal = new BigDecimal(-1D);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("add2SMSSendQueue:" + msgObject.getUserId() + ":" + msgObject.getUsertext() + ":Error add row from sms send queue:" + e.toString());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("add2SMSSendQueue:" + msgObject.getUserId() + ":" + msgObject.getUsertext() + ":Error add row from sms send queue:" + e.toString());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                con.setAutoCommit(true);
            } catch (Exception ex) {
            }
        }
        return bigdecimal;
    }

    protected BigDecimal nextSequence(String name) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        BigDecimal nextId = null;
        String sqlStr = null;
        try {
            if (con != null) {
                sqlStr = "SELECT " + name + ".NEXTVAL FROM DUAL";
                pstmt = con.prepareStatement(sqlStr);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    nextId = rs.getBigDecimal(1);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
                con.setAutoCommit(true);
            } catch (Exception ex) {
            }
        }
        return nextId;
    }
}
