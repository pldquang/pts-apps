/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.process;

import com.pts.db.pool.ConnectionDB;
import com.pts.ptsMain;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author hungdt
 */
public class LoadMO extends Thread {

    private static Logger logger = Logger.getLogger(LoadMO.class);
    MsgQueue queue;
    int processnum;
    int processindex;
    Connection con;
    String operator;
    Properties conf;
    int TIME_DELAY_LOAD_MO = 5000;
    public static String mobileOperators[] = {
        "GPC", "VMS", "VIETEL", "EVN", "SFONE", "HTC"
    };

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public Properties getConf() {
        return conf;
    }

    public void setConf(Properties conf) {
        this.conf = conf;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public LoadMO(MsgQueue queue, int processnum, int processindex) {
        this.queue = null;
        this.processnum = 1;
        this.processindex = 1;
        this.queue = queue;
        this.processnum = processnum;
        this.processindex = processindex;
    }

    @Override
    public void run() {
        String SQL_LOAD;
        MsgObject msgObject = null;
        String serviceId = "";
        String userId = "";
        String info = "";
        String mobileOperator = "";
        String commandCode = "";
        BigDecimal requestId = new BigDecimal(-1D);
        String listCode = trimCode(getConf().getProperty("pts.services.keyword"));
        int status = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        while (ptsMain.getData) {
            try {
                SQL_LOAD = "Select  ID,USER_ID,SERVICE_ID,MOBILE_OPERATOR,COMMAND_CODE,INFO,STATUS from " + getOperator() + ".sms_receive_queue where (STATUS = 0 or STATUS = 2) and COMMAND_CODE in(" + listCode + ") and rownum<5";
                logger.info("LoadMO - Start");
//                logger.info("LoadMO - SQL:" + SQL_LOAD);

                logger.info("getOperator() = " + getOperator());
                try {
                    if ((getCon() == null) || getCon().isClosed()) {
                        logger.info("get connection");
                        ConnectionDB cn = new ConnectionDB();
                        setCon(cn.getConnectionRemote(getConf()));
                    }
                    stmt = getCon().prepareStatement(SQL_LOAD, 1004, 1008);
                    if (stmt.execute()) {
                        for (rs = stmt.getResultSet(); rs.next();) {
                            msgObject = new MsgObject();
                            requestId = rs.getBigDecimal("ID");
                            userId = rs.getString("USER_ID");
                            serviceId = rs.getString("SERVICE_ID");
                            mobileOperator = rs.getString("MOBILE_OPERATOR");
                            commandCode = rs.getString("COMMAND_CODE");
                            info = rs.getString("INFO");
                            status = rs.getInt("STATUS");
                            msgObject.setRequestId(requestId);
                            msgObject.setUserId(userId);
                            msgObject.setServiceId(serviceId);
                            msgObject.setMobileOperator(mobileOperator);
                            msgObject.setCommandCode(commandCode);
                            msgObject.setUsertext(info);
                            msgObject.setStatus(status);
                            try {
                                removeSMSFormReciveQueue(msgObject.getMobileOperator(), msgObject.getRequestId());
                                queue.add(msgObject);
                                logger.info("{LoadMO}-add2queue: " + serviceId + "[" + queue.getSize() + "]" + userId + "@" + info + "}");
                            } catch (Exception ex1) {
                                ex1.printStackTrace();
                                logger.error("Load MO. ex1:" + ex1.toString());
                            }
                        }
                    }
                    if (stmt != null) {
                        stmt.close();
                    }
                    if (rs != null) {
                        rs.close();
                    }
                } catch (SQLException ex3) {
                    logger.error("Load MO. SQLException:" + ex3.toString());
                } catch (Exception ex2) {
                    logger.error("Load MO. Exception:" + ex2.toString());
                }
                sleep(TIME_DELAY_LOAD_MO);
            } catch (InterruptedException ex) {
            }
        }
    }

    private String trimCode(String str) {
        String ret = "";
        for (int i = 0; i < Constants.commandCode.length; i++) {
            ret += "'" + Constants.commandCode[i] + "',";
        }
        ret = ret.substring(0, ret.length() - 1);
        //System.out.println("ret = " + ret);
        return ret;
    }

    private BigDecimal removeSMSFormReciveQueue(String Operator, BigDecimal id) {
        PreparedStatement pstmt = null;
        BigDecimal ret = new BigDecimal(-1D);
        String delete = "delete from " + Operator + ".SMS_RECEIVE_QUEUE where id = ?";
        try {
            pstmt = con.prepareStatement(delete);
            pstmt.setBigDecimal(1, id);
            pstmt.executeUpdate();
            ret = id;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                con.setAutoCommit(true);
            } catch (Exception e) {
            }
        }
        return ret;
    }
}
