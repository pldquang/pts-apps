/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.process;

import java.io.FileInputStream;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;
import org.apache.log4j.Logger;

/**
 *
 * @author hungdt
 */
public class Constants {

    private static Logger logger = Logger.getLogger(Constants.class);
    public static String RUNCLASS[] = null;
    public static String LOGPATH = "log/";
    public static String LOGLEVEL = "info,warn,error,crisis";
    public static String LOGFILE = "${yyyy-MM-dd}.log";
    public static Properties _prop;
    public static String commandCode[] = null;
    public static String listClass[] = null;
    public static String listNumber[] = null;

    public static boolean loadProperties(String propFile) {
        Properties properties = new Properties();
        logger.info("Reading configuration file " + propFile);
        try {
            FileInputStream fin = new FileInputStream(propFile);
            properties.load(fin);
            _prop = properties;
            fin.close();
            String runclass = properties.getProperty("pts.services.runclass", "");
            RUNCLASS = parseString(runclass, ",");
            String command = properties.getProperty("pts.services.keyword", "");
            commandCode = parseString(command, ",");
            String listclass = properties.getProperty("pts.services.classname", "");
            listClass = parseString(listclass, ",");
            String listnum = properties.getProperty("pts.services.listnumber", "");
            listNumber = parseString(listnum, ",");
        } catch (Exception e) {
            logger.error("Reading configuration file " + propFile + "@" + e.toString());
            return false;
        }
        return true;
    }

    public static String[] parseString(String text, String seperator) {
        Vector vResult = new Vector();
        if (text == null || "".equals(text)) {
            return null;
        }
        String tempStr = text.trim();
        String currentLabel = null;
        for (int index = tempStr.indexOf(seperator); index != -1; index = tempStr.indexOf(seperator)) {
            currentLabel = tempStr.substring(0, index).trim();
            if (!"".equals(currentLabel)) {
                vResult.addElement(currentLabel);
            }
            tempStr = tempStr.substring(index + 1);
        }

        currentLabel = tempStr.trim();
        if (!"".equals(currentLabel)) {
            vResult.addElement(currentLabel);
        }
        String re[] = new String[vResult.size()];
        Iterator it = vResult.iterator();
        for (int index = 0; it.hasNext(); index++) {
            re[index] = (String) it.next();
        }

        return re;
    }

    public static boolean isWhiteList(String strNumber) {
        boolean ret = false;
        if (listNumber != null) {
            logger.info("listNumber.length = " + listNumber.length);
            logger.info("strNumber = " + strNumber);
            for (int i = 0; i < listNumber.length; i++) {
                logger.info("listNumber i = " + listNumber[i]);
                if (strNumber.equalsIgnoreCase(listNumber[i])) {
                    ret = true;
                    break;
                }
            }
        }
        logger.info("ret = " + ret);
        return ret;
    }
//    public static void main(String []a){
//        Constants con = new Constants();
//        con.loadProperties("ptsServices.properties");
////        for(int i = 0 ; i < listNumber.length; i++){
////            System.out.println(listNumber[i]);
////        }
//        System.out.println(isWhiteList("84916943692"));
//    }
}
