/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.db.pool;

import com.pts.ptsMain;
import java.sql.Connection;

import java.util.Properties;
import org.apache.commons.dbcp.PoolingDriver;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.commons.pool.KeyedObjectPoolFactory;
import org.apache.commons.pool.impl.GenericKeyedObjectPoolFactory;
import org.apache.log4j.Logger;

/**
 *
 * @author hungdt
 */
public class ConnectionDB {

    private static Logger logger = Logger.getLogger(ConnectionDB.class);
    Connection conContent = null;
    Connection conRemote = null;

    public Connection getConnectionContent() {
        Properties conf = ptsMain.conf;
        try {
//            conf.load(new FileInputStream("ptsServices.properties"));
            GenericObjectPool gPool = new GenericObjectPool();
            Class.forName(conf.getProperty("pts.driver.dbcontent"));

            DriverManagerConnectionFactory cf =
                    new DriverManagerConnectionFactory(
                    conf.getProperty("pts.url.dbcontent"), conf.getProperty("pts.user.dbcontent"), conf.getProperty("pts.pass.dbcontent"));
            KeyedObjectPoolFactory kopf = new GenericKeyedObjectPoolFactory(null, 5);

            PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf,
                    gPool, kopf, null, false, true);

            for (int i = 0; i < 5; i++) {
                gPool.addObject();
            }
            // PoolingDataSource pds = new PoolingDataSource(gPool);
            PoolingDriver pd = new PoolingDriver();
            pd.registerPool("content", gPool);

            for (int i = 0; i < 5; i++) {
                gPool.addObject();
            }

            conContent = java.sql.DriverManager.getConnection("jdbc:apache:commons:dbcp:content");
            logger.info("init - Connection to Content");
        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
        }
        return conContent;
    }

    public void freeConnectionContent() {
        try {
            if (conContent != null) {
                conContent.close();
                logger.info("close - Connection to Content");
            }
        } catch (Exception e) {
        }
    }

    public Connection getConnectionRemote(Properties conf) {
        try {
            GenericObjectPool gPool = new GenericObjectPool();
            Class.forName(conf.getProperty("pts.driver.dbremote"));

            DriverManagerConnectionFactory cf =
                    new DriverManagerConnectionFactory(
                    conf.getProperty("pts.url.dbremote"), conf.getProperty("pts.user.dbremote"), conf.getProperty("pts.pass.dbremote"));
            KeyedObjectPoolFactory kopf = new GenericKeyedObjectPoolFactory(null, 5);

            PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf,
                    gPool, kopf, null, false, true);

            for (int i = 0; i < 5; i++) {
                gPool.addObject();
            }
            // PoolingDataSource pds = new PoolingDataSource(gPool);
            PoolingDriver pd = new PoolingDriver();
            pd.registerPool("remote", gPool);

            for (int i = 0; i < 5; i++) {
                gPool.addObject();
            }

            conRemote = java.sql.DriverManager.getConnection("jdbc:apache:commons:dbcp:remote");
            logger.info("init - Connection to remote");
        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
        }
        return conRemote;
    }

    public void freeConnectionRemote() {
        try {
            if (conRemote != null) {
                conRemote.close();
                logger.info("close - Connection to remote");
            }
        } catch (Exception e) {
        }
    }
}
