/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts;

import com.pts.db.pool.ConnectionDB;
import com.pts.process.Constants;
import com.pts.process.ExecuteQueue;
import com.pts.process.LoadMO;
import com.pts.process.MsgQueue;
import java.io.FileInputStream;
import java.sql.Connection;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author hungdt
 */
public class ptsMain extends Thread {

    private static Logger logger = Logger.getLogger(ptsMain.class);
    final static String VERSION = "1.0";
    public static boolean getData = true;
    public static boolean processData = true;
    MsgQueue queue;
    MsgQueue queueLog;
    LoadMO loadMO[];
    ExecuteQueue executequeue[];
    private static String[] listOperator = {"GPC", "VMS", "VIETEL"};
    ConnectionDB cn;
    Connection con = null;
    public static Properties conf;
    static int TIME_DELAY_LOAD = 1000;
    static int TIME_SLEEP_CLOSE_CON = 5000;

    public ptsMain() {
        queue = new MsgQueue();
        loadMO = new LoadMO[3];
        executequeue = new ExecuteQueue[3];
        conf = new Properties();
        Constants.loadProperties("ptsServices.properties");
        logger.info("Version " + VERSION);
        if (Constants.RUNCLASS != null) {
            logger.info("RunCLASS " + VERSION);
            for (int i = 0; i < Constants.RUNCLASS.length; i++) {
                runthread(Constants.RUNCLASS[i]);
            }
        }
        try {
            conf.load(new FileInputStream("ptsServices.properties"));
//            Init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Starting ProcessServer - version " + VERSION);
        System.out.println("Copyright 2009 PTS - All Rights Reserved.");
        ptsMain smsConsole = new ptsMain();
        smsConsole.start();
    }

    private void Init()
            throws Exception {
        for (int i = 0; i < loadMO.length; i++) {
            loadMO[i] = new LoadMO(queue, loadMO.length, i);
            loadMO[i].setPriority(10);
            loadMO[i].setOperator(listOperator[i]);
            loadMO[i].setCon(con);
            loadMO[i].setConf(conf);
            loadMO[i].start();

        }
        for (int i = 0; i < executequeue.length; i++) {
            executequeue[i] = new ExecuteQueue(queue, i);
            executequeue[i].setPriority(10);
            executequeue[i].setCon(con);
            executequeue[i].setConf(conf);
            executequeue[i].start();
        }

    }

    @Override
    public void run() {
//        int count = 0;
        cn = new ConnectionDB();
        con = cn.getConnectionRemote(conf);
        try {
            Init();
//            while (true) {
//                sleep(TIME_SLEEP_CLOSE_CON);
////                count++;
////                if (count >= TIME_SLEEP_CLOSE_CON) {
//                con.close();
//                cn.freeConnectionRemote();
//                sleep(50L);
//                cn = new ConnectionDB();
//                con = cn.getConnectionRemote(conf);
        //Thread.
//                }
//            }
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    private void runthread(String classname) {
        try {
            Class delegateClass = Class.forName(classname);
            Object delegateObject = null;
            try {
                delegateObject = delegateClass.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            Thread delegate = (Thread) delegateObject;
            delegate.start();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
