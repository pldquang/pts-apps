/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.common;

import java.util.ArrayList;

/**
 *
 * @author hungdt
 */
public class CharactorProcess {

    final String[] mark = {"f", "s", "r", "x", "j", "F", "S", "R", "X", "J"};
    final String[] textarr = {"aa", "aw", "ow", "oo", "ee", "dd", "uw", "AA", "AW", "OO", "OW", "EE", "DD", "UW"};
    final String[] retextarr = {"â", "ă", "ơ", "ô", "ê", "đ", "ư", "Â", "Ă", "Ô", "Ơ", "Ê", "Đ", "Ư"};
    final String[] na_1 = {"a", "o", "e", "u", "y", "i", "A", "O", "E", "U", "Y", "I"};
    final String[] na_2 = {"â", "ă", "ơ", "ô", "ê", "ư", "Â", "Ă", "Ơ", "Ô", "Ê", "Ư"};
    final String[] remark_f_1 = {"à", "ò", "è", "ù", "ỳ", "ì", "À", "Ò", "È", "Ù", "Ỳ", "Ì"};
    final String[] remark_f_2 = {"ầ", "ằ", "ờ", "ồ", "ề", "ừ", "Ầ", "Ằ", "Ờ", "Ồ", "Ề", "Ừ"};
    final String[] remark_s_1 = {"á", "ó", "é", "ú", "ý", "í", "Á", "Ó", "É", "Ú", "Ý", "Í"};
    final String[] remark_s_2 = {"ấ", "ắ", "ớ", "ố", "ế", "ứ", "Ấ", "Ắ", "Ớ", "Ố", "Ế", "Ứ"};
    final String[] remark_r_1 = {"ả", "ỏ", "ẻ", "ủ", "ỷ", "ỉ", "Ả", "Ỏ", "Ẻ", "Ủ", "Ỷ", "Ỉ"};
    final String[] remark_r_2 = {"ẩ", "ẳ", "ở", "ổ", "ể", "ử", "Ẩ", "Ẳ", "Ở", "Ổ", "Ể", "Ử"};
    final String[] remark_x_1 = {"ã", "õ", "ẽ", "ũ", "ỹ", "ĩ", "Ã", "Õ", "Ẽ", "Ũ", "Ỹ", "Ĩ"};
    final String[] remark_x_2 = {"ẫ", "ẵ", "ỡ", "ỗ", "ễ", "ữ", "Ẫ", "Ẵ", "Ỡ", "Ỗ", "Ễ", "Ữ"};
    final String[] remark_j_1 = {"ạ", "ọ", "ẹ", "ụ", "ỵ", "ị", "Ạ", "Ọ", "Ẹ", "Ụ", "Ỵ", "Ị"};
    final String[] remark_j_2 = {"ậ", "ặ", "ợ", "ộ", "ệ", "ự", "Ậ", "Ặ", "Ợ", "Ộ", "Ệ", "Ự"};
    final String[] Unicode = {
        "À", "Á", "Â", "Ã", "È", "É", "Ê", "Ì", "Í", "Ò", "Ó", "Ô", "Õ", "Ù", "Ú", "Ý",
        "à", "á", "â", "ã", "è", "é", "ê", "ì", "í", "ò", "ó", "ô", "õ", "ù", "ú", "ý",
        "Ă", "ă", "Đ", "đ", "Ĩ", "ĩ", "Ũ", "ũ", "Ơ", "ơ", "Ư", "ư", "Ạ", "ạ", "Ả", "ả",
        "Ấ", "ấ", "Ầ", "ầ", "Ẩ", "ẩ", "Ẫ", "ẫ", "Ậ", "ậ", "Ắ", "ắ", "Ằ", "ằ", "Ẳ", "ẳ",
        "Ẵ", "ẵ", "Ặ", "ặ", "Ẹ", "ẹ", "Ẻ", "ẻ", "Ẽ", "ẽ", "Ế", "ế", "Ề", "ề", "Ể", "ể",
        "Ễ", "ễ", "Ệ", "ệ", "Ỉ", "ỉ", "Ị", "ị", "Ọ", "ọ", "Ỏ", "ỏ", "Ố", "ố", "Ồ", "ồ",
        "Ổ", "ổ", "Ỗ", "ỗ", "Ộ", "ộ", "Ớ", "ớ", "Ờ", "ờ", "Ở", "ở", "Ỡ", "ỡ", "Ợ", "ợ",
        "Ụ", "ụ", "Ủ", "ủ", "Ứ", "ứ", "Ừ", "ừ", "Ử", "ử", "Ữ", "ữ", "Ự", "ự", "Ỳ", "ỳ",
        "Ỵ", "ỵ", "Ỷ", "ỷ", "Ỹ", "ỹ"
    };
    final String[] VNI = {
        "AØ", "AÙ", "AÂ", "AÕ", "EØ", "EÙ", "EÂ", "Ì", "Í", "OØ", "OÙ", "OÂ", "OÕ",
        "UØ", "UÙ", "YÙ", "aø", "aù", "aâ", "aõ", "eø", "eù", "eâ", "ì", "í", "oø",
        "où", "oâ", "oõ", "uø", "uù", "yù", "AÊ", "aê", "Ñ", "ñ", "Ó", "ó", "UÕ", "uõ",
        "Ô", "ô", "Ö", "ö", "AÏ", "aï", "AÛ", "aû", "AÁ", "aá", "AÀ", "aà", "AÅ", "aå",
        "AÃ", "aã", "AÄ", "aä", "AÉ", "aé", "AÈ", "aè", "AÚ", "aú", "AÜ", "aü", "AË",
        "aë", "EÏ", "eï", "EÛ", "eû", "EÕ", "eõ", "EÁ", "eá", "EÀ", "eà", "EÅ", "eå",
        "EÃ", "eã", "EÄ", "eä", "Æ", "æ", "Ò", "ò", "OÏ", "oï", "OÛ", "oû", "OÁ", "oá",
        "OÀ", "oà", "OÅ", "oå", "OÃ", "oã", "OÄ", "oä", "ÔÙ", "ôù", "ÔØ", "ôø", "ÔÛ",
        "ôû", "ÔÕ", "ôõ", "ÔÏ", "ôï", "UÏ", "uï", "UÛ", "uû", "ÖÙ", "öù", "ÖØ", "öø",
        "ÖÛ", "öû", "ÖÕ", "öõ", "ÖÏ", "öï", "YØ", "yø", "Î", "î", "YÛ", "yû", "YÕ", "yõ"
    };

    public String ConvertUnicode2VNI(String str) {
        String ret = "";
        int index = -1;
        for (int i = 0; i < str.length(); i++) {
            index = getIndex(String.valueOf(str.charAt(i)));
            if (index > -1) {
                ret += VNI[index];
            } else {
                ret += String.valueOf(str.charAt(i));
            }
        }
        return ret;
    }

    public int getIndex(String str) {
        int ret = -1;
        for (int i = 0; i < Unicode.length; i++) {
            if (str.equals(Unicode[i])) {
                return i;
            }
        }
        return ret;
    }

    public String convertString(String str) {
        String t1 = "", t2 = "";
        String s = "";
        String out = "";
        ArrayList arr = toArray(str);
        String st = "";
        String word = "";
        boolean addsign = false, isUp = false;
        for (int i = 0; i < arr.size(); i++) {
            st = String.valueOf(arr.get(i));
            if (st.length() > 1) {
                s = getMark(st);
                if (!s.equals("")) {
                    st = st.substring(0, st.length() - 1);
                    addsign = true;
                }
                int j = 0;
                while (j < st.length()) {
                    t1 = st.substring(j, j + 1);
                    if (isTextarr(t1)) {
                        if ((j + 2) <= st.length()) {
                            t2 = st.substring(j + 1, j + 2);
                            if (t1.equalsIgnoreCase(t2)) {
                                t1 += t1;
                            } else {
                                t1 = st.substring(j, j + 2);
                                int code = (int) t1.charAt(0);
//                                if (code <= 97) {
//                                    t1 = t1.toUpperCase();
//                                }
                            }
                            j += 2;
                            for (int k = 0; k < textarr.length; k++) {
                                if (t1.equals(textarr[k])) {
                                    t1 = retextarr[k];
                                    if (addsign && (!t1.equals("đ") || t1.equals("Đ"))) {
                                        for (int l = 0; l < na_2.length; l++) {
                                            if (t1.equals(na_2[l])) {
                                                if (s.equalsIgnoreCase("f")) {
                                                    t1 = remark_f_2[l];
                                                } else if (s.equalsIgnoreCase("s")) {
                                                    t1 = remark_s_2[l];
                                                } else if (s.equalsIgnoreCase("r")) {
                                                    t1 = remark_r_2[l];
                                                } else if (s.equalsIgnoreCase("x")) {
                                                    t1 = remark_x_2[l];
                                                } else if (s.equalsIgnoreCase("j")) {
                                                    t1 = remark_j_2[l];
                                                }
                                                addsign = false;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            j++;
                        }
                    } else {
                        j++;
                    }
                    word += t1;
                }
                if (addsign) {
                    word = addSign(word, s);
                }
                if (word.indexOf("ơ") > 0) {
                    word = revert(word);
                }
                out += " " + word;
                word = "";
            } else {
                out += " " + st;
            }
        }
        out = out.trim();
        return out;
    }

    public boolean isTextarr(String t) {
        boolean ret = false;
        for (int i = 0; i < textarr.length; i++) {
            if (t.equals(textarr[i].substring(0, 1))) {
                ret = true;
                break;
            }
        }
        return ret;
    }

//    public String addSign(String str, String s) {
//        String ret = "";
//        String t = "", r = "";
//        boolean b = false;
//        for (int i = str.length(); i > 0; i--) {
//            t = str.substring(i - 1, i);
//            for (int j = 0; j < na_1.length; j++) {
//                if (t.equals(na_1[j])) {
//                    if (i <= str.length()) {
//                        if (s.equalsIgnoreCase("f")) {
//                            r = remark_f_1[j];
//                        } else if (s.equalsIgnoreCase("s")) {
//                            r = remark_s_1[j];
////                        System.out.print("r:" + r);
//                        } else if (s.equalsIgnoreCase("r")) {
//                            r = remark_r_1[j];
//                        } else if (s.equalsIgnoreCase("x")) {
//                            r = remark_x_1[j];
//                        } else if (s.equalsIgnoreCase("j")) {
//                            r = remark_j_1[j];
//                        }
//                        b = true;
//                        break;
//                    }
//                }
//
//            }
//            if (b) {
//                break;
//            }
//        }
//        ret = str.replaceAll(t, r);
////        System.out.println("in " + str + " out: " + ret);
//        return ret;
//    }

    public boolean inNA1(String str) {
        boolean ret = false;
        for (int i = 0; i < na_1.length; i++) {
            if (str.equals(na_1[i])) {
                ret = true;
                break;
            }
        }
        return ret;
    }

    public String getMark(String str) {
        String ret = "";
        for (int i = 0; i < mark.length; i++) {
            if (str.substring(str.length() - 1, str.length()).equalsIgnoreCase(mark[i])) {
                ret = str.substring(str.length() - 1, str.length());
            }
        }
        return ret;
    }

    public ArrayList toArray(String str) {
        String tmp = "";
        ArrayList ret = new ArrayList();
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == ' ') {
                ret.add(tmp);
                tmp = "";
            } else {
                tmp += str.charAt(i);
            }
            if (i == str.length() - 1) {
                ret.add(tmp);
            }
        }

        return ret;
    }

    public String revert(String str) {
//        if (str.indexOf("ơ") > 0) {
        if (str.indexOf("ừ") > 0) {
            str = str.replaceAll("ừơ", "ườ");
        } else if (str.indexOf("ứ") > 0) {
            str = str.replaceAll("ứơ", "ướ");
        } else if (str.indexOf("ử") > 0) {
            str = str.replaceAll("ửơ", "ưở");
        } else if (str.indexOf("ữ") > 0) {
            str = str.replaceAll("ữơ", "ưỡ");
        } else if (str.indexOf("ự") > 0) {
            str = str.replaceAll("ựơ", "ượ");
        }
//        }
        return str;
    }

    public String addSign(String str, String s) {
        String ret = "";
        String t = "", r = "", t1 = "";
        System.out.println("str=" + str + "@" + s);
        boolean b = false;
//        for (int i = str.length(); i > 0; i--) {
        for (int i = 0; i < str.length(); i++) {
//            t = str.substring(i - 1, i);
            t = str.substring(i, i + 1);
            if (i < str.length() - 1) {
                t1 = str.substring(i + 1, i + 2);
            }
            for (int j = 0; j < na_1.length; j++) {
                if (t.equals(na_1[j])) {
                    if (t1.equalsIgnoreCase("a") && !t.equalsIgnoreCase(t1)) {
                        break;
                    }
                    if (i <= str.length()) {
                        if (s.equalsIgnoreCase("f")) {
                            r = remark_f_1[j];
                        } else if (s.equalsIgnoreCase("s")) {
                            r = remark_s_1[j];
                        } else if (s.equalsIgnoreCase("r")) {
                            r = remark_r_1[j];
                        } else if (s.equalsIgnoreCase("x")) {
                            r = remark_x_1[j];
                        } else if (s.equalsIgnoreCase("j")) {
                            r = remark_j_1[j];
                        }
                        b = true;
                        break;
                    }
                }
            }
            if (b) {
                break;
            }
        }
        ret = str.replaceAll(t, r);

//        System.out.println("in " + str + " out: " + ret);
        return ret;
    }

//    public static void main(String[] args) {
//        // TODO code application logic here
//        CharactorProcess cvc = new CharactorProcess();
//        String str = "giang";
//        String out = cvc.convertString(str);
////        String out2 = cvc.revert(str);
//        System.out.println("out = " + out);
////        ImageProcess img = new ImageProcess();
////        img.CreateImg(out, "Times New Roman", "C:/Temp/vuong.bmp");
//    }
}
