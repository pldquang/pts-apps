/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.common;

import java.util.Vector;

/**
 *
 * @author hungdt
 */
public class Queue {

    public Queue() {
        queue = new Vector();
    }

    public Object dequeue() {
        int i = 0;
        Vector vector = queue;
        Object item;
        while (queue.isEmpty()) {
            try {
                queue.wait(1000L);
                i++;
                System.out.println("wait until other thread call notify(): " + i);
            } catch (InterruptedException interruptedexception) {
            }
        }
        item = queue.firstElement();
        queue.removeElement(item);
        return item;
    }

    public void enqueue(Object item) {
        synchronized (queue) {
            queue.addElement(item);
            queue.notifyAll();
        }
    }

    public int size() {
        return queue.size();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }
    protected Vector queue;
}
