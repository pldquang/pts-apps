/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.common;

/**
 *
 * @author hungdt
 */
public class Validator {

    public static String OPERATOR = "So thue bao gui tang khong dung";

    public static boolean validPhoneNumber(String phoneNumber) {
        boolean ret = false;
        String strPhoneNumber = phoneNumber;
        //841266144921	84987654321
        if (phoneNumber.startsWith("0")) {//neu so thue bao co dang 0xxxxxxxxx
            //thay so "0" bang 84
            strPhoneNumber = "84" + strPhoneNumber.substring(1);
        }
        if (strPhoneNumber.length() != 11 && strPhoneNumber.length() != 12) {
            //do dai so thue bao khong dung
            return false;
        }
        //kiem tra mang di dong cua thue bao
        String strOperator = getOperator(strPhoneNumber);
        if (strOperator != null && strOperator != "") {
            ret = true;
            OPERATOR = strOperator;
        }
        return ret;
    }
    //Check

    public static String getOperator(String phoneNumber) {
        if (phoneNumber == null || phoneNumber.length() < 10) {
            return "";
        }
        String[] strPrefixShort1 = {
            "8491", "8494", /*"84123",*/ "8490", "8493", /*"84122",*/ "8497", "8498",
            /*"84168",*/ /*"84169",*/ "8496", "8495", "8492"
        };
        String[] strPrefixLong1 = {
            /*"8491",*/ /*"8494",*/"84123", /*"8490",*/ /*"8493",*/ "84122", /*"8497",*/ /*"8498",*/
            "84168", "84169", "84167", /*"8496",*/ /*"8495",*/ /*"8492",*/ "84166", "84126", "84121", "84165", "84128", "84125"
        };
        String[] strPrefixShort2 = {
            "+8491", "+8494", /*"+84123",*/ "+8490", "+8493", /*"+84122",*/ "+8497", "+8498",
            /*"+84168",*/ /*"+84169",*/ "+8496", "+8495", "+8492"
        };
        String[] strPrefixLong2 = {
            /*"+8491",*/ /*"+8494",*/"+84123", /*"+8490",*/ /*"+8493",*/ "+84122", /*"+8497",*/ /*"+8498",*/
            "+84168", "+84169", "+84167", /*"+8496",*/ /*"+8495",*/ /*"+8492",*/ "+84166", "+84126", "+84121", "+84165", "+84128", "+84125"
        };
        String[] strOperatorShort = {
            "GPC", "GPC", /*"GPC",*/ "VMS", "VMS", /*"VMS",*/ "VIETEL", "VIETEL",
            /*"VIETEL",*/ /*"VIETEL",*/ "EVN", "SFONE", "HTC"
        };
        String[] strOperatorLong = {
            /*"GPC",*/ /*"GPC",*/"GPC", /*"VMS",*/ /*"VMS",*/ "VMS", /*"VIETEL",*/ /*"VIETEL",*/
            "VIETEL", "VIETEL", "VIETEL", /*"EVN",*/ /*"SFONE",*/ /*"HTC",*/ "VIETEL", "VMS", "VMS", "VIETEL", "VMS", "GPC"
        };
        int i = 0;
        //kiem tra thue bao dang 849xxxxxxxx
        for (i = 0; i < strPrefixShort1.length; i++) {
            //thue bao nay chi co do dai = 11
            if (phoneNumber.substring(0, strPrefixShort1[i].length()).equalsIgnoreCase(strPrefixShort1[i]) && phoneNumber.length() == 11) {
                return strOperatorShort[i];
            }
        }
        //kiem tra thue bao dang +849xxxxxxxx
        for (i = 0; i < strPrefixShort2.length; i++) {
            //thue bao nay co do dai = 12 vi co them dau +
            if (phoneNumber.substring(0, strPrefixShort2[i].length()).equalsIgnoreCase(strPrefixShort2[i]) && phoneNumber.length() == 12) {
                return strOperatorShort[i];
            }
        }
        //kiem tra thue bao dang 8412xxxxxxxx
        for (i = 0; i < strPrefixLong1.length; i++) {
            //thue bao nay chi co do dai = 12
            if (phoneNumber.substring(0, strPrefixLong1[i].length()).equalsIgnoreCase(strPrefixLong1[i]) && phoneNumber.length() == 12) {
                return strOperatorLong[i];
            }
        }
        //kiem tra thue bao dang +8412xxxxxxxx
        for (i = 0; i < strPrefixLong2.length; i++) {
            //thue bao nay chi co do dai = 13 vi co them dau +
            if (phoneNumber.substring(0, strPrefixLong2[i].length()).equalsIgnoreCase(strPrefixLong2[i]) && phoneNumber.length() == 13) {
                return strOperatorLong[i];
            }
        }
        return "";
    }
}
