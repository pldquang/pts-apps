/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.common;

import java.io.FileOutputStream;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

/**
 *
 * @author hungdt
 */
public class ImageProcess {

    public void CreateImg(String strdata, String font, String output) {
        // Create image
        int width = 72, height = 14;
        try {
            BufferedImage image = new BufferedImage(width,
                    height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            g.setColor(Color.BLACK);
            g.setBackground(Color.WHITE);
            g.clearRect(0, 0, width, height);
            if (font.equals("VNI-Nhatban")) {
                g.setFont(new Font(font, Font.PLAIN, height - 6));
            } else {
                g.setFont(new Font(font, Font.PLAIN, height - 4));
            }
            if (font.equals("VNI-Nhatban") || font.equals("VNI-Slogan") || font.equals("VNI-Yahoo")) {
                if (strdata.length() < 4) {
                    g.drawString(strdata, 10, height * 3 / 4);
                } else {
                    g.drawString(strdata, 1, height * 3 / 4);
                }
            } else {
                if (isUnder(strdata)) {
                    g.drawString(strdata, (int) Math.floor(((12 - strdata.length())) / 2) * 6 + 2, height * 3 / 4);
                } else {
                    g.drawString(strdata, (int) Math.floor(((12 - strdata.length())) / 2) * 6 + 2, height * 0.85f);
                }
            }
            g.dispose();

            // write to file
            Iterator writers = ImageIO.getImageWritersByFormatName("png");
            ImageWriter writer = (ImageWriter) writers.next();
            if (writer == null) {
                throw new RuntimeException("png not supported?!");
            }
            ImageOutputStream out = ImageIO.createImageOutputStream(
                    new File(output));
            writer.setOutput(out);
            writer.write(image);
            out.close(); // close flushes buffer

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void ConvertImage2Otb(String fileint, String fileout) {
        String f = fileint;
        String fout = fileout;
        BufferedImage bimage;
        try {
            bimage = ImageIO.read(new File(f));
            boolean[][] otb = new boolean[bimage.getWidth()][bimage.getHeight()];
            for (int y = 0; y < bimage.getHeight(); y++) {
                for (int x = 0; x < bimage.getWidth(); x++) {
                    if (bimage.getRGB(x, y) == Color.BLACK.getRGB()) {
                        otb[x][y] = false; // 0

                    } else {
                        otb[x][y] = true; // 1

                    }
                }
            }
            FileOutputStream fs = new FileOutputStream(fout);
            fs.write(0);
            fs.write(bimage.getWidth());
            fs.write(bimage.getHeight());
            fs.write(1);
            byte b;
            for (int y = 0; y < bimage.getHeight(); y++) {
                for (int c = 0; c < 9; c++) {
                    b = 0;
                    for (int x = 0; x < 8; x++) {
                        if (otb[c * 8 + x][y] == false) {
                            b += (byte) Math.pow(2, 7 - x);
                        }
                    }
                    fs.write(b);
                }
            }
            fs.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    final char[] fixUnder = {'q', 'p', 'j', 'g'};

    public boolean isUnder(String str) {
        boolean ret = false;
        for (int i = 0; i < str.length(); i++) {
            for (int j = 0; j < fixUnder.length; j++) {
                if (str.charAt(i) == fixUnder[j]) {
                    ret = true;
                    break;
                }
            }
            if (ret) {
                break;
            }
        }
        return ret;
    }


  /*  public static void main(String a[]) {
        String out = "C:/Temp/hungdt_i.jpg";
        String w = " Thuys";
        String bg = "C:/Temp/317972.jpg";
        ImageProcess img = new ImageProcess();
        CharactorProcess chr = new CharactorProcess();
        img.CreateImg(chr.ConvertUnicode2VNI(chr.convertString(w)), "VNI-HLThuphap", out);
//        img.CreateImgWithBg(bg, chr.convertString(w), "Times New Roman", out);
    }*/
}
