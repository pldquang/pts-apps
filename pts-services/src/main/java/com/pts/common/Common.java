/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pts.common;

import java.util.Calendar;
import java.util.Random;

/**
 *
 * @author hungdt
 */
public class Common {

    public static int parseInt(String value, int defaultValue) {
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static Integer parseInteger(String value, Integer defaultValue) {
        try {
            return Integer.valueOf(value);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static java.sql.Date getSqlDate(java.util.Date date) {
        if (date == null) {
            return null;
        } else {
//System.out.println("-util.date=" + date.toString());
//System.out.println("-util.date.getTime(0)=" + date.getTime());
            java.sql.Date sqlDate = new java.sql.Date(date.getTime());
//System.out.println("-sql.date=" + (sqlDate).toString());
            return new java.sql.Date(date.getTime());
        }
    }

    public static java.sql.Timestamp getSqlTimestamp(java.util.Date date) {
        if (date == null) {
            return null;
        } else {
            return new java.sql.Timestamp(date.getTime());
        }
    }

    public static java.util.Date addDaysToDate(java.util.Date date, int daysToAdd) throws Exception {

//        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:MM:ss.S");

        //java.util.Date parsedDate = sdf.parse(date);

        Calendar now = Calendar.getInstance();

        now.setTime(date);

        now.add(Calendar.DAY_OF_MONTH, daysToAdd);

        //System.out.println(sdf.format(now.getTime()));
//        return sdf.parse(sdf.format(now.getTime()));
        return now.getTime();
    }

    public static int getRandomInteger(int aStart, int aEnd) {
        if (aStart > aEnd) {
            throw new IllegalArgumentException("Start cannot exceed End.");
        }
        //get the range, casting to long to avoid overflow problems
        long range = (long) aEnd - (long) aStart + 1;
        Random aRandom = new Random();
        // compute a fraction of the range, 0 <= frac < range
        long fraction = (long) (range * aRandom.nextDouble());
        int randomNumber = (int) (fraction + aStart);
        return randomNumber;
    }

}
