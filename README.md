pts-apps
========

PTS 8x84 Applications

- Author: quangpld <mailto:pldquang@gmail.com>
- Modules:
   + PTS 8x84 Lottery Service.
   + PTS 8x84 Media Service.
   + PTS 8x84 Services.